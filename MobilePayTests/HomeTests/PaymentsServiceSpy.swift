//
//  PaymentsServiceSpy.swift
//  MobilePayTests
//
//  Created by Marius Kurgonas on 25/02/2021.
//  Copyright © 2021 JadePayments. All rights reserved.
//

import Foundation
@testable import MobilePay

class PaymentsServiceSpy: PaymentsWebServiceContract {

    var invokedResponseParserSetter = false
    var invokedResponseParserSetterCount = 0
    var invokedResponseParser: PaymentsResponseParsingContract?
    var invokedResponseParserList = [PaymentsResponseParsingContract?]()
    var invokedResponseParserGetter = false
    var invokedResponseParserGetterCount = 0
    var stubbedResponseParser: PaymentsResponseParsingContract!

    var responseParser: PaymentsResponseParsingContract? {
        set {
            invokedResponseParserSetter = true
            invokedResponseParserSetterCount += 1
            invokedResponseParser = newValue
            invokedResponseParserList.append(newValue)
        }
        get {
            invokedResponseParserGetter = true
            invokedResponseParserGetterCount += 1
            return stubbedResponseParser
        }
    }

    var invokedAddPaymentMethod = false
    var invokedAddPaymentMethodCount = 0
    var invokedAddPaymentMethodParameters: (data: PaymentMethodRequest, completion: AddPaymentMethodResponse)?
    var invokedAddPaymentMethodParametersList = [(data: PaymentMethodRequest, completion: AddPaymentMethodResponse)]()

    func addPaymentMethod(data: PaymentMethodRequest,
        completion: @escaping AddPaymentMethodResponse) {
        invokedAddPaymentMethod = true
        invokedAddPaymentMethodCount += 1
        invokedAddPaymentMethodParameters = (data, completion)
        invokedAddPaymentMethodParametersList.append((data, completion))
    }

    var invokedRequestAllPaymentMethods = false
    var invokedRequestAllPaymentMethodsCount = 0
    var invokedRequestAllPaymentMethodsParameters: (completion: PaymentMethodsResponse, Void)?
    var invokedRequestAllPaymentMethodsParametersList = [(completion: PaymentMethodsResponse, Void)]()

    func requestAllPaymentMethods(completion: @escaping PaymentMethodsResponse) {
        invokedRequestAllPaymentMethods = true
        invokedRequestAllPaymentMethodsCount += 1
        invokedRequestAllPaymentMethodsParameters = (completion, ())
        invokedRequestAllPaymentMethodsParametersList.append((completion, ()))
    }

    var invokedDeletePaymentMethod = false
    var invokedDeletePaymentMethodCount = 0
    var invokedDeletePaymentMethodParameters: (paymentProfileSettings: [PaymentProfileSettings], completion: GenericResponse)?
    var invokedDeletePaymentMethodParametersList = [(paymentProfileSettings: [PaymentProfileSettings], completion: GenericResponse)]()

    func deletePaymentMethod(paymentProfileSettings: [PaymentProfileSettings],
        completion: @escaping GenericResponse) {
        invokedDeletePaymentMethod = true
        invokedDeletePaymentMethodCount += 1
        invokedDeletePaymentMethodParameters = (paymentProfileSettings, completion)
        invokedDeletePaymentMethodParametersList.append((paymentProfileSettings, completion))
    }

    var invokedCheckTransactionStatus = false
    var invokedCheckTransactionStatusCount = 0
    var invokedCheckTransactionStatusParameters: (qr: String, completion: CheckTransactionStatusResponse)?
    var invokedCheckTransactionStatusParametersList = [(qr: String, completion: CheckTransactionStatusResponse)]()

    func checkTransactionStatus(qr: String,
        completion: @escaping CheckTransactionStatusResponse) {
        invokedCheckTransactionStatus = true
        invokedCheckTransactionStatusCount += 1
        invokedCheckTransactionStatusParameters = (qr, completion)
        invokedCheckTransactionStatusParametersList.append((qr, completion))
    }

    var invokedMakeDefaultPaymentMethod = false
    var invokedMakeDefaultPaymentMethodCount = 0
    var invokedMakeDefaultPaymentMethodParameters: (settings: [PaymentProfileSettings], completion: DefaultPaymentMethodResponse)?
    var invokedMakeDefaultPaymentMethodParametersList = [(settings: [PaymentProfileSettings], completion: DefaultPaymentMethodResponse)]()

    func makeDefaultPaymentMethod(settings: [PaymentProfileSettings],
        completion: @escaping DefaultPaymentMethodResponse) {
        invokedMakeDefaultPaymentMethod = true
        invokedMakeDefaultPaymentMethodCount += 1
        invokedMakeDefaultPaymentMethodParameters = (settings, completion)
        invokedMakeDefaultPaymentMethodParametersList.append((settings, completion))
    }

    var invokedCheckIfAllPaymentProvidersWereConfigured = false
    var invokedCheckIfAllPaymentProvidersWereConfiguredCount = 0
    var invokedCheckIfAllPaymentProvidersWereConfiguredParameters: (completion: PaymentProvidersConfigResponse, Void)?
    var invokedCheckIfAllPaymentProvidersWereConfiguredParametersList = [(completion: PaymentProvidersConfigResponse, Void)]()

    func checkIfAllPaymentProvidersWereConfigured(completion: @escaping PaymentProvidersConfigResponse) {
        invokedCheckIfAllPaymentProvidersWereConfigured = true
        invokedCheckIfAllPaymentProvidersWereConfiguredCount += 1
        invokedCheckIfAllPaymentProvidersWereConfiguredParameters = (completion, ())
        invokedCheckIfAllPaymentProvidersWereConfiguredParametersList.append((completion, ()))
    }

    var invokedRequestClientConfig = false
    var invokedRequestClientConfigCount = 0
    var invokedRequestClientConfigParameters: (integrationKey: String, completion: ClientConfigResult)?
    var invokedRequestClientConfigParametersList = [(integrationKey: String, completion: ClientConfigResult)]()

    func requestClientConfig(integrationKey: String, completion: @escaping ClientConfigResult) {
        invokedRequestClientConfig = true
        invokedRequestClientConfigCount += 1
        invokedRequestClientConfigParameters = (integrationKey, completion)
        invokedRequestClientConfigParametersList.append((integrationKey, completion))
    }

    var invokedGetUserStats = false
    var invokedGetUserStatsCount = 0
    var invokedGetUserStatsParameters: (fanId: String, completion: UserStatsResult)?
    var invokedGetUserStatsParametersList = [(fanId: String, completion: UserStatsResult)]()

    func getUserStats(fanId: String, completion: @escaping UserStatsResult) {
        invokedGetUserStats = true
        invokedGetUserStatsCount += 1
        invokedGetUserStatsParameters = (fanId, completion)
        invokedGetUserStatsParametersList.append((fanId, completion))
    }

    var invokedCancel = false
    var invokedCancelCount = 0

    func cancel() {
        invokedCancel = true
        invokedCancelCount += 1
    }
}
