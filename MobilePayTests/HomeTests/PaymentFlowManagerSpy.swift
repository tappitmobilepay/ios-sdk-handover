//
//  PaymentFlowManagerSpy.swift
//  MobilePayTests
//
//  Created by Marius Kurgonas on 23/08/2021.
//  Copyright © 2021 JadePayments. All rights reserved.
//

import Foundation
@testable import MobilePay

class PaymentFlowManagerSpy: PaymentFlowManagementContract {

    var invokedCurrentStateSetter = false
    var invokedCurrentStateSetterCount = 0
    var invokedCurrentState: PaymentState?
    var invokedCurrentStateList = [PaymentState]()
    var invokedCurrentStateGetter = false
    var invokedCurrentStateGetterCount = 0
    var stubbedCurrentState: PaymentState!

    var currentState: PaymentState {
        set {
            invokedCurrentStateSetter = true
            invokedCurrentStateSetterCount += 1
            invokedCurrentState = newValue
            invokedCurrentStateList.append(newValue)
        }
        get {
            invokedCurrentStateGetter = true
            invokedCurrentStateGetterCount += 1
            return stubbedCurrentState
        }
    }

    var invokedMoveToNextState = false
    var invokedMoveToNextStateCount = 0
    var invokedMoveToNextStateParameters: (paymentData: PaymentData, Void)?
    var invokedMoveToNextStateParametersList = [(paymentData: PaymentData, Void)]()
    var stubbedMoveToNextStateError: Error?

    func moveToNextState(paymentData: PaymentData) throws {
        invokedMoveToNextState = true
        invokedMoveToNextStateCount += 1
        invokedMoveToNextStateParameters = (paymentData, ())
        invokedMoveToNextStateParametersList.append((paymentData, ()))
        if let error = stubbedMoveToNextStateError {
            throw error
        }
    }
}
