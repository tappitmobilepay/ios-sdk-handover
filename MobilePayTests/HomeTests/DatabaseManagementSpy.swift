//
//  DatabaseManagementSpy.swift
//  KansasTests
//
//  Created by Marius Kurgonas on 24/02/2021.
//  Copyright © 2021 admin. All rights reserved.
//

import Foundation
@testable import MobilePay

class DatabaseManagementSpy: DatabaseManagementContract {

    var invokedDeleteAllData = false
    var invokedDeleteAllDataCount = 0

    func deleteAllData() {
        invokedDeleteAllData = true
        invokedDeleteAllDataCount += 1
    }

    var invokedMakeCardDefault = false
    var invokedMakeCardDefaultCount = 0
    var invokedMakeCardDefaultParameters: (id: String, Void)?
    var invokedMakeCardDefaultParametersList = [(id: String, Void)]()

    func makeCardDefault(id: String) {
        invokedMakeCardDefault = true
        invokedMakeCardDefaultCount += 1
        invokedMakeCardDefaultParameters = (id, ())
        invokedMakeCardDefaultParametersList.append((id, ()))
    }

    var invokedDeleteCard = false
    var invokedDeleteCardCount = 0
    var invokedDeleteCardParameters: (id: String, Void)?
    var invokedDeleteCardParametersList = [(id: String, Void)]()

    func deleteCard(id: String) {
        invokedDeleteCard = true
        invokedDeleteCardCount += 1
        invokedDeleteCardParameters = (id, ())
        invokedDeleteCardParametersList.append((id, ()))
    }

    var invokedDeleteAllCards = false
    var invokedDeleteAllCardsCount = 0

    func deleteAllCards() {
        invokedDeleteAllCards = true
        invokedDeleteAllCardsCount += 1
    }

    var invokedSaveBalance = false
    var invokedSaveBalanceCount = 0
    var invokedSaveBalanceParameters: (balance: Balance, Void)?
    var invokedSaveBalanceParametersList = [(balance: Balance, Void)]()

    func save(balance: Balance) {
        invokedSaveBalance = true
        invokedSaveBalanceCount += 1
        invokedSaveBalanceParameters = (balance, ())
        invokedSaveBalanceParametersList.append((balance, ()))
    }

    var invokedSaveGiveawayTokens = false
    var invokedSaveGiveawayTokensCount = 0
    var invokedSaveGiveawayTokensParameters: (giveawayTokens: [GiveawayToken], Void)?
    var invokedSaveGiveawayTokensParametersList = [(giveawayTokens: [GiveawayToken], Void)]()

    func save(giveawayTokens: [GiveawayToken]) {
        invokedSaveGiveawayTokens = true
        invokedSaveGiveawayTokensCount += 1
        invokedSaveGiveawayTokensParameters = (giveawayTokens, ())
        invokedSaveGiveawayTokensParametersList.append((giveawayTokens, ()))
    }

    var invokedSaveCampaigns = false
    var invokedSaveCampaignsCount = 0
    var invokedSaveCampaignsParameters: (campaigns: [Campaign], Void)?
    var invokedSaveCampaignsParametersList = [(campaigns: [Campaign], Void)]()

    func save(campaigns: [Campaign]) {
        invokedSaveCampaigns = true
        invokedSaveCampaignsCount += 1
        invokedSaveCampaignsParameters = (campaigns, ())
        invokedSaveCampaignsParametersList.append((campaigns, ()))
    }

    var invokedSaveComplimentaryBalances = false
    var invokedSaveComplimentaryBalancesCount = 0
    var invokedSaveComplimentaryBalancesParameters: (complimentaryBalances: [ComplimentaryBalance], Void)?
    var invokedSaveComplimentaryBalancesParametersList = [(complimentaryBalances: [ComplimentaryBalance], Void)]()

    func save(complimentaryBalances: [ComplimentaryBalance]) {
        invokedSaveComplimentaryBalances = true
        invokedSaveComplimentaryBalancesCount += 1
        invokedSaveComplimentaryBalancesParameters = (complimentaryBalances, ())
        invokedSaveComplimentaryBalancesParametersList.append((complimentaryBalances, ()))
    }

    var invokedSaveCards = false
    var invokedSaveCardsCount = 0
    var invokedSaveCardsParameters: (cards: [Card], Void)?
    var invokedSaveCardsParametersList = [(cards: [Card], Void)]()

    func saveCards(cards: [Card]) {
        invokedSaveCards = true
        invokedSaveCardsCount += 1
        invokedSaveCardsParameters = (cards, ())
        invokedSaveCardsParametersList.append((cards, ()))
    }

    var invokedGiveawayTokens = false
    var invokedGiveawayTokensCount = 0
    var stubbedGiveawayTokensResult: [GiveawayToken]! = []

    func giveawayTokens() -> [GiveawayToken] {
        invokedGiveawayTokens = true
        invokedGiveawayTokensCount += 1
        return stubbedGiveawayTokensResult
    }

    var invokedCampaigns = false
    var invokedCampaignsCount = 0
    var invokedCampaignsParameters: (type: CampaignsTypeFetch, Void)?
    var invokedCampaignsParametersList = [(type: CampaignsTypeFetch, Void)]()
    var stubbedCampaignsResult: [Campaign]! = []

    func campaigns(type: CampaignsTypeFetch) -> [Campaign] {
        invokedCampaigns = true
        invokedCampaignsCount += 1
        invokedCampaignsParameters = (type, ())
        invokedCampaignsParametersList.append((type, ()))
        return stubbedCampaignsResult
    }

    var invokedGiftcards = false
    var invokedGiftcardsCount = 0
    var stubbedGiftcardsResult: [ComplimentaryBalance]! = []

    func giftcards() -> [ComplimentaryBalance] {
        invokedGiftcards = true
        invokedGiftcardsCount += 1
        return stubbedGiftcardsResult
    }

    var invokedComplimentaryBalances = false
    var invokedComplimentaryBalancesCount = 0
    var stubbedComplimentaryBalancesResult: [ComplimentaryBalance]! = []

    func complimentaryBalances() -> [ComplimentaryBalance] {
        invokedComplimentaryBalances = true
        invokedComplimentaryBalancesCount += 1
        return stubbedComplimentaryBalancesResult
    }

    var invokedHasCreditCards = false
    var invokedHasCreditCardsCount = 0
    var stubbedHasCreditCardsResult: Bool! = false

    func hasCreditCards() -> Bool {
        invokedHasCreditCards = true
        invokedHasCreditCardsCount += 1
        return stubbedHasCreditCardsResult
    }

    var invokedAllCreditCards = false
    var invokedAllCreditCardsCount = 0
    var invokedAllCreditCardsParameters: (onlyDefault: Bool, Void)?
    var invokedAllCreditCardsParametersList = [(onlyDefault: Bool, Void)]()
    var stubbedAllCreditCardsResult: [Card]! = []

    func allCreditCards(onlyDefault: Bool) -> [Card] {
        invokedAllCreditCards = true
        invokedAllCreditCardsCount += 1
        invokedAllCreditCardsParameters = (onlyDefault, ())
        invokedAllCreditCardsParametersList.append((onlyDefault, ()))
        return stubbedAllCreditCardsResult
    }

    var invokedAdd = false
    var invokedAddCount = 0
    var invokedAddParameters: (card: Card, Void)?
    var invokedAddParametersList = [(card: Card, Void)]()

    func add(card: Card) {
        invokedAdd = true
        invokedAddCount += 1
        invokedAddParameters = (card, ())
        invokedAddParametersList.append((card, ()))
    }

    var invokedAllTransactions = false
    var invokedAllTransactionsCount = 0
    var stubbedAllTransactionsResult: [Transaction]! = []

    func allTransactions() -> [Transaction] {
        invokedAllTransactions = true
        invokedAllTransactionsCount += 1
        return stubbedAllTransactionsResult
    }

    var invokedSaveTransactions = false
    var invokedSaveTransactionsCount = 0
    var invokedSaveTransactionsParameters: (transactions: [Transaction], Void)?
    var invokedSaveTransactionsParametersList = [(transactions: [Transaction], Void)]()

    func saveTransactions(transactions: [Transaction]) {
        invokedSaveTransactions = true
        invokedSaveTransactionsCount += 1
        invokedSaveTransactionsParameters = (transactions, ())
        invokedSaveTransactionsParametersList.append((transactions, ()))
    }

    var invokedClientConfig = false
    var invokedClientConfigCount = 0
    var stubbedClientConfigResult: ClientConfig!

    func clientConfig() -> ClientConfig? {
        invokedClientConfig = true
        invokedClientConfigCount += 1
        return stubbedClientConfigResult
    }

    var invokedSaveClientConfig = false
    var invokedSaveClientConfigCount = 0
    var invokedSaveClientConfigParameters: (clientConfig: ClientConfig, Void)?
    var invokedSaveClientConfigParametersList = [(clientConfig: ClientConfig, Void)]()

    func saveClientConfig(clientConfig: ClientConfig) {
        invokedSaveClientConfig = true
        invokedSaveClientConfigCount += 1
        invokedSaveClientConfigParameters = (clientConfig, ())
        invokedSaveClientConfigParametersList.append((clientConfig, ()))
    }
}
