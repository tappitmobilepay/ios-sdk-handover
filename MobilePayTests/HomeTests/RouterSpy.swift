//
//  RouterSpy.swift
//  KansasTests
//
//  Created by Marius Kurgonas on 24/02/2021.
//  Copyright © 2021 admin. All rights reserved.
//

import Foundation
import UIKit
@testable import MobilePay

@available(iOS 13, *)
class RouterSpy: RouterContract {

    var invokedContextGetter = false
    var invokedContextGetterCount = 0
    var stubbedContext: ApiContext!

    var context: ApiContext {
        invokedContextGetter = true
        invokedContextGetterCount += 1
        return stubbedContext
    }

    var invokedCredStoreGetter = false
    var invokedCredStoreGetterCount = 0
    var stubbedCredStore: CredentialsStoringContract!

    var credStore: CredentialsStoringContract {
        invokedCredStoreGetter = true
        invokedCredStoreGetterCount += 1
        return stubbedCredStore
    }

    var invokedLoginManagerGetter = false
    var invokedLoginManagerGetterCount = 0
    var stubbedLoginManager: LoginManagingContract!

    var loginManager: LoginManagingContract {
        invokedLoginManagerGetter = true
        invokedLoginManagerGetterCount += 1
        return stubbedLoginManager
    }

    var invokedDbGetter = false
    var invokedDbGetterCount = 0
    var stubbedDb: DatabaseManager!

    var db: DatabaseManager {
        invokedDbGetter = true
        invokedDbGetterCount += 1
        return stubbedDb
    }

    var invokedDeepLinkGetter = false
    var invokedDeepLinkGetterCount = 0
    var stubbedDeepLink: DeepLink!

    var deepLink: DeepLink? {
        invokedDeepLinkGetter = true
        invokedDeepLinkGetterCount += 1
        return stubbedDeepLink
    }

    var invokedSsoLoginSetter = false
    var invokedSsoLoginSetterCount = 0
    var invokedSsoLogin: SSOLogin?
    var invokedSsoLoginList = [SSOLogin]()
    var invokedSsoLoginGetter = false
    var invokedSsoLoginGetterCount = 0
    var stubbedSsoLogin: SSOLogin!

    var ssoLogin: SSOLogin {
        set {
            invokedSsoLoginSetter = true
            invokedSsoLoginSetterCount += 1
            invokedSsoLogin = newValue
            invokedSsoLoginList.append(newValue)
        }
        get {
            invokedSsoLoginGetter = true
            invokedSsoLoginGetterCount += 1
            return stubbedSsoLogin
        }
    }

    var invokedPaymentFlowManagerSetter = false
    var invokedPaymentFlowManagerSetterCount = 0
    var invokedPaymentFlowManager: PaymentFlowManagementContract?
    var invokedPaymentFlowManagerList = [PaymentFlowManagementContract?]()
    var invokedPaymentFlowManagerGetter = false
    var invokedPaymentFlowManagerGetterCount = 0
    var stubbedPaymentFlowManager: PaymentFlowManagementContract!

    var paymentFlowManager: PaymentFlowManagementContract! {
        set {
            invokedPaymentFlowManagerSetter = true
            invokedPaymentFlowManagerSetterCount += 1
            invokedPaymentFlowManager = newValue
            invokedPaymentFlowManagerList.append(newValue)
        }
        get {
            invokedPaymentFlowManagerGetter = true
            invokedPaymentFlowManagerGetterCount += 1
            return stubbedPaymentFlowManager
        }
    }

    var invokedGoBackTo = false
    var invokedGoBackToCount = 0
    var invokedGoBackToParameters: (to: NavigationLocations, Void)?
    var invokedGoBackToParametersList = [(to: NavigationLocations, Void)]()

    func goBack(to: NavigationLocations) {
        invokedGoBackTo = true
        invokedGoBackToCount += 1
        invokedGoBackToParameters = (to, ())
        invokedGoBackToParametersList.append((to, ()))
    }

    var invokedGoBackPin = false
    var invokedGoBackPinCount = 0
    var invokedGoBackPinParameters: (pin: Bool, Void)?
    var invokedGoBackPinParametersList = [(pin: Bool, Void)]()

    func goBack(pin: Bool) {
        invokedGoBackPin = true
        invokedGoBackPinCount += 1
        invokedGoBackPinParameters = (pin, ())
        invokedGoBackPinParametersList.append((pin, ()))
    }

    var invokedGoBack = false
    var invokedGoBackCount = 0

    func goBack() {
        invokedGoBack = true
        invokedGoBackCount += 1
    }

    var invokedGoToRoot = false
    var invokedGoToRootCount = 0

    func goToRoot() {
        invokedGoToRoot = true
        invokedGoToRootCount += 1
    }

    var invokedBuildSSO = false
    var invokedBuildSSOCount = 0
    var stubbedBuildSSOResult: UIViewController!

    func buildSSO() -> UIViewController {
        invokedBuildSSO = true
        invokedBuildSSOCount += 1
        return stubbedBuildSSOResult
    }

    var invokedPresentOTPConfirmation = false
    var invokedPresentOTPConfirmationCount = 0

    func presentOTPConfirmation() {
        invokedPresentOTPConfirmation = true
        invokedPresentOTPConfirmationCount += 1
    }

    var invokedPresentSSOInformation = false
    var invokedPresentSSOInformationCount = 0
    var invokedPresentSSOInformationParameters: (animated: Bool, Void)?
    var invokedPresentSSOInformationParametersList = [(animated: Bool, Void)]()

    func presentSSOInformation(animated: Bool) {
        invokedPresentSSOInformation = true
        invokedPresentSSOInformationCount += 1
        invokedPresentSSOInformationParameters = (animated, ())
        invokedPresentSSOInformationParametersList.append((animated, ()))
    }

    var invokedPresentAuthentication = false
    var invokedPresentAuthenticationCount = 0
    var invokedPresentAuthenticationParameters: (animated: Bool, Void)?
    var invokedPresentAuthenticationParametersList = [(animated: Bool, Void)]()

    func presentAuthentication(animated: Bool) {
        invokedPresentAuthentication = true
        invokedPresentAuthenticationCount += 1
        invokedPresentAuthenticationParameters = (animated, ())
        invokedPresentAuthenticationParametersList.append((animated, ()))
    }

    var invokedPresentSetupPin = false
    var invokedPresentSetupPinCount = 0
    var invokedPresentSetupPinParameters: (navigationMethod: NavigationMethod, Void)?
    var invokedPresentSetupPinParametersList = [(navigationMethod: NavigationMethod, Void)]()

    func presentSetupPin(navigationMethod: NavigationMethod) {
        invokedPresentSetupPin = true
        invokedPresentSetupPinCount += 1
        invokedPresentSetupPinParameters = (navigationMethod, ())
        invokedPresentSetupPinParametersList.append((navigationMethod, ()))
    }

    var invokedPresentSetupBiometric = false
    var invokedPresentSetupBiometricCount = 0
    var invokedPresentSetupBiometricParameters: (navigationMethod: NavigationMethod, Void)?
    var invokedPresentSetupBiometricParametersList = [(navigationMethod: NavigationMethod, Void)]()

    func presentSetupBiometric(navigationMethod: NavigationMethod) {
        invokedPresentSetupBiometric = true
        invokedPresentSetupBiometricCount += 1
        invokedPresentSetupBiometricParameters = (navigationMethod, ())
        invokedPresentSetupBiometricParametersList.append((navigationMethod, ()))
    }

    var invokedPresentEnterPin = false
    var invokedPresentEnterPinCount = 0
    var invokedPresentEnterPinParameters: (animated: Bool, Void)?
    var invokedPresentEnterPinParametersList = [(animated: Bool, Void)]()

    func presentEnterPin(animated: Bool) {
        invokedPresentEnterPin = true
        invokedPresentEnterPinCount += 1
        invokedPresentEnterPinParameters = (animated, ())
        invokedPresentEnterPinParametersList.append((animated, ()))
    }

    var invokedPresentEnterBiometric = false
    var invokedPresentEnterBiometricCount = 0
    var invokedPresentEnterBiometricParameters: (animated: Bool, Void)?
    var invokedPresentEnterBiometricParametersList = [(animated: Bool, Void)]()

    func presentEnterBiometric(animated: Bool) {
        invokedPresentEnterBiometric = true
        invokedPresentEnterBiometricCount += 1
        invokedPresentEnterBiometricParameters = (animated, ())
        invokedPresentEnterBiometricParametersList.append((animated, ()))
    }

    var invokedPresentGiftCards = false
    var invokedPresentGiftCardsCount = 0
    var invokedPresentGiftCardsParameters: (paymentData: PaymentData, mode: GiveawayTokensScreenMode, removingLast: Bool)?
    var invokedPresentGiftCardsParametersList = [(paymentData: PaymentData, mode: GiveawayTokensScreenMode, removingLast: Bool)]()

    func presentGiftCards(paymentData: PaymentData, mode: GiveawayTokensScreenMode, removingLast: Bool) {
        invokedPresentGiftCards = true
        invokedPresentGiftCardsCount += 1
        invokedPresentGiftCardsParameters = (paymentData, mode, removingLast)
        invokedPresentGiftCardsParametersList.append((paymentData, mode, removingLast))
    }

    var invokedPresentRedeemGiftcard = false
    var invokedPresentRedeemGiftcardCount = 0

    func presentRedeemGiftcard() {
        invokedPresentRedeemGiftcard = true
        invokedPresentRedeemGiftcardCount += 1
    }

    var invokedPresentLoader = false
    var invokedPresentLoaderCount = 0

    func presentLoader() {
        invokedPresentLoader = true
        invokedPresentLoaderCount += 1
    }

    var invokedHideLoader = false
    var invokedHideLoaderCount = 0

    func hideLoader() {
        invokedHideLoader = true
        invokedHideLoaderCount += 1
    }

    var invokedPresentTips = false
    var invokedPresentTipsCount = 0
    var invokedPresentTipsParameters: (paymentData: PaymentData, removingLast: Bool)?
    var invokedPresentTipsParametersList = [(paymentData: PaymentData, removingLast: Bool)]()

    func presentTips(paymentData: PaymentData, removingLast: Bool) {
        invokedPresentTips = true
        invokedPresentTipsCount += 1
        invokedPresentTipsParameters = (paymentData, removingLast)
        invokedPresentTipsParametersList.append((paymentData, removingLast))
    }

    var invokedPresentAddInitialCard = false
    var invokedPresentAddInitialCardCount = 0
    var invokedPresentAddInitialCardParameters: (animated: Bool, Void)?
    var invokedPresentAddInitialCardParametersList = [(animated: Bool, Void)]()

    func presentAddInitialCard(animated: Bool) {
        invokedPresentAddInitialCard = true
        invokedPresentAddInitialCardCount += 1
        invokedPresentAddInitialCardParameters = (animated, ())
        invokedPresentAddInitialCardParametersList.append((animated, ()))
    }

    var invokedPresentAddCard = false
    var invokedPresentAddCardCount = 0

    func presentAddCard() {
        invokedPresentAddCard = true
        invokedPresentAddCardCount += 1
    }

    var invokedPresentViewCardDetails = false
    var invokedPresentViewCardDetailsCount = 0
    var invokedPresentViewCardDetailsParameters: (card: Card, Void)?
    var invokedPresentViewCardDetailsParametersList = [(card: Card, Void)]()

    func presentViewCardDetails(card: Card) {
        invokedPresentViewCardDetails = true
        invokedPresentViewCardDetailsCount += 1
        invokedPresentViewCardDetailsParameters = (card, ())
        invokedPresentViewCardDetailsParametersList.append((card, ()))
    }

    var invokedPresentMyPaymentMethods = false
    var invokedPresentMyPaymentMethodsCount = 0

    func presentMyPaymentMethods() {
        invokedPresentMyPaymentMethods = true
        invokedPresentMyPaymentMethodsCount += 1
    }

    var invokedPresentSuccess = false
    var invokedPresentSuccessCount = 0
    var invokedPresentSuccessParameters: (type: CompletionType, message: String?)?
    var invokedPresentSuccessParametersList = [(type: CompletionType, message: String?)]()
    var shouldInvokePresentSuccessCompletion = false

    func presentSuccess(type: CompletionType, message: String?, completion: (() -> Void)?) {
        invokedPresentSuccess = true
        invokedPresentSuccessCount += 1
        invokedPresentSuccessParameters = (type, message)
        invokedPresentSuccessParametersList.append((type, message))
        if shouldInvokePresentSuccessCompletion {
            completion?()
        }
    }

    var invokedPresentHomeAnimated = false
    var invokedPresentHomeAnimatedCount = 0
    var invokedPresentHomeAnimatedParameters: (animated: Bool, replaceLast: Bool)?
    var invokedPresentHomeAnimatedParametersList = [(animated: Bool, replaceLast: Bool)]()

    func presentHome(animated: Bool, replaceLast: Bool) {
        invokedPresentHomeAnimated = true
        invokedPresentHomeAnimatedCount += 1
        invokedPresentHomeAnimatedParameters = (animated, replaceLast)
        invokedPresentHomeAnimatedParametersList.append((animated, replaceLast))
    }

    var invokedPresentHome = false
    var invokedPresentHomeCount = 0
    var invokedPresentHomeParameters: (animated: Bool, Void)?
    var invokedPresentHomeParametersList = [(animated: Bool, Void)]()

    func presentHome(animated: Bool) {
        invokedPresentHome = true
        invokedPresentHomeCount += 1
        invokedPresentHomeParameters = (animated, ())
        invokedPresentHomeParametersList.append((animated, ()))
    }

    var invokedPresentRefund = false
    var invokedPresentRefundCount = 0

    func presentRefund() {
        invokedPresentRefund = true
        invokedPresentRefundCount += 1
    }

    var invokedPresentPay = false
    var invokedPresentPayCount = 0
    var invokedPresentPayParameters: (paymentData: PaymentData, removingLast: Bool)?
    var invokedPresentPayParametersList = [(paymentData: PaymentData, removingLast: Bool)]()

    func presentPay(paymentData: PaymentData, removingLast: Bool) {
        invokedPresentPay = true
        invokedPresentPayCount += 1
        invokedPresentPayParameters = (paymentData, removingLast)
        invokedPresentPayParametersList.append((paymentData, removingLast))
    }

    var invokedPresentTransactionsHistory = false
    var invokedPresentTransactionsHistoryCount = 0

    func presentTransactionsHistory() {
        invokedPresentTransactionsHistory = true
        invokedPresentTransactionsHistoryCount += 1
    }

    var invokedPresentSettings = false
    var invokedPresentSettingsCount = 0

    func presentSettings() {
        invokedPresentSettings = true
        invokedPresentSettingsCount += 1
    }

    var invokedPresentHelp = false
    var invokedPresentHelpCount = 0

    func presentHelp() {
        invokedPresentHelp = true
        invokedPresentHelpCount += 1
    }

    var invokedPresentGiveawayTokens = false
    var invokedPresentGiveawayTokensCount = 0
    var invokedPresentGiveawayTokensParameters: (paymentData: PaymentData, mode: GiveawayTokensScreenMode)?
    var invokedPresentGiveawayTokensParametersList = [(paymentData: PaymentData, mode: GiveawayTokensScreenMode)]()

    func presentGiveawayTokens(paymentData: PaymentData, mode: GiveawayTokensScreenMode) {
        invokedPresentGiveawayTokens = true
        invokedPresentGiveawayTokensCount += 1
        invokedPresentGiveawayTokensParameters = (paymentData, mode)
        invokedPresentGiveawayTokensParametersList.append((paymentData, mode))
    }

    var invokedPresentCreditCardScanner = false
    var invokedPresentCreditCardScannerCount = 0
    var invokedPresentCreditCardScannerParameters: (delegate: CreditCardScannerViewControllerDelegate, Void)?
    var invokedPresentCreditCardScannerParametersList = [(delegate: CreditCardScannerViewControllerDelegate, Void)]()

    func presentCreditCardScanner(delegate: CreditCardScannerViewControllerDelegate) {
        invokedPresentCreditCardScanner = true
        invokedPresentCreditCardScannerCount += 1
        invokedPresentCreditCardScannerParameters = (delegate, ())
        invokedPresentCreditCardScannerParametersList.append((delegate, ()))
    }

    var invokedPresentCampaignDetails = false
    var invokedPresentCampaignDetailsCount = 0
    var invokedPresentCampaignDetailsParameters: (campaign: Campaign, Void)?
    var invokedPresentCampaignDetailsParametersList = [(campaign: Campaign, Void)]()

    func presentCampaignDetails(campaign: Campaign) {
        invokedPresentCampaignDetails = true
        invokedPresentCampaignDetailsCount += 1
        invokedPresentCampaignDetailsParameters = (campaign, ())
        invokedPresentCampaignDetailsParametersList.append((campaign, ()))
    }

    var invokedPresentTransactionDetails = false
    var invokedPresentTransactionDetailsCount = 0
    var invokedPresentTransactionDetailsParameters: (transaction: Transaction, currencySymbol: String)?
    var invokedPresentTransactionDetailsParametersList = [(transaction: Transaction, currencySymbol: String)]()

    func presentTransactionDetails(transaction: Transaction, currencySymbol: String) {
        invokedPresentTransactionDetails = true
        invokedPresentTransactionDetailsCount += 1
        invokedPresentTransactionDetailsParameters = (transaction, currencySymbol)
        invokedPresentTransactionDetailsParametersList.append((transaction, currencySymbol))
    }

    var invokedDismissModalController = false
    var invokedDismissModalControllerCount = 0

    func dismissModalController() {
        invokedDismissModalController = true
        invokedDismissModalControllerCount += 1
    }

    var invokedPresentApplePay = false
    var invokedPresentApplePayCount = 0
    var invokedPresentApplePayParameters: (data: NotificationContent, Void)?
    var invokedPresentApplePayParametersList = [(data: NotificationContent, Void)]()

    func presentApplePay(data: NotificationContent) {
        invokedPresentApplePay = true
        invokedPresentApplePayCount += 1
        invokedPresentApplePayParameters = (data, ())
        invokedPresentApplePayParametersList.append((data, ()))
    }
}
