//
//  HomePresenterTests+Extensions.swift
//  MobilePayTests
//
//  Created by Marius Kurgonas on 25/02/2021.
//  Copyright © 2021 JadePayments. All rights reserved.
//

import Foundation
@testable import MobilePay

@available(iOS 13, *)
extension HomePresenterTests {
    
    func getClientConfig(myOffersEnabled: Bool) -> ClientConfig {
        let tipsConfig = TipsConfiguration(enabled: false, percentages: [])
        let settings = ClientConfigSettings(loginEndpointOverride: nil,
                                            tokensEndpointOverride: nil,
                                            balanceEndpointOverride: nil,
                                            tips: tipsConfig,
                                            showGiveawayTokens: false,
                                            showGiftCard: false,
                                            showMyOffers: myOffersEnabled,
                                            preloadTopup: false,
                                            showPrivacyPolicy: false,
                                            registrationType: 1,
                                            privacyPolicy: nil,
                                            termsAndConditions: nil,
                                            help: nil)
        let clientConfiguration = ClientConfigConfiguration(settings: settings,
                                                            appName: "")
        return ClientConfig(configuration: clientConfiguration,
                                  clientToken: "",
                                  clientId: "",
                                  clientCurrency: ClientCurrency(code: "", symbol: ""))
    }
}
