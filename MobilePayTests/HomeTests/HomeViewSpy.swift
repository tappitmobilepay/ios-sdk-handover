//
//  HomeViewSpy.swift
//  KansasTests
//
//  Created by Marius Kurgonas on 24/02/2021.
//  Copyright © 2021 admin. All rights reserved.
//

import Foundation
@testable import MobilePay

class HomeViewSpy: HomeViewingContract {

    var invokedPresenterSetter = false
    var invokedPresenterSetterCount = 0
    var invokedPresenter: HomePresentingContract?
    var invokedPresenterList = [HomePresentingContract?]()
    var invokedPresenterGetter = false
    var invokedPresenterGetterCount = 0
    var stubbedPresenter: HomePresentingContract!

    var presenter: HomePresentingContract! {
        set {
            invokedPresenterSetter = true
            invokedPresenterSetterCount += 1
            invokedPresenter = newValue
            invokedPresenterList.append(newValue)
        }
        get {
            invokedPresenterGetter = true
            invokedPresenterGetterCount += 1
            return stubbedPresenter
        }
    }

    var invokedUpdate = false
    var invokedUpdateCount = 0
    var invokedUpdateParameters: (data: [HomeCellBranding], Void)?
    var invokedUpdateParametersList = [(data: [HomeCellBranding], Void)]()

    func update(data: [HomeCellBranding]) {
        invokedUpdate = true
        invokedUpdateCount += 1
        invokedUpdateParameters = (data, ())
        invokedUpdateParametersList.append((data, ()))
    }
}
