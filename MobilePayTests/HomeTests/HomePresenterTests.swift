//
//  HomePresenterTests.swift
//  MobilePayShowcase
//
//  Created by Marius Kurgonas on 24/02/2021.
//  Copyright © 2021 admin. All rights reserved.
//

import Quick
import Nimble
@testable import MobilePay

@available(iOS 13, *)
class HomePresenterTests: QuickSpec {
    override func spec() {
        describe("Home presenter") {
            var presenter: HomePresenter!
            var view: HomeViewSpy!
            var router: RouterSpy!
            var database: DatabaseManagementSpy!
            var paymentsService: PaymentsServiceSpy!
            var paymentFlowManager: PaymentFlowManagerSpy!
            
            beforeEach {
                view = HomeViewSpy()
                router = RouterSpy()
                paymentFlowManager = PaymentFlowManagerSpy()
                paymentsService = PaymentsServiceSpy()
                database = DatabaseManagementSpy()
                presenter = HomePresenter(view: view,
                                          router: router,
                                          db: database,
                                          paymentsService: paymentsService,
                                          paymentFlowManager: paymentFlowManager)
            }
            
//            context("when show my offers is enabled and deep link is enabled") {
//                
//                beforeEach {
//                    database.stubbedClientConfigResult = self.getClientConfig(myOffersEnabled: true)
//                    
//                    router.stubbedDeepLink = DeepLink(url: URL(string: "https://www.google.com")!)
//                }
//                
//                context("when view did load is called") {
//                    
//                    beforeEach {
//                        presenter.viewDidLoad()
//                    }
//                    
//                    it("should call setup on view to enable my offers and deep link") {
//                        expect(view.invokedUpdateCount).to(equal(1))
//                        expect(view.invokedUpdateParameters?.showDeepLink).to(beTrue())
//                        expect(view.invokedUpdateParameters?.showOffers).to(beTrue())
//                    }
//                }
//            }
//            
//            context("when show my offers is disabled and deep link is enabled") {
//                
//                beforeEach {
//                    database.stubbedClientConfigResult = self.getClientConfig(myOffersEnabled: false)
//                    
//                    router.stubbedDeepLink = DeepLink(url: URL(string: "https://www.google.com")!)
//                }
//                
//                context("when view did load is called") {
//                    
//                    beforeEach {
//                        presenter.viewDidLoad()
//                    }
//                    
//                    it("should call setup on view to enable my offers and deep link") {
//                        expect(view.invokedUpdateCount).to(equal(1))
//                        expect(view.invokedUpdateParameters?.showDeepLink).to(beTrue())
//                        expect(view.invokedUpdateParameters?.showOffers).to(beFalse())
//                    }
//                }
//            }
//            
//            context("when show my offers is disabled and deep link is disabled") {
//                
//                beforeEach {
//                    database.stubbedClientConfigResult = self.getClientConfig(myOffersEnabled: false)
//                    
//                    router.stubbedDeepLink = nil
//                }
//                
//                context("when view did load is called") {
//                    
//                    beforeEach {
//                        presenter.viewDidLoad()
//                    }
//                    
//                    it("should call setup on view to enable my offers and deep link") {
//                        expect(view.invokedUpdateCount).to(equal(1))
//                        expect(view.invokedUpdateParameters?.showDeepLink).to(beFalse())
//                        expect(view.invokedUpdateParameters?.showOffers).to(beFalse())
//                    }
//                }
//            }
//            
//            context("when show my offers is enabled and deep link is disabled") {
//                
//                beforeEach {
//                    database.stubbedClientConfigResult = self.getClientConfig(myOffersEnabled: true)
//                    
//                    router.stubbedDeepLink = nil
//                }
//                
//                context("when view did load is called") {
//                    
//                    beforeEach {
//                        presenter.viewDidLoad()
//                    }
//                    
//                    it("should call setup on view to enable my offers and deep link") {
//                        expect(view.invokedUpdateCount).to(equal(1))
//                        expect(view.invokedUpdateParameters?.showDeepLink).to(beFalse())
//                        expect(view.invokedUpdateParameters?.showOffers).to(beTrue())
//                    }
//                }
//            }
        }
    }
}


