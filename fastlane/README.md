fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew install fastlane`

# Available Actions
## iOS
### ios build_release
```
fastlane ios build_release
```
Build a universal framework
### ios deploy
```
fastlane ios deploy
```
Build and deploy the binary
### ios release_on_gitlab
```
fastlane ios release_on_gitlab
```
Create release on gitlab
### ios deploy_binary
```
fastlane ios deploy_binary
```
Commit binary to remote

----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
