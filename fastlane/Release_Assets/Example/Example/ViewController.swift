//
//  ViewController.swift
//  Example
//
//  Created by Marius Kurgonas on 14/11/2019.
//  Copyright © 2019 Marius Kurgonas. All rights reserved.
//

import UIKit
import MobilePay

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        initializeSDK()
    }
    
    func initializeSDK() {
        // 1. Initialize all the variables necessary
                
        let ssoLogin = SSOLogin(customerId: UIDevice.current.identifierForVendor!.uuidString, // Unique client customer id
                                integrationKey: "integration_id", // An integration key for the client
                                email: "example@email.com", // optional customer email address. Defaults to nil
                                environment: .Test) // Optional environment. Default production
        
        
        // 2. Initialize root SDK controller
        let rootController = MobilePayViewController(login: ssoLogin)
        
    
                                                     
        // 3. Display the controller
        navigationController?.pushViewController(rootController, animated: true)
    }
    
    func initializeSDKWithDeepLink() {
        // 1. Initialize all the variables necessary
                
        let ssoLogin = SSOLogin(customerId: UIDevice.current.identifierForVendor!.uuidString, // Unique client customer id
                                integrationKey: "integration_id", // An integration key for the client
                                email: "example@email.com", // optional customer email address. Defaults to nil
                                environment: .Test) // Optional environment. Default production
         
        // Will enable additional button on home screen. Refer to the integration guide for the optional image name needed for the button
        let deepLink = DeepLink(url: URL(string: "my://deeplink")!)
        
        // 2. Initialize root SDK controller
        
        let rootController = MobilePayViewController(login: ssoLogin,
                                                     deepLink: deepLink)
    
                                                     
        // 3. Display the controller
        navigationController?.pushViewController(rootController, animated: true)
    }
    
    func initializeSDKWithExtraIntegrationIds() {
        // 1. Initialize all the variables necessary
                
        // integrationType: TicketMaster = 0, Tickets.Com = 1, Paciolan = 2 AXS = 3 TicketSocket = 4 LoyaltyId = 5 SkiData = 6
        // valueType: User_ID = 0, API_TOKEN = 1
        let ticketmasterIntegrationData = IntegrationData(integrationType: 0,
                                                          valueType: 0,
                                                          value: "Ticketmaster_customer_id",
                                                          isGlobal: true) // Pass true only if 100% sure that third party id will be unique globally throughout all services or applications for a customer
        
        let ssoLogin = SSOLogin(customerId: UIDevice.current.identifierForVendor!.uuidString, // Unique client customer id
                                integrationKey: "integration_id", // An integration key for the client
                                email: "example@email.com", // optional customer email address. Defaults to nil
                                environment: .Test, // Optional environment. Default production
                                integrationData: [ticketmasterIntegrationData]) // Third party services integration data
         
        
        // 2. Initialize root SDK controller
        
        let rootController = MobilePayViewController(login: ssoLogin)
    
                                                     
        // 3. Display the controller
        navigationController?.pushViewController(rootController, animated: true)
    }
}

