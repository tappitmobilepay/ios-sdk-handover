# iOS MobilePay

# Requirements

Xcode 11.4.1 or above

Cocoapods (Optional)

# How to install

#### Cocoapods

-   Add `'pod MobilePay','~> 3.0'` to your Podfile under a target app
-   Run `pod install`
-   Refer to the Example project if in doubt

# How to use

## Steps to use the SDK:

1. Add **NSFaceIDUsageDescription** key and a description to apps info plist file - SDK allows optionally authenticating using biometrics

2. Import framework in Swift source code: **_import MobilePay_**

4. Initialize initial view controller: MobilePayViewController()

```swift
   MobilePayViewController(login: SSO_CREDENTIALS,
                           deepLink: OPTIONAL_DEEP_LINK)
```

- `login` - a SSOLogin object encapsulating login information

  ```swift
  let ssoLogin = SSOLogin(customerId: "client_customer_id", 
                                  integrationKey: "integration_key", 
                                  email: "example@email.com", 
                                  environment: .Test)
  ```

  -   customerId: id of the customer in a third party scope
  -   integrationKey: A unique client key for the environment
  -   email: Optional customer email address. Defaults to nil.
  -   environment: Optional environment variable. Defaults to .Production

-   `deepLink` - optional DeepLink object, that if set - will enable and show an extra button on the home screen of the SDK that on tap would attempt to execute the deep link - will try to open an URL registered to handle the deep link. Refer to the Example document for the deep link setup in an app.

5. Present the controller by pushing it to UINavigationController full screen.

## Providing additional integration ids for third party services

Optionally, there is an ability to provide third party ids list when initializing an SDK.

```swift
let ssoLogin = SSOLogin(customerId: "client_customer_id", 
                                integrationKey: "integration_key", 
                                email: "example@email.com", 
                                environment: <Environment>,
                                integrationData: <List of third party integration ids>)
```



Here is an example object of an integration id:

```swift
let integrationId = IntegrationData(integrationType: 0,
                                   valueType: 0,
                                   value: "example",
                                   isGlobal: true)
```

The following are supported values for each item. This list is going to be updated as development progresses:

1. integrationType: TicketMaster = 0, Tickets.Com = 1, Paciolan = 2 AXS = 3 TicketSocket = 4 LoyaltyId = 5 SkiData = 6
2. valueType: User ID = 0, API TOKEN = 1
3. value: actual value of the id
4. isGlobal: true or false depending on if the value is unique globally on any application or not

## Branding assets

### Branding images override

* `tmw_navigation_logo` - main app logo
* `tmw_deep_link` - if your app require deep inking from the SDK into other place. Will add additional button with this image on home screen. (Use alongside optional initialization parameter to enable this feature)

Provide the images named as described inside application's `xcassets` catalog. Refer to the Example project for reference.

### How to override colors

SDK supports these colors overrides:

-   `tmw_back_indicator` - back button tint color
-   `tmw_biometric_indicator` - biometric image tint color
-    `tmw_navigation_background` - navigation bar background tint color
-   `tmw_onboarding_background_text` - text that is on onboarding background tint color
-   `tmw_onboarding_background` - onboarding background tint color
-   `tmw_onboarding_button_background` - buttons which are on the onboarding background tint color
-   `tmw_onboarding_button_text` - onboarding buttons text tint color
-   `tmw_page_header_text` - headers of the page tint color
-   `tmw_pay_button_background` - pay button background tint color
-   `tmw_pay_button_text` - pay button text tint color
-   `tmw_primary_background_text` - primary object background text tint color
-   `tmw_primary_background` - primary object background tint color
-   `tmw_secondary_background_text` - secondary object background text tint color
-   `tmw_secondary_background` - secondary object background tint color

To override any combination of those put each color inside the `xcassets` catalog as each separate color item. Refer to the Example project for more information. Override all the colors as needed.

### Other branding

SDK supports the following overrides for text and links:

* `tmw_brand_name` - brand name to customize `Mobile pay`. E.g. if value provided was `Example` then the references within the SDK will become `Example pay` etc.
* `tmw_help_url` - Help section URL that will load when customers go into the `help` page of the SDK
* `tmw_terms_conditions_url` - link to terms and conditions of the brand that will occur throughout the SDK
* `tmw_fonts` - values of the fonts to be overriden. Supported fonts in the SDK are:
  * `bold` - name of the family of custom bold font
  * `normal` - name of the family of custom normal font

Override any or all of the values listed above by putting the following into applications info.plist file:

```xml
<key>tmw_tappit_config</key>
	<dict>
		<key>tmw_fonts</key>
		<dict>
			<key>bold</key>
			<string>sans-serif-bold</string>
			<key>normal</key>
			<string>sans-serif</string>
		</dict>
		<key>tmw_terms_conditions_url</key>
		<string>https://asgardvikings.zendesk.com/hc/en-us/articles/360015453097-Asgard-Vikings-Terms-Conditions</string>
		<key>tmw_help_page_url</key>
		<string>https://asgardvikings.zendesk.com/</string>
		<key>tmw_brand_name</key>
		<string>Vikings</string>
	</dict>
```

If using custom fonts remember to provide entry of the fonts provided by the application as well:



```xml
<key>UIAppFonts</key>
	<array>
		<string>bold.ttf</string>
		<string>normal.ttf</string>
	</array>
```

