//
//  TMWCommonUtil.swift
//  MobilePay
//
//  Created by Pandi on 17/07/19.
//  Copyright © 2019 JadePayments. All rights reserved.
//

import CommonCrypto
import CoreData
import PassKit
import UIKit
import UserNotifications

func onlyDebugPrint(_ string: String) {
    #if DEBUG
        debugPrint("\(string)")
    #endif
}

// import "MMDrawController.h"

// MARK: - Core Data stack

enum TappitIntegrationError: Error {
    case error(message: String)
}

enum TMWCommonUtil {
    static let userDefaults: UserDefaults! = UserDefaults(suiteName: "com.tappit.sdk.userdefaults")

    static let reachability: Reachability! = try? Reachability()

    static func showInternetUnavailableAlert() {
        showAlert(message: "Internet connection unavailable. Check it and try again.")
    }

    static func isInternetAvailable() -> Bool {
        let reachability = try! Reachability()
        return reachability.connection != .unavailable
    }

    static var thirdPartyFontNamesForTypes = [TappitFont.Family: String]()

    static func checkAndLoadFonts() {
        thirdPartyFontNamesForTypes = ClientSpecificValue.fonts.toDictionary()
    }
}

protocol Crypter {
    func encrypt(_ digest: Data) throws -> Data
    func decrypt(_ encrypted: Data) throws -> Data
}

extension AES256Crypter: Crypter {
    func encrypt(_ digest: Data) throws -> Data {
        return try crypt(input: digest, operation: CCOperation(kCCEncrypt))
    }

    func decrypt(_ encrypted: Data) throws -> Data {
        return try crypt(input: encrypted, operation: CCOperation(kCCDecrypt))
    }
}

struct AES256Crypter {
    private var key: Data
    private var iv: Data

    init(key: Data, iv: Data) throws {
        guard key.count == kCCKeySizeAES256 else {
            throw Error.badKeyLength
        }
        guard iv.count == kCCBlockSizeAES128 else {
            throw Error.badInputVectorLength
        }
        self.key = key
        self.iv = iv
    }

    enum Error: Swift.Error {
        case keyGeneration(status: Int)
        case cryptoFailed(status: CCCryptorStatus)
        case badKeyLength
        case badInputVectorLength
    }

    private func crypt(input: Data, operation: CCOperation) throws -> Data {
        var outLength = Int(0)
        var outBytes = [UInt8](repeating: 0, count: input.count + kCCBlockSizeAES128)
        var status = CCCryptorStatus(kCCSuccess)
        input.withUnsafeBytes { (encryptedBytes: UnsafePointer<UInt8>!) -> Void in
            iv.withUnsafeBytes { (ivBytes: UnsafePointer<UInt8>!) in
                key.withUnsafeBytes { (keyBytes: UnsafePointer<UInt8>!) -> Void in
                    status = CCCrypt(operation,
                                     CCAlgorithm(kCCAlgorithmAES128), // algorithm
                                     CCOptions(kCCOptionPKCS7Padding), // options
                                     keyBytes, // key
                                     key.count, // keylength
                                     ivBytes, // iv
                                     encryptedBytes, // dataIn
                                     input.count, // dataInLength
                                     &outBytes, // dataOut
                                     outBytes.count, // dataOutAvailable
                                     &outLength) // dataOutMoved
                }
            }
        }
        guard status == kCCSuccess else {
            throw Error.cryptoFailed(status: status)
        }
        return Data(bytes: UnsafePointer<UInt8>(outBytes), count: outLength)
    }
}
