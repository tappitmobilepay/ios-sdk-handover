//
//  GiveawayTokensResponseParsingContract.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 03/01/2021.
//  Copyright © 2021 JadePayments. All rights reserved.
//

import Foundation

protocol GiveawayTokensResponseParsingContract {
    func giveawayTokensResponse(from data: Data?) throws -> [GiveawayToken]
    func benefitsResponse(from data: Data?) throws -> Benefits
}
