//
//  CampaignsResponseParser.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 31/08/2021.
//  Copyright © 2021 JadePayments. All rights reserved.
//

import Foundation

class CampaignsResponseParser: CampaignsResponseParsingContract {
    func campaignsResponse(from data: Data?) throws -> [Campaign] {
        let decoder = JSONDecoder()

        if let d = data, let campaigns = try? decoder.decode([Campaign].self, from: d) {
            return campaigns
        }

        else {
            throw invalidDataError
        }
    }
}
