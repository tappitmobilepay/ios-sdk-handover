//
//  CampaignsURLSessionWebService.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 31/08/2021.
//  Copyright © 2021 JadePayments. All rights reserved.
//

import Foundation

class CampaignsURLSessionWebService: NSURLSessionWebService, CampaignsServicingContract {
    var responseParser: CampaignsResponseParsingContract?

    func getCampaigns(completion: @escaping CampaignsResponse) {
        guard let responseParser = responseParser else {
            assert(false)
            return
        }

        task = get(at: CampaignsApiRoute.all, completion: { data, error in
            if error == nil {
                do {
                    let campaigns = try responseParser.campaignsResponse(from: data)
                    DispatchQueue.main.async {
                        completion(campaigns, nil)
                    }
                }
                catch {
                    DispatchQueue.main.async {
                        completion(nil, error)
                    }
                }
            }
            else {
                DispatchQueue.main.async {
                    completion(nil, error)
                }
            }
        })
    }

    func cancel() {
        task?.cancel()
        task = nil
    }

    var task: URLSessionDataTask?
}
