//
//  Campaign.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 31/08/2021.
//  Copyright © 2021 JadePayments. All rights reserved.
//

import CoreData
import Foundation

struct Campaign: Codable {
    let campaign_id: String
    let campaign_name: String
    let campaign_description: String
    let campaign_start_date: String
    let campaign_end_date: String?
    let benefit_type: BenefitType?
    let trigger_type: TriggerType
    let trigger_value: String
    let benefit_display_text: String?
    let qualified_date: String?
    let benefit_id: String?
    let benefit_expiration_date: String
    let complimentary_balance_redeemed_amount: Decimal?
    let is_give_away_token_used: Bool?
    let campaign_progress_value: String?
    let progress_percentage: Decimal?
    var is_qualified: Bool
    let day_configuration: DayConfigurationModel?
    let time_configuration: TimeConfigurationModel?
    let trigger_configuration: TriggerConfigurationModel?
    let benefit_configuration: BenefitConfigurationModel?
}

let daysArray = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]

extension Campaign {
    func monthDaysString(array: [Int]?) -> String {
        if let array = array, !array.isEmpty {
            let sorted = array.sorted()
            var ranges = [String]()

            var lastItem: Int?
            var lastBeginning = sorted.first!
            for item in sorted {
                if let lastItem = lastItem, item - 1 != lastItem {
                    if lastItem != lastBeginning {
                        ranges.append("\(lastBeginning)-\(lastItem)")
                    }
                    else {
                        ranges.append("\(lastBeginning)")
                    }
                    lastBeginning = item
                }

                lastItem = item
            }

            return ranges.joined(separator: ",")
        }

        return "-"
    }

    func validOnString() -> String {
        guard let daytype = day_configuration?.day_type else {
            return "-"
        }
        switch daytype {
        case .EveryDay: return "Every day"
        case .EveryTwoWeeks, .Weekly: return (day_configuration?.week_days?.map { $0 >= daysArray.count ? "" : daysArray[$0] })?.joined(separator: ", ") ?? "-"
        case .Monthly: return monthDaysString(array: day_configuration?.month_days)
        }
    }

    func validOnDayDetailsString() -> String {
        guard let daytype = day_configuration?.day_type else {
            return "-"
        }
        switch daytype {
        case .EveryDay: return "Every day"
        case .EveryTwoWeeks: return "Every two weeks"
        case .Weekly: return "Weekly"
        case .Monthly: return "Monthly"
        }
    }

    func validOnTimeString() -> String {
        let defaultValue = "-"
        guard let timeType = time_configuration?.time_type else {
            return defaultValue
        }
        switch timeType {
        case .AllDay: return "All day"
        case .SelectedTime:
            if let start = time_configuration?.start_time, let end = time_configuration?.end_time {
                return "\(start) - \(end)"
            }
            else {
                return defaultValue
            }
        }
    }

    func benefitsString(currency: String) -> String {
        let defaultValue = "-"
        guard let benefitType = benefit_configuration?.benefit_type else {
            return defaultValue
        }
        switch benefitType {
        case .complimentary_balance:

            if let amount = benefit_configuration?.amount {
                return "\(currency) \(amount.double().format(f: ".2")) Complimentary Balance"
            }
            else {
                return defaultValue
            }
        case .give_away_token:

            if let benefitText = benefit_configuration?.product_name {
                return "\(benefitText) Giveaway Token"
            }
            else {
                return defaultValue
            }
        }
    }

    func startsString(clientTimezone: TimeZone) -> String {
        let date = campaign_start_date.replacingOccurrences(of: "T", with: " ")
            .replacingOccurrences(of: "Z", with: "")
        return date.toDate(dateFormat: "yyyy-MM-dd HH:mm:ss")?.toString(timezone: clientTimezone) ?? date
    }

    func endsString(clientTimezone: TimeZone) -> String {
        let date = campaign_end_date?.replacingOccurrences(of: "T", with: " ")
            .replacingOccurrences(of: "Z", with: "")
        return (date?.toDate(dateFormat: "yyyy-MM-dd HH:mm:ss")?.toString(timezone: clientTimezone) ?? date) ?? "-"
    }

    func validForDetailsString() -> String {
        if day_configuration?.day_type == .some(.EveryDay) {
            return validOnDayDetailsString()
        }
        else {
            return "\(validOnDayDetailsString())\n\(validOnString())"
        }
    }

    func persiondString() -> String {
        return validOnTimeString()
    }

    func howString(currency: String) -> String {
        let defaultValue = "-"
        guard let triggerType = trigger_configuration?.trigger_type else {
            return defaultValue
        }
        switch triggerType {
        case .AmountSpend:
            if let triggerValue = trigger_configuration?.amount {
                return "Spend at least \(triggerValue)"
            }
            else {
                return defaultValue
            }
        case .NumberOfTransaction:

            if let triggerValue = trigger_configuration?.count {
                return "Make at least \(triggerValue) transactions"
            }
            else {
                return defaultValue
            }
        case .RegistrationDate:

            if let beforeOrAfter = dateTypeString(),
               let registrationDate = trigger_configuration?.registration_date
            {
                return "Registered \(beforeOrAfter) \(registrationDate)"
            }
            else {
                return defaultValue
            }
        case .CardAdded:

            return "Add a card with a type of \(trigger_value)"
        default:
            return defaultValue
        }
    }

    func dateTypeString() -> String? {
        let defaultValue = "-"
        guard let dateType = trigger_configuration?.date_type else {
            return defaultValue
        }
        switch dateType {
        case .After: return "after"
        case .Before: return "before"
        }
    }

    static func from(managedCampaign: ManagedCampaign) -> Campaign {
        let jsonDecoder = JSONDecoder()
        var dayConfig: DayConfigurationModel?
        if let dayCFG = managedCampaign.day_configuration {
            dayConfig = try? jsonDecoder.decode(DayConfigurationModel.self, from: dayCFG)
        }
        var timeConfig: TimeConfigurationModel?
        if let dayCFG = managedCampaign.time_configuration {
            timeConfig = try? jsonDecoder.decode(TimeConfigurationModel.self, from: dayCFG)
        }
        var triggerConfig: TriggerConfigurationModel?
        if let dayCFG = managedCampaign.trigger_configuration {
            triggerConfig = try? jsonDecoder.decode(TriggerConfigurationModel.self, from: dayCFG)
        }
        var benefitConfig: BenefitConfigurationModel?
        if let dayCFG = managedCampaign.benefit_configuration {
            benefitConfig = try? jsonDecoder.decode(BenefitConfigurationModel.self, from: dayCFG)
        }

        var benefitType: BenefitType?
        if let bt = managedCampaign.benefit_type {
            benefitType = BenefitType(rawValue: bt)
        }

        return Campaign(campaign_id: managedCampaign.campaign_id!,
                        campaign_name: managedCampaign.campaign_name!,
                        campaign_description: managedCampaign.campaign_description!,
                        campaign_start_date: managedCampaign.campaign_start_date!,
                        campaign_end_date: managedCampaign.campaign_end_date,
                        benefit_type: benefitType,
                        trigger_type: TriggerType(rawValue: managedCampaign.trigger_type!)!,
                        trigger_value: managedCampaign.trigger_value!,
                        benefit_display_text: managedCampaign.benefit_display_text,
                        qualified_date: managedCampaign.qualified_date,
                        benefit_id: managedCampaign.benefit_id,
                        benefit_expiration_date: managedCampaign.benefit_expiration_date!,
                        complimentary_balance_redeemed_amount: managedCampaign.complimentary_balance_redeemed_amount?.decimalValue,
                        is_give_away_token_used: managedCampaign.is_give_away_token_used?.boolValue,
                        campaign_progress_value: managedCampaign.campaign_progress_value,
                        progress_percentage: managedCampaign.progress_percentage?.decimalValue,
                        is_qualified: managedCampaign.is_qualified,
                        day_configuration: dayConfig,
                        time_configuration: timeConfig,
                        trigger_configuration: triggerConfig,
                        benefit_configuration: benefitConfig)
    }

    static func toManaged(campaign: Campaign, context: NSManagedObjectContext) -> ManagedCampaign {
        let encoder = JSONEncoder()

        var dayConfig: Data?
        if let dayCFG = campaign.day_configuration {
            dayConfig = try? encoder.encode(dayCFG)
        }
        var timeConfig: Data?
        if let dayCFG = campaign.time_configuration {
            timeConfig = try? encoder.encode(dayCFG)
        }
        var triggerConfig: Data?
        if let dayCFG = campaign.trigger_configuration {
            triggerConfig = try? encoder.encode(dayCFG)
        }
        var benefitConfig: Data?
        if let dayCFG = campaign.benefit_configuration {
            benefitConfig = try? encoder.encode(dayCFG)
        }

        let managedCampaign = ManagedCampaign(context: context)
        managedCampaign.campaign_id = campaign.campaign_id
        managedCampaign.campaign_name = campaign.campaign_name
        managedCampaign.campaign_description = campaign.campaign_description
        managedCampaign.campaign_start_date = campaign.campaign_start_date
        managedCampaign.campaign_end_date = campaign.campaign_end_date
        managedCampaign.benefit_type = campaign.benefit_type?.rawValue
        managedCampaign.trigger_type = campaign.trigger_type.rawValue
        managedCampaign.trigger_value = campaign.trigger_value
        managedCampaign.benefit_display_text = campaign.benefit_display_text
        managedCampaign.qualified_date = campaign.qualified_date
        managedCampaign.benefit_id = campaign.benefit_id
        managedCampaign.benefit_expiration_date = campaign.benefit_expiration_date
        managedCampaign.complimentary_balance_redeemed_amount = campaign.complimentary_balance_redeemed_amount as NSDecimalNumber?
        if let tokenUsed = campaign.is_give_away_token_used {
            managedCampaign.is_give_away_token_used = NSNumber(booleanLiteral: tokenUsed)
        }
        managedCampaign.campaign_progress_value = campaign.campaign_progress_value
        if let value = campaign.progress_percentage {
            managedCampaign.progress_percentage = NSDecimalNumber(decimal: value)
        }
        managedCampaign.is_qualified = campaign.is_qualified
        managedCampaign.day_configuration = dayConfig
        managedCampaign.time_configuration = timeConfig
        managedCampaign.trigger_configuration = triggerConfig
        managedCampaign.benefit_configuration = benefitConfig
        return managedCampaign
    }
}

enum BenefitType: String, Codable { case
    complimentary_balance,
    give_away_token
}

enum BenefitTypeInt: Int, Codable { case
    complimentary_balance = 0,
    give_away_token = 1
}

enum TriggerType: String, Codable { case
    amount_spend,
    number_of_transaction,
    registration_date,
    add_switch_card,
    sponsored
}

enum TriggerTypeInt: Int, Codable { case
    AmountSpend = 0,
    NumberOfTransaction = 1,
    RegistrationDate = 2,
    CardAdded = 3,
    Sponsored = 4
}

enum TimeType: Int, Codable { case
    AllDay = 0,
    SelectedTime = 1
}

enum DateType: Int, Codable { case
    Before = 0,
    After = 1
}

enum DayType: Int, Codable { case
    EveryDay = 0,
    Weekly = 1,
    EveryTwoWeeks = 2,
    Monthly = 3
}

enum BalanceCalculationType: Int, Codable { case
    FixedAmount = 0,
    Percentage = 1
}

struct BenefitTypeModel: Codable {
    let name: BenefitType
}

struct TriggerTypeModel: Codable {
    let name: TriggerType
}

struct DayConfigurationModel: Codable {
    let day_type: DayType
    let week_days: [Int]?
    let month_days: [Int]?
}

struct TimeConfigurationModel: Codable {
    let time_type: TimeType
    let start_time: String?
    let end_time: String?
}

struct TriggerConfigurationModel: Codable {
    let trigger_type: TriggerTypeInt
    let amount: Decimal?
    let count: Int?
    let date_type: DateType?
    let registration_date: String?
}

struct BenefitConfigurationModel: Codable {
    let benefit_type: BenefitTypeInt
    let amount: Decimal?
    let balance_calculation_type: BalanceCalculationType?
    let expiry_date: String?
    let description: String
    let sku: String?
    let product_name: String?
    let start_date: String?
    let end_date: String?
}
