//
//  GiveawayTokensURLSessionService.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 23/12/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation

class GiveawayTokensURLSessionService: NSURLSessionWebService, GiveawayTokensServiceContract {
    var responseParser: GiveawayTokensResponseParsingContract?

    func getBenefits(fanId: String, completion: @escaping BenefitsResponse) {
        guard let responseParser = responseParser else {
            assert(false)
            return
        }

        task = get(at: UserApiRoute.benefits(fanId: fanId), completion: { data, error in
            if error == nil {
                do {
                    let benefits = try responseParser.benefitsResponse(from: data)
                    DispatchQueue.main.async {
                        completion(benefits, nil)
                    }
                }
                catch {
                    DispatchQueue.main.async {
                        completion(nil, error)
                    }
                }
            }
            else {
                DispatchQueue.main.async {
                    completion(nil, error)
                }
            }
        })
    }

    func getGiveawayTokens(completion: @escaping GiveawayTokensResponse) {
        guard let responseParser = responseParser else {
            assert(false)
            return
        }

        task = get(at: UserApiRoute.giveawayTokens, completion: { data, error in
            if error == nil {
                do {
                    let tokens = try responseParser.giveawayTokensResponse(from: data)
                    DispatchQueue.main.async {
                        completion(tokens, nil)
                    }
                }
                catch {
                    DispatchQueue.main.async {
                        completion(nil, error)
                    }
                }
            }
            else {
                DispatchQueue.main.async {
                    completion(nil, error)
                }
            }
        })
    }

    override func headers() -> [String: String] {
        var headers = super.headers()
        headers["x-api-key"] = credStore.apiKey ?? ""
        return headers
    }

    func cancel() {
        task?.cancel()
        task = nil
    }

    var task: URLSessionDataTask?
}
