//
//  GiveawayTokensPresenter.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 22/12/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation

enum GiveawayTokensScreenMode { case
    viewOnly, select
}

class GiveawayTokensPresenter: GiveawayTokensPresentingContract {
    weak var view: GiveawayTokensViewingContract?
    let router: RouterContract
    let giveawayTokensService: GiveawayTokensServiceContract
    let db: DatabaseManager
    let mode: GiveawayTokensScreenMode
    var paymentData: PaymentData
    let paymentFlowManager: PaymentFlowManagementContract
    let campaignsService: CampaignsServicingContract

    var currencySymbol: String = ""
    let userService: UserWebServiceContract

    var selectedTokens = NSMutableSet()

    init(mode: GiveawayTokensScreenMode,
         view: GiveawayTokensViewingContract,
         router: RouterContract,
         giveawayTokensService: GiveawayTokensServiceContract,
         db: DatabaseManager,
         paymentData: PaymentData,
         paymentFlowManager: PaymentFlowManagementContract,
         userService: UserWebServiceContract,
         campaignsService: CampaignsServicingContract)
    {
        self.view = view
        self.router = router
        self.db = db
        self.giveawayTokensService = giveawayTokensService
        self.mode = mode
        self.paymentData = paymentData
        self.paymentFlowManager = paymentFlowManager
        self.campaignsService = campaignsService

        currencySymbol = db.clientConfig()?.clientCurrency.symbol ?? ""
        self.userService = userService
    }

    func viewDidLoad() {
        refresh()
        view?.setup(mode: mode)
        refreshData()
    }

    func viewDidAppear() {
        if mode == .viewOnly {
            if giveawayTokens().isEmpty,
               complimentaryBalances().isEmpty,
               campaigns().isEmpty
            {
                Analytics.reportShowMyOffersEmptyScreenEvent()
            }
            else {
                Analytics.reportShowMyOffersExistsScreenEvent()
            }
        }
        else {
            Analytics.reportShowSelectOffersEvent()
        }
    }

    func refreshData() {
        selectedTokens = NSMutableSet()
        getGiveawayTokens()
    }

    func getGiveawayTokens() {
        giveawayTokensService.cancel()
        giveawayTokensService.getGiveawayTokens(completion: { [weak self] tokens, _ in
            if let tokens = tokens {
                self?.db.save(giveawayTokens: tokens)
            }
            self?.getComplimentaryBalances()
        })
    }

    func getComplimentaryBalances() {
        userService.cancel()
        userService.requestBalance(completion: { [weak self] balance, _ in
            if let balance = balance {
                self?.db.save(complimentaryBalances: balance.complimentaryBalances)
            }

            self?.getCampaigns()
        })
    }

    func getCampaigns() {
        campaignsService.cancel()
        campaignsService.getCampaigns(completion: { [weak self] campaigns, _ in
            self?.view?.hideLoading()
            if let campaigns = campaigns {
                self?.db.save(campaigns: campaigns)
            }
            self?.refresh()
        })
    }

    func giveawayTokens() -> [GiveawayToken] {
        let config = db.clientConfig()
        return config?.configuration.settings.showGiveawayTokens == .some(true) ? db.giveawayTokens() : []
    }

    func complimentaryBalances() -> [ComplimentaryBalance] {
        return db.complimentaryBalances()
    }

    func campaigns() -> [Campaign] {
        return router.credStore.showRedeemedCampaigns == .some(true) ? db.campaigns(type: .all)
            : db.campaigns(type: .notRedeemed)
    }

    func refresh() {
        view?.updateWith(tokens: giveawayTokens(),
                         balances: complimentaryBalances(),
                         campaigns: mode == .viewOnly ? campaigns() : [],
                         currency: currencySymbol)
    }

    func informationText() -> String {
        if giveawayTokens().isEmpty, complimentaryBalances().isEmpty {
            return "All Member Benefits will automatically\n" +
                "be applied when you Scan to Pay."
        }
        else if mode == .viewOnly {
            return "Please see your current offers.\n" +
                "\n" +
                "All Member Benefits will automatically\n" +
                "be applied when you Scan to Pay."
        }
        else {
            return "Select the offers to apply to your purchase.\n" +
                " \n" +
                "When you scan to pay:\n" +
                "Any Comp Balances\n" +
                "All Member Benefits\n" +
                " \n" +
                "Will be automatically applied."
        }
    }

    func didTapSkip() {
        Analytics.reportNoThanksOnSelectOffersEvent()
        moveNext()
    }

    func moveNext() {
        do {
            try paymentFlowManager.moveToNextState(paymentData: paymentData)
        }
        catch {
            assert(false)
        }
    }

    func didTapContinue() {
        Analytics.reportContinueOnSelectOffersEvent()
        if selectedTokens.count != 0, let finalArray = selectedTokens.allObjects as? [Int] {
            paymentData.tokens = finalArray
            moveNext()
        }
        else {
            let message = "Please select at least one token before continuing."
            Analytics.reportNoTokensSelectedAlertEvent(failureMessage: message)
            showAlert(message: message)
        }
    }

    func didSelectTokenAt(position: Int) {
        let tokens = giveawayTokens()
        assert(tokens.count > position)
        if mode == .select,
           tokens.count > position
        {
            let token = tokens[position]
            selectedTokens.add(token.id)
        }
    }

    func didDeselectTokenAt(position: Int) {
        let tokens = giveawayTokens()
        assert(tokens.count > position)
        if mode == .select,
           tokens.count > position
        {
            let token = tokens[position]
            selectedTokens.remove(token.id)
        }
    }

    func didSelectCampaignAt(position: Int) {
        let cpns = campaigns()
        assert(cpns.count > position)
        if mode == .viewOnly,
           cpns.count > position
        {
            let campaign = cpns[position]
            router.presentCampaignDetails(campaign: campaign)
        }
    }

    func didChangeCampaignsListing(showRedeemed: Bool) {
        var credStore = router.credStore
        credStore.showRedeemedCampaigns = showRedeemed
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
            self?.refresh()
        }
    }

    func showRedeemedCampaigns() -> Bool {
        return router.credStore.showRedeemedCampaigns ?? false
    }

    var clientTimezoneString: String {
        return router.credStore.clientTimezone ?? ""
    }

    func hasRedeemedCampaigns() -> Bool {
        return !db.campaigns(type: CampaignsTypeFetch.redeemed).isEmpty
    }
}
