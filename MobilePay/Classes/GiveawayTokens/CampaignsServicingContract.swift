//
//  CampaignsServicingContract.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 31/08/2021.
//  Copyright © 2021 JadePayments. All rights reserved.
//

import Foundation

typealias CampaignsResponse = (_ campaigns: [Campaign]?, _ error: Error?) -> Void

protocol CampaignsServicingContract: CancelableService {
    func getCampaigns(completion: @escaping CampaignsResponse)
}
