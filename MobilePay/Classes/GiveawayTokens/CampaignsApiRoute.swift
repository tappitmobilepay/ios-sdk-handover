//
//  CampaignsApiRoute.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 31/08/2021.
//  Copyright © 2021 JadePayments. All rights reserved.
//

import Foundation

enum CampaignsApiRoute: ApiRoute { case

    all

    var path: String {
        switch self {
        case .all: return "campaigns/all"
        }
    }

    func url(for environment: ApiEnvironment) -> String {
        return "\(environment.url)/\(path)"
    }
}
