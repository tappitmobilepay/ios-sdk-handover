//
//  CampaignTableViewCell.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 31/08/2021.
//  Copyright © 2021 JadePayments. All rights reserved.
//

import Foundation
import UIKit

class CampaignTableViewCell: UITableViewCell {
    @IBOutlet var statusButton: UIButton!
    @IBOutlet var containerView: UIView!
    @IBOutlet var concessionaireLabel: UILabel!
    @IBOutlet var expirationLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        containerView.addDefaultRoundCorners()
        concessionaireLabel.font = UIFont.with(size: .Large, weight: .bold)
        expirationLabel.font = UIFont.with(size: .LargeMedium, weight: .normal)
        statusButton.addRoundCorners(radius: Int(statusButton.bounds.size.height * 0.5))
        statusButton.titleLabel?.font = UIFont.with(size: .LargeMedium, weight: .campaignStatusButton)
        statusButton.addDefaultRoundCorners()
    }

    func setup(campaign: Campaign) {
        let redeemed = campaign.is_qualified
        let redeemedBackground = DevUtil.color(name: Colors.tmw_redeemed_campaign_card_background.rawValue)
        let redeemedText = DevUtil.color(name: Colors.tmw_redeemed_campaign_card_text.rawValue)
        let redeemedButtonBackground = DevUtil.color(name: Colors.tmw_redeemed_campaign_button_background.rawValue)
        let redeemedButtonText = DevUtil.color(name: Colors.tmw_redeemed_campaign_button_text.rawValue)
        let redeemedBackgroundBorderColor = DevUtil.color(name: Colors.tmw_redeemed_campaign_card_border.rawValue)
        let redeemedButtonBorderColor = DevUtil.color(name: Colors.tmw_redeemed_campaign_button_border.rawValue)

        let redeemableBackground = DevUtil.color(name: Colors.tmw_redeemable_campaign_card_background.rawValue)
        let redeemableText = DevUtil.color(name: Colors.tmw_redeemable_campaign_card_text.rawValue)
        let redeemableButtonBackground = DevUtil.color(name: Colors.tmw_redeemable_campaign_button_background.rawValue)
        let redeemableButtonText = DevUtil.color(name: Colors.tmw_redeemable_campaign_button_text.rawValue)
        let redeemableBackgroundBorderColor = DevUtil.color(name: Colors.tmw_redeemable_campaign_card_border.rawValue)
        let redeemableButtonBorderColor = DevUtil.color(name: Colors.tmw_redeemable_campaign_button_border.rawValue)

        concessionaireLabel.text = campaign.campaign_name
        expirationLabel.text = "Valid on: \(campaign.validOnString()) | From: \(campaign.validOnTimeString())"

        containerView.layer.borderWidth = 1
        statusButton.layer.borderWidth = 1

        if redeemed {
            statusButton.setTitle("REDEEMED", for: .normal)
            containerView.backgroundColor = redeemedBackground
            containerView.layer.borderColor = redeemedBackgroundBorderColor.cgColor
            statusButton.layer.borderColor = redeemedButtonBorderColor.cgColor
            concessionaireLabel.textColor = redeemedText
            expirationLabel.textColor = redeemedText
            statusButton.backgroundColor = redeemedButtonBackground
            statusButton.setTitleColor(redeemedButtonText, for: .normal)
        }
        else {
            statusButton.layer.borderColor = redeemableButtonBorderColor.cgColor
            statusButton.setTitle("MORE INFO", for: .normal)
            containerView.backgroundColor = redeemableBackground
            containerView.layer.borderColor = redeemableBackgroundBorderColor.cgColor
            concessionaireLabel.textColor = redeemableText
            expirationLabel.textColor = redeemableText
            statusButton.backgroundColor = redeemableButtonBackground
            statusButton.setTitleColor(redeemableButtonText, for: .normal)
        }

        if campaign.trigger_type == .sponsored {
            expirationLabel.text = "Sponsored"
        }
    }
}
