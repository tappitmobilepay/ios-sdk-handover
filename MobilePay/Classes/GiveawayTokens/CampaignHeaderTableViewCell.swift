//
//  CampaignHeaderTableViewCell.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 31/08/2021.
//  Copyright © 2021 JadePayments. All rights reserved.
//

import Foundation
import UIKit

class CampaignHeaderTableViewCell: UITableViewCell {
    @IBOutlet var infoLabel: UILabel!
    @IBOutlet var mainSwitch: UISwitch!
    @IBOutlet var switchLabel: UILabel!

    var switchChangedValue: ((_ isOn: Bool) -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        infoLabel.font = UIFont.with(size: .Large, weight: .normal)
        infoLabel.text = "Current Campaigns"

        switchLabel.font = UIFont.with(size: .Medium, weight: .normal)
        mainSwitch.onTintColor = DevUtil.color(name: Colors.tmw_primary_background.rawValue)
    }

    @IBAction func switchValueDidChange(_ sender: UISwitch) {
        switchChangedValue?(sender.isOn)
    }

    func hideSwitch() {
        switchLabel.isHidden = true
        mainSwitch.isHidden = true
    }
}
