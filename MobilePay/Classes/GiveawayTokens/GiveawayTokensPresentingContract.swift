//
//  GiveawayTokensPresentingContract.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 22/12/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation

protocol GiveawayTokensPresentingContract {
    var clientTimezoneString: String { get }

    func showRedeemedCampaigns() -> Bool

    func viewDidLoad()

    func viewDidAppear()

    func informationText() -> String

    func refreshData()

    func didTapSkip()

    func didTapContinue()

    func didSelectCampaignAt(position: Int)

    func didSelectTokenAt(position: Int)

    func didDeselectTokenAt(position: Int)

    func didChangeCampaignsListing(showRedeemed: Bool)

    func hasRedeemedCampaigns() -> Bool
}
