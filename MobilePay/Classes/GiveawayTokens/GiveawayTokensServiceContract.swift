//
//  GiveawayTokensServiceContract.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 23/12/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation

typealias GiveawayTokensResponse = (_ tokens: [GiveawayToken]?, _ error: Error?) -> Void

typealias BenefitsResponse = (_ benefits: Benefits?, _ error: Error?) -> Void

protocol GiveawayTokensServiceContract: CancelableService {
    func getGiveawayTokens(completion: @escaping GiveawayTokensResponse)
    func getBenefits(fanId: String, completion: @escaping BenefitsResponse)
}

struct Benefits: Codable {
    let tokens: [GiveawayToken]
    let complimentaryBalances: [ComplimentaryBalance]
}
