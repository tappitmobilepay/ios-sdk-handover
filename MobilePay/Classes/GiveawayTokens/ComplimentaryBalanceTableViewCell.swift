//
//  ComplimentaryBalanceTableViewCell.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 03/01/2021.
//  Copyright © 2021 JadePayments. All rights reserved.
//

import UIKit

enum ComplimentaryBalanceType: Int { case
    complimentary_balance = 0,
    bonus_credit = 1,
    gift_card = 2
}

class ComplimentaryBalanceTableViewCell: UITableViewCell {
    @IBOutlet var containerView: UIView!
    @IBOutlet var balanceValueLabel: UILabel!
    @IBOutlet var expiryLabel: UILabel!
    @IBOutlet var infoLabel: UILabel!
    @IBOutlet var concessionaireLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        containerView.addPrimaryColorBorder()
        concessionaireLabel.font = UIFont.with(size: .Large, weight: .normal)
        infoLabel.font = concessionaireLabel.font
        expiryLabel.font = UIFont.with(size: .LargeMedium, weight: .normal)
        balanceValueLabel.font = UIFont.with(size: .BigBig, weight: .bold)
    }

    func setup(complimentaryBalance: ComplimentaryBalance,
               currency: String,
               clientTimezoneString: String)
    {
        let currencySymbol = currency
        balanceValueLabel.text = currencySymbol + " " + complimentaryBalance.amount.double().format(f: ".2")
        concessionaireLabel.text = complimentaryBalance.concessionaires.joined(separator: ", ")

        let date = complimentaryBalance.expiryDate?.replacingOccurrences(of: "T", with: " ")
            .replacingOccurrences(of: "Z", with: "")
        let timezone = TimeZone(identifier: clientTimezoneString) ?? TimeZone(abbreviation: "UTC")!
        let dateString = date?.toDate(dateFormat: "yyyy-MM-dd HH:mm:ss")?.toString(timezone: timezone) ?? date

        expiryLabel.text = "Expires " + (dateString ?? "-")
    }
}
