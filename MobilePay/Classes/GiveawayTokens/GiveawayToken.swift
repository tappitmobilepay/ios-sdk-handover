//
//  GiveawayToken.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 22/12/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import CoreData
import Foundation

struct GiveawayToken: Codable {
    let id: Int
    let name: String
    let expiryDate: String?
}

struct GiveawayTokens: Codable {
    let tokens: [GiveawayToken]
}

extension GiveawayToken {
    static func from(managedGiveawayToken: ManagedGiveawayToken) -> GiveawayToken {
        return GiveawayToken(id: managedGiveawayToken.id!.intValue,
                             name: managedGiveawayToken.name!,
                             expiryDate: managedGiveawayToken.expiryDate)
    }

    static func toManagedGiveawayToken(token: GiveawayToken, context: NSManagedObjectContext) -> ManagedGiveawayToken {
        let managedToken = ManagedGiveawayToken(context: context)
        managedToken.name = token.name
        managedToken.id = NSDecimalNumber(integerLiteral: token.id)
        managedToken.expiryDate = token.expiryDate
        return managedToken
    }
}
