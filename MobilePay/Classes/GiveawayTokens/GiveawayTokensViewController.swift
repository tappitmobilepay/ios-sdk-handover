//
//  GiveawayTokensViewController.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 22/12/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation
import UIKit

class GiveawayTokensViewController: BaseScreenViewController, GiveawayTokensViewingContract {
    let giveawayTokensSection = 1
    let balancesSection = 0
    let campaignsSection = 2

    var presenter: GiveawayTokensPresentingContract!
    @IBOutlet var skipButton: UIButton!
    @IBOutlet var continueButton: UIButton!
    @IBOutlet var topInfoLabel: UILabel!
    @IBOutlet var tableView: UITableView!
    var giveawayTokens = [GiveawayToken]()
    var campaigns = [Campaign]()
    var complimentaryBalances = [ComplimentaryBalance]()
    var currency = ""
    var mode: GiveawayTokensScreenMode = .viewOnly

    private let refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()

        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }

        presenter.viewDidLoad()
    }

    func setup(mode: GiveawayTokensScreenMode) {
        self.mode = mode
        addDefaultTitleViewLogo()
        tableView.delegate = self
        tableView.dataSource = self

        topInfoLabel.font = UIFont.with(size: .Large, weight: .normal)
        topInfoLabel.text = presenter.informationText()

        tableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refreshStarted(_:)), for: .valueChanged)

        if mode == .select {
            continueButton.setTitle("CONTINUE", for: .normal)
            skipButton.setTitle("NO THANKS", for: .normal)
            skipButton.layer.borderWidth = 1
            skipButton.layer.borderColor = DevUtil.color(name: Colors.tmw_secondary_button_border.rawValue).cgColor
            skipButton.setTitleColor(DevUtil.color(name: Colors.tmw_secondary_button_text.rawValue), for: .normal)
            skipButton.backgroundColor = DevUtil.color(name: Colors.tmw_secondary_button_background.rawValue)
            skipButton.titleLabel?.font = UIFont.with(size: .LargeMedium, weight: .secondaryButton)
            skipButton.addDefaultRoundCorners()

            continueButton.setTitleColor(DevUtil.color(name: Colors.tmw_primary_button_text.rawValue), for: .normal)
            continueButton.layer.borderWidth = 1
            continueButton.layer.borderColor = DevUtil.color(name: Colors.tmw_primary_button_border.rawValue).cgColor
            continueButton.backgroundColor = DevUtil.color(name: Colors.tmw_primary_button_background.rawValue)
            continueButton.addDefaultRoundCorners()
            continueButton.titleLabel?.font = UIFont.with(size: .LargeMedium, weight: .primaryButton)
        }
        else {
            skipButton.removeFromSuperview()
            continueButton.removeFromSuperview()
            tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: 16.0).isActive = true
        }
    }

    func showLoading() {
        refreshControl.beginRefreshing()
    }

    func hideLoading() {
        refreshControl.endRefreshing()
    }

    func updateWith(tokens: [GiveawayToken],
                    balances: [ComplimentaryBalance],
                    campaigns: [Campaign],
                    currency: String)
    {
        topInfoLabel.text = presenter.informationText()
        giveawayTokens = tokens
        self.campaigns = campaigns
        complimentaryBalances = balances
        self.currency = currency
        tableView.reloadData()
    }

    @objc
    func refreshStarted(_ sender: Any) {
        presenter.refreshData()
    }

    override func viewDidAppear(_ animated: Bool) {
        // Analytics.analyticsPaymentMethodsPageVisitEvent()
        showBackButton()
        navigationController?.setNavigationBarHidden(false, animated: true)
        presenter.viewDidAppear()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        showBackButton()
        navigationController?.setNavigationBarHidden(false, animated: true)
    }

    @IBAction func didTapSkipButton(_ sender: UIButton) {
        presenter.didTapSkip()
    }

    @IBAction func didTapContinueButton(_ sender: UIButton) {
        presenter.didTapContinue()
    }
}

extension GiveawayTokensViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return mode == .viewOnly ? 3 : 2
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == balancesSection {
            return complimentaryBalances.count
        }
        else if section == campaignsSection {
            return campaigns.count
        }

        return giveawayTokens.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == balancesSection {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "\(ComplimentaryBalanceTableViewCell.self)") as? ComplimentaryBalanceTableViewCell else {
                return UITableViewCell()
            }

            cell.setup(complimentaryBalance: complimentaryBalances[indexPath.row],
                       currency: currency,
                       clientTimezoneString: presenter!.clientTimezoneString)
            return cell
        }
        else if indexPath.section == campaignsSection {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "\(CampaignTableViewCell.self)") as? CampaignTableViewCell else {
                return UITableViewCell()
            }

            cell.setup(campaign: campaigns[indexPath.row])
            return cell
        }
        else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "\(GiveawayTokensTableViewCell.self)") as? GiveawayTokensTableViewCell else {
                return UITableViewCell()
            }

            cell.setup(token: giveawayTokens[indexPath.row],
                       mode: mode,
                       clientTimezoneString: presenter!.clientTimezoneString)
            return cell
        }
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let defaultView = UIView(frame: CGRect(x: 0, y: 0, width: 1, height: 1))

        if section == campaignsSection {
            guard let header = tableView.dequeueReusableCell(withIdentifier: "\(CampaignHeaderTableViewCell.self)") as? CampaignHeaderTableViewCell else {
                return defaultView
            }

            header.mainSwitch.isOn = presenter!.showRedeemedCampaigns()

            if !presenter.hasRedeemedCampaigns() {
                header.hideSwitch()
            }
            header.switchChangedValue = { [weak self] isOn in
                self?.presenter?.didChangeCampaignsListing(showRedeemed: isOn)
            }

            return header
        }

        return defaultView
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == campaignsSection {
            return UITableView.automaticDimension
        }

        return 0
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == giveawayTokensSection {
            presenter.didSelectTokenAt(position: indexPath.row)
        }
        else if indexPath.section == campaignsSection {
            tableView.deselectRow(at: indexPath, animated: false)
            presenter.didSelectCampaignAt(position: indexPath.row)
        }
    }

    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if indexPath.section == giveawayTokensSection {
            presenter.didDeselectTokenAt(position: indexPath.row)
        }
    }
}
