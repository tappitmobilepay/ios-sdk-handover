//
//  CampaignDetails.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 31/08/2021.
//  Copyright © 2021 JadePayments. All rights reserved.
//

import Foundation
import UIKit

class CampaignDetails: UIViewController {
    var db: DatabaseManagementContract!

    var clientTimezoneString: String?

    var campaign: Campaign? {
        didSet {
            setupView()
        }
    }

    @IBOutlet var containerView: UIView!
    @IBOutlet var mainLabel: UILabel!
    @IBOutlet var subtitleTextView: UITextView!
    @IBOutlet var closeButton: UIButton!

    @IBOutlet var benefitsTitleLabel: UILabel!
    @IBOutlet var benefitsValueLabel: UILabel!
    @IBOutlet var startsTitleLabel: UILabel!
    @IBOutlet var startsValueLabel: UILabel!
    @IBOutlet var validForTitleLabel: UILabel!
    @IBOutlet var validForValueLabel: UILabel!

    @IBOutlet var howTitleLabel: UILabel!
    @IBOutlet var howValueLabel: UILabel!
    @IBOutlet var endsTitleLabel: UILabel!
    @IBOutlet var endsValueLabel: UILabel!
    @IBOutlet var periodTitleLabel: UILabel!
    @IBOutlet var persiodValueLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }

        containerView.addDefaultRoundCorners()

        setupView()
    }

    func setupView() {
        if isViewLoaded, let campaign = campaign {
            if #available(iOS 13.0, *) {
                overrideUserInterfaceStyle = .light
            }

            let redeemed = campaign.is_qualified
            let redeemedBackground = DevUtil.color(name: Colors.tmw_redeemed_campaign_card_background.rawValue)

            let redeemableBackground = DevUtil.color(name: Colors.tmw_redeemable_campaign_card_background.rawValue)

            let finalTextHighlightColor = redeemed ? redeemedBackground : redeemableBackground

            mainLabel.textColor = finalTextHighlightColor
            mainLabel.font = UIFont.with(size: .LargeMedium, weight: .normal)
            closeButton.setImage(DevUtil.imageSkin(name: "tmw_close"), for: .normal)
            closeButton.tintColor = finalTextHighlightColor
            subtitleTextView.font = UIFont.with(size: .Medium, weight: .normal)

            benefitsTitleLabel.textColor = finalTextHighlightColor
            benefitsTitleLabel.font = UIFont.with(size: .Medium, weight: .normal)
            benefitsValueLabel.font = UIFont.with(size: .Medium, weight: .normal)
            startsTitleLabel.textColor = finalTextHighlightColor
            startsTitleLabel.font = UIFont.with(size: .Medium, weight: .normal)
            startsValueLabel.font = UIFont.with(size: .Medium, weight: .normal)
            validForTitleLabel.textColor = finalTextHighlightColor
            validForTitleLabel.font = UIFont.with(size: .Medium, weight: .normal)
            validForValueLabel.font = UIFont.with(size: .Medium, weight: .normal)

            howTitleLabel.textColor = finalTextHighlightColor
            howTitleLabel.font = UIFont.with(size: .Medium, weight: .normal)
            howValueLabel.font = UIFont.with(size: .Medium, weight: .normal)
            endsTitleLabel.textColor = finalTextHighlightColor
            endsTitleLabel.font = UIFont.with(size: .Medium, weight: .normal)
            endsValueLabel.font = UIFont.with(size: .Medium, weight: .normal)
            periodTitleLabel.textColor = finalTextHighlightColor
            periodTitleLabel.font = UIFont.with(size: .Medium, weight: .normal)
            persiodValueLabel.font = UIFont.with(size: .Medium, weight: .normal)

            benefitsTitleLabel.text = "\u{2022} Benefits"
            startsTitleLabel.text = "\u{2022} Starts"
            validForTitleLabel.text = "\u{2022} Valid For"

            howTitleLabel.text = "\u{2022} How"
            endsTitleLabel.text = "\u{2022} Ends"
            periodTitleLabel.text = "\u{2022} Period"

            let currency = db.clientConfig()?.clientCurrency.symbol ?? ""

            var timezone = TimeZone(abbreviation: "UTC")!
            if let clientTZString = clientTimezoneString,
               let clientTZ = TimeZone(identifier: clientTZString)
            {
                timezone = clientTZ
            }

            mainLabel.text = campaign.campaign_name
            subtitleTextView.attributedText = campaign.campaign_description.html2AttributedString
            howValueLabel.text = campaign.howString(currency: currency)
            benefitsValueLabel.text = campaign.benefitsString(currency: currency)
            persiodValueLabel.text = campaign.persiondString()
            validForValueLabel.text = campaign.validForDetailsString()
            let starts = campaign.startsString(clientTimezone: timezone)
            startsValueLabel.text = starts
            let ends = campaign.endsString(clientTimezone: timezone)
            endsValueLabel.text = ends

            if campaign.trigger_type == .sponsored {
                startsTitleLabel.removeFromSuperview()
                startsValueLabel.removeFromSuperview()
                validForTitleLabel.removeFromSuperview()
                validForValueLabel.removeFromSuperview()
                endsTitleLabel.removeFromSuperview()
                endsValueLabel.removeFromSuperview()
                periodTitleLabel.removeFromSuperview()
                persiodValueLabel.removeFromSuperview()

                benefitsTitleLabel.text = "\u{2022} Starts"
                howTitleLabel.text = "\u{2022} Ends"

                benefitsValueLabel.text = starts
                howValueLabel.text = ends
            }
        }
    }

    @IBAction func didTapCloseButton(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}
