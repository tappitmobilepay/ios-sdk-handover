//
//  GiveawayTokensViewingContract.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 22/12/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation

protocol GiveawayTokensViewingContract: AnyObject {
    func setup(mode: GiveawayTokensScreenMode)

    func updateWith(tokens: [GiveawayToken],
                    balances: [ComplimentaryBalance],
                    campaigns: [Campaign],
                    currency: String)

    func showLoading()

    func hideLoading()
}
