//
//  GiveawayTokensTableViewCell.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 22/12/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import UIKit

class GiveawayTokensTableViewCell: UITableViewCell {
    @IBOutlet var statusButton: UIButton!
    @IBOutlet var containerView: UIView!
    @IBOutlet var concessionaireLabel: UILabel!
    @IBOutlet var expirationLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        containerView.addPrimaryColorBorder()
        concessionaireLabel.font = UIFont.with(size: .Large, weight: .normal)
        expirationLabel.font = UIFont.with(size: .LargeMedium, weight: .normal)
        statusButton.setTitle("ACTIVE", for: .normal)
        statusButton.setTitleColor(.white, for: .normal)
        statusButton.addRoundCorners(radius: Int(statusButton.bounds.size.height * 0.5))
        statusButton.titleLabel?.font = UIFont.with(size: .LargeMedium, weight: .benefitStatusButton)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        statusButton.layer.borderWidth = 1

        if selected {
            statusButton.layer.borderColor = DevUtil.color(name: Colors.tmw_selected_benefit_button_border.rawValue).cgColor
            statusButton.backgroundColor = DevUtil.color(name: Colors.tmw_selected_benefit_button_background.rawValue)
            statusButton.setTitleColor(DevUtil.color(name: Colors.tmw_selected_benefit_button_text.rawValue), for: .normal)
        }
        else {
            statusButton.layer.borderColor = DevUtil.color(name: Colors.tmw_unselected_benefit_button_border.rawValue).cgColor
            statusButton.backgroundColor = DevUtil.color(name: Colors.tmw_unselected_benefit_button_background.rawValue)
            statusButton.setTitleColor(DevUtil.color(name: Colors.tmw_unselected_benefit_button_text.rawValue), for: .normal)
        }
    }

    func setup(token: GiveawayToken,
               mode: GiveawayTokensScreenMode,
               clientTimezoneString: String)
    {
        if mode == .viewOnly {
            statusButton.removeFromSuperview()
        }

        if let expiryDate = token.expiryDate {
            let date = expiryDate.replacingOccurrences(of: "T", with: " ")
                .replacingOccurrences(of: "Z", with: "")
            let timezone = TimeZone(identifier: clientTimezoneString) ?? TimeZone(abbreviation: "UTC")!
            let dateString = date.toDate(dateFormat: "yyyy-MM-dd HH:mm:ss")?.toString(timezone: timezone) ?? date

            expirationLabel.text = "Expires " + dateString
        }
        else {
            expirationLabel.text = "Expires -"
        }

        concessionaireLabel.text = token.name
    }
}
