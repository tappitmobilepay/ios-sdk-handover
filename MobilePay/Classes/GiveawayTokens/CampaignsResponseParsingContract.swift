//
//  CampaignsResponseParsingContract.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 31/08/2021.
//  Copyright © 2021 JadePayments. All rights reserved.
//

import Foundation

protocol CampaignsResponseParsingContract {
    func campaignsResponse(from data: Data?) throws -> [Campaign]
}
