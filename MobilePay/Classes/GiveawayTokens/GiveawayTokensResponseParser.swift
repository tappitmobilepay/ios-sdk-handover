//
//  GiveawayTokensResponseParser.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 03/01/2021.
//  Copyright © 2021 JadePayments. All rights reserved.
//

import Foundation

class GiveawayTokensResponseParser: GiveawayTokensResponseParsingContract {
    func benefitsResponse(from data: Data?) throws -> Benefits {
        let decoder = JSONDecoder()

        if let d = data, let benefits = try? decoder.decode(Benefits.self, from: d) {
            return benefits
        }
        else {
            throw invalidDataError
        }
    }

    func giveawayTokensResponse(from data: Data?) throws -> [GiveawayToken] {
        let decoder = JSONDecoder()

        if let d = data, let tokens = try? decoder.decode(GiveawayTokens.self, from: d) {
            return tokens.tokens
        }
        else {
            throw invalidDataError
        }
    }
}
