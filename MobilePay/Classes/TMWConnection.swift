//
//  TMWConnection.swift
//  tappitVisitorApp
//
//  Created by Pandi on 17/01/17.
//  Copyright © 2017 JadePayments. All rights reserved.
//

import UIKit

protocol ConnectionDelegate: AnyObject {
    func didReceiveResponse(_ response: Data)
    func didFailToReceiveResponse(_ errorString: String, code: Int, customErrorCode: Int?)
}

class TMWConnection: NSObject, URLSessionDelegate {
    weak var delegate: ConnectionDelegate?
    var connectionUrl: URLSession!
    var Url: URL!

    func getServerData(_ url: URL, urlRequest: URLRequest, delegate: AnyObject) {
        //        let defaultConfigObject = NSURLSessionConfiguration . defaultSessionConfiguration ()
        //        let defaultSession =  NSURLSession(configuration:defaultConfigObject, delegate: self, delegateQueue: NSOperationQueue.mainQueue())
        //        let downloadTask = defaultSession.dataTaskWithRequest(loginRequest)
        //        downloadTask.resume()
        Url = url
        self.delegate = delegate as? ConnectionDelegate
        let session = URLSession(configuration: URLSessionConfiguration.ephemeral,
                                 delegate: self,
                                 delegateQueue: nil)
        let task = session.dataTask(with: urlRequest, completionHandler: { data, response, error -> Void in

            if let e = error {
                DispatchQueue.main.async {
                    TMWLoaderView.shareInstance().hideLoader()

                    self.delegate?.didFailToReceiveResponse((e as NSError).localizedDescription, code: (e as NSError).code, customErrorCode: nil)
                }
            }
            else if let value = data {
                if self.httpStatusOK(response: response!) {
                    DispatchQueue.main.async {
                        self.delegate?.didReceiveResponse(value)
                    }
                }
                else {
                    DispatchQueue.main.async {
                        TMWLoaderView.shareInstance().hideLoader()

                        if let info = self.dictionarywith(data: value), let error = info["error"] as? [AnyHashable: Any] {
                            self.operationFailed(result: error, urlResponse: response!)
                        }
                    }
                }
            }
        })
        task.resume()
    }

    private func showError(error: Error?) {
        let alert = UIAlertController(title: "Alert", message: "\(error?.localizedDescription ?? "")", preferredStyle: UIAlertController.Style.alert)

        if #available(iOS 13.0, *) {
            alert.overrideUserInterfaceStyle = .light
        }

        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))

        var rootViewController = UIApplication.shared.keyWindow?.rootViewController

        if let navigationController = rootViewController as? UINavigationController {
            rootViewController = navigationController.viewControllers.first
        }

        if let tabBarController = rootViewController as? UITabBarController {
            rootViewController = tabBarController.selectedViewController
        }

        rootViewController?.present(alert, animated: true, completion: nil)
    }

    private func operationFailed(result: [AnyHashable: Any]?, urlResponse: URLResponse) {
        let httpResponse = (urlResponse as! HTTPURLResponse)

        let errCode = httpResponse.statusCode

        var userInfo = [String: Any]()

        var customCode: Int?

        if let code = result?["code"] as? Int {
            userInfo["code"] = code
            customCode = code
        }

        if let message = result?["message"] {
            userInfo[NSLocalizedDescriptionKey] = message
        }

        let error = NSError(domain: "MobilePay", code: errCode, userInfo: userInfo)

        delegate?.didFailToReceiveResponse((error as NSError).localizedDescription, code: (error as NSError).code, customErrorCode: customCode)
//        self.showError(error: error)
    }

    private func dictionarywith(data: Data) -> [AnyHashable: Any]? {
        let jsonResponse = [AnyHashable: Any]()

        if let json = ((try? JSONSerialization.jsonObject(with: data, options: []) as? [AnyHashable: Any]) as [AnyHashable: Any]??) {
            return json
        }
        return jsonResponse
    }

    private func httpStatusOK(response: URLResponse) -> Bool {
        let httpResponse = (response as! HTTPURLResponse)

        var status: Bool = false

        switch httpResponse.statusCode {
        case 200, 201, 202, 204, 206:
            status = true
        default:
            status = false
        }

        return status
    }

    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Swift.Void) {
        onlyDebugPrint("URLSession1  ------> ")
        if challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust {
            if let serverTrust = challenge.protectionSpace.serverTrust {
                var secresult = SecTrustResultType.invalid
                let status = SecTrustEvaluate(serverTrust, &secresult)
                if errSecSuccess == status {
                    if let serverCertificate = SecTrustGetCertificateAtIndex(serverTrust, 0) {
                        let serverCertificateData = SecCertificateCopyData(serverCertificate)
                        let data = CFDataGetBytePtr(serverCertificateData)
                        let size = CFDataGetLength(serverCertificateData)
                        let cert1 = NSData(bytes: data, length: size)
                        let file_der = Bundle.main.path(forResource: "tappit", ofType: "crt")
                        onlyDebugPrint("URLSession2  ------> ")
                        completionHandler(URLSession.AuthChallengeDisposition.useCredential, URLCredential(trust: serverTrust))
                        return
                            /* if let file = file_der {
                                    if let cert2 = NSData(contentsOfFile: file) {
                                        onlyDebugPrint("URLSession3  ------> ")
                                        //if cert1.isEqual(to: cert2 as Data) {
                                            completionHandler(URLSession.AuthChallengeDisposition.useCredential, URLCredential(trust:serverTrust))
                                            return
                                        //}
                                    }
                                } */
                    }
                }
            }
        }
        // Pinning failed
        // completionHandler(URLSession.AuthChallengeDisposition.cancelAuthenticationChallenge, nil)
    }
}
