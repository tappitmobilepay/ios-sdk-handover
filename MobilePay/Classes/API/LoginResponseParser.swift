//
//  LoginResponseParser.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 03/01/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation

class LoginResponseParser: LoginResponseParsingContract {
    func loginResponse(from data: Data?) throws -> LoginResponse {
        let decoder = JSONDecoder()

        if let d = data, let registrationResponse = try? decoder.decode(LoginResponse.self, from: d) {
            return registrationResponse
        }

        else {
            throw invalidDataError
        }
    }

    func clientConfigResponse(from data: Data?) throws -> ClientConfig {
        let decoder = JSONDecoder()

        if let d = data, let response = try? decoder.decode(ClientConfig.self, from: d) {
            return response
        }

        else {
            throw invalidDataError
        }
    }

    func generalErrorResponse(from data: Data?) throws {}
}
