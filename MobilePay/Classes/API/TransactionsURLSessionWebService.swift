//
//  TransactionsURLSessionWebService.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 30/12/2019.
//  Copyright © 2019 JadePayments. All rights reserved.
//

import Foundation

class TransactionsURLSessionWebService: NSURLSessionWebService, TransactionsWebServiceContract {
    func requestTransactionsHistory(page: Int, elementsPerPage: Int, fromDate: Date?, toDate: Date?, completion: @escaping TransactionsHistoryResult) {
        guard let responseParser = responseParser else {
            assert(false)
            return
        }

        var params: [String: Any] = [
            "page": page,
            "pageRecordCount": elementsPerPage
        ]

        if let startDate = fromDate, let endDate = toDate {
            let dateEncoder = DateFormatter()
            dateEncoder.dateFormat = "yyyy-MM-dd'T'HH:mm'Z'"
            params["startDate"] = dateEncoder.string(from: startDate)
            params["endDate"] = dateEncoder.string(from: endDate)
        }

        task = post(at: TransactionsApiRoute.transactionsHistory, params: params, completion: { data, error in
            if error == nil {
                do {
                    let transactionsList = try responseParser.transactionsHistoryResponse(from: data)
                    DispatchQueue.main.async {
                        completion(transactionsList, nil)
                    }
                }
                catch {
                    DispatchQueue.main.async {
                        completion(nil, error)
                    }
                }
            }
            else {
                DispatchQueue.main.async {
                    completion(nil, error)
                }
            }
        })
    }

    var responseParser: TransactionsResponseParsingContract?

    func cancel() {
        task?.cancel()
        task = nil
    }

    var task: URLSessionDataTask?
}
