//
//  LoginResponse.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 03/01/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation

struct LoginResponse: Codable {
    var token: String
    var hashId: Int
    var forceResetAuth: Bool
}
