//
//  LoginURLSessionWebService.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 03/01/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation
import UIKit

class LoginURLSessionWebService: NSURLSessionWebService, LoginWebServiceContract {
    func logout(username: String, completion: @escaping GenericResponse) {
        guard let _ = responseParser,
              let uuid = UIDevice.current.identifierForVendor?.uuidString
        else {
            fatalError("Elements not set")
        }

        let params: [String: Any] = [
            "username": username,
            "deviceId": "\(uuid)"
        ]

        task = post(at: OnboardingApiRoute.logout, params: params, completion: { [weak self] data, error in
            DispatchQueue.main.async {
                if error == nil {
                    do {
                        try self?.responseParser?.generalErrorResponse(from: data)
                        DispatchQueue.main.async {
                            completion(nil)
                        }
                    }
                    catch {
                        DispatchQueue.main.async {
                            completion(error)
                        }
                    }
                }
                else {
                    DispatchQueue.main.async {
                        completion(error)
                    }
                }
            }
        })
    }

    func login(username: String, password: String, completion: @escaping LoginResult) {
        guard let responseParser = responseParser,
              let uuid = UIDevice.current.identifierForVendor?.uuidString
        else {
            fatalError("Elements not set")
        }

        let params: [String: Any] = [
            "username": username,
            "password": password,
            "IsUsernamePasswordAuthentication": true,
            "deviceId": "\(uuid)",
            "platform": "iOS"
        ]

        task = post(at: OnboardingApiRoute.login, params: params, completion: { data, error in
            if error == nil {
                do {
                    let loginResponse = try responseParser.loginResponse(from: data)
                    DispatchQueue.main.async {
                        completion(loginResponse, nil)
                    }
                }
                catch {
                    DispatchQueue.main.async {
                        completion(nil, error)
                    }
                }
            }
            else {
                DispatchQueue.main.async {
                    completion(nil, error)
                }
            }
        })
    }

    func sso(request: LoginRequest, completion: @escaping LoginResult) {
        guard let responseParser = responseParser else {
            fatalError("Elements not set")
        }

        task = post(at: OnboardingApiRoute.login, params: request.dictionary!, completion: { data, error in
            if error == nil {
                do {
                    let loginResponse = try responseParser.loginResponse(from: data)
                    DispatchQueue.main.async {
                        completion(loginResponse, nil)
                    }
                }
                catch {
                    DispatchQueue.main.async {
                        completion(nil, error)
                    }
                }
            }
            else {
                DispatchQueue.main.async {
                    completion(nil, error)
                }
            }
        })
    }

    func resetPassword(username: String, password: String, otp: String, completion: @escaping ResetPasswordResult) {
        let params: [String: Any] = [
            "password": password,
            "email": username,
            "otp": otp
        ]
        task = post(at: OnboardingApiRoute.resetPassword, params: params, completion: { [weak self] data, error in
            DispatchQueue.main.async {
                if error == nil {
                    do {
                        try self?.responseParser?.generalErrorResponse(from: data)
                        DispatchQueue.main.async {
                            completion(nil)
                        }
                    }
                    catch {
                        DispatchQueue.main.async {
                            completion(error)
                        }
                    }
                }
                else {
                    DispatchQueue.main.async {
                        completion(error)
                    }
                }
            }
        })
    }

    func forgotPin(resetPinRequest: ResetPinRequest, completion: @escaping GenericResponse) {
        guard let params = resetPinRequest.dictionary else {
            assert(false)
            completion(NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Failed to parse request parameters"]))
            return
        }

        task = post(at: OnboardingApiRoute.resetAuth, params: params, completion: { [weak self] data, error in
            DispatchQueue.main.async {
                if error == nil {
                    do {
                        try self?.responseParser?.generalErrorResponse(from: data)
                        DispatchQueue.main.async {
                            completion(nil)
                        }
                    }
                    catch {
                        DispatchQueue.main.async {
                            completion(error)
                        }
                    }
                }
                else {
                    DispatchQueue.main.async {
                        completion(error)
                    }
                }
            }
        })
    }

    func informServerPinWasReset(resetPinRequest: ResetPinRequest, completion: @escaping GenericResponse) {
        guard let params = resetPinRequest.dictionary else {
            assert(false)
            completion(NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Failed to parse request parameters"]))
            return
        }

        task = post(at: OnboardingApiRoute.completeResetAuth, params: params, completion: { [weak self] data, error in
            DispatchQueue.main.async {
                if error == nil {
                    do {
                        try self?.responseParser?.generalErrorResponse(from: data)
                        DispatchQueue.main.async {
                            completion(nil)
                        }
                    }
                    catch {
                        DispatchQueue.main.async {
                            completion(error)
                        }
                    }
                }
                else {
                    DispatchQueue.main.async {
                        completion(error)
                    }
                }
            }
        })
    }

    var responseParser: LoginResponseParsingContract?

    func cancel() {
        task?.cancel()
        task = nil
    }

    var task: URLSessionDataTask?
}
