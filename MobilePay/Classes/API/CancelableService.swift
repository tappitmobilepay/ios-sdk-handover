//
//  CancelableService.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 09/12/2019.
//  Copyright © 2019 JadePayments. All rights reserved.
//

import Foundation

protocol CancelableService {
    func cancel()
}
