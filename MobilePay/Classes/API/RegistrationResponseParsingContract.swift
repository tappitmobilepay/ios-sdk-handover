//
//  RegistrationResponseParsingContract.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 11/12/2019.
//  Copyright © 2019 JadePayments. All rights reserved.
//

import Foundation

protocol RegistrationResponseParsingContract {
    init(errorParser: ServerErrorParsingContract)
    func registrationResponse(from data: Data?) throws -> RegistrationResponse
    func otpSucceeded(from data: Data?) throws
    func changePasswordSucceeded(from data: Data?) throws
}
