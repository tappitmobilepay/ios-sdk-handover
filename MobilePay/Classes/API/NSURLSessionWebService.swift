//
//  NSURLSessionWebService.swift
//  CityInfo
//
//  Created by Marius Kurgonas on 05/09/2018.
//  Copyright © 2018 Marius Kurgonas. All rights reserved.
//

import Foundation

enum HTTPMethod: String {
    case POST, GET, UPDATE, DELETE, PATCH, PUT
}

enum URLSessionDataEncodingMethod {
    case json, query
}

class NSURLSessionWebService {
    init(context: ApiContext,
         session: URLSession,
         apiCredentialsProvider: CredentialsStoringContract,
         db: DatabaseManager)
    {
        self.context = context
        self.session = session
        credStore = apiCredentialsProvider
        self.db = db
    }

    let context: ApiContext
    let session: URLSession
    let credStore: CredentialsStoringContract
    let db: DatabaseManager
    let logger = Logger()
    let serverErrorParser: ServerErrorParsingContract = ServerErrorParser()

    func get(at route: ApiRoute, encoding: URLSessionDataEncodingMethod = .json, completion: @escaping (Data?, Error?) -> Void) -> URLSessionDataTask? {
        return request(at: route,
                       method: .GET,
                       encoding: encoding,
                       params: nil,
                       completion: completion)
    }

    func delete(at route: ApiRoute, params: Any, encoding: URLSessionDataEncodingMethod = .json, completion: @escaping (Data?, Error?) -> Void) -> URLSessionDataTask? {
        return request(at: route,
                       method: .DELETE,
                       encoding: encoding,
                       params: params,
                       completion: completion)
    }

    func post(at route: ApiRoute, params: [String: Any], encoding: URLSessionDataEncodingMethod = .json, completion: @escaping (Data?, Error?) -> Void) -> URLSessionDataTask? {
        return request(at: route,
                       method: .POST,
                       encoding: encoding,
                       params: params,
                       completion: completion)
    }

    func put(at route: ApiRoute, params: Any, encoding: URLSessionDataEncodingMethod = .json, completion: @escaping (Data?, Error?) -> Void) -> URLSessionDataTask? {
        return request(at: route,
                       method: .PUT,
                       encoding: encoding,
                       params: params,
                       completion: completion)
    }

    private func request(at route: ApiRoute, method: HTTPMethod, encoding: URLSessionDataEncodingMethod = .json, params: Any?, completion: @escaping (Data?, Error?) -> Void) -> URLSessionDataTask? {
        var urlString = route.url(for: context.environment)
        if route.path == OnboardingApiRoute.login.path {
            let clientConfig = db.clientConfig()
            if let loginOverridePath = clientConfig?.configuration.settings.loginEndpointOverride {
                urlString = OverridenEndpointTypes.url(for: context.environment, endpoint: loginOverridePath)
            }
        }
        else if route.path == UserApiRoute.giveawayTokens.path {
            let clientConfig = db.clientConfig()
            if let tokensOverride = clientConfig?.configuration.settings.tokensEndpointOverride {
                urlString = OverridenEndpointTypes.url(for: context.environment, endpoint: tokensOverride)
            }
        }
        else if route.path == UserApiRoute.balance.path {
            let clientConfig = db.clientConfig()
            if let balanceOverride = clientConfig?.configuration.settings.balanceEndpointOverride {
                urlString = OverridenEndpointTypes.url(for: context.environment, endpoint: balanceOverride)
            }
        }

        var bodyData: Data?

        if encoding == .json, let p = params {
            guard let json = try? JSONSerialization.data(withJSONObject: p, options: JSONSerialization.WritingOptions.sortedKeys) else {
                assert(false)
                return nil
            }

            bodyData = json
        }
        else if encoding == .query, let params = params {
            guard let json = try? JSONSerialization.data(withJSONObject: params, options: JSONSerialization.WritingOptions.sortedKeys) else {
                assert(false)
                return nil
            }

            let queryString = "data=\(json.base64EncodedString())"
            bodyData = queryString.data(using: .utf8)
        }

        guard let url = URL(string: urlString) else {
            assert(false)
            return nil
        }

        let urlRequest = NSMutableURLRequest(url: url)

        for item in headers() {
            urlRequest.setValue(item.value, forHTTPHeaderField: item.key)
        }

        var encodingString = "application/json"
        if encoding == .query {
            encodingString = "application/x-www-form-urlencoded"
        }

        urlRequest.setValue(encodingString, forHTTPHeaderField: "Content-Type")
        urlRequest.httpBody = bodyData
        urlRequest.httpMethod = method.rawValue

        logger.log(request: urlRequest as URLRequest)

        let task = session.dataTask(with: urlRequest as URLRequest) { [weak self] data, response, error in

            self?.logger.log(data: data, response: response as? HTTPURLResponse, error: error)

            if let e = error as NSError?, e.code == NSURLErrorCancelled {
                return
            }

            var error = error
            if let response = response as? HTTPURLResponse, response.statusCode >= 400 {
                var message = "Request failed. Please try again"
                if let serverError = try? self?.serverErrorParser.parseError(from: data) {
                    message = serverError.localizedDescription
                }
                else if let data = data, let string = String(data: data, encoding: .utf8) {
                    message = string
                }

                error = NSError(domain: "Unacceptable HTTP response", code: response.statusCode, userInfo: [NSLocalizedDescriptionKey: message])
            }

            completion(data, error)
        }
        task.resume()
        return task
    }

    func headers() -> [String: String] {
        var headers = [String: String]()

        if let token = credStore.bearerToken {
            headers["Authorization"] = "Bearer \(token)"
        }

        return headers
    }
}
