//
//  UserResponseParser.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 17/12/2019.
//  Copyright © 2019 JadePayments. All rights reserved.
//

import Foundation

class UserResponseParser: UserResponseParsingContract {
    func balanceResponse(from data: Data?) throws -> Balance {
        let decoder = JSONDecoder()

        if let d = data, var balance = try? decoder.decode(Balance.self, from: d) {
            return balance
        }

        else {
            throw invalidDataError
        }
    }
}
