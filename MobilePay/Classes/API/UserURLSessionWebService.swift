//
//  UserURLSessionWebService.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 17/12/2019.
//  Copyright © 2019 JadePayments. All rights reserved.
//

import Foundation

class UserURLSessionWebService: NSURLSessionWebService, UserWebServiceContract {
    var responseParser: UserResponseParsingContract?

    func requestBalance(completion: @escaping BalanceResult) {
        guard let responseParser = responseParser else {
            assert(false)
            return
        }

        task = get(at: UserApiRoute.balance, completion: { data, error in
            if error == nil {
                do {
                    let balance = try responseParser.balanceResponse(from: data)
                    DispatchQueue.main.async {
                        completion(balance, nil)
                    }
                }
                catch {
                    DispatchQueue.main.async {
                        completion(nil, error)
                    }
                }
            }
            else {
                DispatchQueue.main.async {
                    completion(nil, error)
                }
            }
        })
    }

    func cancel() {
        task?.cancel()
        task = nil
    }

    var task: URLSessionDataTask?
}
