//
//  UserResponseParsingContract.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 17/12/2019.
//  Copyright © 2019 JadePayments. All rights reserved.
//

import Foundation

protocol UserResponseParsingContract {
    func balanceResponse(from data: Data?) throws -> Balance
}
