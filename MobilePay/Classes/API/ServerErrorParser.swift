//
//  ServerErrorParser.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 11/12/2019.
//  Copyright © 2019 JadePayments. All rights reserved.
//

import Foundation

class ServerErrorParser: ServerErrorParsingContract {
    func parseError(from data: Data?) throws -> Error? {
        let decoder = JSONDecoder()

        if let d = data, let message = try? decoder.decode(ServerErrorResponse.self, from: d) {
            return NSError(domain: "Server message", code: 0, userInfo: [NSLocalizedDescriptionKey: message.message])
        }

        return nil
    }
}
