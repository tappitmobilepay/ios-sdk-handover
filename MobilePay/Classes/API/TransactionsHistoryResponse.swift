//
//  TransactionsHistoryResponse.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 30/12/2019.
//  Copyright © 2019 JadePayments. All rights reserved.
//

import Foundation

struct TransactionsHistoryResponse: Codable {
    var transactionsList: [Transaction]

    enum CodingKeys: String, CodingKey {
        case transactionsList = "list"
    }
}

struct Transaction: Codable {
    var clientTransactionId: String
    var type: Decimal
    var amount: Decimal
    var date: String
    var currency: String
    var event: String
    var concessionaire: String
    var items: [TransactionItem]
    var tip: Decimal
    var complimentaryBalanceUsage: [ComplimentaryBalanceUsage]
    var chargedAmount: Decimal
    var tokenAmount: Decimal
    var donationAmount: Decimal
    var discountAmount: Decimal
    var taxAmount: Decimal
}

struct ComplimentaryBalanceUsage: Codable {
    var charged: Decimal
    let concessionaires: [String]
    let amount: Decimal
    let expiryDate: String?
}

struct TransactionItem: Codable {
    var unitPrice: Decimal
    var sku: String
    var name: String
    var quantity: Decimal
    var type: String
    var amount: Decimal?
}
