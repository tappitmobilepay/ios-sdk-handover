//
//  LoginResponseParsingContract.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 03/01/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation

protocol LoginResponseParsingContract {
    func loginResponse(from data: Data?) throws -> LoginResponse
    func generalErrorResponse(from data: Data?) throws
}
