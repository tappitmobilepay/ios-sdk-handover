//
//  ServerError.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 12/12/2019.
//  Copyright © 2019 JadePayments. All rights reserved.
//

import Foundation

struct ServerErrorResponse: Codable {
    var message: String
}
