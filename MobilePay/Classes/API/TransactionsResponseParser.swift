//
//  TransactionsResponseParser.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 30/12/2019.
//  Copyright © 2019 JadePayments. All rights reserved.
//

import Foundation

class TransactionsResponseParser: TransactionsResponseParsingContract {
    func transactionsHistoryResponse(from data: Data?) throws -> TransactionsHistoryResponse {
        let decoder = JSONDecoder()

        if let d = data {
            do {
                let asdf = try JSONDecoder().decode(TransactionsHistoryResponse.self, from: d)
                return asdf
            }
            catch {
                throw invalidDataError
            }
        }

        else {
            throw invalidDataError
        }
    }
}
