//
//  OnboardingApiRoute.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 09/12/2019.
//  Copyright © 2019 JadePayments. All rights reserved.
//

import Foundation

enum OnboardingApiRoute: ApiRoute { case

    getOTP,
    register,
    login,
    resetPassword,
    changePassword,
    logout,
    config(integrationKey: String),
    resetAuth,
    completeResetAuth

    var path: String {
        switch self {
        case .getOTP: return "customer/otp"
        case .register: return "customer/register"
        case .login: return "v3/authentication/login"
        case .resetPassword: return "customer/resetPassword"
        case .changePassword: return "customer/changePassword"
        case .logout: return "authentication/logout"
        case .config(let integrationKey): return "paymentapp/getAppConfig/\(integrationKey)"
        case .resetAuth: return "customer/resetPin"
        case .completeResetAuth: return "customer/completeResetPin"
        }
    }

    func url(for environment: ApiEnvironment) -> String {
        return "\(environment.url)/\(path)"
    }
}
