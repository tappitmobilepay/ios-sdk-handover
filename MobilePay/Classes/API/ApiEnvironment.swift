//
//  ApiEnvironment.swift
//  CityInfo
//
//  Created by Marius Kurgonas on 05/09/2018.
//  Copyright © 2018 Marius Kurgonas. All rights reserved.
//

import Foundation

enum ApiEnvironment: String { case
    sandbox,
    sandboxPayments,
    sandboxGiftcards,
    sandboxCampaigns,
    test,
    testPayments,
    testGiftcards,
    testCampaigns,
    demo,
    demoPayments,
    demoGiftcards,
    demoCampaigns,
    production,
    productionPayments,
    productionGiftcards,
    productionCampaigns,
    productionAnalytics,
    stagingAnalytics,
    staging,
    stagingGiftcards,
    stagingPayments,
    stagingCampaigns

    var url: String {
        switch self {
        case .sandbox:
            return "https://sandbox-customer.tappit.us"
        case .sandboxPayments:
            return "https://sandbox-mobile-payment.tappit.us"
        case .sandboxGiftcards:
            return "https://sandbox-mobile-gift.tappit.us"
        case .sandboxCampaigns:
            return "https://sandbox-campaign.tappit.us"
        case .test:
            return "https://test-customer.tappit.us"
        case .testPayments:
            return "https://test-mobile-payment.tappit.us"
        case .testGiftcards:
            return "https://test-mobile-gift.tappit.us"
        case .testCampaigns:
            return "https://test-campaign.tappit.us"
        case .demo:
            return "https://demo-customer.tappit.us"
        case .demoPayments:
            return "https://demo-mobile-payment.tappit.us"
        case .demoGiftcards:
            return "https://demo-mobile-gift.tappit.us"
        case .demoCampaigns:
            return "https://demo-campaign.tappit.us"
        case .staging:
            return "https://staging-customer.tappit.us"
        case .stagingPayments:
            return "https://staging-mobile-payment.tappit.us"
        case .stagingGiftcards:
            return "https://staging-mobile-gift.tappit.us"
        case .stagingCampaigns:
            return "https://staging-campaign.tappit.us"
        case .production:
            return "https://prod-customer.tappit.us"
        case .productionPayments:
            return "https://prod-mobile-payment.tappit.us"
        case .productionGiftcards:
            return "https://prod-mobile-gift.tappit.us"
        case .productionCampaigns:
            return "https://prod-campaign.tappit.us"
        case .productionAnalytics, .stagingAnalytics:
            return "https://www.google-analytics.com"
        }
    }

    var analyticsMeasureId: String {
        switch self {
        case .productionAnalytics:
            return "G-8EV6D2FEGY"
        default:
            return "G-ZSTSHPZEE0"
        }
    }

    var analyticsApiSecret: String {
        switch self {
        case .productionAnalytics:
            return "PhNhkJmgS1CflwSXvcEDVg"
        default:
            return "OLqLeQ3MQVq-7Cvnm-kmgg"
        }
    }
}
