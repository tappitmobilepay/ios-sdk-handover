//
//  TransactionsApiRoute.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 30/12/2019.
//  Copyright © 2019 JadePayments. All rights reserved.
//

import Foundation

enum TransactionsApiRoute: ApiRoute { case

    transactionsHistory

    var path: String {
        switch self {
        case .transactionsHistory: return "v3/transactions"
        }
    }

    func url(for environment: ApiEnvironment) -> String {
        return "\(environment.url)/\(path)"
    }
}
