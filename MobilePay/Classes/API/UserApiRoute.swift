//
//  UserApiRoute.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 17/12/2019.
//  Copyright © 2019 JadePayments. All rights reserved.
//

import Foundation

enum UserApiRoute: ApiRoute { case

    balance,
    giveawayTokens,
    benefits(fanId: String)

    var path: String {
        switch self {
        case .balance: return "v2/balance"
        case .giveawayTokens: return "tokens"
        case .benefits(let fanId): return "users/\(fanId)/benefits"
        }
    }

    func url(for environment: ApiEnvironment) -> String {
        return "\(environment.url)/\(path)"
    }
}
