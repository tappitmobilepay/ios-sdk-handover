//
//  TransactionsWebServiceContract.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 30/12/2019.
//  Copyright © 2019 JadePayments. All rights reserved.
//

import Foundation

typealias TransactionsHistoryResult = (_ transactions: TransactionsHistoryResponse?, _ error: Error?) -> Void

protocol TransactionsWebServiceContract: CancelableService {
    var responseParser: TransactionsResponseParsingContract? { get set }

    func requestTransactionsHistory(page: Int, elementsPerPage: Int, fromDate: Date?, toDate: Date?, completion: @escaping TransactionsHistoryResult)
}
