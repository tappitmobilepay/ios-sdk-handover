//
//  UserWebServiceContract.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 17/12/2019.
//  Copyright © 2019 JadePayments. All rights reserved.
//

import Foundation

typealias BalanceResult = (_ balance: Balance?, _ error: Error?) -> Void

protocol UserWebServiceContract: CancelableService {
    var responseParser: UserResponseParsingContract? { get set }

    func requestBalance(completion: @escaping BalanceResult)
}
