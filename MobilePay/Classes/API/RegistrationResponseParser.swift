//
//  RegistrationResponseParser.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 11/12/2019.
//  Copyright © 2019 JadePayments. All rights reserved.
//

import Foundation

let invalidDataError = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Server returned invalid data. Please try again."])

class RegistrationResponseParser: RegistrationResponseParsingContract {
    let errorParser: ServerErrorParsingContract

    required init(errorParser: ServerErrorParsingContract) {
        self.errorParser = errorParser
    }

    func registrationResponse(from data: Data?) throws -> RegistrationResponse {
        let decoder = JSONDecoder()

        if let d = data, let registrationResponse = try? decoder.decode(RegistrationResponse.self, from: d) {
            return registrationResponse
        }

        else {
            throw invalidDataError
        }
    }

    func otpSucceeded(from data: Data?) throws {
        guard let error = try errorParser.parseError(from: data) else {
            return
        }

        throw error
    }

    func changePasswordSucceeded(from data: Data?) throws {
        guard let error = try errorParser.parseError(from: data) else {
            return
        }

        throw error
    }
}
