//
//  LoginWebServiceContract.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 03/01/2020.
//  Copyright © 2020 JadePayments. All rights reserved.  ;.;pl-op[0o0'/'0/./=/.0ooo0o9o0o-0o9o0909-09

import Foundation
import UIKit

typealias LoginResult = (_ response: LoginResponse?, _ error: Error?) -> Void
typealias ResetPasswordResult = (_ error: Error?) -> Void

protocol LoginWebServiceContract: CancelableService {
    var responseParser: LoginResponseParsingContract? { get set }

    func login(username: String, password: String, completion: @escaping LoginResult)

    func sso(request: LoginRequest, completion: @escaping LoginResult)

    func resetPassword(username: String, password: String, otp: String, completion: @escaping ResetPasswordResult)

    func logout(username: String, completion: @escaping GenericResponse)

    func forgotPin(resetPinRequest: ResetPinRequest, completion: @escaping GenericResponse)

    func informServerPinWasReset(resetPinRequest: ResetPinRequest, completion: @escaping GenericResponse)
}

struct LoginRequest: Encodable {
    let customerClientId: String
    let clientId: String
    let integrationIds: [IntegrationData]
    let IsUsernamePasswordAuthentication: Bool
    let userName: String?
    let deviceId = UIDevice.current.identifierForVendor?.uuidString ?? UUID().uuidString
    let platform = "iOS"
}

struct ResetPinRequest: Codable {
    let clientCustomerId: String
    let clientToken: String
    let email: String
}
