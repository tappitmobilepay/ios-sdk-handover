//
//  RegistrationResponse.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 11/12/2019.
//  Copyright © 2019 JadePayments. All rights reserved.
//

import Foundation

struct RegistrationResponse: Codable {
    var token: String
    var hashId: Int
}
