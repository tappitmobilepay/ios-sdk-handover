//
//  NonPersistentContext.swift
//  CityInfo
//
//  Created by Marius Kurgonas on 05/09/2018.
//  Copyright © 2018 Marius Kurgonas. All rights reserved.
//

import Foundation

class NonPersistentApiContext: ApiContext {
    init(environment: ApiEnvironment) {
        self.environment = environment
    }

    var environment: ApiEnvironment
}
