//
//  TransactionsResponseParsingContract.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 30/12/2019.
//  Copyright © 2019 JadePayments. All rights reserved.
//

import Foundation

protocol TransactionsResponseParsingContract {
    func transactionsHistoryResponse(from data: Data?) throws -> TransactionsHistoryResponse
}
