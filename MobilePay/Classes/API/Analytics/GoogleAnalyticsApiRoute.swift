//
//  GoogleAnalyticsApiRoute.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 08/09/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation

enum GoogleAnalyticsApiRoute: ApiRoute { case

    reportEvent(measureId: String, apiSecret: String)

    var path: String {
        switch self {
        case .reportEvent(let measureId, let apiSecret):
            return "mp/collect?measurement_id=\(measureId)&api_secret=\(apiSecret)"
        }
    }

    func url(for environment: ApiEnvironment) -> String {
        return "\(environment.url)/\(path)"
    }
}
