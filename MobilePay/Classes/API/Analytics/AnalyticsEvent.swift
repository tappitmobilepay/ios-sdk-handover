//
//  AnalyticsEvent.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 08/09/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation
import UIKit

struct AnalyticsEventsReport: Codable {
    let client_id: String
    let user_id: String
    let events: [AnalyticsEvent]
}

struct AnalyticsEvent: Codable {
    let name: String
    let params: Param
}

struct Param: Codable {
    let clientName: String
    let clientID: String
    var totalOpened: String? = nil
    var totalAddedCardOnly: String? = nil
    var totalSingleTransaction: String? = nil
    var totalMultipleTransaction: String? = nil
    var totalGiftCardsAdded: String? = nil
    var totalAuthChanges: String? = nil
    var transactionAmt: String? = nil
    var totalRefunds: String? = nil
    var totalDefaultCards: String? = nil
    var totalAddtlCardsAdded: String? = nil
    var totalCardsDeleted: String? = nil
    var totalFirstCardAdded: String? = nil
    var totalLoginResets: String? = nil
    var totalPinSetUps: String? = nil
    var tipType: String? = nil
    var totalGCRedeem: String? = nil
    var totalGiveAways: String? = nil
    var currency: String? = nil
    var cardType: String? = nil
    var totalPaymentNewQRFailures: String? = nil
    var totalPaymentFailures: String? = nil
    var totalPaymentNewQRSuccess: String? = nil
    var totalPaymentSuccess: String? = nil
    var totalPaymentStarts: String? = nil
    var loginType: String? = nil
    var totalLogins: String? = nil
    var totalBioSetUps: String? = nil
    var failedReason: String? = nil
    var platformType: String = "iOS Mobile App"
}
