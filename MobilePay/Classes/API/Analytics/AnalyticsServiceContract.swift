//
//  AnalyticsServiceContract.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 08/09/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation

typealias ReportEventResult = (_ error: Error?) -> Void

protocol AnalyticsServiceContract: CancelableService {
    func reportEvent(event: AnalyticsEventsReport, completion: @escaping ReportEventResult)
}
