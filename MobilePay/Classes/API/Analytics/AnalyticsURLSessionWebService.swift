//
//  AnalyticsURLSessionWebService.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 08/09/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation

class AnalyticsURLSessionWebService: NSURLSessionWebService, AnalyticsServiceContract {
    func reportEvent(event: AnalyticsEventsReport, completion: @escaping ReportEventResult) {
        let params = event.dictionary
        task = post(at: GoogleAnalyticsApiRoute.reportEvent(measureId: context.environment.analyticsMeasureId, apiSecret: context.environment.analyticsApiSecret), params: params ?? [:], encoding: .json, completion: { _, error in

            if error == nil {
                DispatchQueue.main.async {
                    completion(nil)
                }
            }
            else {
                DispatchQueue.main.async {
                    completion(error)
                }
            }
        })
    }

    override func headers() -> [String: String] {
        return [:]
    }

    func cancel() {
        task?.cancel()
        task = nil
    }

    var task: URLSessionDataTask?
}
