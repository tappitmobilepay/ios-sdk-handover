//
//  PaymentsApiRoute.swift
//  MobilePay-PAYG
//
//  Created by Marius Kurgonas on 03/06/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation

enum PaymentsApiRoute: ApiRoute { case

    addPaymentMethod,
    paymentMethods,
    makePaymentMethodDefault,
    paymentProvidersConfiguration,
    paymentProvidersInfo,
    deletePaymentMethod,
    checkTransactionStatus(qr: String),
    customerStats(fanId: String)

    var path: String {
        switch self {
        case .addPaymentMethod: return "paymentProfiles/v2/create"
        case .paymentMethods: return "paymentProfiles"
        case .paymentProvidersConfiguration: return "paymentProfiles/paymentProviderInfo/isAllConfigured"
        case .paymentProvidersInfo: return "paymentProfiles/paymentProviderInfo"
        case .deletePaymentMethod: return "paymentProfiles/multipleProfiles"
        case .checkTransactionStatus(let qr): return "v2/transactions?qrCode=\(qr)"
        case .makePaymentMethodDefault: return "paymentProfiles/setAsDefault"
        case .customerStats(let fanId): return "users/\(fanId)/stats"
        }
    }

    func url(for environment: ApiEnvironment) -> String {
        return "\(environment.url)/\(path)"
    }
}
