//
//  UIButton+Extensions.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 04/03/2021.
//  Copyright © 2021 JadePayments. All rights reserved.
//

import Foundation
import UIKit

extension UIButton {
    func underlineButton(text: String) {
        let titleString = NSMutableAttributedString(string: text)
        titleString.addAttribute(NSAttributedString.Key.underlineStyle,
                                 value: NSUnderlineStyle.single.rawValue,
                                 range: NSMakeRange(0, text.count))
        setAttributedTitle(titleString, for: .normal)
    }
}
