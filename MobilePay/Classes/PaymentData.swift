//
//  PaymentData.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 5/18/21.
//  Copyright © 2021 JadePayments. All rights reserved.
//

import Foundation

enum PaymentType: Int { case
    none = 0,
    nativeApplePay = 1
}

struct PaymentData {
    var tokens: [Int]? = nil
    var tips: [Int]? = nil
    var paymentMethod: PaymentType = .none
    var giftcards: [Int]? = nil
}
