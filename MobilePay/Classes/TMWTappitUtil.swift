//
//  Util.swift
//  Corper
//
//  Created by Redcaso Solutions Pvt on 02/02/17.
//  Copyright © 2017 Redcaso Solutions Pvt. All rights reserved.
//

// TODO: - Updated Library URLs

// NVActivityIndicatorView - https://github.com/ninjaprox/NVActivityIndicatorView

// Localize - https://github.com/marmelroy/Localize-Swift

// Toast - https://github.com/scalessec/Toast-Swift

// PickerDialog - https://github.com/aguynamedloren/ios-picker-dialog

// Device - https://github.com/dennisweissmann/DeviceKit

// LocationService - https://github.com/igroomgrim/CLLocationManager-TMWSingleton-in-Swift

// SWXMLHash - https://github.com/drmohundro/SWXMLHash

// HyperLabel - https://github.com/null09264/FRHyperLabel

// SideMenu   - https://github.com/SSA111/SSASideMenu

import Foundation
import LocalAuthentication
import SystemConfiguration
import UIKit

class Util: NSObject {
    var accountSelectIndex: Int!
    enum Tappit {
        enum Path {
            static let destinationPath = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .allDomainsMask, true)[0] as String
            static let temp = NSTemporaryDirectory()
        }

        /// checks if Biometric id is avaiable on device
        static func biometricIDAvailable() -> Bool {
            let context = LAContext()
            if context.canEvaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, error: nil), context.biometryType == .faceID || context.biometryType == .touchID {
                return true
            }

            return false
        }

        static func getSymbolForCurrencyCode(code: String) -> String {
            var candidates: [String] = []
            let locales: [String] = NSLocale.availableLocaleIdentifiers
            for localeID in locales {
                guard let symbol = findMatchingSymbol(localeID: localeID, currencyCode: code) else {
                    continue
                }
                if symbol.count == 1 {
                    return symbol
                }
                candidates.append(symbol)
            }
            let sorted = sortAscByLength(list: candidates)
            if sorted.count < 1 {
                return ""
            }
            return sorted[0]
        }

        static func findMatchingSymbol(localeID: String, currencyCode: String) -> String? {
            let locale = Locale(identifier: localeID as String)
            guard let code = locale.currencyCode else {
                return nil
            }
            if code != currencyCode {
                return nil
            }
            guard let symbol = locale.currencySymbol else {
                return nil
            }
            return symbol
        }

        static func sortAscByLength(list: [String]) -> [String] {
            return list.sorted(by: { $0.count < $1.count })
        }
    }

    enum ScreenSize {
        static let SCREEN_WIDTH = UIScreen.main.bounds.size.width
        static let SCREEN_HEIGHT = UIScreen.main.bounds.size.height
        static let SCREEN_MAX_LENGTH = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
        static let SCREEN_MIN_LENGTH = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    }

    enum DeviceType {
        static let IS_IPHONE_4S = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
        static let IS_IPHONE_5 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
        static let IS_IPHONE_6 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
        static let IS_IPHONE_6P = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
        static let IS_IPHONE_6S = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
        static let IS_IPHONE_6SP = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
        static let IS_IPHONE_7 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
        static let IS_IPHONE_8 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
        static let IS_IPHONE_X = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 812.0
        static let IS_IPHONE_MAX = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH > 812.0
        static let IS_IPAD = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
    }

    enum VerificationType: String {
        case date_of_birth
        case last_four_digit_number
        case phone
        case id_number
        case email
        case activation_code
    }

    enum CardStatus: String {
        case Unassigned = "0"
        case Assigned = "5"
        case Dispatched = "8"
        case Active = "10"
        case Lost = "41"
        case Stolen = "43"
        case Expired = "54"
        case Restricted = "62"
        case Destroyed = "83"
        case Voided = "99"
        case External = "100"
    }

    enum CardStatusName: String {
        case Unassigned = "UNASSIGNED"
        case Assigned = "ASSIGNED"
        case Dispatched = "DISPATCHED"
        case Active = "ACTIVE"
        case Lost = "LOST"
        case Stolen = "STOLEN"
        case Expired = "EXPIRED"
        case Restricted = "RESTRICTED"
        case Destroyed = "DESTROYED"
        case Voided = "VOIDED"
        case External = "EXTERNAL"
    }

    enum PaymentDeviceType: Int {
        case card = 1
        case wristband = 2
        case mobileWallet = 3
        case mobileNfc = 4
        case emcChipCard = 5
        case ticket = 6
    }

    enum ProgramType: Int {
        case prepaidGiftCard = 1
        case prepaidGPR = 2
        case walletGift = 3
        case walletGPR = 4
    }

    enum BrandId: String {
        case capeTown10s = "84"
        case sevens = "60"
    }

    enum OwnerId: Int {
        case capeTown10s = 84
        case sevens = 60
    }

    struct MasterDetail {
        var brand_based_config: String!
        var cus_req_man_fields: String!
        var verification_fields: String!
        var wallet_programs: String!
        init(dictionary: [String: AnyObject]) {
            brand_based_config = dictionary["brand_based_config"] as? String
            cus_req_man_fields = dictionary["cus_req_man_fields"] as? String
            verification_fields = dictionary["verification_fields"] as? String
            wallet_programs = dictionary["wallet_programs"] as? String
        }
    }

    enum CustomerMandatoryFields: String {
        case firstName = "first_name"
        case middleName = "middle_name"
        case last_name
        case email
        case phone
        case date_of_birth
        case gender
        case address1
        case addresss2 = "address2"
        case city
        case state
        case zipCode = "zip_code"
        case countryCode = "country_code"
    }
}
