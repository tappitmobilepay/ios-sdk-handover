//
//  JWTBody.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 07/09/2021.
//  Copyright © 2021 JadePayments. All rights reserved.
//

import Foundation

struct JWTBody: Codable {
    let TimeZone: String
}
