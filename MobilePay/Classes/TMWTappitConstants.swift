//
//  Constants.swift
//  Corper
//
//  Created by Redcaso Solutions Pvt on 02/02/17.
//  Copyright © 2017 Redcaso Solutions Pvt. All rights reserved.
//

import Foundation
import UIKit

extension String {
    var localized: String {
        let bundle = Bundle(for: BaseScreenViewController.self)

        var result = bundle.localizedString(forKey: self, value: nil, table: nil)

        if result == self {
            result = bundle.localizedString(forKey: self, value: nil, table: "Default")
        }

        return result
    }
}

// MARK: - ----Fonts------

enum TappitFont {
    enum Size: CGFloat {
        case LightNormal1 = 12.0
        case Medium = 14.0
        case LargeMedium = 16.0
        case Large = 18.0
        case BigRegular = 20.0
        case BigNormal = 22.0
        case BigMedium = 26.0
        case Big = 35.0
        case BigBig = 40.0
        case BigBigBig = 50.0
    }

    enum Family: String, CaseIterable {
        case bold
        case normal
        case payButton
        case homeButton
        case primaryButton
        case secondaryButton
        case customTipButton
        case benefitStatusButton
        case campaignStatusButton
    }
}
