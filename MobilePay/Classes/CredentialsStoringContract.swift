//
//  CredentialsStoringContract.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 07/07/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation

protocol CredentialsStoringContract {
    var bearerToken: String? { get set }

    var clientTimezone: String? { get set }

    var apiKey: String? { get set }

    var userId: Int? { get set }

    var pinLockdownDate: Date? { get set }

    var wrongPinsInARow: Int? { get set }

    var showRedeemedCampaigns: Bool? { get set }

    var pin: String? { get set }

    var loggedInWithIntegrationKey: String? { get set }

    var loggedInWithEnvironment: String? { get set }

    var authenticationMethod: AuthenticationMethod? { get set }

    var shouldResetAuthentication: Bool? { get set }

    var customerStats: CustomerStats? { get set }

    func wipe()

    func softWipe()
}
