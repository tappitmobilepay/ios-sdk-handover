//
//  ClientConfig.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 07/01/2021.
//  Copyright © 2021 JadePayments. All rights reserved.
//

import Foundation

struct ClientConfig: Decodable {
    let configuration: ClientConfigConfiguration
    let clientToken: String
    let clientId: String
    let clientCurrency: ClientCurrency
}

struct ClientCurrency: Codable {
    let code: String
    let symbol: String
}

struct ClientConfigConfiguration: Decodable {
    let settings: ClientConfigSettings
    let appName: String
}

struct ClientConfigSettings: Decodable {
    var loginEndpointOverride: String?
    var tokensEndpointOverride: String?
    var balanceEndpointOverride: String?
    var tips: TipsConfiguration
    var showGiveawayTokens: Bool
    var showGiftCard: Bool
    var showMyOffers: Bool
    var preloadTopup: Bool
    var showPrivacyPolicy: Bool
    var registrationType: Int
    var privacyPolicy: String?
    var termsAndConditions: String?
    var help: String?

    enum CodingKeys: String, CodingKey { case
        overridenEndpoints,
        showGiveawayTokens,
        showMyOffers,
        showGiftCard,
        preloadTopup,
        showPrivacyPolicy,
        registrationType,
        tips,
        privacyPolicy,
        termsAndConditions,
        help
    }

    init(loginEndpointOverride: String?,
         tokensEndpointOverride: String?,
         balanceEndpointOverride: String?,
         tips: TipsConfiguration,
         showGiveawayTokens: Bool,
         showGiftCard: Bool,
         showMyOffers: Bool,
         preloadTopup: Bool,
         showPrivacyPolicy: Bool,
         registrationType: Int,
         privacyPolicy: String?,
         termsAndConditions: String?,
         help: String?)
    {
        self.loginEndpointOverride = loginEndpointOverride
        self.tokensEndpointOverride = tokensEndpointOverride
        self.balanceEndpointOverride = balanceEndpointOverride
        self.tips = tips
        self.showGiftCard = showGiftCard
        self.showGiveawayTokens = showGiveawayTokens
        self.showMyOffers = showMyOffers
        self.preloadTopup = preloadTopup
        self.showPrivacyPolicy = showPrivacyPolicy
        self.registrationType = registrationType
        self.privacyPolicy = privacyPolicy
        self.termsAndConditions = termsAndConditions
        self.help = help
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        tips = try values.decode(TipsConfiguration.self, forKey: .tips)
        showGiveawayTokens = try values.decode(Bool.self, forKey: .showGiveawayTokens)
        showMyOffers = try values.decode(Bool.self, forKey: .showMyOffers)
        showGiftCard = try values.decode(Bool.self, forKey: .showGiftCard)
        preloadTopup = try values.decode(Bool.self, forKey: .preloadTopup)
        showPrivacyPolicy = try values.decode(Bool.self, forKey: .showPrivacyPolicy)
        registrationType = try values.decode(Int.self, forKey: .registrationType)
        privacyPolicy = (try? values.decode(String.self, forKey: .privacyPolicy))?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        if let pp = privacyPolicy, !pp.hasPrefix("https://"), !pp.hasPrefix("http://") {
            privacyPolicy = "http://" + pp
        }
        termsAndConditions = (try? values.decode(String.self, forKey: .termsAndConditions))?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        if let tc = termsAndConditions, !tc.hasPrefix("https://"), !tc.hasPrefix("http://") {
            termsAndConditions = "http://" + tc
        }
        help = (try? values.decode(String.self, forKey: .help))?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        if let h = help, !h.hasPrefix("https://"), !h.hasPrefix("http://") {
            help = "http://" + h
        }
        if let endpoints = try? values.decode([OverridenEndpoint].self, forKey: .overridenEndpoints) {
            for endpoint in endpoints {
                if let endpointEnum = OverridenEndpointTypes(rawValue: endpoint.endpoint) {
                    switch endpointEnum {
                    case .Login:
                        loginEndpointOverride = endpoint.url
                    case .GetTokens:
                        tokensEndpointOverride = endpoint.url
                    case .GetBalance:
                        balanceEndpointOverride = endpoint.url
                    }
                }
            }
        }
    }
}

struct TipsConfiguration: Decodable {
    let enabled: Bool
    var percentages: [Int]
}

struct OverridenEndpoint: Decodable {
    let endpoint: Int
    let url: String
}

enum OverridenEndpointTypes: Int, Decodable { case
    Login = 0,
    GetTokens = 4,
    GetBalance = 5

    static func url(for environment: ApiEnvironment, endpoint: String) -> String {
        return "\(environment.url)/\(endpoint)"
    }
}
