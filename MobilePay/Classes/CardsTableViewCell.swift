//
//  CardsTableViewCell.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 06/05/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import UIKit

class CardsTableViewCell: UITableViewCell {
    let cardController: CreditCardViewController! = UIStoryboard.initViewController(for: CreditCardViewController.self) as? CreditCardViewController

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func setup(card: Card, isDefault: Bool) {
        selectionStyle = .none
        addChildIfNeeded()

        cardController.setup(card: card, isDefault: isDefault)
    }

    func addChildIfNeeded() {
        if cardController.view.superview != nil {
            return
        }
        let child: CreditCardViewController! = cardController
        contentView.addSubview(child.view)
        child.view.translatesAutoresizingMaskIntoConstraints = false
        child.view.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -20).isActive = true
        child.view.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
        child.view.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
        child.view.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
    }
}
