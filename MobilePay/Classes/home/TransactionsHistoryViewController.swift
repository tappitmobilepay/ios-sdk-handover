//
//  TransactionsHistoryViewController.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 30/12/2019.
//  Copyright © 2019 JadePayments. All rights reserved.
//

import UIKit

enum TransactionType: Double {
    case
        Spend = 0,
        Preload = 1,
        Refund = 2,
        ComplimentaryLoad = 3,
        RefundLoad = 4,
        TokenSpend = 5

    func toString() -> String {
        switch self {
        case .Spend:
            return "Spend"
        case .Preload:
            return "Preload"
        case .Refund:
            return "Refund"
        case .ComplimentaryLoad:
            return "Complimentary Load"
        case .RefundLoad:
            return "Refund Load"
        case .TokenSpend:
            return "Token Spend"
        }
    }
}

class TransactionsHistoryViewController: BaseScreenViewController {
    @IBOutlet var balanceContainerTopConstraint: NSLayoutConstraint!
    @IBOutlet var balanceContainerView: UIView!
    @IBOutlet var mainTitleLabel: TitleLabel!
    @IBOutlet var tableViewContainerView: UIView!
//    #if !PAYG
//        var balanceController: BalanceViewController?
//    #endif
    @IBOutlet var balanceContainerHeightConstraint: NSLayoutConstraint!
    var router: RouterContract?
    var tableViewController: (UIViewController & TransactionsListTableViewControllerContract)?

    override func viewDidLoad() {
        super.viewDidLoad()

        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }

        guard let tableViewController = tableViewController else {
            assert(false)
            return
        }

        balanceContainerHeightConstraint.constant = 0
        balanceContainerHeightConstraint.priority = UILayoutPriority(1000)
        balanceContainerTopConstraint.constant = 0
        addChildViewController(child: tableViewController, toView: tableViewContainerView)
        mainTitleLabel.text = "Transaction History"
        mainTitleLabel.textColor = DevUtil.color(name: Colors.tmw_page_header_text.rawValue)
        addDefaultTitleViewLogo()
        addRefundButton()
    }

    func addRefundButton() {
        let refundButton = HighlightedCustomButton()
        refundButton.addDefaultRoundCorners()
        refundButton.setTitle("Refund", for: .normal)
        refundButton.titleLabel?.font = UIFont.with(size: .Large, weight: .normal)
        refundButton.sizeToFit()
        refundButton.addTarget(self, action: #selector(didTapRefundButton), for: .touchUpInside)
        let titleColor = DevUtil.color(name: Colors.tmw_navigation_background.rawValue)
        refundButton.setTitleColor(titleColor, for: .normal)
        let highlightedColor = titleColor.withAlphaComponent(0.6)
        refundButton.setTitleColor(highlightedColor, for: .highlighted)
        refundButton.backgroundColor = DevUtil.color(name: Colors.tmw_back_indicator.rawValue)
        let refundItem = UIBarButtonItem(customView: refundButton)
        navigationItem.rightBarButtonItem = refundItem

        let widthConstraint = NSLayoutConstraint(item: refundButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: refundButton.bounds.size.width + 16)

        refundButton.addConstraints([widthConstraint])
    }

    @objc
    func didTapRefundButton() {
        Analytics.reportRefundClickedEvent()
        router?.presentRefund()
    }

    override func viewWillAppear(_ animated: Bool) {
        showBackButton()
        navigationController?.setNavigationBarHidden(false, animated: true)
    }

    override func viewDidAppear(_ animated: Bool) {
        showBackButton()
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
}
