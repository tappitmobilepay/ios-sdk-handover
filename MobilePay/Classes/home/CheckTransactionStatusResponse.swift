//
//  CheckTransactionStatusResponse.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 16/07/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation

enum TransactionStatus: String {
    case failed, completed, pending
}

struct TransactionStatusResponse: Codable {
    var status: String
    var message: String?
    var chargeAmount: Decimal?
}
