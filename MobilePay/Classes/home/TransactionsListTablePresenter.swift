//
//  TransactionsListTablePresenter.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 30/12/2019.
//  Copyright © 2019 JadePayments. All rights reserved.
//

import Foundation

class TransactionsListTablePresenter: TransactionsListTablePresenterContract {
    weak var view: TransactionsListTableViewControllerContract?
    let webService: TransactionsWebServiceContract
    let db: DatabaseManager
    let router: RouterContract

    var currencySymbol: String = ""

    init(view: TransactionsListTableViewControllerContract,
         webService: TransactionsWebServiceContract,
         db: DatabaseManager,
         router: RouterContract)
    {
        self.view = view
        self.webService = webService
        self.db = db
        self.router = router

        currencySymbol = db.clientConfig()?.clientCurrency.symbol ?? ""
    }

    func viewDidAppear() {
        Analytics.reportShowTransactionsHistoryScreenEvent()
    }

    func viewDidLoad() {
        view?.setup()
        view?.updateWith(transactions: db.allTransactions())
        view?.showLoading()
        refreshData()
    }

    func getData() {
        guard TMWCommonUtil.isInternetAvailable() else {
            TMWCommonUtil.showInternetUnavailableAlert()
            view?.hideLoading()
            return
        }

        webService.cancel()
        webService.requestTransactionsHistory(page: 1, elementsPerPage: 100, fromDate: nil, toDate: nil) { [weak self] response, _ in
            if let r = response {
                self?.db.saveTransactions(transactions: r.transactionsList)

                self?.view?.updateWith(transactions: r.transactionsList)
            }
            self?.view?.hideLoading()
        }
    }

    func refreshData() {
        getData()
    }

    func didSelectItem(at: Int) {
        let transaction = db.allTransactions()[at]
        if !transaction.items.isEmpty {
            Analytics.reportShowTransactionDetailsScreenEvent()
            router.presentTransactionDetails(transaction: transaction, currencySymbol: currencySymbol)
        }
    }
}
