//
//  PaymentViewContract.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 19/11/2019.
//  Copyright © 2019 JadePayments. All rights reserved.
//

import Foundation
import UIKit

protocol PaymentViewContract: AnyObject {
    var presenter: PaymentPresenterContract? { get set }

    func startAnimation(duration: Double, beginingPercentage: Double, completion: @escaping () -> Void)

    func showQR(for text: String)

    func setTimeLeft(timeLeftString: String)

    var nav: UINavigationController? { get }

    func askUserToRenewTheQR()

    func setup(topText: String)

    func hideQRView()

    func showQRView()
}
