//
//  TransactionItemTableViewCell.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 18/03/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import UIKit

class TransactionItemTableViewCell: UITableViewCell {
    @IBOutlet private var itemsLabel: UILabel!
    @IBOutlet private var quantityLabel: UILabel!
    @IBOutlet private var priceLabel: UILabel!
    @IBOutlet private var bgView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        bgView.addDefaultRoundCorners()
    }

    func setup(item: TransactionItem, currencyCode: String?) {
        itemsLabel.text = item.name
        itemsLabel.font = UIFont.with(size: .LightNormal1, weight: .normal)
        quantityLabel.text = "\(item.quantity)"
        quantityLabel.font = UIFont.with(size: .LightNormal1, weight: .normal)
        let currencySymbol = Util.Tappit.getSymbolForCurrencyCode(code: currencyCode ?? "")
        priceLabel.text = "\(currencySymbol) " + (item.unitPrice * item.quantity).format(f: ".2")
        priceLabel.font = UIFont.with(size: .LightNormal1, weight: .normal)
    }
}
