//
//  PaymentPresenter.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 19/11/2019.
//  Copyright © 2019 JadePayments. All rights reserved.
//

import Foundation
import UIKit
import UserNotifications

let kPaymentWaitDurationSeconds = isUITesting() ? 5 : 60.0
let kApiPaymentCheckTime = 3.0

class PaymentPresenter: PaymentPresenterContract {
    weak var view: PaymentViewContract?

    let timeLeftProvider: QRCodeTimeLeftContract

    let credStore: CredentialsStoringContract

    let webService: PaymentsWebServiceContract

    var hashids: Hashids

    var confirmedQRRenew = false

    var originalScreenBrightness: CGFloat?

    var dialogShowing: Bool = false

    var router: RouterContract

    var backgroundTask: UIBackgroundTaskIdentifier = .invalid

    var timer: Timer?
    var serverRequestTimer: Timer?
    var timeLeft: TimeInterval = kPaymentWaitDurationSeconds
    var currentQRString: String?
    var mode: QRMode
    let paymentData: PaymentData

    init(view: PaymentViewContract,
         timeLeftProvider: QRCodeTimeLeftContract,
         credStore: CredentialsStoringContract,
         webService: PaymentsWebServiceContract,
         router: RouterContract,
         mode: QRMode,
         paymentData: PaymentData)
    {
        self.view = view
        self.timeLeftProvider = timeLeftProvider
        self.credStore = credStore
        self.webService = webService
        self.router = router
        hashids = Hashids(salt: "Rightsoft!*Salt@!")
        self.paymentData = paymentData
        self.mode = mode
    }

    func viewDidLoad() {
        var topText = "SCAN ME TO GET A REFUND"

        if mode == .Pay {
            topText = "Scan Me To Pay"
        }

        view?.setup(topText: topText)
        requestNewQR()
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged(note:)), name: .reachabilityChanged, object: TMWCommonUtil.reachability)
        NotificationCenter.default.addObserver(self, selector: #selector(didEnterBG), name: UIApplication.willResignActiveNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(didEnterBG), name: UIApplication.willEnterForegroundNotification, object: nil)
    }

    func viewWillAppear() {
        if !TMWCommonUtil.isInternetAvailable() {
            TMWCommonUtil.showInternetUnavailableAlert()
        }

        changeBrightness()
    }

    func viewWillDisappear(isMovingFromParent: Bool) {
        timer?.invalidate()

        if isMovingFromParent {
            invalidateTimers()
        }

        changeBrightness(max: false)
    }

    @objc
    func didEnterBG() {
        changeBrightness(max: false)
    }

    @objc
    func didEnterFG() {
        changeBrightness()
    }

    func changeBrightness(max: Bool = true) {
        if max {
            if originalScreenBrightness == nil {
                originalScreenBrightness = UIApplication.shared.keyWindow?.screen.brightness
            }
            UIApplication.shared.keyWindow?.screen.brightness = 1
        }
        else {
            if let original = originalScreenBrightness {
                UIApplication.shared.keyWindow?.screen.brightness = original
                originalScreenBrightness = nil
            }
        }
    }

    @objc func reachabilityChanged(note: Notification) {
        let reachability = note.object as! Reachability

        switch reachability.connection {
        case .wifi, .cellular:
            if serverRequestTimer == nil, !dialogShowing {
                setupNetworkTimer()
            }
        case .unavailable, .none:
            serverRequestTimer?.invalidate()
            serverRequestTimer = nil
        }
    }

    func generateNewQR() {
        timeLeft = kPaymentWaitDurationSeconds
        timeLeftProvider.endInterval = Date().addingTimeInterval(timeLeft).timeIntervalSince1970
        generateQRString()
        if let qrString = currentQRString {
            view?.showQR(for: qrString)
        }
        else {
            showAlert(message: "Could not generate QR code. User id not fount")
        }
    }

    func invalidateTimers() {
        timer?.invalidate()
        serverRequestTimer?.invalidate()
        serverRequestTimer = nil
    }

    func requestNewQR() {
        view?.showQRView()
        dialogShowing = false
        generateNewQR()
        setupQRAnimation()
        setupLocalUpdatesTimer()
        if TMWCommonUtil.isInternetAvailable() {
            setupNetworkTimer()
        }
    }

    func didTapConfirmQRRenew() {
        confirmedQRRenew = true
        if mode == .Refund {
            Analytics.reportGenerateNewQRRefundConfirmEvent()
        }
        else {
            Analytics.reportGenerateNewQRPaymentConfirmEvent()
        }
        requestNewQR()
    }

    func didTapCancelQRRenew() {
        if mode == .Refund {
            Analytics.reportGenerateNewQRRefundCancelEvent()
        }
        else {
            Analytics.reportGenerateNewQRPaymentCancelEvent()
        }
        cancel()
    }

    func cancel() {
        dialogShowing = false
        invalidateTimers()
        view?.nav?.popViewController(animated: true)
    }

    func setupQRAnimation() {
        let endInterval = timeLeftProvider.endInterval
        let timeRemaining = endInterval - Date().timeIntervalSince1970

        if timeRemaining > 0 {
            timeLeft = timeRemaining

            onlyDebugPrint("Time remaining: \(timeRemaining)")

            let percentage = timeRemaining / kPaymentWaitDurationSeconds

            onlyDebugPrint("Percentage: \(percentage)")
            view?.startAnimation(duration: timeRemaining, beginingPercentage: percentage, completion: {
                onlyDebugPrint("Animation completed")
            })

            view?.setTimeLeft(timeLeftString: timeLeft.time)
        }
    }

    func setupNetworkTimer() {
        serverRequestTimer = Timer.scheduledTimer(timeInterval: kApiPaymentCheckTime,
                                                  target: self,
                                                  selector: #selector(runServerRequestTimer),
                                                  userInfo: nil,
                                                  repeats: true)
    }

    func setupLocalUpdatesTimer() {
        invalidateTimers()

        timer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true, block: { [weak self] _ in

            guard let strongSelf = self else {
                return
            }

            onlyDebugPrint("Still running: time remaining - \(strongSelf.timeLeft * 1000.0)")

            let diff = strongSelf.timeLeftProvider.endInterval - Date().timeIntervalSince1970
            strongSelf.timeLeft = diff < 0 ? 0 : diff
            if strongSelf.timeLeft > 0 {
                strongSelf.view?.setTimeLeft(timeLeftString: strongSelf.timeLeft.time)
            }
            else {
                strongSelf.view?.setTimeLeft(timeLeftString: strongSelf.timeLeft.time)
                onlyDebugPrint("QR code expired")
                strongSelf.invalidateTimers()
                strongSelf.dialogShowing = true
                strongSelf.view?.hideQRView()
                strongSelf.view?.askUserToRenewTheQR()
                if strongSelf.mode == .Refund {
                    Analytics.reportShowGenerateNewQRRefundPopupEvent()
                }
                else {
                    Analytics.reportShowGenerateNewPaymentQRPopupEvent()
                }
            }
        })
    }

    @objc
    func runServerRequestTimer() {
        guard let qrString = currentQRString else {
            assert(false)
            return
        }

        webService.cancel()
        webService.checkTransactionStatus(qr: qrString) { [weak self] response, error in
            if let response = response {
                self?.dialogShowing = false
                var type = CompletionType.TransactionUndetermined

                if let status = TransactionStatus(rawValue: response.status) {
                    switch status {
                    case .failed:
                        type = .TransactionFailure
                        if self?.mode == .some(.Pay) {
                            if self?.confirmedQRRenew == .some(true) {
                                Analytics.reportTransactionNewQRFailureEvent(failedReason: response.message ?? "error")
                            }
                            else {
                                Analytics.reportTransactionFailureEvent(failedReason: response.message ?? "error")
                            }
                        }
                        else {
                            if self?.confirmedQRRenew == .some(true) {
                                Analytics.reportRefundNewQRFailureEvent(failureMessage: response.message ?? "error")
                            }
                            else {
                                Analytics.reportRefundFailureEvent(failureMessage: response.message ?? "error")
                            }
                        }
                    case .completed:
                        type = .TransactionSuccess
                        if self?.mode == .some(.Pay) {
                            if self?.confirmedQRRenew == .some(true) {
                                Analytics.reportTransactionNewQRSuccessEvent(tipType: self?.paymentData.tips?.first ?? 0,
                                                                             totalTokens: self?.paymentData.tokens?.count ?? 0,
                                                                             totalGiftcards: self?.paymentData.giftcards?.count ?? 0, transactionAmt: response.chargeAmount ?? 0)
                            }
                            else {
                                Analytics.reportTransactionSuccessEvent(tipType: self?.paymentData.tips?.first ?? 0,
                                                                        totalTokens: self?.paymentData.tokens?.count ?? 0,
                                                                        totalGiftcards: self?.paymentData.giftcards?.count ?? 0, transactionAmt: response.chargeAmount ?? 0)
                            }
                        }
                        else {
                            if self?.confirmedQRRenew == .some(true) {
                                Analytics.reportRefundNewQRSuccessEvent(transactionAmt: response.chargeAmount ?? 0)
                            }
                            else {
                                Analytics.reportRefundSuccessEvent(transactionAmt: response.chargeAmount ?? 0)
                            }
                        }
                    default:
                        return
                    }
                }

                self?.invalidateTimers()

                self?.router.presentSuccess(type: type, message: response.message, completion: { [weak self] in
                    self?.router.goBack(to: .Home)
                })
            }
            else if let error = error {
                onlyDebugPrint("Error requesting transaction status: \(error.localizedDescription)")
            }
        }
    }

    func generateQRString() {
        guard let userId = credStore.userId else {
            fatalError()
        }

        let utcDate = Date()
        let currentTime = Int(utcDate.timeIntervalSince1970)
        let qrVersion = 3
        let generalInformation = [qrVersion, userId, currentTime]

        onlyDebugPrint("general qr info before hash: \(generalInformation)")

        var finalData = hashids.encode(generalInformation)
        finalData? += "|" // Tips separator

        if let tips = paymentData.tips,
           let tipsData = hashids.encode(tips)
        {
            onlyDebugPrint("tips before hash: \(tips)")
            finalData? += tipsData
        }

        finalData? += "|" // tokens separator

        if let tokens = paymentData.tokens,
           let tokensData = hashids.encode(tokens)
        {
            onlyDebugPrint("tokens before hash: \(tokens)")
            finalData? += tokensData
        }

        finalData? += "|" // gift cards separator

        if let giftcards = paymentData.giftcards,
           let giftcardsData = hashids.encode(giftcards)
        {
            onlyDebugPrint("giftcards before hash: \(giftcards)")
            finalData? += giftcardsData
        }

        finalData? += "|" // For payment information

        if let hashedData = hashids.encode(paymentData.paymentMethod.rawValue) {
            finalData? += "\(hashedData)"
        }

        currentQRString = finalData
        onlyDebugPrint("\(userId)")
        onlyDebugPrint("\(currentTime)")
        onlyDebugPrint("\(currentQRString!)")
        onlyDebugPrint("")
    }
}
