//
//  PaymentViewController.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 18/11/2019.
//  Copyright © 2019 JadePayments. All rights reserved.
//

import Foundation
import UIKit

class PaymentViewController: BaseScreenViewController, PaymentViewContract {
    var nav: UINavigationController? {
        return navigationController
    }

    var presenter: PaymentPresenterContract?

    @IBOutlet var topUpButton: UIButton!
    @IBOutlet var balanceContainerView: UIView!
    @IBOutlet var balanceContainerHeightConstraint: NSLayoutConstraint!
    @IBOutlet private var circularProgressBar: CircularProgressBar!

    @IBOutlet var topLabel: UILabel!

    @IBOutlet var qrCodeImageView: UIImageView!

    @IBOutlet var timeLeftLabel: UILabel!

    var popupView = UIView()

    deinit {
        UNUserNotificationCenter.current().delegate = nil
    }

    func showQR(for text: String) {
        qrCodeImageView.image = UIImage.qrImage(from: text, with: qrCodeImageView.bounds.size)
    }

    override func viewDidAppear(_ animated: Bool) {
        showBackButton()
        navigationController?.setNavigationBarHidden(false, animated: true)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        showBackButton()
        navigationController?.setNavigationBarHidden(false, animated: true)

        guard let presenter = presenter else {
            assert(false)
            return
        }

        presenter.viewWillAppear()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        guard let presenter = presenter else {
            assert(false)
            return
        }

        presenter.viewWillDisappear(isMovingFromParent: isMovingFromParent)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }

        guard let presenter = presenter else {
            fatalError()
        }

        presenter.viewDidLoad()
    }

    func setup(topText: String) {
        balanceContainerHeightConstraint.constant = 0
        balanceContainerHeightConstraint.priority = UILayoutPriority(1000)
        topUpButton.isHidden = true

        // Setup view
        let doNotHaveAccountString = "Need to Top Up?"
        let createAccountString = "Click Here"

        let attributedTitle = NSMutableAttributedString(string: "\(doNotHaveAccountString) \(createAccountString)")
        attributedTitle.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSRange(location: 0, length: doNotHaveAccountString.count + 1))
        attributedTitle.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red, range: NSRange(location: doNotHaveAccountString.count + 1, length: createAccountString.count))

        topUpButton.titleLabel?.font = UIFont.with(size: .LargeMedium, weight: .bold)
        topUpButton.setAttributedTitle(attributedTitle, for: .normal)

        circularProgressBar.strokeColor = DevUtil.color(name: Colors.tmw_primary_background.rawValue)
        circularProgressBar.bgStrokeColor = .white
        circularProgressBar.lineWidth = 12

        timeLeftLabel.font = UIFont.with(size: .LargeMedium, weight: .bold)

        view.backgroundColor = .white

        topLabel.font = UIFont.with(size: .LargeMedium, weight: .bold)
        topLabel.textColor = UIColor.black
        topLabel.text = topText

        addDefaultTitleViewLogo()
    }

    func startAnimation(duration: Double, beginingPercentage: Double, completion: @escaping () -> Void) {
        circularProgressBar.setProgress(to: 0, withAnimation: true, begining: beginingPercentage, duration: duration, animationCompletion:
            completion)
    }

    func setTimeLeft(timeLeftString: String) {
        let expTimeTextStart = "EXPIRES IN "
        let expTimeTextEnd = " SECONDS"
        let expTime = "\(expTimeTextStart)\(timeLeftString)\(expTimeTextEnd)"

        let expTimeStartRange = NSRange(location: 0, length: expTimeTextStart.count)
        let expTimeValueRange = NSRange(location: expTimeStartRange.location + expTimeStartRange.length, length: timeLeftString.count)
        let expTimeEndRange = NSRange(location: expTimeValueRange.location + expTimeValueRange.length, length: expTimeTextEnd.count)

        let att = NSMutableAttributedString(string: expTime)
        att.addAttribute(NSAttributedString.Key.foregroundColor,
                         value: UIColor.black,
                         range: expTimeStartRange)
        att.addAttribute(NSAttributedString.Key.foregroundColor,
                         value: UIColor.red,
                         range: expTimeValueRange)
        att.addAttribute(NSAttributedString.Key.foregroundColor,
                         value: UIColor.black,
                         range: expTimeEndRange)
        att.addAttribute(NSAttributedString.Key.font,
                         value: UIFont.with(size: .Medium, weight: .bold) as Any,
                         range: expTimeStartRange)
        att.addAttribute(NSAttributedString.Key.font,
                         value: UIFont.with(size: .Medium, weight: .bold) as Any,
                         range: expTimeValueRange)
        att.addAttribute(NSAttributedString.Key.font,
                         value: UIFont.with(size: .Medium, weight: .bold) as Any,
                         range: expTimeEndRange)
        timeLeftLabel.attributedText = att
    }

    func hideQRView() {
        qrCodeImageView.isHidden = true
    }

    func showQRView() {
        qrCodeImageView.isHidden = false
    }
}

extension PaymentViewController {
    func askUserToRenewTheQR() {
        let popupHeight: CGFloat = 200.0
        popupView = UIView()
        popupView.addDefaultRoundCorners()
        popupView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        popupView.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.5)
        view.addSubview(popupView)

        let pv = UIView()
        pv.frame = CGRect(x: 20,
                          y: (view.frame.size.height - (view.frame.size.height / 2.5)) / 2,
                          width: view.frame.size.width - 40,
                          height: popupHeight)
        pv.backgroundColor = UIColor.white
        popupView.addSubview(pv)

        let headHeight: CGFloat = 45.0
        let headerView = UIView()
        headerView.frame = CGRect(x: 0, y: 0, width: pv.frame.size.width, height: headHeight)
        headerView.backgroundColor = DevUtil.color(name: Colors.tmw_primary_background.rawValue)
        pv.addSubview(headerView)

        let headerLabel1 = UILabel()
        headerLabel1.frame = CGRect(x: 20,
                                    y: 0,
                                    width: pv.frame.size.width - (headHeight + 20),
                                    height: headHeight)
        headerLabel1.text = "QR CODE TIMED OUT"
        headerLabel1.font = UIFont.with(size: .LightNormal1, weight: .normal)
        headerLabel1.textAlignment = NSTextAlignment.left
        headerLabel1.textColor = DevUtil.color(name: Colors.tmw_primary_background_text.rawValue)
        pv.addSubview(headerLabel1)

        let headerLabel = UILabel()
        headerLabel.frame = CGRect(x: 20,
                                   y: headerView.frame.size.height + 25,
                                   width: pv.frame.size.width - 40,
                                   height: 40)
        headerLabel.text = "GENERATE NEW QR CODE?".localized
        headerLabel.textAlignment = NSTextAlignment.left
        headerLabel.font = UIFont.with(size: .LightNormal1, weight: .normal)
        headerLabel.numberOfLines = 0
        headerLabel.textColor = UIColor.black
        pv.addSubview(headerLabel)

        let lineView = UIView()
        lineView.frame = CGRect(x: 20,
                                y: pv.frame.size.height - 65,
                                width: pv.frame.size.width - 10,
                                height: 1)
        lineView.backgroundColor = Constants.colorBorder

        let btnWidth = (pv.frame.size.width - 20 * 3) / 2

        let submitBtn = UIButton()
        submitBtn.addDefaultRoundCorners()
        submitBtn.frame = CGRect(x: pv.frame.size.width - btnWidth - 20,
                                 y: pv.frame.size.height - 70,
                                 width: btnWidth, height: 45)

        submitBtn.layer.borderWidth = 1
        submitBtn.layer.borderColor = DevUtil.color(name: Colors.tmw_primary_button_border.rawValue).cgColor
        submitBtn.backgroundColor = DevUtil.color(name: Colors.tmw_primary_button_background.rawValue)
        submitBtn.setTitle("CONFIRM", for: UIControl.State.normal)
        submitBtn.setTitleColor(DevUtil.color(name: Colors.tmw_primary_button_text.rawValue), for: UIControl.State.normal)
        submitBtn.titleLabel?.font = UIFont.with(size: .LargeMedium, weight: .primaryButton)
        submitBtn.addTarget(self, action: #selector(didTapRequestnewQRButton(sender:)), for: UIControl.Event.touchUpInside)
        pv.addSubview(submitBtn)

        let cancelBtn = UIButton()
        cancelBtn.addDefaultRoundCorners()

        cancelBtn.layer.borderWidth = 1
        cancelBtn.layer.borderColor = DevUtil.color(name: Colors.tmw_secondary_button_border.rawValue).cgColor
        cancelBtn.frame = CGRect(x: 20, y: pv.frame.size.height - 70, width: btnWidth, height: 45)
        cancelBtn.backgroundColor = DevUtil.color(name: Colors.tmw_secondary_button_background.rawValue)
        cancelBtn.setTitle("CANCEL", for: UIControl.State.normal)
        cancelBtn.setTitleColor(DevUtil.color(name: Colors.tmw_secondary_button_text.rawValue), for: UIControl.State.normal)
        cancelBtn.titleLabel?.font = UIFont.with(size: .LargeMedium, weight: .secondaryButton)
        cancelBtn.addTarget(self, action: #selector(didTapCancelButton(sender:)), for: UIControl.Event.touchUpInside)
        pv.addSubview(cancelBtn)
    }

    @objc
    func didTapRequestnewQRButton(sender: UIButton) {
        guard let presenter = presenter else {
            assert(false)
            return
        }

        presenter.didTapConfirmQRRenew()
        popupView.removeFromSuperview()
    }

    @objc
    func didTapCancelButton(sender: UIButton) {
        guard let presenter = presenter else {
            assert(false)
            return
        }

        presenter.didTapCancelQRRenew()
        popupView.removeFromSuperview()
    }
}
