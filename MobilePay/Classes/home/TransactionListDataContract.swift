//
//  TransactionListDataContract.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 19/03/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation

protocol TransactionListDataContract {
    func store(transactions: [Transaction])

    func transactions() -> [Transaction]
}
