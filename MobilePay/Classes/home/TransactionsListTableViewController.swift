//
//  TransactionsListTableViewController.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 30/12/2019.
//  Copyright © 2019 JadePayments. All rights reserved.
//

import UIKit

class TransactionsListTableViewController: UITableViewController, TransactionsListTableViewControllerContract {
    @IBOutlet var transactionDetailsLabel: UILabel!
    @IBOutlet var transactionAmmountLabel: UILabel!
    @IBOutlet var openTransactionDetailsLabel: UILabel!

    var transactions = [Transaction]()

    var presenter: TransactionsListTablePresenterContract?

    override func viewDidLoad() {
        super.viewDidLoad()

        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }

        guard let presenter = presenter else {
            assert(false)
            return
        }

        presenter.viewDidLoad()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        presenter?.viewDidAppear()
    }

    func setup() {
        transactionDetailsLabel.font = UIFont.with(size: .LargeMedium, weight: .normal)
        transactionDetailsLabel.textColor = .black
        transactionAmmountLabel.font = transactionDetailsLabel.font
        transactionAmmountLabel.textColor = .black
        openTransactionDetailsLabel.font = transactionDetailsLabel.font
        openTransactionDetailsLabel.textColor = .black
    }

    func updateWith(transactions: [Transaction]) {
        self.transactions = transactions
        tableView.reloadData()
    }

    func showError(message: String) {
        showAlert(message: message)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return transactions.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "reusableCell", for: indexPath) as? TransactionTableViewCell else {
            return UITableViewCell()
        }

        let transaction = transactions[indexPath.row]
        cell.setup(transaction: transaction, currencySymbol: presenter!.currencySymbol)

        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let presenter = presenter else {
            assert(false)
            return
        }

        presenter.didSelectItem(at: indexPath.row)
        tableView.deselectRow(at: indexPath, animated: true)
    }

    @IBAction func refreshStarted(_ sender: Any) {
        guard let presenter = presenter else {
            assert(false)
            return
        }

        presenter.refreshData()
    }

    func showLoading() {
        refreshControl?.beginRefreshing()
    }

    func hideLoading() {
        refreshControl?.endRefreshing()
    }
}
