//
//  TransactionsListTablePresenterContract.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 30/12/2019.
//  Copyright © 2019 JadePayments. All rights reserved.
//

import Foundation

protocol TransactionsListTablePresenterContract {
    var currencySymbol: String { get }

    func viewDidLoad()

    func viewDidAppear()

    func refreshData()

    func didSelectItem(at: Int)
}
