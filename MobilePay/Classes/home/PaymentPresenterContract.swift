//
//  PaymentPresenterProtocol.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 19/11/2019.
//  Copyright © 2019 JadePayments. All rights reserved.
//

import Foundation

protocol PaymentPresenterContract: AnyObject {
    func viewDidLoad()

    func viewWillAppear()

    func viewWillDisappear(isMovingFromParent: Bool)

    func requestNewQR()

    func cancel()

    func didTapConfirmQRRenew()

    func didTapCancelQRRenew()
}
