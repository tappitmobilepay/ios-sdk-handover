//
//  TransactionsListTableViewControllerContract.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 30/12/2019.
//  Copyright © 2019 JadePayments. All rights reserved.
//

import Foundation

protocol TransactionsListTableViewControllerContract: AnyObject {
    var presenter: TransactionsListTablePresenterContract? { get set }

    func setup()

    func updateWith(transactions: [Transaction])

    func showError(message: String)

    func showLoading()

    func hideLoading()
}
