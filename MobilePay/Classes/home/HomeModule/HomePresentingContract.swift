//
//  HomePresentingContract.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 24/02/2021.
//  Copyright © 2021 JadePayments. All rights reserved.
//

import Foundation

protocol HomePresentingContract {
    func viewDidLoad()

    func viewWillAppear()

    func viewDidAppear()

    func didTapPay()

    func didTapMyOffers()

    func didTapPaymentMethods()

    func didTapTransactionsHistory()

    func didTapSettings()

    func didTapHelp()

    func didTapDeepLink()
}
