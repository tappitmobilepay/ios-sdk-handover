//
//  HomeViewingContract.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 24/02/2021.
//  Copyright © 2021 JadePayments. All rights reserved.
//

import Foundation

protocol HomeViewingContract: AnyObject {
    var presenter: HomePresentingContract! { set get }

    func update(data: [HomeCellBranding])
}
