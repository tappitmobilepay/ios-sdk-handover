//
//  HomeViewController.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 16/12/2019.
//  Copyright © 2019 JadePayments. All rights reserved.
//

import CoreData
import UIKit

struct HomeCellBranding {
    var isRegular: Bool = true
    let backgroundColor: UIColor
    let textColor: UIColor
    let font: UIFont
    let leftTitle: String
    let rightTitle: String?
    let image: UIImage?
    let badgeNumber: Int
    let borderColor: UIColor
    let leftAction: () -> Void
    let rightAction: (() -> Void)?
}

class HomeViewController: BaseScreenViewController, HomeViewingContract {
    var presenter: HomePresentingContract!

    var data: [HomeCellBranding] = []

    @IBOutlet var tableView: UITableView!
    @IBOutlet var mainTitleLabel: TitleLabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }

        presenter.viewDidLoad()
    }

    func update(data: [HomeCellBranding]) {
        tableView.dataSource = self
        tableView.delegate = self
        mainTitleLabel.textColor = DevUtil.color(name: Colors.tmw_page_header_text.rawValue)
        UINavigationBar.appearance().barTintColor = DevUtil.color(name: Colors.tmw_navigation_background.rawValue)

        addDefaultTitleViewLogo()

        self.data = data
        tableView.reloadData()
    }

    override func viewDidAppear(_ animated: Bool) {
        hideBackButton()
        navigationController?.setNavigationBarHidden(false, animated: true)

        tableView.reloadData()
        presenter.viewDidAppear()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        hideBackButton()
        navigationController?.setNavigationBarHidden(false, animated: true)
        presenter.viewWillAppear()
    }

    @IBAction func didTapPayButton(_ sender: Any) {
        presenter.didTapPay()
    }

    @IBAction func didTapPaymentMethodsButton(_ sender: UIButton) {
        presenter.didTapPaymentMethods()
    }

    @IBAction func didTapTransactionsButton(_ sender: Any) {
        presenter.didTapTransactionsHistory()
    }

    @IBAction func didTapProfileButton(_ sender: Any) {
        presenter.didTapSettings()
    }

    @IBAction func didTapHelpButton(_ sender: Any) {
        presenter.didTapHelp()
    }

    @IBAction func didTapDeepLinkButton(_ sender: Any) {
        presenter.didTapDeepLink()
    }

    @IBAction func didTapGiveawayTokensButton(_ sender: Any) {
        presenter.didTapMyOffers()
    }
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let branding = data[indexPath.row]

        // Image cell
        if let _ = branding.image {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "\(HomeImageTableViewCell.self)") as? HomeImageTableViewCell else {
                return UITableViewCell()
            }
            cell.setup(branding: branding)
            return cell
        }
        // Double cell
        else if let _ = branding.rightTitle {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "\(HomeDoubleTableViewCell.self)") as? HomeDoubleTableViewCell else {
                return UITableViewCell()
            }
            cell.setup(branding: branding)
            return cell
        }
        // Single regular size cell
        else if branding.isRegular {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "\(HomeSingleTableViewCell.self)") as? HomeSingleTableViewCell else {
                return UITableViewCell()
            }
            cell.setup(branding: branding)
            return cell
        }
        // Single large size cell
        else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "\(HomeBigSingleTableViewCell.self)") as? HomeBigSingleTableViewCell else {
                return UITableViewCell()
            }
            cell.setup(branding: branding)
            return cell
        }
    }
}
