//
//  HomePresenter.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 24/02/2021.
//  Copyright © 2021 JadePayments. All rights reserved.
//

import Foundation
import UIKit

class HomePresenter: HomePresentingContract {
    weak var view: HomeViewingContract?
    let router: RouterContract
    let db: DatabaseManagementContract
    let paymentsService: PaymentsWebServiceContract
    var checkedPaymentConfigurations = false
    var paymentFlowManager: PaymentFlowManagementContract

    init(view: HomeViewingContract,
         router: RouterContract,
         db: DatabaseManagementContract,
         paymentsService: PaymentsWebServiceContract,
         paymentFlowManager: PaymentFlowManagementContract)
    {
        self.view = view
        self.router = router
        self.db = db
        self.paymentsService = paymentsService
        self.paymentFlowManager = paymentFlowManager
    }

    func viewDidLoad() {}

    func reload() {
        var items = [HomeCellBranding]()
        // Pay button
        let payBranding = HomeCellBranding(isRegular: false,
                                           backgroundColor: DevUtil.color(name: Colors.tmw_pay_button_background.rawValue),
                                           textColor: DevUtil.color(name: Colors.tmw_pay_button_text.rawValue),
                                           font: UIFont.with(size: .BigBigBig, weight: .payButton),
                                           leftTitle: "PAY",
                                           rightTitle: nil,
                                           image: nil,
                                           badgeNumber: 0,
                                           borderColor: DevUtil.color(name: Colors.tmw_pay_button_border.rawValue),
                                           leftAction: { [weak self] in
                                               self?.didTapPay()
                                           },
                                           rightAction: nil)
        items.append(payBranding)
        let clientConfig = db.clientConfig()
        if clientConfig?.configuration.settings.showMyOffers == .some(true) {
            let giveawayTokens = db.giveawayTokens().count
            let complimentaryBalances = db.complimentaryBalances().count
            let campaigns = db.campaigns(type: .notRedeemed).count
            let finalCount = giveawayTokens + complimentaryBalances + campaigns
            let myOffersBranding = HomeCellBranding(backgroundColor: DevUtil.color(name: Colors.tmw_home_button_background.rawValue),
                                                    textColor: DevUtil.color(name: Colors.tmw_home_button_text.rawValue),
                                                    font: UIFont.with(size: .Large, weight: .homeButton),
                                                    leftTitle: "MY OFFERS",
                                                    rightTitle: nil,
                                                    image: nil,
                                                    badgeNumber: finalCount,
                                                    borderColor: DevUtil.color(name: Colors.tmw_home_button_border.rawValue),
                                                    leftAction: { [weak self] in
                                                        self?.didTapMyOffers()
                                                    },
                                                    rightAction: nil)
            items.append(myOffersBranding)
        }

        if clientConfig?.configuration.settings.showGiftCard == .some(true) {
            let giftCardsCount = db.giftcards().count
            let giftCardsBranding = HomeCellBranding(backgroundColor: DevUtil.color(name: Colors.tmw_home_button_background.rawValue),
                                                     textColor: DevUtil.color(name: Colors.tmw_home_button_text.rawValue),
                                                     font: UIFont.with(size: .Large, weight: .homeButton),
                                                     leftTitle: "GIFT CARD",
                                                     rightTitle: nil,
                                                     image: nil,
                                                     badgeNumber: giftCardsCount,
                                                     borderColor: DevUtil.color(name: Colors.tmw_home_button_border.rawValue),
                                                     leftAction: { [weak self] in
                                                         self?.didTapGiftCards()
                                                     },
                                                     rightAction: nil)
            items.append(giftCardsBranding)
        }

        let paymentMethodsBranding = HomeCellBranding(backgroundColor: DevUtil.color(name: Colors.tmw_home_button_background.rawValue),
                                                      textColor: DevUtil.color(name: Colors.tmw_home_button_text.rawValue),
                                                      font: UIFont.with(size: .Large, weight: .homeButton),
                                                      leftTitle: "MY PAYMENT METHOD",
                                                      rightTitle: nil,
                                                      image: nil,
                                                      badgeNumber: 0,
                                                      borderColor: DevUtil.color(name: Colors.tmw_home_button_border.rawValue),
                                                      leftAction: { [weak self] in
                                                          self?.didTapPaymentMethods()
                                                      },
                                                      rightAction: nil)
        items.append(paymentMethodsBranding)

        let transactionsHistory = HomeCellBranding(backgroundColor: DevUtil.color(name: Colors.tmw_home_button_background.rawValue),
                                                   textColor: DevUtil.color(name: Colors.tmw_home_button_text.rawValue),
                                                   font: UIFont.with(size: .Large, weight: .homeButton),
                                                   leftTitle: "TRANSACTION HISTORY",
                                                   rightTitle: nil,
                                                   image: nil,
                                                   badgeNumber: 0,
                                                   borderColor: DevUtil.color(name: Colors.tmw_home_button_border.rawValue),
                                                   leftAction: { [weak self] in
                                                       self?.didTapTransactionsHistory()
                                                   },
                                                   rightAction: nil)
        items.append(transactionsHistory)

        let settingsHelpBranding = HomeCellBranding(backgroundColor: DevUtil.color(name: Colors.tmw_home_button_background.rawValue),
                                                    textColor: DevUtil.color(name: Colors.tmw_home_button_text.rawValue),
                                                    font: UIFont.with(size: .Large, weight: .homeButton),
                                                    leftTitle: "SETTINGS",
                                                    rightTitle: "HELP",
                                                    image: nil,
                                                    badgeNumber: 0,
                                                    borderColor: DevUtil.color(name: Colors.tmw_home_button_border.rawValue),
                                                    leftAction: { [weak self] in
                                                        self?.didTapSettings()
                                                    },
                                                    rightAction: { [weak self] in
                                                        self?.didTapHelp()
                                                    })
        items.append(settingsHelpBranding)

        if router.deepLink != nil {
            let deepLinkBranding = HomeCellBranding(backgroundColor: DevUtil.color(name: Colors.tmw_home_button_background.rawValue),
                                                    textColor: DevUtil.color(name: Colors.tmw_home_button_text.rawValue),
                                                    font: UIFont.with(size: .Large, weight: .homeButton),
                                                    leftTitle: "",
                                                    rightTitle: nil,
                                                    image: DevUtil.imageSkin(name: "tmw_deep_link"),
                                                    badgeNumber: 0,
                                                    borderColor: DevUtil.color(name: Colors.tmw_home_button_border.rawValue),
                                                    leftAction: { [weak self] in
                                                        self?.didTapDeepLink()
                                                    },
                                                    rightAction: nil)
            items.append(deepLinkBranding)
        }

        view?.update(data: items)
    }

    func viewWillAppear() {
        paymentFlowManager.currentState = .home
        reload()
    }

    func viewDidAppear() {
        Analytics.reportShowHomeScreenEvent()
    }

    func didTapPay() {
        Analytics.reportPressPayEvent()

        guard db.hasCreditCards() else {
            showAlert(message: "Add at least one payment method to proceed.", title: "No payment methods", okTitle: "Ok") { [weak self] in
                self?.router.presentMyPaymentMethods()
            }
            return
        }

        let paymentData = PaymentData()

        do {
            try paymentFlowManager.moveToNextState(paymentData: paymentData)
        }
        catch {
            assert(false)
        }
    }

    func didTapGiftCards() {
        router.presentGiftCards(paymentData: PaymentData(), mode: .viewOnly, removingLast: false)
    }

    func didTapPaymentMethods() {
        router.presentMyPaymentMethods()
    }

    func didTapTransactionsHistory() {
        router.presentTransactionsHistory()
    }

    func didTapSettings() {
        router.presentSettings()
    }

    func didTapDeepLink() {
        Analytics.reportCustomLinkOnHomepagePressed()
        if let dl = router.deepLink {
            UIApplication.shared.open(dl.url, options: [:], completionHandler: nil)
        }
    }

    func didTapMyOffers() {
        router.presentGiveawayTokens(paymentData: PaymentData(), mode: .viewOnly)
    }

    func didTapHelp() {
        router.presentHelp()
    }
}
