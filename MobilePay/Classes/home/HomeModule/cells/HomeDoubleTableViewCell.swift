//
//  HomeDoubleTableViewCell.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 2021-08-03.
//  Copyright © 2021 JadePayments. All rights reserved.
//

import Foundation
import UIKit

class HomeDoubleTableViewCell: UITableViewCell {
    @IBOutlet var leftButton: UIButton!
    @IBOutlet var rightButton: UIButton!

    var leftAction: () -> Void = {}
    var rightAction: () -> Void = {}

    override func awakeFromNib() {
        super.awakeFromNib()

        leftButton.addDefaultRoundCorners()
        rightButton.addDefaultRoundCorners()
    }

    func setup(branding: HomeCellBranding) {
        leftButton.layer.borderWidth = 1
        leftButton.layer.borderColor = branding.borderColor.cgColor

        rightButton.layer.borderWidth = 1
        rightButton.layer.borderColor = branding.borderColor.cgColor

        leftButton.backgroundColor = branding.backgroundColor
        leftButton.titleLabel?.font = branding.font
        leftButton.setTitle(branding.leftTitle, for: .normal)
        leftButton.setTitleColor(branding.textColor, for: .normal)
        leftAction = branding.leftAction

        rightButton.backgroundColor = branding.backgroundColor
        rightButton.titleLabel?.font = branding.font
        rightButton.setTitle(branding.rightTitle, for: .normal)
        rightButton.setTitleColor(branding.textColor, for: .normal)
        if let ra = branding.rightAction {
            rightAction = ra
        }
    }

    @IBAction func didPressLeftButton(_ sender: UIButton) {
        leftAction()
    }

    @IBAction func didPressRightButton(_ sender: UIButton) {
        rightAction()
    }
}
