//
//  HomeImageTableViewCell.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 2021-08-03.
//  Copyright © 2021 JadePayments. All rights reserved.
//

import Foundation
import UIKit

class HomeImageTableViewCell: UITableViewCell {
    @IBOutlet var button: UIButton!

    var buttonAction: () -> Void = {}

    override func awakeFromNib() {
        super.awakeFromNib()

        button.addDefaultRoundCorners()
    }

    func setup(branding: HomeCellBranding) {
        button.setBackgroundImage(branding.image, for: .normal)
        buttonAction = branding.leftAction
    }

    @IBAction func didPressButton(_ sender: UIButton) {
        buttonAction()
    }
}
