//
//  HomeSingleTableViewCell.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 2021-08-03.
//  Copyright © 2021 JadePayments. All rights reserved.
//

import Foundation
import UIKit

class HomeSingleTableViewCell: UITableViewCell {
    @IBOutlet var badgeView: UIView!
    @IBOutlet var button: UIButton!

    var buttonAction: () -> Void = {}
//    var badge: BadgeHub!

    override func awakeFromNib() {
        super.awakeFromNib()

        button.addDefaultRoundCorners()
        badgeView.backgroundColor = .clear
    }

    func setup(branding: HomeCellBranding) {
        button.layer.borderWidth = 1
        button.layer.borderColor = branding.borderColor.cgColor

        button.backgroundColor = branding.backgroundColor
        button.titleLabel?.font = branding.font
        button.setTitle(branding.leftTitle, for: .normal)
        button.setTitleColor(branding.textColor, for: .normal)

        buttonAction = branding.leftAction
        if branding.badgeNumber > 0 {
            badgeView.isHidden = false
            var appearance = BadgeAppearance()
            appearance.backgroundColor = DevUtil.color(name: Colors.tmw_notification_badge_background.rawValue)
            appearance.textColor = DevUtil.color(name: Colors.tmw_notification_badge_text.rawValue)
            appearance.font = UIFont.with(size: .LargeMedium, weight: .normal)
            appearance.animate = false
            appearance.duration = 0
            appearance.distanceFromCenterX = UIScreen.main.bounds.size.width * 0.5 - 20
            badgeView.badge(text: "\(branding.badgeNumber)", appearance: appearance)
        }
        else {
            badgeView.isHidden = true
            badgeView.badge(text: nil)
        }
    }

    @IBAction func didPressButton(_ sender: UIButton) {
        buttonAction()
    }
}
