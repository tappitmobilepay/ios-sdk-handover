////
////  TransactionListDataManager.swift
////  MobilePay
////
////  Created by Marius Kurgonas on 19/03/2020.
////  Copyright © 2020 JadePayments. All rights reserved.
////
//
// import CoreData
// import Foundation
//
// class TransactionListDataManager: TransactionListDataContract {
//    func store(transactions: [Transaction]) {
//        removeTransactionList()
//        saveDataToDB(transactions: transactions.map { transactionToManagedTransaction(transaction: $0) })
//    }
//
//    func transactions() -> [Transaction] {
//        return fetchTransactionList().map { managedTransactionToTransaction(managedTransaction: $0) }
//    }
//
//    func managedTransactionToTransaction(managedTransaction: TransactionList) -> Transaction {
//        let items = managedTransaction.items!.allObjects as! [TransactionListItem]
//        return Transaction(id: managedTransaction.id,
//                           alt_tran_id: managedTransaction.alt_tran_id,
//                           amount: managedTransaction.amount,
//                           auth_code: managedTransaction.auth_code,
//                           available_balance: managedTransaction.available_balance,
//                           credit: managedTransaction.credit,
//                           currency_code: managedTransaction.currency_code,
//                           date: managedTransaction.date,
//                           description: managedTransaction.description1,
//                           location: managedTransaction.location,
//                           transaction_status: managedTransaction.transaction_status,
//                           transaction_type: managedTransaction.transaction_type,
//                           item_json: items.map { managedItemToItem(managedItem: $0) })
//    }
//
//    func transactionToManagedTransaction(transaction: Transaction) -> TransactionList {
//        let managedTransaction = TransactionList(context: TMWCommonUtil.persistentContainer.viewContext)
//        managedTransaction.id = transaction.id
//        managedTransaction.alt_tran_id = transaction.alt_tran_id
//        managedTransaction.amount = transaction.amount
//        managedTransaction.auth_code = transaction.auth_code
//        managedTransaction.available_balance = transaction.available_balance
//        managedTransaction.credit = transaction.credit
//        managedTransaction.currency_code = transaction.currency_code
//        managedTransaction.date = transaction.date
//        managedTransaction.description1 = transaction.description
//        managedTransaction.location = transaction.location
//        managedTransaction.transaction_status = transaction.transaction_status
//        managedTransaction.transaction_type = transaction.transaction_type
//        managedTransaction.items = NSSet(array: transaction.item_json.map { itemToManagedItem(item: $0) })
//
//        return managedTransaction
//    }
//
//    func managedItemToItem(managedItem: TransactionListItem) -> TransactionItem {
//        return TransactionItem(price: managedItem.price!, quantity: managedItem.quantity!, item_name: managedItem.name!)
//    }
//
//    func itemToManagedItem(item: TransactionItem) -> TransactionListItem {
//        let managedItem = TransactionListItem(context: TMWCommonUtil.persistentContainer.viewContext)
//        managedItem.price = item.price
//        managedItem.quantity = item.quantity
//        managedItem.name = item.item_name
//
//        return managedItem
//    }
// }
//
// extension TransactionListDataManager {
//    func saveDataToDB(transactions: [TransactionList]) { //
//        do {
//            try TMWCommonUtil.persistentContainer.viewContext.save()
//        }
//        catch {
//            assert(false)
//            // TODO: TransactionListDataManager - Handle error when we could not save data to DB
//        }
//    }
//
//    func removeTransactionList() {
//        let fetchRequest: NSFetchRequest<TransactionList> = TransactionList.fetchRequest()
//        do {
//            let searchResults = try? TMWCommonUtil.persistentContainer.viewContext.fetch(fetchRequest)
//            if let results = searchResults {
//                for i in 0..<results.count {
//                    let cards = results[i]
//                    TMWCommonUtil.persistentContainer.viewContext.delete(cards)
//                }
//            }
//        }
//    }
//
//    func fetchTransactionList() -> [TransactionList] {
//        let fetchRequest: NSFetchRequest<TransactionList> = TransactionList.fetchRequest()
//
//        if let searchResults = try? TMWCommonUtil.persistentContainer.viewContext.fetch(fetchRequest) {
//            return searchResults
//        }
//
//        return []
//    }
// }
