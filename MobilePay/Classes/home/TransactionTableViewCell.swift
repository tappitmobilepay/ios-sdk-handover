//
//  TransactionTableViewCell.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 30/12/2019.
//  Copyright © 2019 JadePayments. All rights reserved.
//

import UIKit

class TransactionTableViewCell: UITableViewCell {
    @IBOutlet private var bdView: UIView!
    @IBOutlet private var transactionTypeLabel: UILabel!
    @IBOutlet var concessionaireNameLabel: UILabel!
    @IBOutlet private var transactionAmmountLabel: UILabel!
    @IBOutlet private var transactionDetailImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        transactionTypeLabel.font = UIFont.with(size: .Large, weight: .normal)
        transactionTypeLabel.textColor = DevUtil.color(name: Colors.tmw_transaction_card_text.rawValue)
        transactionAmmountLabel.font = UIFont.with(size: .LargeMedium, weight: .normal)
        transactionAmmountLabel.textColor = DevUtil.color(name: Colors.tmw_transaction_card_text.rawValue)
        transactionDetailImageView.image = DevUtil.imageSkin(name: "tmw_transaction_details")?.withRenderingMode(.alwaysTemplate)
        concessionaireNameLabel.font = UIFont.with(size: .LightNormal1, weight: .normal)
        concessionaireNameLabel.textColor = DevUtil.color(name: Colors.tmw_transaction_card_text.rawValue)
        transactionDetailImageView.tintColor = DevUtil.color(name: Colors.tmw_transaction_card_text.rawValue)
        bdView.addDefaultRoundCorners()
        bdView.backgroundColor = DevUtil.color(name: Colors.tmw_transaction_card_background.rawValue)
        bdView.layer.borderWidth = 1
        bdView.layer.borderColor = DevUtil.color(name: Colors.tmw_transaction_card_border.rawValue).cgColor
    }

    func setup(transaction: Transaction, currencySymbol: String) {
        let transType = TransactionType(rawValue: transaction.type.double())?.toString() ?? "\(transaction.type.int())"
        transactionTypeLabel.text = transType.uppercased()
        concessionaireNameLabel.text = transaction.concessionaire
        let totalAmountString = "\(currencySymbol) " + transaction.chargedAmount.double().format(f: ".2")
        transactionAmmountLabel.text = totalAmountString

        transactionDetailImageView.isHidden = transaction.items.isEmpty
    }
}
