//
//  BalancePresenter.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 08/07/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation

class BalancePresenter: BalancePresentingContract {
    weak var view: BalanceViewContract?
    let balanceProvider: BalanceProvidingContract
    let service: UserWebServiceContract
    let db: DatabaseManager

    init(view: BalanceViewContract,
         balanceProvider: BalanceProvidingContract,
         service: UserWebServiceContract,
         db: DatabaseManager)
    {
        self.view = view
        self.balanceProvider = balanceProvider
        self.service = service
        self.db = db
    }

    func viewDidLoad() {
        updateBalance()
    }

    func updateBalance() {
//        if let balance = balanceProvider.balance {
//            view?.update(balance: balance)
//        }

        service.cancel()
        service.requestBalance { [weak self] balance, error in
            if let balance = balance {
//                self?.db.save(balance: balance)
                self?.view?.update(balance: balance)
            }
            else if let _ = error {}
        }
    }
}
