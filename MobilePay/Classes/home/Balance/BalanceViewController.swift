//
//  BalanceViewController.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 19/12/2019.
//  Copyright © 2019 JadePayments. All rights reserved.
//

import UIKit

class BalanceViewController: UIViewController, BalanceViewContract {
    @IBOutlet var balanceTitleLabel: UILabel!
    @IBOutlet var balanceValueLabel: UILabel!
    @IBOutlet var updatedAtLabel: UILabel!

    var presenter: BalancePresentingContract?

    override func viewDidLoad() {
        super.viewDidLoad()

        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }

        updatedAtLabel.text = "Turn on internet to update."
        updatedAtLabel.textColor = .lightGray
        updatedAtLabel.font = UIFont.with(size: .Medium, weight: .normal)

        balanceTitleLabel.text = "Comp Balance"
        balanceTitleLabel.textColor = .black
        balanceTitleLabel.font = UIFont.with(size: .BigNormal, weight: .normal)

        balanceValueLabel.font = UIFont.with(size: .BigBig, weight: .bold)
        balanceValueLabel.textColor = .black
        balanceValueLabel.text = "0.0"

        updateBalanceLabel()
    }

    func updateBalanceLabel() {
        guard let presenter = presenter else {
            assert(false)
            return
        }

        presenter.updateBalance()
    }

    func update(balance: Balance) {
        let symbol = Util.Tappit.getSymbolForCurrencyCode(code: balance.currency)
        balanceValueLabel.text = "\(symbol) \(balance.preloadAmount)"
//        if let date = balance.updateFromServerAt {
//            updatedAtLabel.text = "Expiry date: \(date)"
//        }
    }
}
