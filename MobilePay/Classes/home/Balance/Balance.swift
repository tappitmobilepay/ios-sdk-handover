//
//  Balance.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 08/07/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation

struct Balance: Codable {
    let currency: String
    let preloadAmount: Decimal
    let complimentaryBalances: [ComplimentaryBalance]
}

struct ComplimentaryBalance: Codable {
    let id: String
    let slot: Int?
    let concessionaires: [String]
    let amount: Decimal
    let expiryDate: String?
    let type: Int
    let description: String
}

extension Balance {
    static func from(managedBalance: ManagedBalance) -> Balance {
        return Balance(currency: managedBalance.currency!,
                       preloadAmount: managedBalance.preloadAmount!.decimalValue,
                       complimentaryBalances: managedBalance.complimentaryBalances!.map { ComplimentaryBalance.from(managedComplimentaryBalance: $0 as! ManagedComplimentaryBalance) })
    }
}

extension ComplimentaryBalance {
    static func from(managedComplimentaryBalance: ManagedComplimentaryBalance) -> ComplimentaryBalance {
        return ComplimentaryBalance(id: managedComplimentaryBalance.id!,
                                    slot: Int(managedComplimentaryBalance.slot),
                                    concessionaires: (managedComplimentaryBalance.concessionaires as! NSSet).allObjects as! [String],
                                    amount: managedComplimentaryBalance.amount!.decimalValue,
                                    expiryDate: managedComplimentaryBalance.expiryDate,
                                    type: Int(managedComplimentaryBalance.type),
                                    description: managedComplimentaryBalance.details!)
    }
}
