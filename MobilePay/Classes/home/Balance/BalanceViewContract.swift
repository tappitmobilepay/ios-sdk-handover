//
//  BalanceViewContract.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 08/07/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation

protocol BalanceViewContract: AnyObject {
    var presenter: BalancePresentingContract? { get set }

    func update(balance: Balance)
}
