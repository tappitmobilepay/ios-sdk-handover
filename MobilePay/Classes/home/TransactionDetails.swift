//
//  TransactionDetail.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 18/03/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation
import UIKit

class TransactionDetails: UIViewController {
    @IBOutlet private var tableView: UITableView!
    @IBOutlet private var offersDiscountTitleLabel: UILabel!
    @IBOutlet private var offersDiscountValueLabel: UILabel!
    @IBOutlet private var concessionaireLabel: UILabel!
    @IBOutlet private var titleLabel: UILabel!
    @IBOutlet private var closeButton: UIButton!
    @IBOutlet private var referenceLabel: UILabel!
    @IBOutlet private var dateLabel: UILabel!
    @IBOutlet private var topLineView: UIView!
    @IBOutlet private var totalLabel: UILabel!
    @IBOutlet private var amountLabel: UILabel!
    @IBOutlet private var bottomLineView: UIView!
    @IBOutlet private var itemsTitleLabel: UILabel!
    @IBOutlet private var quantityTitleLabel: UILabel!
    @IBOutlet private var priceLabel: UILabel!
    @IBOutlet private var containerView: UIView!
    @IBOutlet private var itemsAmountLabel: UILabel!
    @IBOutlet private var itemsAmountValueLabel: UILabel!
    @IBOutlet private var balanceLabel: UILabel!
    @IBOutlet private var balanceAmountLabel: UILabel!
    @IBOutlet private var tipsLabel: UILabel!
    @IBOutlet private var tipsAmountLabel: UILabel!
    @IBOutlet private var discountsTitleLabel: UILabel!
    @IBOutlet private var discountsValueLabel: UILabel!
    @IBOutlet private var donationsTitleLabel: UILabel!
    @IBOutlet private var donationsValueLabel: UILabel!
    @IBOutlet private var taxesTitleLabel: UILabel!
    @IBOutlet private var taxesValueLabel: UILabel!

    var transaction: Transaction? {
        didSet {
            setupView()
        }
    }

    var currencySymbol: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }

        containerView.addDefaultRoundCorners()

        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableView.automaticDimension
        tableView.delegate = self
        tableView.dataSource = self

        setupView()
    }

    func setupView() {
        if isViewLoaded, let transaction = transaction {
            if #available(iOS 13.0, *) {
                overrideUserInterfaceStyle = .light
            }

            itemsAmountLabel.text = "Items total"
            itemsAmountLabel.font = UIFont.with(size: .Medium, weight: .normal)
            balanceLabel.text = "Complimentary balance"
            balanceLabel.font = itemsAmountLabel.font
            tipsLabel.text = "Tips"
            tipsLabel.font = itemsAmountLabel.font
            offersDiscountTitleLabel.text = "Offers"
            offersDiscountTitleLabel.font = itemsAmountLabel.font
            donationsTitleLabel.text = "Donations"
            donationsTitleLabel.font = itemsAmountLabel.font
            discountsTitleLabel.text = "Discounts"
            discountsTitleLabel.font = itemsAmountLabel.font
            taxesTitleLabel.text = "Taxes"
            taxesTitleLabel.font = itemsAmountLabel.font

            itemsAmountValueLabel.text = "\(currencySymbol) \(transaction.amount.format(f: ".2"))"
            itemsAmountValueLabel.font = itemsAmountLabel.font
            let complimentaryBalancesAmounts = transaction.complimentaryBalanceUsage.map { $0.charged }
            let complimentaryCharge = complimentaryBalancesAmounts.reduce(0, +)
            balanceAmountLabel.text = "\(currencySymbol) \(complimentaryCharge.format(f: ".2"))"
            balanceAmountLabel.font = itemsAmountLabel.font
            tipsAmountLabel.text = "\(currencySymbol) \(transaction.tip.double().format(f: ".2"))"
            tipsAmountLabel.font = itemsAmountLabel.font
            offersDiscountValueLabel.text = "\(currencySymbol) \(transaction.tokenAmount.format(f: ".2"))"
            offersDiscountValueLabel.font = itemsAmountLabel.font
            donationsValueLabel.text = "\(currencySymbol) \(transaction.donationAmount.format(f: ".2"))"
            donationsValueLabel.font = itemsAmountLabel.font
            discountsValueLabel.text = "\(currencySymbol) \(transaction.discountAmount.format(f: ".2"))"
            discountsValueLabel.font = itemsAmountLabel.font
            taxesValueLabel.text = "\(currencySymbol) \(transaction.taxAmount.format(f: ".2"))"
            taxesValueLabel.font = itemsAmountLabel.font
            concessionaireLabel.font = UIFont.with(size: .LightNormal1, weight: .normal)
            concessionaireLabel.textColor = .black
            concessionaireLabel.text = transaction.concessionaire
            closeButton.setImage(DevUtil.imageSkin(name: "tmw_close"), for: .normal)
            closeButton.tintColor = DevUtil.color(name: Colors.tmw_primary_background.rawValue)
            titleLabel.textColor = DevUtil.color(name: Colors.tmw_primary_background.rawValue)
            titleLabel.font = UIFont.with(size: .LightNormal1, weight: .normal)
            referenceLabel.text = "REF# \(transaction.clientTransactionId)"
            referenceLabel.font = UIFont.with(size: .LightNormal1, weight: .normal)
            let transactionDate = transaction.date.replacingOccurrences(of: "T", with: " ")
                .replacingOccurrences(of: "Z", with: "")
            let dateString = transactionDate.toDate(dateFormat: "yyyy-MM-dd HH:mm:ss.SSSSSS")?.toString() ?? transactionDate
            dateLabel.text = "Date: " + dateString
            dateLabel.font = UIFont.with(size: .LightNormal1, weight: .normal)
            totalLabel.text = "Total"
            totalLabel.font = UIFont.with(size: .Large, weight: .bold)
            totalLabel.textColor = DevUtil.color(name: Colors.tmw_primary_background.rawValue)
            amountLabel.text = "\(currencySymbol) \(transaction.chargedAmount.double().format(f: ".2"))"
            amountLabel.textColor = DevUtil.color(name: Colors.tmw_primary_background.rawValue)
            amountLabel.font = UIFont.with(size: .Large, weight: .bold)
            topLineView.backgroundColor = .black
            bottomLineView.backgroundColor = .black
            itemsTitleLabel.font = UIFont.with(size: .LightNormal1, weight: .normal)
            quantityTitleLabel.font = UIFont.with(size: .LightNormal1, weight: .normal)
            priceLabel.font = UIFont.with(size: .LightNormal1, weight: .normal)
            tableView.reloadData()
        }
    }

    @IBAction private func didTapCloseButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}

extension TransactionDetails: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let transaction = transaction else {
            return 0
        }

        return transaction.items.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "transactionDetailsCell") as? TransactionItemTableViewCell, let transaction = transaction else {
            assert(false)
            return UITableViewCell()
        }

        cell.setup(item: transaction.items[indexPath.row], currencyCode: transaction.currency)

        return cell
    }
}
