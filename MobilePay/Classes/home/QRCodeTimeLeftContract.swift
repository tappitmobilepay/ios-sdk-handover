//
//  QRCodeTimeLeftContract.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 22/11/2019.
//  Copyright © 2019 JadePayments. All rights reserved.
//

import Foundation

protocol QRCodeTimeLeftContract: AnyObject {
    var endInterval: TimeInterval { set get }
}
