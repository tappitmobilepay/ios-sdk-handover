//
//  PaymentFlowManager.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 5/18/21.
//  Copyright © 2021 JadePayments. All rights reserved.
//

import Foundation

enum PaymentState { case
    home,
    tokens,
    tips,
    giftcards,
    payQR
}

enum PaymentFlowError: Error { case
    illegalTransitionRequest
}

class PaymentFlowManager: PaymentFlowManagementContract {
    var currentState: PaymentState
    private let db: DatabaseManagementContract
    private let credStore: CredentialsStoringContract
    private let router: RouterContract

    init(currentState: PaymentState,
         db: DatabaseManagementContract,
         credStore: CredentialsStoringContract,
         router: RouterContract)
    {
        self.currentState = currentState
        self.db = db
        self.credStore = credStore
        self.router = router
    }

    private let statesRelationships: [PaymentState: [PaymentState]] = [
        .home: [.tokens, .tips, .giftcards, .payQR],
        .tokens: [.tips, .giftcards, .payQR],
        .tips: [.giftcards, .payQR],
        .giftcards: [.payQR]
    ]

    func moveToNextState(paymentData: PaymentData) throws {
        guard let states = statesRelationships[currentState] else {
            throw PaymentFlowError.illegalTransitionRequest
        }

        let removeLast = currentState != .home

        for state in states {
            if canTransitionFrom(fromState: currentState, toState: state) {
                switch state {
                case .home:
                    throw PaymentFlowError.illegalTransitionRequest
                case .tokens:
                    router.presentGiveawayTokens(paymentData: paymentData, mode: .select)
                    currentState = state
                    return
                case .tips:
                    router.presentTips(paymentData: paymentData, removingLast: removeLast)
                    currentState = state
                    return
                case .payQR:
                    router.presentPay(paymentData: paymentData, removingLast: removeLast)
                    currentState = state
                    return
                case .giftcards:
                    router.presentGiftCards(paymentData: paymentData, mode: .select, removingLast: removeLast)
                    currentState = state
                    return
                }
            }
        }

        throw PaymentFlowError.illegalTransitionRequest
    }

    private func canTransitionFrom(fromState: PaymentState, toState: PaymentState) -> Bool {
        let config = db.clientConfig()

        if let states = statesRelationships[fromState], let _ = states.firstIndex(of: toState) {
            switch toState {
            case .home:
                return false
            case .tokens:
                return !db.giveawayTokens().isEmpty && config?.configuration.settings.showGiveawayTokens == .some(true)
            case .tips:
                return config?.configuration.settings.tips.enabled == .some(true)
            case .giftcards:
                return !db.giftcards().isEmpty && config?.configuration.settings.showGiftCard == .some(true)
            case .payQR:
                return true
            }
        }

        return false
    }
}
