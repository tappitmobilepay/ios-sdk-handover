//
//  SSOInformation.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 09/08/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation
import UIKit

class SSOInformation: BaseScreenViewController {
    @IBOutlet var logoImage: UIImageView!
    @IBOutlet var sloganLabel: UILabel!
    @IBOutlet var continueButton: UIButton!
    @IBOutlet var firstHolderView: UIView!
    @IBOutlet var secondHolderView: UIView!
    @IBOutlet var thirdHolderView: UIView!
    @IBOutlet var firstLabel: UILabel!
    @IBOutlet var secondLabel: UILabel!
    @IBOutlet var thirdLabel: UILabel!
    @IBOutlet var firstInfoLabel: UILabel!
    @IBOutlet var secondInfoLabel: UILabel!
    @IBOutlet var thirdInfoLabel: UILabel!
    @IBOutlet var fourthHolderView: UIView!
    @IBOutlet var fourthLabel: UILabel!
    @IBOutlet var fourthInfoLabel: UILabel!

    var router: RouterContract!
    var db: DatabaseManager!

    override func viewDidLoad() {
        super.viewDidLoad()

        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }

        view.backgroundColor = DevUtil.color(name: Colors.tmw_onboarding_background.rawValue)
        logoImage.image = DevUtil.imageSkin(name: "tmw_navigation_logo")
        sloganLabel.textColor = DevUtil.color(name: Colors.tmw_onboarding_background_text.rawValue)
        sloganLabel.font = UIFont.with(size: .Large, weight: .bold)
        continueButton.setTitle("NEXT", for: UIControl.State.normal)
        continueButton.setTitleColor(DevUtil.color(name: Colors.tmw_onboarding_button_text.rawValue), for: UIControl.State.normal)
        continueButton.titleLabel?.font = UIFont.with(size: .LightNormal1, weight: .bold)
        continueButton.backgroundColor = DevUtil.color(name: Colors.tmw_onboarding_button_background.rawValue)
        continueButton.addDefaultRoundCorners()

        firstLabel.font = UIFont.with(size: .LightNormal1, weight: .bold)
        firstLabel.textColor = DevUtil.color(name: Colors.tmw_onboarding_button_text.rawValue)
        secondLabel.font = UIFont.with(size: .LightNormal1, weight: .bold)
        secondLabel.textColor = DevUtil.color(name: Colors.tmw_onboarding_button_text.rawValue)
        thirdLabel.font = UIFont.with(size: .LightNormal1, weight: .bold)
        thirdLabel.textColor = DevUtil.color(name: Colors.tmw_onboarding_button_text.rawValue)
        fourthLabel.font = UIFont.with(size: .LightNormal1, weight: .bold)
        fourthLabel.textColor = DevUtil.color(name: Colors.tmw_onboarding_button_text.rawValue)

        let brandName = db.clientConfig()?.configuration.appName ?? DevUtil.brandName()

        firstInfoLabel.font = UIFont.with(size: .LightNormal1, weight: .normal)
        firstInfoLabel.textColor = DevUtil.color(name: Colors.tmw_onboarding_background_text.rawValue)
        firstInfoLabel.text = "Activate \(brandName) Pay with your\nPIN, face or fingerprint"
        secondInfoLabel.textColor = DevUtil.color(name: Colors.tmw_onboarding_background_text.rawValue)
        secondInfoLabel.font = UIFont.with(size: .LightNormal1, weight: .normal)
        thirdInfoLabel.textColor = DevUtil.color(name: Colors.tmw_onboarding_background_text.rawValue)
        thirdInfoLabel.font = UIFont.with(size: .LightNormal1, weight: .normal)
        fourthInfoLabel.textColor = DevUtil.color(name: Colors.tmw_onboarding_background_text.rawValue)
        fourthInfoLabel.font = UIFont.with(size: .LightNormal1, weight: .normal)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        Analytics.reportShowWellcomeScreenEvent()

        hideBackButton()
        navigationController?.setNavigationBarHidden(true, animated: true)

        makeCircles()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        hideBackButton()
        navigationController?.setNavigationBarHidden(true, animated: true)

        makeCircles()
    }

    func makeCircles() {
        firstHolderView.makeCircle(backgroundColor: DevUtil.color(name: Colors.tmw_onboarding_button_background.rawValue))
        secondHolderView.makeCircle(backgroundColor: DevUtil.color(name: Colors.tmw_onboarding_button_background.rawValue))
        thirdHolderView.makeCircle(backgroundColor: DevUtil.color(name: Colors.tmw_onboarding_button_background.rawValue))
        fourthHolderView.makeCircle(backgroundColor: DevUtil.color(name: Colors.tmw_onboarding_button_background.rawValue))
    }

    @IBAction func didTapContinueButton(_ sender: Any) {
        isAuthendicated(router: router,
                        animated: true)
    }
}
