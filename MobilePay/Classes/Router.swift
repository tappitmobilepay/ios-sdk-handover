//
//  Router.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 04/06/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation
import PassKit
import UIKit

enum NavigationLocations {
    case Home, Authentication, PaymentMethods, Giftcards
}

enum QRMode { case
    Pay, Refund
}

protocol RouterContract {
    var context: ApiContext { get }
    var credStore: CredentialsStoringContract { get }
    var loginManager: LoginManagingContract { get }
    var db: DatabaseManager { get }
    var deepLink: DeepLink? { get }
    var ssoLogin: SSOLogin { get set }
    var paymentFlowManager: PaymentFlowManagementContract! { get set }

    // MARK: Navigating backwards

    func goBack(to: NavigationLocations)
    func goBack(pin: Bool)
    func goBack()
    func goToRoot()

    // MARK: Onboarding

    func buildSSO() -> UIViewController
    func presentOTPConfirmation()
    func presentSSOInformation(animated: Bool)
    func presentAuthentication(animated: Bool)
    func presentSetupPin(navigationMethod: NavigationMethod)
    func presentSetupBiometric(navigationMethod: NavigationMethod)
    func presentEnterPin(animated: Bool)
    func presentEnterBiometric(animated: Bool)
    func presentGiftCards(paymentData: PaymentData, mode: GiveawayTokensScreenMode, removingLast: Bool)
    func presentRedeemGiftcard()

    func presentLoader()

    func hideLoader()

    // MARK: Payments

    func presentTips(paymentData: PaymentData, removingLast: Bool)
    func presentAddInitialCard(animated: Bool)
    func presentAddCard()
    func presentViewCardDetails(card: Card)
    func presentMyPaymentMethods()
    func presentSuccess(type: CompletionType, message: String?, completion: (() -> Void)?)

    // MARK: Home

    func presentHome(animated: Bool, replaceLast: Bool)
    func presentHome(animated: Bool)
    func presentRefund()
    func presentPay(paymentData: PaymentData, removingLast: Bool)
    func presentTransactionsHistory()
    func presentSettings()
    func presentHelp()
    func presentGiveawayTokens(paymentData: PaymentData, mode: GiveawayTokensScreenMode)
    @available(iOS 13, *)
    func presentCreditCardScanner(delegate: CreditCardScannerViewControllerDelegate)
    func presentCampaignDetails(campaign: Campaign)

    // MARK: Transactions

    func presentTransactionDetails(transaction: Transaction, currencySymbol: String)

    func dismissModalController()

    func presentApplePay(data: NotificationContent)
}

class Router {
    // MARK: - Properties

    private let mainNavigationController: UINavigationController
    let context: ApiContext
    var credStore: CredentialsStoringContract
    var db = DatabaseManager()
    let deepLink: DeepLink?
    var loginManager: LoginManagingContract
    var ssoLogin: SSOLogin
    var paymentFlowManager: PaymentFlowManagementContract!
    lazy var session: URLSession = {
        let configuration = URLSessionConfiguration.ephemeral
        if #available(iOS 13.0, *) {
            configuration.tlsMinimumSupportedProtocolVersion = .TLSv12
        }
        else {
            configuration.tlsMinimumSupportedProtocol = .dtlsProtocol12
        }
        return URLSession(configuration: configuration)
    }()

    @available(*, unavailable)
    init() {
        fatalError("Do not use init. Use init with navigation")
    }

    // MARK: - Initializers

    init(navigationController: UINavigationController,
         ssoLogin: SSOLogin,
         credStore: CredentialsStoringContract,
         deepLink: DeepLink?)
    {
        #if PROD
            let environment = ssoLogin.environment == MobilePayEnvironment.Production ?
                ApiEnvironment.productionAnalytics :
                ApiEnvironment.stagingAnalytics
        #else
            let environment = ApiEnvironment.stagingAnalytics
        #endif

        context = NonPersistentApiContext(environment: ssoLogin.environment.baseEnvironment())
        self.ssoLogin = ssoLogin
        self.deepLink = deepLink
        self.credStore = credStore
        mainNavigationController = navigationController
        loginManager = LoginManager(db: db,
                                    credStore: credStore)
        Analytics.service = AnalyticsURLSessionWebService(context: NonPersistentApiContext(environment: environment),
                                                          session: session,
                                                          apiCredentialsProvider: credStore,
                                                          db: db)
        Analytics.login = ssoLogin
        Analytics.db = db
        Analytics.credStore = credStore
    }
}

extension Router: RouterContract {
    func goBack(to: NavigationLocations) {
        switch to {
        case .Home:
            mainNavigationController.backToViewController(vc: HomeViewController.self)
        case .Authentication:
            for controller in mainNavigationController.viewControllers {
                if type(of: controller) == EnterPinViewController.self {
                    mainNavigationController.backToViewController(vc: EnterPinViewController.self, animated: false)
                    return
                }
                else if type(of: controller) == EnterBiometryViewController.self {
                    mainNavigationController.backToViewController(vc: EnterBiometryViewController.self, animated: false)
                    return
                }
            }

            if credStore.authenticationMethod == .some(.pin) {
                let pin = buildEnterPin()
                mainNavigationController.viewControllers = [pin]
            }
            else if credStore.authenticationMethod == .some(.biometric) {
                let bio = buildEnterBiometric()
                mainNavigationController.viewControllers = [bio]
            }

        case .PaymentMethods:
            mainNavigationController.backToViewController(vc: CardsTableViewController.self)
        case .Giftcards:
            mainNavigationController.backToViewController(vc: GiftcardsViewController.self)
        }
    }

    func goBack(pin: Bool) {
        var vc = mainNavigationController.viewControllers
        vc.removeLast()
        goBack()

        vc = vc.filter { "\(type(of: $0)).Type" != "\(type(of: SetupPinViewController.self))" &&
            "\(type(of: $0)).Type" != "\(type(of: SetupBiometricAccessViewController.self))" &&
            "\(type(of: $0)).Type" != "\(type(of: EnterBiometryViewController.self))" &&
            "\(type(of: $0)).Type" != "\(type(of: EnterPinViewController.self))"
        }

        let element = pin ? buildEnterPin() : buildEnterBiometric()

        vc.insert(element, at: vc.count - 2)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [weak self] in
            self?.mainNavigationController.setViewControllers(vc, animated: false)
        }
    }

    func goBack() {
        mainNavigationController.popViewController(animated: true)
    }

    func goToRoot() {
        mainNavigationController.popToRootViewController(animated: true)
    }

    func presentOTPConfirmation() {}

    func presentAuthentication(animated: Bool) {
        let view = UIStoryboard.initViewController(for: AuthenticationViewController.self)
        view.presenter = AuthenticationPresenter(view: view, router: self)
        mainNavigationController.pushViewController(view, animated: animated)
    }

    func buildSetupPin(navigationMethod: NavigationMethod) -> UIViewController {
        let view = UIStoryboard.initViewController(for: SetupPinViewController.self)
        view.presenter = SetupPinPresenter(view: view,
                                           router: self,
                                           navigationMethod: navigationMethod,
                                           db: db,
                                           credStore: credStore)
        return view
    }

    func presentSetupPin(navigationMethod: NavigationMethod) {
        let view = buildSetupPin(navigationMethod: navigationMethod)
        mainNavigationController.pushViewController(view, animated: true)
    }

    func buildSetupBiometric(navigationMethod: NavigationMethod) -> UIViewController {
        let view = UIStoryboard.initViewController(for: SetupBiometricAccessViewController.self)
        view.navigationMethod = navigationMethod
        view.db = db
        // TODO: Make presenter incapsulate router.....
        view.router = self
        return view
    }

    func presentSetupBiometric(navigationMethod: NavigationMethod) {
        let view = buildSetupBiometric(navigationMethod: navigationMethod)
        mainNavigationController.pushViewController(view, animated: true)
    }

    func buildEnterPin() -> UIViewController {
        let pin = UIStoryboard.initViewController(for: EnterPinViewController.self)
        pin.loginManager = loginManager
        let loginParser = LoginResponseParser()
        let webService = LoginURLSessionWebService(context: context,
                                                   session: session,
                                                   apiCredentialsProvider: credStore, db: db)
        webService.responseParser = loginParser
        pin.presenter = EnterPinPresenter(view: pin,
                                          router: self,
                                          db: db,
                                          credStore: credStore,
                                          webService: webService)
        pin.dismissAction = { [weak self] in
            self?.mainNavigationController.popToRootViewController(animated: true)
        }
        return pin
    }

    func presentEnterPin(animated: Bool) {
        if let presentscreen = UIApplication.topViewController(),
           !presentscreen.isKind(of: EnterBiometryViewController.self)
        {
            let pin = buildEnterPin()
            mainNavigationController.pushViewController(pin, animated: animated)
        }
    }

    func buildEnterBiometric() -> UIViewController {
        let pin = UIStoryboard.initViewController(for: EnterBiometryViewController.self)
        pin.loginManager = loginManager
        let loginParser = LoginResponseParser()
        let webService = LoginURLSessionWebService(context: context,
                                                   session: session,
                                                   apiCredentialsProvider: credStore, db: db)
        webService.responseParser = loginParser
        pin.webService = webService
        pin.router = self
        pin.db = db
        return pin
    }

    func presentEnterBiometric(animated: Bool) {
        if let presentscreen = UIApplication.topViewController(),
           !presentscreen.isKind(of: EnterBiometryViewController.self)
        {
            let pin = buildEnterBiometric()
            mainNavigationController.pushViewController(pin, animated: animated)
        }
    }

    func buildPaymentWebService() -> PaymentsWebServiceContract {
        let paypalService = PaymentsURLSessionWebService(context: NonPersistentApiContext(environment: ssoLogin.environment.basePaymentsEnvironment()),
                                                         session: session,
                                                         apiCredentialsProvider: credStore, db: db)
        paypalService.responseParser = PaymentsResponseParser()
        return paypalService
    }

    func presentAddInitialCard(animated: Bool) {
        let view = UIStoryboard.initViewController(for: CardViewController.self)
        let presenter = CardPresenter(view: view,
                                      router: self,
                                      paymentsService: buildPaymentWebService(),
                                      db: db,
                                      card: nil,
                                      isInitialCardSetup: true)
        view.presenter = presenter

        mainNavigationController.pushViewController(view, animated: animated)
    }

    private func replaceLastWithHome() {
        let view = buildHome()
        var vc = mainNavigationController.viewControllers
        vc.removeLast()
        vc.append(view)
        mainNavigationController.setViewControllers(vc, animated: true)
    }

    func presentAddCard() {
        let view = UIStoryboard.initViewController(for: CardViewController.self)
        let presenter = CardPresenter(view: view,
                                      router: self,
                                      paymentsService: buildPaymentWebService(),
                                      db: db,
                                      card: nil,
                                      isInitialCardSetup: false)
        view.presenter = presenter

        mainNavigationController.pushViewController(view, animated: true)
    }

    func presentViewCardDetails(card: Card) {
        let view = UIStoryboard.initViewController(for: CardViewController.self)
        let presenter = CardPresenter(view: view,
                                      router: self,
                                      paymentsService: buildPaymentWebService(),
                                      db: db,
                                      card: card,
                                      isInitialCardSetup: false)
        view.presenter = presenter

        mainNavigationController.pushViewController(view, animated: true)
    }

    private func buildHome() -> UIViewController {
        let view = UIStoryboard.initViewController(for: HomeViewController.self)
        let presenter = HomePresenter(view: view,
                                      router: self,
                                      db: db,
                                      paymentsService: buildPaymentWebService(),
                                      paymentFlowManager: paymentFlowManager)
        view.presenter = presenter
        return view
    }

    func presentHome(animated: Bool, replaceLast: Bool) {
        if replaceLast {
            replaceLastWithHome()
        }
        else {
            presentHome(animated: animated)
        }
    }

    func presentHome(animated: Bool) {
        let view = buildHome()
        mainNavigationController.pushViewController(view, animated: animated)
    }

    func presentPay(paymentData: PaymentData, removingLast: Bool) {
        presentQR(paymentData: paymentData, removingLast: removingLast, mode: .Pay)
    }

    func presentRefund() {
        presentQR(paymentData: PaymentData(), removingLast: true, mode: .Refund)
    }

    func presentQR(paymentData: PaymentData, removingLast: Bool, mode: QRMode) {
        let view = PaymentViewController(nibName: "\(PaymentViewController.self)", bundle: Bundle(for: PaymentViewController.self))
        let webService = PaymentsURLSessionWebService(context: NonPersistentApiContext(environment: ssoLogin.environment.basePaymentsEnvironment()), session: session, apiCredentialsProvider: credStore, db: db)
        webService.responseParser = PaymentsResponseParser()
        let presenter = PaymentPresenter(view: view,
                                         timeLeftProvider: TMWCommonUtil.userDefaults,
                                         credStore: credStore,
                                         webService: webService,
                                         router: self,
                                         mode: mode,
                                         paymentData: paymentData)
        view.presenter = presenter

        if removingLast {
            var viewControllers = mainNavigationController.viewControllers
            viewControllers.removeLast()
            viewControllers.append(view)
            mainNavigationController.setViewControllers(viewControllers, animated: true)
        }
        else {
            mainNavigationController.pushViewController(view, animated: true)
        }
    }

    func presentMyPaymentMethods() {
        let view = UIStoryboard.initViewController(for: CardsTableViewController.self)
        let presenter = CardsTableViewPresenter(view: view,
                                                router: self,
                                                paymentsService: buildPaymentWebService(),
                                                db: db,
                                                crediantialStore: credStore)
        view.presenter = presenter
        mainNavigationController.pushViewController(view, animated: true)
    }

    func presentTransactionsHistory() {
        let context = NonPersistentApiContext(environment: ssoLogin.environment.basePaymentsEnvironment())
        let responseParser = TransactionsResponseParser()
        let webService = TransactionsURLSessionWebService(context: context, session: session, apiCredentialsProvider: credStore, db: db)
        webService.responseParser = responseParser
        let view = UIStoryboard.initViewController(for: TransactionsListTableViewController.self)
        let presenter = TransactionsListTablePresenter(view: view,
                                                       webService: webService,
                                                       db: db,
                                                       router: self)
        view.presenter = presenter

        let historyView = UIStoryboard.initViewController(for: TransactionsHistoryViewController.self)
        historyView.router = self
        historyView.tableViewController = view
        mainNavigationController.pushViewController(historyView, animated: true)
    }

    func presentSettings() {
        let mainView = UIStoryboard.initViewController(for: SettingsViewController.self)
        mainView.router = self
        let view = UIStoryboard.initViewController(for: SettingsTableViewController.self, storyboardName: "\(SettingsViewController.self)")
        let loginParser = LoginResponseParser()
        let webService = LoginURLSessionWebService(context: context,
                                                   session: session,
                                                   apiCredentialsProvider: credStore, db: db)
        webService.responseParser = loginParser
        view.presenter = SettingsTablePresenter(view: view,
                                                router: self,
                                                loginManager: loginManager,
                                                webService: webService)
        mainView.settingsTable = view
        mainNavigationController.pushViewController(mainView, animated: true)
    }

    func presentHelp() {
        let view = HelpViewController()
        view.db = db
        mainNavigationController.pushViewController(view, animated: true)
    }

    func presentTips(paymentData: PaymentData, removingLast: Bool) {
        let view = UIStoryboard.initViewController(for: TipsViewController.self)
        let presenter = TipsPresenter(view: view,
                                      router: self,
                                      paymentData: paymentData,
                                      paymentFlowManager: paymentFlowManager)
        view.presenter = presenter
        if removingLast {
            var viewControllers = mainNavigationController.viewControllers
            viewControllers.removeLast()
            viewControllers.append(view)
            mainNavigationController.setViewControllers(viewControllers, animated: true)
        }
        else {
            mainNavigationController.pushViewController(view, animated: true)
        }
    }

    func presentTransactionDetails(transaction: Transaction, currencySymbol: String) {
        let view = UIStoryboard.initViewController(for: TransactionDetails.self)
        view.transaction = transaction
        view.currencySymbol = currencySymbol
        view.modalPresentationStyle = .overCurrentContext
        view.modalTransitionStyle = .crossDissolve

        mainNavigationController.present(view, animated: true, completion: nil)
    }

    func presentSuccess(type: CompletionType, message: String?, completion: (() -> Void)? = nil) {
        let view = UIStoryboard.initViewController(for: SuccessFailureViewController.self)
        view.router = self
        view.type = type
        view.completion = completion
        view.message = message
        mainNavigationController.pushViewController(view, animated: true)
    }

    func buildSSO() -> UIViewController {
        let view = UIStoryboard.initViewController(for: SSOViewController.self)
        let loginParser = LoginResponseParser()
        let webService = LoginURLSessionWebService(context: context, session: session, apiCredentialsProvider: credStore, db: db)
        webService.responseParser = loginParser
        let giveawayTokensResponseParser = GiveawayTokensResponseParser()

        let paymentsContext = NonPersistentApiContext(environment: ssoLogin.environment.basePaymentsEnvironment())

        let service = GiveawayTokensURLSessionService(context: paymentsContext,
                                                      session: session,
                                                      apiCredentialsProvider: credStore, db: db)
        service.responseParser = giveawayTokensResponseParser
        let userResponseParser = UserResponseParser()
        let userWebService = UserURLSessionWebService(context: paymentsContext,
                                                      session: session,
                                                      apiCredentialsProvider:
                                                      credStore,
                                                      db: db)
        userWebService.responseParser = userResponseParser
        let campaignsResponseParser = CampaignsResponseParser()
        let campaignsService = CampaignsURLSessionWebService(context: NonPersistentApiContext(environment: ssoLogin.environment.baseCampaignsEnvironment()),
                                                             session: session,
                                                             apiCredentialsProvider: credStore,
                                                             db: db)
        campaignsService.responseParser = campaignsResponseParser
        let presenter = SSOPresenter(view: view,
                                     router: self,
                                     webService: webService,
                                     credStore: credStore,
                                     giveawayTokensService: service,
                                     db: db,
                                     paymentsService: buildPaymentWebService(),
                                     balancesWebServide: userWebService,
                                     campaignsService: campaignsService)
        view.presenter = presenter
        return view
    }

    func presentSSOInformation(animated: Bool) {
        let view = UIStoryboard.initViewController(for: SSOInformation.self)
        view.router = self
        view.db = db
        mainNavigationController.pushViewController(view, animated: animated)
    }

    func presentGiveawayTokens(paymentData: PaymentData, mode: GiveawayTokensScreenMode) {
        let userResponseParser = UserResponseParser()
        let giveawayTokensResponseParser = GiveawayTokensResponseParser()

        let paymentsContext = NonPersistentApiContext(environment: ssoLogin.environment.basePaymentsEnvironment())

        let service = GiveawayTokensURLSessionService(context: paymentsContext,
                                                      session: session,
                                                      apiCredentialsProvider: credStore, db: db)
        service.responseParser = giveawayTokensResponseParser

        let webService = UserURLSessionWebService(context: paymentsContext, session: session, apiCredentialsProvider: credStore, db: db)
        webService.responseParser = userResponseParser
        let userWebService = UserURLSessionWebService(context: paymentsContext,
                                                      session: session,
                                                      apiCredentialsProvider:
                                                      credStore,
                                                      db: db)
        userWebService.responseParser = userResponseParser
        let view = UIStoryboard.initViewController(for: GiveawayTokensViewController.self)
        let campaignsResponseParser = CampaignsResponseParser()
        let campaignsService = CampaignsURLSessionWebService(context: NonPersistentApiContext(environment: ssoLogin.environment.baseCampaignsEnvironment()),
                                                             session: session,
                                                             apiCredentialsProvider: credStore,
                                                             db: db)
        campaignsService.responseParser = campaignsResponseParser
        let presenter = GiveawayTokensPresenter(mode: mode,
                                                view: view,
                                                router: self,
                                                giveawayTokensService: service,
                                                db: db,
                                                paymentData: paymentData,
                                                paymentFlowManager: paymentFlowManager,
                                                userService: userWebService,
                                                campaignsService: campaignsService)
        view.presenter = presenter
        mainNavigationController.pushViewController(view, animated: true)
    }

    func presentLoader() {
        TMWLoaderView.shareInstance().showLoader(mainNavigationController.view)
    }

    func hideLoader() {
        TMWLoaderView.shareInstance().hideLoader()
    }

    @available(iOS 13, *)
    func presentCreditCardScanner(delegate: CreditCardScannerViewControllerDelegate) {
        let creditCardScannerViewController = CreditCardScannerViewController(delegate: delegate)
        mainNavigationController.present(creditCardScannerViewController, animated: true)
    }

    func dismissModalController() {
        mainNavigationController.presentedViewController?.dismiss(animated: true, completion: nil)
    }

    func presentApplePay(data: NotificationContent) {
        // Create a payment request

        let request = PKPaymentRequest()
        request.merchantIdentifier = "merchant.com.tappit.mariustest"
        var supportedNetworks = [PKPaymentNetwork]() // data.Merchant.SupportedNetworks.compactMap { PKPaymentNetwork(rawValue: $0) }
        data.Merchant.SupportedNetworks.forEach { element in
            if element.lowercased().contains("amex") {
                supportedNetworks.append(.amex)
            }
            else if element.lowercased().contains("bancaires") {
                supportedNetworks.append(.cartesBancaires)
            }
            else if element.lowercased().contains("chinaunion") {
                supportedNetworks.append(.chinaUnionPay)
            }
            else if element.lowercased().contains("discover") {
                supportedNetworks.append(.discover)
            }
            else if element.lowercased().contains("idcredit") {
                supportedNetworks.append(.idCredit)
            }
            else if element.lowercased().contains("interac") {
                supportedNetworks.append(.interac)
            }
            else if element.lowercased().contains("jcb") {
                supportedNetworks.append(.JCB)
            }
            else if element.lowercased().contains("mastercard") {
                supportedNetworks.append(.masterCard)
            }
            else if element.lowercased().contains("privatelabel") {
                supportedNetworks.append(.privateLabel)
            }
            else if element.lowercased().contains("quicpay") {
                supportedNetworks.append(.quicPay)
            }
            else if element.lowercased().contains("suica") {
                supportedNetworks.append(.suica)
            }
            else if element.lowercased().contains("visa") {
                supportedNetworks.append(.visa)
            }
        }
        request.supportedNetworks = supportedNetworks
        request.supportedCountries = ["US"]
        var capabilities: PKMerchantCapability = []
        data.Merchant.Capabilities.forEach { element in
            if element.lowercased().contains("3ds") {
                capabilities.insert(.capability3DS)
            }
            else if element.lowercased().contains("debit") {
                capabilities.insert(.capabilityDebit)
            }
            else if element.lowercased().contains("credit") {
                capabilities.insert(.capabilityCredit)
            }
            else if element.lowercased().contains("emv") {
                capabilities.insert(.capabilityEMV)
            }
        }
        request.merchantCapabilities = [.capabilityDebit, .capabilityCredit]
        request.countryCode = data.Merchant.CountryCode
        request.currencyCode = data.Merchant.CurrencyCode
        request.paymentSummaryItems = data.Payment.Items.map { PKPaymentSummaryItem(label: $0.Name, amount: NSDecimalNumber(decimal: $0.Amount)) }

        let controller = PKPaymentAuthorizationViewController(paymentRequest: request)
        if controller != nil {
            controller!.delegate = mainNavigationController
            mainNavigationController.present(controller!, animated: true, completion: nil)
        }
    }

    func presentGiftCards(paymentData: PaymentData, mode: GiveawayTokensScreenMode, removingLast: Bool) {
        let giveawayTokensResponseParser = GiveawayTokensResponseParser()

        let paymentsContext = NonPersistentApiContext(environment: ssoLogin.environment.basePaymentsEnvironment())

        let service = GiveawayTokensURLSessionService(context: paymentsContext,
                                                      session: session,
                                                      apiCredentialsProvider: credStore, db: db)
        service.responseParser = giveawayTokensResponseParser
        let view = UIStoryboard.initViewController(for: GiftcardsViewController.self)
        let presenter = GiftcardsPresenter(mode: mode,
                                           view: view,
                                           router: self,
                                           giveawayTokensService: service,
                                           db: db,
                                           paymentData: paymentData,
                                           paymentFlowManager: paymentFlowManager)
        view.presenter = presenter

        if removingLast {
            var viewControllers = mainNavigationController.viewControllers
            viewControllers.removeLast()
            viewControllers.append(view)
            mainNavigationController.setViewControllers(viewControllers, animated: true)
        }
        else {
            mainNavigationController.pushViewController(view, animated: true)
        }
    }

    func presentRedeemGiftcard() {
        let giftcardsResponseParser = GiftcardsResponseParser()

        let paymentsContext = NonPersistentApiContext(environment: ssoLogin.environment.baseGiftcardsEnvironment())

        let service = GiftcardsURLSessionWebService(context: paymentsContext,
                                                    session: session,
                                                    apiCredentialsProvider: credStore, db: db)
        service.responseParser = giftcardsResponseParser
        let view = UIStoryboard.initViewController(for: RedeemGiftcardsViewController.self)
        let presenter = RedeemGiftcardsPresenter(view: view,
                                                 service: service,
                                                 router: self)
        view.presenter = presenter
        mainNavigationController.pushViewController(view, animated: true)
    }

    func presentCampaignDetails(campaign: Campaign) {
        let view = UIStoryboard.initViewController(for: CampaignDetails.self)
        view.campaign = campaign
        view.db = db
        view.clientTimezoneString = credStore.clientTimezone
        view.modalPresentationStyle = .overCurrentContext
        view.modalTransitionStyle = .crossDissolve

        mainNavigationController.present(view, animated: true, completion: nil)
    }
}

extension UINavigationController: PKPaymentAuthorizationViewControllerDelegate {
    public func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
        controller.dismiss(animated: true, completion: nil)
    }

    public func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, handler completion: @escaping (PKPaymentAuthorizationResult) -> Void) {
        completion(PKPaymentAuthorizationResult(status: .success, errors: nil))
    }
}
