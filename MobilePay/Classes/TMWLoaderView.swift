//
//  TMWLoaderView.swift
//  tappitVisitorApp
//
//  Created by Pandi on 12/01/17.
//  Copyright © 2017 JadePayments. All rights reserved.
//

import UIKit

class TMWLoaderView: UIView {
    var containerView: UIView!
    var loaderView: UIView!
    var path: UIBezierPath!
    var isLoading = false
    weak var masterView: UIView?

    private static var shared = TMWLoaderView()

    static func shareInstance() -> TMWLoaderView {
        return shared
    }

    func showLoader(_ view: UIView) {
        guard !isLoading else {
            return
        }

        masterView = view

        isLoading = true

        containerView = UIView()
        containerView.translatesAutoresizingMaskIntoConstraints = false
        containerView.frame = view.bounds
        containerView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        view.addSubview(containerView)
        addLayer()
        loaderView.translatesAutoresizingMaskIntoConstraints = false
        containerView.addSubview(loaderView)
        containerView.contentMode = .redraw
        view.contentMode = .redraw

        flipAnimation()

        DispatchQueue.main.async { [weak self] in
            self?.setNeedsLayout()
        }
    }

    func addLayer() {
        loaderView = UIView(frame: containerView.frame)
        loaderView.backgroundColor = .clear

        guard let view = loaderView else {
            assert(false)
            return
        }

        let outerThickness: CGFloat = 7
        let size: CGFloat = 50
        let distanceBetweenRings: CGFloat = 10

        let radius: CGFloat = size - outerThickness
        let path = UIBezierPath(roundedRect: CGRect(x: view.frame.origin.x + view.frame.size.width * 0.5 - size * 0.5, y: view.frame.origin.y + view.frame.size.height * 0.5 - size * 0.5, width: size, height: size), cornerRadius: size * 0.5)
        let circlePath = UIBezierPath(roundedRect: CGRect(x: path.bounds.origin.x + path.bounds.size.width * 0.5 - radius * 0.5, y: path.bounds.origin.y + path.bounds.size.height * 0.5 - radius * 0.5, width: radius, height: radius), cornerRadius: radius)
        path.append(circlePath)

        let fillLayer = CAShapeLayer()
        fillLayer.cornerRadius = size * 0.5
        fillLayer.path = path.cgPath
        fillLayer.fillRule = .evenOdd
        fillLayer.fillColor = DevUtil.color(name: Colors.tmw_primary_background.rawValue).cgColor
        view.layer.addSublayer(fillLayer)

        let innerThickness: CGFloat = outerThickness
        let size2: CGFloat = size - outerThickness - distanceBetweenRings
        let radius2: CGFloat = size2 - innerThickness
        let path2 = UIBezierPath(roundedRect: CGRect(x: circlePath.bounds.origin.x + circlePath.bounds.size.width * 0.5 - size2 * 0.5, y: circlePath.bounds.origin.y + circlePath.bounds.size.height * 0.5 - size2 * 0.5, width: size2, height: size2), cornerRadius: size2 * 0.5)
        let circlePath2 = UIBezierPath(roundedRect: CGRect(x: path2.bounds.origin.x + path2.bounds.size.width * 0.5 - radius2 * 0.5, y: path2.bounds.origin.y + path2.bounds.size.height * 0.5 - radius2 * 0.5, width: radius2, height: radius2), cornerRadius: radius2)
        path2.append(circlePath2)

        let fillLayer2 = CAShapeLayer()
        fillLayer2.cornerRadius = size2 * 0.5
        fillLayer2.path = path2.cgPath
        fillLayer2.fillRule = .evenOdd
        fillLayer2.fillColor = DevUtil.color(name: Colors.tmw_secondary_background.rawValue).cgColor
        fillLayer.addSublayer(fillLayer2)
    }

    func hideLoader() {
        guard isLoading else {
            return
        }

        isLoading = false
        if containerView != nil {
            loaderView.layer.removeAllAnimations()
            containerView.removeFromSuperview()
            loaderView.removeFromSuperview()
            loaderView = nil
            containerView = nil
        }
    }

    @objc
    func flipAnimation() {
        guard let flipView = loaderView else {
            return
        }

        UIView.transition(with: flipView, duration: 0.5, options: .transitionFlipFromLeft, animations: { () -> Void in

        }, completion: { [weak self] (finished: Bool) -> Void in

            if self?.loaderView == nil || !finished {
                return
            }
            self?.flipAnimation()
        })
    }
}
