//
//  CardsTableViewController.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 06/05/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation
import UIKit

class CardsTableViewController: BaseScreenViewController, CardsTableViewContract {
    @IBOutlet var tableView: UITableView!
    @IBOutlet var addNewCardButton: UIButton!
    var presenter: CardsTableViewPresentingContract!
    var cards: [Card] = []

    private let refreshControl = UIRefreshControl()

    let cellReuseId = "cell"
    override func viewDidLoad() {
        super.viewDidLoad()

        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }

        tableView.register(CardsTableViewCell.self, forCellReuseIdentifier: cellReuseId)

        addDefaultTitleViewLogo()
        tableView.delegate = self
        tableView.dataSource = self

        tableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(CardsTableViewController.refreshStarted(_:)), for: .valueChanged)

        guard let presenter = presenter else {
            assert(false)
            return
        }

        presenter.viewDidLoad()
    }

    override func viewDidAppear(_ animated: Bool) {
        presenter!.viewDidAppear()
        showBackButton()
        navigationController?.setNavigationBarHidden(false, animated: true)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        showBackButton()
        navigationController?.setNavigationBarHidden(false, animated: true)

        guard let presenter = presenter else {
            assert(false)
            return
        }

        presenter.viewWillAppear()
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cards.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseId) as? CardsTableViewCell else {
            return UITableViewCell()
        }

        let card = cards[indexPath.row]
        cell.setup(card: card, isDefault: card.isDefault)

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard let presenter = presenter else {
            assert(false)
            return
        }

        presenter.didSelectCardAtIndex(index: indexPath.row)
    }

    @IBAction
    func didTapAddNewCardButton(_ sender: Any) {
        guard let presenter = presenter else {
            assert(false)
            return
        }

        presenter.didTapAddNewCard()
    }

    @objc
    func refreshStarted(_ sender: Any) {
        guard let presenter = presenter else {
            assert(false)
            return
        }

        presenter.refreshData()
    }

    func setup() {
        addNewCardButton.setTitleColor(DevUtil.color(name: Colors.tmw_primary_button_text.rawValue), for: UIControl.State.normal)
        addNewCardButton.layer.borderWidth = 1
        addNewCardButton.layer.borderColor = DevUtil.color(name: Colors.tmw_primary_button_border.rawValue).cgColor
        addNewCardButton.setTitle("ADD ANOTHER CARD", for: .normal)
        addNewCardButton.titleLabel?.font = UIFont.with(size: .LargeMedium, weight: .primaryButton)
        addNewCardButton.backgroundColor = DevUtil.color(name: Colors.tmw_primary_button_background.rawValue)
        addNewCardButton.addDefaultRoundCorners()
    }

    func showLoading() {
        refreshControl.beginRefreshing()
    }

    func hideLoading() {
        refreshControl.endRefreshing()
    }

    func updateWith(cards: [Card]) {
        self.cards = cards
        tableView.reloadData()
    }
}

extension CardsTableViewController: UITableViewDelegate, UITableViewDataSource {}
