//
//  HighlightedCustomButton.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 18/11/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation
import UIKit

class HighlightedCustomButton: UIButton {
    override var isHighlighted: Bool {
        didSet {
            backgroundColor = isHighlighted ? backgroundColor?.withAlphaComponent(0.4) : backgroundColor?.withAlphaComponent(1)
        }
    }
}
