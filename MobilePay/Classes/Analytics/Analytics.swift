//
//  Analytics.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 08/09/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation
import UIKit

enum UsserSegmentation: Int { case
    registered = 1,
    activated = 2,
    active = 3,
    dormant = 4
}

let kAnalyticsDefaultParameterValue = "1"

enum Analytics {
    static var service: AnalyticsServiceContract!
    static var login: SSOLogin!
    static var db: DatabaseManagementContract!
    static var credStore: CredentialsStoringContract!
}

extension Analytics {
    static func reportEvent(name: String, with parameters: Param) {
        let event = AnalyticsEvent(name: name,
                                   params: parameters)

        let request = AnalyticsEventsReport(client_id: UIDevice.current.identifierForVendor?.uuidString ?? "",
                                            user_id: db.clientConfig()?.clientToken ?? "",
                                            events: [
                                                event
                                            ])

        service.reportEvent(event: request) { _ in }
    }

    static func reportShowWellcomeScreenEvent() {
        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken)

        reportEvent(name: "welcome_screen", with: parameters)
    }

    static func reportShowSetupAuthenticationScreenEvent() {
        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken)

        reportEvent(name: "setup_authentication_screen", with: parameters)
    }

    static func reportSetupPinFailedEvent(failureMessage: String) {
        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken,
                               failedReason: failureMessage)

        reportEvent(name: "setup_pin_failed", with: parameters)
    }

    static func reportSetupBiometricSucceededEvent() {
        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken,
                               totalBioSetUps: kAnalyticsDefaultParameterValue)

        reportEvent(name: "setup_biometric_btn", with: parameters)
    }

    static func reportSuccessfulLoginEvent() {
        let clientConfig = db.clientConfig()!

        let loginType = credStore.authenticationMethod == .some(.pin) ? "pin" :
            "biometric"

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken,
                               totalOpened: credStore.customerStats?.userSegmentation == .some(UsserSegmentation.registered.rawValue) ? kAnalyticsDefaultParameterValue : nil,
                               totalAddedCardOnly: credStore.customerStats?.userSegmentation == .some(UsserSegmentation.activated.rawValue) ? kAnalyticsDefaultParameterValue : nil,
                               totalSingleTransaction: credStore.customerStats?.userSegmentation == .some(UsserSegmentation.active.rawValue) ? kAnalyticsDefaultParameterValue : nil,
                               totalMultipleTransaction: credStore.customerStats?.userSegmentation == .some(UsserSegmentation.dormant.rawValue) ? kAnalyticsDefaultParameterValue : nil,
                               loginType: loginType,
                               totalLogins: kAnalyticsDefaultParameterValue)

        reportEvent(name: "login_success", with: parameters)
    }

    static func reportFailedLoginEvent(failureMessage: String) {
        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken,
                               failedReason: failureMessage)

        reportEvent(name: "login_failed", with: parameters)
    }

    static func reportShowHomeScreenEvent() {
        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken)

        reportEvent(name: "my_account_details_screen", with: parameters)
    }

    static func reportShowInitialAddPaymentMethodScreenEvent() {
        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken)

        reportEvent(name: "new_payment_method_add_screen", with: parameters)
    }

    static func reportFailedAddingInitialPaymentMethodEvent(failureMessage: String) {
        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken,
                               failedReason: failureMessage)

        reportEvent(name: "new_payment_method_added_failed", with: parameters)
    }

    static func reportFailedAddingSubsequentPaymentMethodEvent(failureMessage: String) {
        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken,
                               failedReason: failureMessage)

        reportEvent(name: "new_card_added_fail", with: parameters)
    }

    static func reportShowMyOffersEmptyScreenEvent() {
        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken)

        reportEvent(name: "my_offers_screen_empty", with: parameters)
    }

    static func reportShowMyOffersExistsScreenEvent() {
        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken)

        reportEvent(name: "my_offers_screen_existing", with: parameters)
    }

    static func reportShowHelpScreenEvent() {
        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken)

        reportEvent(name: "help_screen", with: parameters)
    }

    static func reportPressPayEvent() {
        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken,
                               totalPaymentStarts: kAnalyticsDefaultParameterValue)

        reportEvent(name: "make_payment_start_transaction", with: parameters)
    }

    static func reportShowSelectOffersEvent() {
        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken)

        reportEvent(name: "make_payment_select_offers_screen", with: parameters)
    }

    static func reportNoThanksOnSelectOffersEvent() {
        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken)

        reportEvent(name: "make_payment_select_offers_nothanks", with: parameters)
    }

    static func reportContinueOnSelectOffersEvent() {
        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken)

        reportEvent(name: "make_payment_select_offers_continue", with: parameters)
    }

    static func reportNoThanksOnTipsEvent() {
        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken)

        reportEvent(name: "make_payment_add_a_tip_nothanks", with: parameters)
    }

    static func reportContinueOnTipsEvent() {
        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken)

        reportEvent(name: "make_payment_add_a_tip_continue", with: parameters)
    }

    static func reportTransactionSuccessEvent(tipType: Int,
                                              totalTokens: Int,
                                              totalGiftcards: Int,
                                              transactionAmt: Decimal)
    {
        let clientConfig = db.clientConfig()!
        let cardType = db.allCreditCards(onlyDefault: true).first?.cardType.lowercased()

        var tip = "zero"
        if tipType == TipsType.Percentage.rawValue {
            tip = "predefined"
        }
        else if tipType == TipsType.Monetary.rawValue {
            tip = "custom"
        }

        let tokens = totalTokens > 0 ? "\(totalTokens)" : nil
        let giftcards = totalGiftcards > 0 ? "\(totalGiftcards)" : nil

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken,
                               transactionAmt: "\(transactionAmt)",
                               tipType: tip,
                               totalGCRedeem: giftcards,
                               totalGiveAways: tokens,
                               currency: clientConfig.clientCurrency.code,
                               cardType: cardType,
                               totalPaymentSuccess: kAnalyticsDefaultParameterValue)

        reportEvent(name: "make_payment_success_transaction", with: parameters)
    }

    static func reportTransactionFailureEvent(failedReason: String) {
        let clientConfig = db.clientConfig()!
        let cardType = db.allCreditCards(onlyDefault: true).first?.cardType.lowercased()

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken,
                               currency: clientConfig.clientCurrency.code,
                               cardType: cardType,
                               totalPaymentFailures: kAnalyticsDefaultParameterValue,
                               failedReason: failedReason)

        reportEvent(name: "make_payment_failure_transaction", with: parameters)
    }

    static func reportShowTransactionsHistoryScreenEvent() {
        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken)

        reportEvent(name: "transaction_history_screen", with: parameters)
    }

    static func reportShowTransactionDetailsScreenEvent() {
        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken)

        reportEvent(name: "view_transaction_details", with: parameters)
    }

    static func reportShowSetupPinScreenEvent() {
        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken)

        reportEvent(name: "setup_new_login_pin_screen", with: parameters)
    }

    static func reportPinSetupEvent() {
        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken,
                               totalPinSetUps: kAnalyticsDefaultParameterValue)

        reportEvent(name: "setup_pin_success", with: parameters)
    }

    static func reportShowEnterPinScreenEvent() {
        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken)

        reportEvent(name: "enter_login_pin_screen", with: parameters)
    }

    static func reportForgotPinPressedEvent() {
        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken)

        reportEvent(name: "forgot_pin_link", with: parameters)
    }

    static func reportForgotPinEmailSentEvent() {
        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken)

        reportEvent(name: "forgot_pin_success", with: parameters)
    }

    static func reportForgotPinEmailFailedEvent(failureMessage: String) {
        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken,
                               failedReason: failureMessage)

        reportEvent(name: "forgot_pin_failed", with: parameters)
    }

    static func reportPinResetSuccessEvent() {
        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken,
                               totalLoginResets: kAnalyticsDefaultParameterValue)

        reportEvent(name: "reset_pin_success", with: parameters)
    }

    static func reportBionResetSuccessEvent() {
        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken,
                               totalLoginResets: kAnalyticsDefaultParameterValue)

        reportEvent(name: "reset_biometric_success", with: parameters)
    }

    static func reportMyPaymentMethodsNumberOfCardsEvent() {
        let cards = db.allCreditCards(onlyDefault: false)
        let cardsString = cards.isEmpty ? "empty" : cards.analyticsCardsTypesString().lowercased()
        let eventType = cards.isEmpty ? "my_payment_methods_screen_empty" : "my_payment_method_details_existing"

        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken,
                               cardType: cardsString)

        reportEvent(name: eventType, with: parameters)
    }

    static func reportAddedFirstPaymentMethodSuccess(cardType: String) {
        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken,
                               totalFirstCardAdded: kAnalyticsDefaultParameterValue,
                               cardType: cardType.lowercased())

        reportEvent(name: "new_payment_method_added_success", with: parameters)
    }

    static func reportCustomLinkOnHomepagePressed() {
        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken)

        reportEvent(name: "account_details_screen_customlink", with: parameters)
    }

    static func reportOpenCardDetailsEvent() {
        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken)

        reportEvent(name: "view_card_details_screen", with: parameters)
    }

    static func reportCardDeletedEvent() {
        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken,
                               totalCardsDeleted: kAnalyticsDefaultParameterValue)

        reportEvent(name: "payment_method_deleted", with: parameters)
    }

    static func reportAddAnotherCardEvent() {
        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken)

        reportEvent(name: "add_another_card_screen", with: parameters)
    }

    static func reportAdditionalCardAddSuccessEvent(cardType: String) {
        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken,
                               totalAddtlCardsAdded: kAnalyticsDefaultParameterValue,
                               cardType: cardType.lowercased())

        reportEvent(name: "new_card_added_success", with: parameters)
    }

    static func reportClickedOnMakeCardDefaultButtonEvent(cardType: String) {
        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken,
                               totalDefaultCards: kAnalyticsDefaultParameterValue,
                               cardType: cardType.lowercased())

        reportEvent(name: "mark_a_card_default", with: parameters)
    }

    static func reportRefundClickedEvent() {
        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken)

        reportEvent(name: "select_transaction_refund", with: parameters)
    }

    static func reportRefundSuccessEvent(transactionAmt: Decimal) {
        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken,
                               transactionAmt: "\(transactionAmt)",
                               totalRefunds: kAnalyticsDefaultParameterValue,
                               currency: clientConfig.clientCurrency.code)

        reportEvent(name: "refund_success", with: parameters)
    }

    static func reportRefundFailureEvent(failureMessage: String) {
        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken,
                               failedReason: failureMessage)

        reportEvent(name: "refund_fail", with: parameters)
    }

    static func reportShowGenerateNewQRRefundPopupEvent() {
        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken)

        reportEvent(name: "refund_generate_newcode", with: parameters)
    }

    static func reportGenerateNewQRRefundConfirmEvent() {
        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken)

        reportEvent(name: "refund_generate_newcode_confirm", with: parameters)
    }

    static func reportGenerateNewQRRefundCancelEvent() {
        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken)

        reportEvent(name: "refund_generate_newcode_cancel", with: parameters)
    }

    static func reportRefundNewQRSuccessEvent(transactionAmt: Decimal) {
        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken,
                               transactionAmt: "\(transactionAmt)",
                               totalRefunds: kAnalyticsDefaultParameterValue,
                               currency: clientConfig.clientCurrency.code)

        reportEvent(name: "refund_generate_newcode_success", with: parameters)
    }

    static func reportRefundNewQRFailureEvent(failureMessage: String) {
        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken,
                               failedReason: failureMessage)

        reportEvent(name: "refund_generate_newcode_fail", with: parameters)
    }

    static func reportShowSettingsEvent() {
        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken)

        reportEvent(name: "settings_screen", with: parameters)
    }

    static func reportShowAuthenticationSettingsPopupEvent() {
        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken)

        reportEvent(name: "settings_select_auth_method_overlay", with: parameters)
    }

    static func reportSettingsSetupPinSelectedEvent() {
        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken)

        reportEvent(name: "settings_select_auth_method_pin", with: parameters)
    }

    static func reportSettingsSetupBioSelectedEvent() {
        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken)

        reportEvent(name: "settings_select_auth_method_biometric", with: parameters)
    }

    static func reportSubsequestAuthenticationSetupSuccess() {
        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken,
                               totalAuthChanges: kAnalyticsDefaultParameterValue)

        reportEvent(name: "change_auth_method_success", with: parameters)
    }

    static func reportOpenGiftcardsEmptyEvent() {
        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken)

        reportEvent(name: "gift_card_details_screen_empty", with: parameters)
    }

    static func reportOpenGiftcardsExistsEvent() {
        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken)

        reportEvent(name: "gift_card_details_screen_existing", with: parameters)
    }

    static func reportOpenAddGiftcardsScreenEvent() {
        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken)

        reportEvent(name: "add_gift_card_screen", with: parameters)
    }

    static func reportAddedFirstGiftcardEvent() {
        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken,
                               totalGiftCardsAdded: kAnalyticsDefaultParameterValue)

        reportEvent(name: "add_gift_card_success", with: parameters)
    }

    static func reportAddedSubsequentGiftcardEvent() {
        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken,
                               totalGiftCardsAdded: kAnalyticsDefaultParameterValue)

        reportEvent(name: "add_additional_gift_card_success", with: parameters)
    }

    static func reportAddingGiftcardFailed(failureMessage: String) {
        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken,
                               failedReason: failureMessage)

        reportEvent(name: "add_gift_card_failed", with: parameters)
    }

    static func reportNoTokensSelectedAlertEvent(failureMessage: String) {
        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken,
                               failedReason: failureMessage)

        reportEvent(name: "make_payment_select_offers_alert", with: parameters)
    }

    static func reportShowAddTipScreenEvent() {
        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken)

        reportEvent(name: "make_payment_add_a_tip_screen", with: parameters)
    }

    static func reportShowGenerateNewPaymentQRPopupEvent() {
        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken)

        reportEvent(name: "make_payment_generate_newcode", with: parameters)
    }

    static func reportGenerateNewQRPaymentConfirmEvent() {
        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken)

        reportEvent(name: "make_payment_generate_newcode_confirm", with: parameters)
    }

    static func reportGenerateNewQRPaymentCancelEvent() {
        let clientConfig = db.clientConfig()!

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken)

        reportEvent(name: "make_payment_generate_newcode_cancel", with: parameters)
    }

    static func reportTransactionNewQRSuccessEvent(tipType: Int,
                                                   totalTokens: Int,
                                                   totalGiftcards: Int,
                                                   transactionAmt: Decimal)
    {
        let clientConfig = db.clientConfig()!
        let cardType = db.allCreditCards(onlyDefault: true).first?.cardType.lowercased()

        var tip = "zero"
        if tipType == TipsType.Percentage.rawValue {
            tip = "predefined"
        }
        else if tipType == TipsType.Monetary.rawValue {
            tip = "custom"
        }

        let tokens = totalTokens > 0 ? "\(totalTokens)" : nil
        let giftcards = totalGiftcards > 0 ? "\(totalGiftcards)" : nil

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken,
                               transactionAmt: "\(transactionAmt)",
                               tipType: tip,
                               totalGCRedeem: giftcards,
                               totalGiveAways: tokens,
                               currency: clientConfig.clientCurrency.code,
                               cardType: cardType,
                               totalPaymentNewQRSuccess: kAnalyticsDefaultParameterValue)

        reportEvent(name: "make_payment_generate_newcode_success", with: parameters)
    }

    static func reportTransactionNewQRFailureEvent(failedReason: String) {
        let clientConfig = db.clientConfig()!
        let cardType = db.allCreditCards(onlyDefault: true).first?.cardType.lowercased()

        let parameters = Param(clientName: clientConfig.configuration.appName,
                               clientID: clientConfig.clientToken,
                               currency: clientConfig.clientCurrency.code,
                               cardType: cardType,
                               totalPaymentNewQRFailures: kAnalyticsDefaultParameterValue,
                               failedReason: failedReason)

        reportEvent(name: "make_payment_generate_newcode_failure", with: parameters)
    }
}
