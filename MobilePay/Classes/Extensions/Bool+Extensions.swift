//
//  Bool+Extensions.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 24/11/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation

extension Bool {
    var data: NSData {
        var _self = self
        return NSData(bytes: &_self, length: MemoryLayout.size(ofValue: self))
    }

    init?(data: NSData) {
        guard data.length == 1 else { return nil }
        var value = false
        data.getBytes(&value, length: MemoryLayout<Bool>.size)
        self = value
    }
}
