//
//  Collection+Extensions.swift
//  MobilePay-PAYG
//
//  Created by Marius Kurgonas on 05/06/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation

extension Collection {
    func distance(to index: Index) -> Int { distance(from: startIndex, to: index) }

    var pairs: [SubSequence] {
        var startIndex = self.startIndex
        let count = self.count
        let n = count / 2 + count % 2
        return (0..<n).map { _ in
            let endIndex = index(startIndex, offsetBy: 2, limitedBy: self.endIndex) ?? self.endIndex
            defer { startIndex = endIndex }
            return self[startIndex..<endIndex]
        }
    }
}

extension Collection where Iterator.Element == Card {
    func analyticsCardsTypesString() -> String {
        let sorted = self.sorted { $0.isDefault && !$1.isDefault }
        return (sorted.map { $0.cardType }).joined(separator: ":")
    }
}
