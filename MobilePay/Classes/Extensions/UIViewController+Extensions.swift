//
//  UIViewController+extensions.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 04/11/2019.
//  Copyright © 2019 JadePayments. All rights reserved.
//

import CoreData
import Foundation
import UIKit

extension UIViewController {
    func addChildViewController(child: UIViewController, toView view: UIView) {
        addChild(child)
        child.willMove(toParent: self)
        view.addSubview(child.view)
        child.view.translatesAutoresizingMaskIntoConstraints = false
        child.view.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        child.view.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        child.view.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        child.view.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
    }
}
