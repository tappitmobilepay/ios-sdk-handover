//
//  UIView+extensions.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 16/03/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    func addDefaultRoundCorners() {
        addRoundCorners(radius: 10)
    }

    func addRoundCorners(radius: Int) {
        layer.cornerRadius = CGFloat(radius)
        layer.masksToBounds = true
    }

    func makeCircle(backgroundColor: UIColor = .white) {
        layer.cornerRadius = frame.size.height * 0.5
        layer.masksToBounds = true
        layer.backgroundColor = backgroundColor.cgColor
    }

    func addDefaultBorder(width: CGFloat = 1) {
        // Add a border with color
        layer.borderWidth = width
        addDefaultRoundCorners()
    }

//    func addDefaultColorBorder() {
//        addDefaultBorder()
//        layer.borderColor = MobilePayCore.shared.config.secondaryColor.cgColor
//    }

    func addPrimaryColorBorder() {
        addColorBorder(color: DevUtil.color(name: Colors.tmw_primary_background.rawValue))
    }

    func addColorBorder(color: UIColor) {
        addDefaultBorder()
        layer.borderColor = color.cgColor
    }
}
