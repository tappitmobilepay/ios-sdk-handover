//
//  UIDevice+Extensions.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 04/12/2019.
//  Copyright © 2019 JadePayments. All rights reserved.
//

import Foundation
import UIKit

extension UIDevice {
    // Base width in point, use iPhone 6
    static let base: CGFloat = 375
    static var ratio: CGFloat {
        return UIScreen.main.bounds.width / base
    }

    func deviceModelName() -> String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let modelCode = withUnsafePointer(to: &systemInfo.machine) {
            $0.withMemoryRebound(to: CChar.self, capacity: 1) {
                ptr in String(validatingUTF8: ptr)
            }
        }
        return modelCode ?? UIDevice.current.model
    }
}
