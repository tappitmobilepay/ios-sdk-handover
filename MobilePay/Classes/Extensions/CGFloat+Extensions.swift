//
//  CGFloat+extensions.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 04/12/2019.
//  Copyright © 2019 JadePayments. All rights reserved.
//

import CoreGraphics
import Foundation
import UIKit

extension CGFloat {
    var adjusted: CGFloat {
        return self * UIDevice.ratio
    }
}
