//
//  Date+Extensions.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 5/18/21.
//  Copyright © 2021 JadePayments. All rights reserved.
//

import Foundation

extension Date {
    func toString(dateFormat: String = "MM/dd/yyyy | HH:mm:ss", timezone: TimeZone = TimeZone(abbreviation: "UTC")!) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        dateFormatter.timeZone = timezone

        return dateFormatter.string(from: self)
    }
}
