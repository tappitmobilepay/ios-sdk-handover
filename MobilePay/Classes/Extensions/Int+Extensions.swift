//
//  Int+extensions.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 12/11/2019.
//  Copyright © 2019 JadePayments. All rights reserved.
//

import CoreGraphics
import Foundation
import UIKit

extension Int {
    var degreesToRadians: CGFloat {
        return CGFloat(self) * .pi / 180
    }

    var adjusted: CGFloat {
        return CGFloat(self) * UIDevice.ratio
    }
}
