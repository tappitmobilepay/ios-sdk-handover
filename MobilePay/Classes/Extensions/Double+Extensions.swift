//
//  Double+extensions.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 04/12/2019.
//  Copyright © 2019 JadePayments. All rights reserved.
//

import CoreGraphics
import Foundation
import UIKit

extension Double {
    var adjusted: CGFloat {
        return CGFloat(self) * UIDevice.ratio
    }

    func format(f: String) -> String {
        return String(format: "%\(f)f", self)
    }
}
