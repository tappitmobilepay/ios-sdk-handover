//
//  UIWindow+extensions.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 22/11/2019.
//  Copyright © 2019 JadePayments. All rights reserved.
//

import Foundation
import UIKit

func showAlert(message: String, title: String = "alert_title".localized, okTitle: String = "Ok", okCompletion: (() -> Void)? = nil) {
    UIWindow.showAlert(title: title, message: message, okTitle: okTitle, cancelTitle: nil, okCompletion: okCompletion)
}

func showTwoButtonAlert(title: String, message: String, okTitle: String, cancelTitle: String, okCompletion: (() -> Void)?) {
    UIWindow.showAlert(title: title, message: message, okTitle: okTitle, cancelTitle: cancelTitle, okCompletion: okCompletion)
}

func showActionSheet(titles: [String], actions: [() -> Void]) {
    UIWindow.showActionSheet(titles: titles, actions: actions)
}

extension UIWindow {
    static func showAlert(title: String, message: String, okTitle: String, cancelTitle: String?, okCompletion: (() -> Void)?) {
        DispatchQueue.main.async {
            let alertViewController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            if #available(iOS 13.0, *) {
                alertViewController.overrideUserInterfaceStyle = .light
            }
            alertViewController.addAction(UIAlertAction(title: okTitle, style: .default, handler: { _ in
                okCompletion?()
            }))
            if let cancelTitle = cancelTitle {
                alertViewController.addAction(UIAlertAction(title: cancelTitle, style: .cancel, handler: nil))
            }
            UIApplication.shared.windows[0].rootViewController?.present(alertViewController, animated: true, completion: nil)
        }
    }

    static func showActionSheet(titles: [String], actions: [() -> Void]) {
        guard !titles.isEmpty else {
            assert(false, "There has to be at least 1 title")
            return
        }

        guard titles.count == actions.count else {
            assert(false, "Titles number has to match actions number")
            return
        }

        let alertViewController = UIAlertController(title: "Select authentication method:", message: "", preferredStyle: .actionSheet)
        if #available(iOS 13.0, *) {
            alertViewController.overrideUserInterfaceStyle = .light
        }

        for i in 0..<titles.count {
            let title = titles[i]
            let action = actions[i]
            let alertAction = UIAlertAction(title: title, style: .default) { _ in
                action()
            }
            alertViewController.addAction(alertAction)
        }

        alertViewController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))

        UIApplication.shared.windows[0].rootViewController?.present(alertViewController, animated: true, completion: nil)
    }
}
