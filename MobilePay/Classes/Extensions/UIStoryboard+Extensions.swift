//
//  UIStoryboard+extensions.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 28/11/2019.
//  Copyright © 2019 JadePayments. All rights reserved.
//

import Foundation
import UIKit

extension UIStoryboard {
    static func initViewController<T: UIViewController>(for aClass: T.Type) -> T {
        return initViewController(for: aClass, storyboardName: "\(aClass)")
    }

    static func initViewController<T: UIViewController>(for aClass: T.Type, storyboardName: String) -> T {
        return UIStoryboard(name: storyboardName, bundle: Bundle(for: aClass)).instantiateViewController(withIdentifier: "\(aClass)") as! T
    }
}
