//
//  UINavigationController+extension.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 09/01/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation
import UIKit

extension UINavigationController {
    func backToViewController(vc: Any, animated: Bool = true) {
        // iterate to find the type of vc
        for element in viewControllers as Array {
            if "\(type(of: element)).Type" == "\(type(of: vc))" {
                popToViewController(element, animated: animated)
                break
            }
        }
    }
}
