//
//  UIFont+extensions.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 20/01/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {
    static func with(size: TappitFont.Size, weight: TappitFont.Family) -> UIFont {
        var systemFontWeight = UIFont.Weight.regular
        switch weight {
        case .bold,
             .payButton,
             .primaryButton,
             .secondaryButton,
             .customTipButton,
             .benefitStatusButton,
             .campaignStatusButton,
             .homeButton:
            systemFontWeight = .bold
        default: // Normal
            break
        }

        guard let clientFontName = TMWCommonUtil.thirdPartyFontNamesForTypes[weight],
              let clientFont = UIFont(name: clientFontName, size: size.rawValue)
        else {
            return systemFont(ofSize: size.rawValue, weight: systemFontWeight)
        }

        return clientFont
    }
}
