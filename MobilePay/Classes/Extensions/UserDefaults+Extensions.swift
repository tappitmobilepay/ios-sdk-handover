//
//  UserDefaults+extensions.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 22/11/2019.
//  Copyright © 2019 JadePayments. All rights reserved.
//

import Foundation

extension UserDefaults: QRCodeTimeLeftContract {
    var endInterval: TimeInterval {
        set {
            set(newValue, forKey: Constants.kQRCodeTimeLeftUserDefaultsKey)
            synchronize()
        }

        get {
            double(forKey: Constants.kQRCodeTimeLeftUserDefaultsKey)
        }
    }
}
