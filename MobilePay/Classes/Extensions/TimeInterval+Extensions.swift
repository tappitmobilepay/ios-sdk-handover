//
//  TimeInterval+extensions.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 12/11/2019.
//  Copyright © 2019 JadePayments. All rights reserved.
//

import Foundation

extension TimeInterval {
    var time: String {
        let seconds = 60.0
        let rounded = truncatingRemainder(dividingBy: seconds) == 0 ? Int(self) : Int(ceil(truncatingRemainder(dividingBy: seconds)))
        return String(format: "%02d", rounded)
    }
}
