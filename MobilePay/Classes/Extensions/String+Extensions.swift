//
//  String+extensions.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 06/11/2019.
//  Copyright © 2019 JadePayments. All rights reserved.
//

import CommonCrypto
import Foundation
import UIKit

// Defines types of hash string outputs available
enum HashOutputType {
    // standard hex string output
    case hex
    // base 64 encoded string output
    case base64
}

// Defines types of hash algorithms available
enum HashType {
    case md5
    case sha1
    case sha224
    case sha256
    case sha384
    case sha512

    var length: Int32 {
        switch self {
        case .md5: return CC_MD5_DIGEST_LENGTH
        case .sha1: return CC_SHA1_DIGEST_LENGTH
        case .sha224: return CC_SHA224_DIGEST_LENGTH
        case .sha256: return CC_SHA256_DIGEST_LENGTH
        case .sha384: return CC_SHA384_DIGEST_LENGTH
        case .sha512: return CC_SHA512_DIGEST_LENGTH
        }
    }
}

extension String {
    func isValidEmailAddress() -> Bool {
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"

        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = self as NSString
            let results = regex.matches(in: self, range: NSRange(location: 0, length: nsString.length))

            if results.isEmpty {
                returnValue = false
            }
        }
        catch let error as NSError {
            onlyDebugPrint("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }

        return returnValue
    }

    func toDate(dateFormat: String, timezone: TimeZone = TimeZone(abbreviation: "UTC")!) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        dateFormatter.timeZone = timezone

        return dateFormatter.date(from: self)
    }

//    func UTCToLocal(dateFormat: String = "H:mm:ss", convertToDateFormat: String = "yyyy/MM/dd | HH:mm:ss") -> String? {
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = dateFormat
//        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
//
//        let dt = dateFormatter.date(from: self)
//        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
//        dateFormatter.dateFormat = dateFormat
//
//        guard let date = dt else {
//            return nil
//        }
//
//        return dateFormatter.string(from: date)
//    }

    static func format(strings: [String],
                       boldFont: UIFont = UIFont.boldSystemFont(ofSize: 14),
                       boldColor: UIColor = UIColor.blue,
                       inString string: String,
                       font: UIFont = UIFont.systemFont(ofSize: 14),
                       color: UIColor = UIColor.black) -> NSAttributedString
    {
        let attributedString =
            NSMutableAttributedString(string: string,
                                      attributes: [
                                          NSAttributedString.Key.font: font,
                                          NSAttributedString.Key.foregroundColor: color
                                      ])
        let boldFontAttribute = [NSAttributedString.Key.font: boldFont, NSAttributedString.Key.foregroundColor: boldColor]
        for bold in strings {
            attributedString.addAttributes(boldFontAttribute, range: (string as NSString).range(of: bold))
        }
        return attributedString
    }

    func isValidPassword() -> Bool {
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9])(?=.*[a-z]).{8,24}$")
        return passwordTest.evaluate(with: self)
    }

    func formattedCreditCard() -> String {
        return inserting(separator: " ", every: 4)
    }

    var html2AttributedString: NSAttributedString? {
        Data(utf8).html2AttributedString
    }

    var html2String: String {
        html2AttributedString?.string ?? ""
    }
}

extension StringProtocol where Self: RangeReplaceableCollection {
    mutating func insert<S: StringProtocol>(separator: S, every n: Int) {
        for index in indices.dropFirst().reversed()
            where distance(to: index).isMultiple(of: n)
        {
            insert(contentsOf: separator, at: index)
        }
    }

    func inserting<S: StringProtocol>(separator: S, every n: Int) -> Self {
        var string = self
        string.insert(separator: separator, every: n)
        return string
    }
}

extension String {
    func fromBase64() -> String? {
        guard let data = Data(base64Encoded: self) else {
            return nil
        }

        return String(data: data, encoding: .utf8)
    }

    func toBase64() -> String {
        return Data(utf8).base64EncodedString()
    }
}
