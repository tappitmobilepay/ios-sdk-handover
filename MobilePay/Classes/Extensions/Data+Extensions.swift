//
//  Data+extensions.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 18/03/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import CommonCrypto
import Foundation

extension Data {
    var prettyPrintedJSONString: NSString? { /// NSString gives us a nice sanitized debugDescription
        guard let object = try? JSONSerialization.jsonObject(with: self, options: []),
              let data = try? JSONSerialization.data(withJSONObject: object, options: [.prettyPrinted]),
              let prettyPrintedString = NSString(data: data, encoding: String.Encoding.utf8.rawValue) else { return nil }

        return prettyPrintedString
    }

    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        }
        catch {
            print("error:", error)
            return nil
        }
    }

    var html2String: String { html2AttributedString?.string ?? "" }
}
