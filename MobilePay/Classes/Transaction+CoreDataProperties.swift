//
//  Transaction+CoreDataProperties.swift
//
//
//  Created by Marius Kurgonas on 17/07/2020.
//
//

import CoreData
import Foundation

public extension Transaction {
    @nonobjc class func fetchRequest() -> NSFetchRequest<Transaction> {
        return NSFetchRequest<Transaction>(entityName: "Transaction")
    }

    @NSManaged var clientTransactionId: String?
    @NSManaged var type: Double
    @NSManaged var amount: Double
    @NSManaged var date: String?
    @NSManaged var currency: String?
    @NSManaged var event: String?
    @NSManaged var concessionaire: String?
    @NSManaged var items: NSSet?
}

// MARK: Generated accessors for items

public extension Transaction {
    @objc(addItemsObject:)
    @NSManaged func addToItems(_ value: TransactionItem)

    @objc(removeItemsObject:)
    @NSManaged func removeFromItems(_ value: TransactionItem)

    @objc(addItems:)
    @NSManaged func addToItems(_ values: NSSet)

    @objc(removeItems:)
    @NSManaged func removeFromItems(_ values: NSSet)
}
