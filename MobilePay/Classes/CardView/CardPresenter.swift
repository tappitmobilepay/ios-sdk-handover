//
//  CardPresenter.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 23/09/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation
import UIKit

enum CardValidationErrosTypes: String { case
    number,
    name,
    month,
    year,
    date,
    cvc,
    zip,
    terms
}

typealias ValidatedCardDetails = (cardNumber: String,
                                  name: String,
                                  expiryMonth: String,
                                  expiryYear: String,
                                  zipcode: String,
                                  cvc: String)

class CardPresenter: CardPresentingContract {
    weak var view: CardViewingContract?
    let router: RouterContract
    let paymentsService: PaymentsWebServiceContract
    let db: DatabaseManager
    var card: Card?
    let isAddingInitialCard: Bool
    var errorMessages = [CardValidationErrosTypes: String]()

    init(view: CardViewingContract,
         router: RouterContract,
         paymentsService: PaymentsWebServiceContract,
         db: DatabaseManager,
         card: Card?,
         isInitialCardSetup: Bool = false)
    {
        self.view = view
        self.router = router
        self.paymentsService = paymentsService
        self.db = db
        self.card = card
        isAddingInitialCard = isInitialCardSetup
    }

    func viewDidLoad() {
        view?.setup(card: card, isInitial: isAddingInitialCard)
    }

    func willGoBack() {}

    func didTapTermsAndConditions() {
        var devUrl = DevUtil.termsUrl()
        if let remoteUrlString = db.clientConfig()?.configuration.settings.termsAndConditions,
           let remoteUrl = URL(string: remoteUrlString)
        {
            devUrl = remoteUrl
        }

        UIApplication.shared.open(devUrl, options: [:], completionHandler: nil)
    }

    func didTapDelete() {
        view?.confirmDelete()
    }

    func didConfirmDelete() {
        if let card = card,
           let paymentProfileSettings = card.paymentProfileSettings
        {
            view?.showLoading()
            paymentsService.cancel()
            paymentsService.deletePaymentMethod(paymentProfileSettings: paymentProfileSettings) { [weak self] error in
                if let e = error {
                    showAlert(message: e.localizedDescription)
                }
                else {
                    Analytics.reportCardDeletedEvent()
                    self?.db.deleteCard(id: card.id)
                    self?.cardDeleted()
                }

                self?.view?.hideLoading()
            }
        }
        else {
            showAlert(message: "Internal error. Try again later", title: "Could not delete the card.", okTitle: "Ok", okCompletion: nil)
        }
    }

    func viewDidAppear() {
        if isAddingInitialCard {
            if card == nil {
                Analytics.reportShowInitialAddPaymentMethodScreenEvent()
            }
            view?.hideBackButton()
        }
        else {
            if let _ = card {
                Analytics.reportOpenCardDetailsEvent()
            }
            else {
                Analytics.reportAddAnotherCardEvent()
            }
            view?.showBackButton()
        }

        view?.showNavigationBar()
    }

    func tintFields() {
        view?.tintNumber(hasError: validateCardNumber() == nil)
        view?.tintName(hasError: validateName() == nil)
        let month = validateExpiryMonth()
        let year = validateExpiryYear()
        view?.tintMonth(hasError: month == nil)
        view?.tintYear(hasError: year == nil)
        if let m = month, let y = year {
            view?.tintDate(hasError: !validateDate(month: m, year: y))
        }
        view?.tintCvc(hasError: validateCvc() == nil)
        view?.tintZipcode(hasError: validateZip() == nil)
        view?.tintTerms(hasError: !validateTerms())
    }

    func viewWillAppear() {
        if isAddingInitialCard {
            view?.hideBackButton()
        }
        else {
            view?.showBackButton()
        }
        view?.showNavigationBar()
    }

    func didTapMakeDefault() {
        if let card = card {
            Analytics.reportClickedOnMakeCardDefaultButtonEvent(cardType: card.cardType)
        }

        view?.confirmMakeDefault()
    }

    func didConfirmMakeDefault() {
        if let card = card {
            view?.showLoading()

            guard let cardSettings = card.paymentProfileSettings else {
                // TODO: Check when this would be happening?!
                fatalError()
            }
            paymentsService.cancel()
            paymentsService.makeDefaultPaymentMethod(settings: cardSettings) { [weak self] response, error in
                if let e = error {
                    showAlert(message: e.localizedDescription)
                }
                else if response?.missingConfiguration == .some(true) {
                    self?.purgeCardsData()
                }
                else {
                    self?.db.makeCardDefault(id: card.id)
                    self?.cardMadeDefault()
                }

                self?.view?.hideLoading()
            }
        }
    }

    func didTapConfirm() {
        addCard()
    }

    func validateCardNumber() -> String? {
        // Validate card number
        guard let cardNumber = view?.cardNumber?
            .replacingOccurrences(of: " ", with: "")
            .trimmingCharacters(in: .whitespacesAndNewlines),
            !cardNumber.isEmpty
        else {
            errorMessages[.number] = "Please provide a card number."
            return nil
        }

        guard CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: cardNumber)) else {
            errorMessages[.number] = "Card number should only contain numbers."
            return nil
        }

        return cardNumber
    }

    func validateName() -> String? {
        // Validate name
        guard let name = view?.name?
            .trimmingCharacters(in: .whitespacesAndNewlines),
            !name.isEmpty
        else {
            errorMessages[.name] = "Please provide a name."
            return nil
        }

        return name
    }

    func validateExpiryMonth() -> String? {
        // Validate expiry month
        guard let expiryMonth = view?.expiryMonth?
            .trimmingCharacters(in: .whitespacesAndNewlines),
            !expiryMonth.isEmpty
        else {
            errorMessages[.month] = "Please provide an expiry month."
            return nil
        }

        guard expiryMonth.count == 2,
              let firstChar = expiryMonth.first,
              let firstInt = Int("\(firstChar)"),
              let lastChar = expiryMonth.last,
              let lastInt = Int("\(lastChar)"),
              firstInt == 0 && lastInt >= 1 && lastInt <= 9 ||
              firstInt == 1 && lastInt >= 0 && lastInt <= 2
        else {
            errorMessages[.month] = "Please provide a valid expiry month."
            return nil
        }

        guard CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: expiryMonth)) else {
            errorMessages[.month] = "Expiry month should only contain numbers."
            return nil
        }

        return expiryMonth
    }

    func validateExpiryYear() -> String? {
        // Validate expiry year
        guard let expiryYear = view?.expiryYear?
            .trimmingCharacters(in: .whitespacesAndNewlines),
            !expiryYear.isEmpty
        else {
            errorMessages[.year] = "Please provide an expiry year."
            return nil
        }

        guard expiryYear.count == 2 else {
            errorMessages[.year] = "Please provide a valid expiry year"
            return nil
        }

        guard CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: expiryYear)) else {
            errorMessages[.year] = "Expiry year should only contain numbers."
            return nil
        }

        return expiryYear
    }

    func validateDate(month: String?, year: String?) -> Bool {
        var valid = true

        let calendar = Calendar.current
        let dateComponents = calendar.dateComponents([.year, .month], from: Date())

        if let month = month,
           let year = year,
           let date = calendar.date(from: dateComponents)
        {
            // Check that expiry is in the future
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM/yy"

            if let enteredDate = dateFormatter.date(from: "\(month)/\(year)"),
               let currentDate = dateFormatter.date(from: dateFormatter.string(from: date)),
               enteredDate.compare(currentDate) != ComparisonResult.orderedAscending
            {
            }
            else {
                errorMessages[.date] = "Please provide expiry date that is in the future."
                valid = false
            }
        }
        else {
            errorMessages[.date] = "Please fill in the expiry date."
            valid = false
        }

        return valid
    }

    func validateCvc() -> String? {
        // Validate cvc
        guard let cvc = view?.cvc?
            .trimmingCharacters(in: .whitespacesAndNewlines),
            !cvc.isEmpty
        else {
            errorMessages[.cvc] = "Please provide a cvc."
            return nil
        }

        guard cvc.count > 2 else {
            errorMessages[.cvc] = "Please provide a valid cvc number"
            return nil
        }

        guard CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: cvc)) else {
            errorMessages[.cvc] = "cvc should only contain numbers."
            return nil
        }

        return cvc
    }

    func validateZip() -> String? {
        // Validate zipcode
        guard let zipcode = view?.zipcode?
            .trimmingCharacters(in: .whitespacesAndNewlines),
            !zipcode.isEmpty
        else {
            errorMessages[.zip] = "Please provide a zipcode."
            return nil
        }

        return zipcode
    }

    func validateTerms() -> Bool {
        // Validate accepted terms and conditions
        guard view?.aggreedToTerms == .some(true) else {
            errorMessages[.terms] = "Please agree to the terms and conditions."
            return false
        }

        return true
    }

    func validateInput() -> ValidatedCardDetails? {
        errorMessages = [:]

        let cn = validateCardNumber()
        let n = validateName()
        let month = validateExpiryMonth()
        let year = validateExpiryYear()
        let c = validateCvc()
        let z = validateZip()

        guard let cardNumber = cn,
              let name = n,
              let expirationMonth = month,
              let expirationYear = year,
              let cvc = c,
              let zipCode = z
        else {
            return nil
        }

        _ = validateDate(month: expirationMonth, year: expirationYear)

        _ = validateTerms()

        return errorMessages.count > 0 ? nil :
            ValidatedCardDetails(cardNumber: cardNumber,
                                 name: name,
                                 expiryMonth: expirationMonth,
                                 expiryYear: expirationYear,
                                 zipcode: zipCode,
                                 cvc: cvc)
    }

    func didTapSkipAddCard() {
        router.presentHome(animated: true, replaceLast: true)
    }

    func addCard() {
        defer {
            tintFields()
        }

        guard let validatedCardDetails = validateInput() else {
            showErrors()
            return
        }

        createCard(validatedCardDetails: validatedCardDetails)
    }

    func showErrors() {
        if errorMessages.count > 0 {
            let validationErrors = errorMessages.values.joined(separator: "\n")
            view?.showValidationError(errorString: validationErrors)
        }
        else {
            view?.hideValidationError()
        }
    }

    func didChangeDataInForm() {
        let cardData = validateInput()
        view?.tintConfirmButton(highlight: cardData != nil)
    }

    func createCard(validatedCardDetails: ValidatedCardDetails) {
        view?.showLoading()

        let cardInfo = CardInformation(cardNumber: validatedCardDetails.cardNumber, expirationMonth: validatedCardDetails.expiryMonth, expirationYear: validatedCardDetails.expiryYear, nameOnCard: validatedCardDetails.name, cvv: validatedCardDetails.cvc, zipCode: validatedCardDetails.zipcode)

        guard let encryptedCard = cardInfo.encrypted() else {
            showAlert(message: "Could not encrypt card details")
            view?.hideLoading()
            return
        }

        let data = PaymentMethodRequest(data: encryptedCard,
                                        refId: NSUUID().uuidString)

        paymentsService.cancel()
        paymentsService.addPaymentMethod(data: data) { [weak self] card, error in
            self?.view?.hideLoading()
            if let error = error {
                let errorMessage = error.localizedDescription
                if self?.isAddingInitialCard == .some(true) {
                    Analytics.reportFailedAddingInitialPaymentMethodEvent(failureMessage: errorMessage)
                }
                else {
                    Analytics.reportFailedAddingSubsequentPaymentMethodEvent(failureMessage: errorMessage)
                }
                showAlert(message: errorMessage)
            }
            else if let card = card {
                if self?.isAddingInitialCard == .some(true) {
                    Analytics.reportAddedFirstPaymentMethodSuccess(cardType: card.cardType)
                }
                else {
                    Analytics.reportAdditionalCardAddSuccessEvent(cardType: card.cardType)
                }
                self?.db.add(card: card)
                self?.presentNextScreen()
            }
            else {
                assert(false)
            }
        }
    }

    func presentNextScreen() {
        router.presentSuccess(type: .AddCard, message: nil) { [weak self] in
            if self?.isAddingInitialCard == .some(true) {
                self?.router.presentHome(animated: true, replaceLast: true)
            }
            else {
                self?.router.goBack(to: .PaymentMethods)
            }
        }
    }

    func cardMadeDefault() {
        router.presentSuccess(type: .MakeCardDefault, message: nil) { [weak self] in
            self?.router.goBack(to: .PaymentMethods)
        }
    }

    func cardDeleted() {
        router.presentSuccess(type: .DeleteCard, message: nil) { [weak self] in
            self?.router.goBack(to: .PaymentMethods)
        }
    }

    func purgeCardsData() {
        db.deleteAllCards()
        router.presentAddInitialCard(animated: true)
    }

    func didTapScanCardButton() {
        if #available(iOS 13, *) {
            router.presentCreditCardScanner(delegate: self)
        }
    }
}

@available(iOS 13, *)
extension CardPresenter: CreditCardScannerViewControllerDelegate {
    func creditCardScannerViewControllerDidCancel(_ viewController: CreditCardScannerViewController) {
        router.dismissModalController()
    }

    func creditCardScannerViewController(_ viewController: CreditCardScannerViewController, didErrorWith error: CreditCardScannerError) {
        showAlert(message: error.failureReason ?? "Something went wrong.")
        router.dismissModalController()
    }

    func creditCardScannerViewController(_ viewController: CreditCardScannerViewController, didFinishWith card: CreditCard) {
        var expiryYear: String?
        var expiryMonth: String?

        if let year = card.expireDate?.year,
           let month = card.expireDate?.month
        {
            expiryYear = "\(year % 100)"
            expiryMonth = month < 10 ? "0\(month)" : "\(month)"
        }

        let scannedCard = ScannedCardDetails(number: card.number?.formattedCreditCard(),
                                             name: card.name,
                                             expiryYear: expiryYear,
                                             expiryMonth: expiryMonth)
        view?.populate(card: scannedCard)
        router.dismissModalController()
    }
}

struct ScannedCardDetails {
    let number: String?
    let name: String?
    let expiryYear: String?
    let expiryMonth: String?
}
