//
//  CardViewingContract.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 23/09/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation

protocol CardViewingContract: AnyObject {
    func setup(card: Card?, isInitial: Bool)

    func populate(card: ScannedCardDetails)

    func showLoading()

    func hideLoading()

    func confirmDelete()

    func showBackButton()

    func hideBackButton()

    func showNavigationBar()

    func confirmMakeDefault()

    var cardNumber: String? { get }

    var name: String? { get }

    var expiryMonth: String? { get }

    var expiryYear: String? { get }

    var cvc: String? { get }

    var zipcode: String? { get }

    var aggreedToTerms: Bool { get }

    func tintNumber(hasError: Bool)

    func tintName(hasError: Bool)

    func tintMonth(hasError: Bool)

    func tintYear(hasError: Bool)

    func tintDate(hasError: Bool)

    func tintCvc(hasError: Bool)

    func tintZipcode(hasError: Bool)

    func tintTerms(hasError: Bool)

    func showValidationError(errorString: String)

    func hideValidationError()

    func tintConfirmButton(highlight: Bool)
}
