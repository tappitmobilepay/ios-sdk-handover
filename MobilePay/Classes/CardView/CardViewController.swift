//
//  CardViewController.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 05/05/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation
import UIKit

class CardViewController: BaseScreenViewController, CardViewingContract {
    var presenter: CardPresentingContract?

    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var visaImageView: UIImageView!
    @IBOutlet var mastercardImageView: UIImageView!
    @IBOutlet var americanExpressImageView: UIImageView!
    @IBOutlet var paypalImageView: UIImageView!
    @IBOutlet var visaImageContainer: UIView!
    @IBOutlet var mastercardImageContainer: UIView!
    @IBOutlet var amexImageContainer: UIView!
    @IBOutlet var paypalImageContainer: UIView!
    @IBOutlet var confirmButton: UIButton!
    @IBOutlet var makeDefaultButton: UIButton!
    @IBOutlet var makeDefaultButtonHeightConstaint: NSLayoutConstraint!
    @IBOutlet var cardNumberTextfield: UITextField!
    @IBOutlet var nameTextField: UITextField!
    @IBOutlet var expiryMonthTextField: UITextField!
    @IBOutlet var expiryYearTextField: UITextField!
    @IBOutlet var cvcTextfield: UITextField!
    @IBOutlet var paymentMethodsLabel: UILabel!
    @IBOutlet var cardNumberLabel: UILabel!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var expiryLabel: UILabel!
    @IBOutlet var cvcLabel: UILabel!
    @IBOutlet var checkbox: CheckBox!
    @IBOutlet var termsAndConditionsLabel: UILabel!
    @IBOutlet var zipcodeTextfield: UITextField!
    @IBOutlet var zipcodeLabel: UILabel!
    @IBOutlet var errorContainerView: UIView!
    @IBOutlet var errorTextLabel: UILabel!

    var name: String? {
        return nameTextField.text
    }

    var cardNumber: String? {
        return cardNumberTextfield.text
    }

    var expiryMonth: String? {
        return expiryMonthTextField.text
    }

    var expiryYear: String? {
        return expiryYearTextField.text
    }

    var cvc: String? {
        return cvcTextfield.text
    }

    var aggreedToTerms: Bool {
        return checkbox.isChecked
    }

    var zipcode: String? {
        return zipcodeTextfield.text
    }

    var termsRange: NSRange?

    override func viewDidLoad() {
        super.viewDidLoad()

        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }

        guard let presenter = presenter else {
            fatalError()
        }

        presenter.viewDidLoad()
    }

    private func hideMakeDefaultButton() {
        makeDefaultButtonHeightConstaint.isActive = false
        makeDefaultButton.isHidden = true
        let makeDefaultButtonHeightConstraint = NSLayoutConstraint(item: makeDefaultButton as Any, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 0)
        makeDefaultButton.addConstraint(makeDefaultButtonHeightConstraint)
    }

    override func pop() {
        guard let presenter = presenter else {
            fatalError()
        }

        presenter.willGoBack()

        super.pop()
    }

    @objc
    func didTapTermsAndConditions(gesture: UITapGestureRecognizer) {
        guard let range = termsRange else {
            return
        }

        if gesture.didTapAttributedTextInLabel(label: termsAndConditionsLabel, inRange: range) {
            guard let presenter = presenter else {
                fatalError()
            }

            presenter.didTapTermsAndConditions()
        }
        else {}
    }

    func confirmDelete() {
        showTwoButtonAlert(title: "Delete payment method", message: "Are you sure you want to delete this payment method?", okTitle: "Delete", cancelTitle: "Cancel") { [weak self] in
            self?.presenter?.didConfirmDelete()
        }
    }

    @objc
    func didTapDelete() {
        guard let presenter = presenter else {
            fatalError()
        }

        presenter.didTapDelete()
    }

    func showNavigationBar() {
        navigationController?.setNavigationBarHidden(false, animated: true)
    }

    func showLoading() {
        TMWLoaderView.shareInstance().showLoader(navigationController!.view)
    }

    func hideLoading() {
        TMWLoaderView.shareInstance().hideLoader()
    }

    override func viewDidAppear(_ animated: Bool) {
        guard let presenter = presenter else {
            fatalError()
        }

        presenter.viewDidAppear()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        guard let presenter = presenter else {
            fatalError()
        }

        presenter.viewWillAppear()
    }

    func confirmMakeDefault() {
        showTwoButtonAlert(title: "Make default",
                           message: "Are you sure you want to make this the default card?",
                           okTitle: "Confirm",
                           cancelTitle: "Cancel") { [weak self] in
            self?.presenter?.didConfirmMakeDefault()
        }
    }

    @IBAction func didTapMakeDefaultButton(_ sender: Any) {
        guard let presenter = presenter else {
            fatalError()
        }

        presenter.didTapMakeDefault()
    }

    @IBAction func didTapConfirmButton(_ sender: Any) {
        guard let presenter = presenter else {
            fatalError()
        }

        presenter.didTapConfirm()
    }

    func tintNumber(hasError: Bool) {
        cardNumberTextfield.layer.borderColor = hasError ?
            UIColor.red.cgColor :
            UIColor.black.cgColor
    }

    func tintName(hasError: Bool) {
        nameTextField.layer.borderColor = hasError ?
            UIColor.red.cgColor :
            UIColor.black.cgColor
    }

    func tintMonth(hasError: Bool) {
        expiryMonthTextField.layer.borderColor = hasError ?
            UIColor.red.cgColor :
            UIColor.black.cgColor
    }

    func tintYear(hasError: Bool) {
        expiryYearTextField.layer.borderColor = hasError ?
            UIColor.red.cgColor :
            UIColor.black.cgColor
    }

    func tintDate(hasError: Bool) {
        tintMonth(hasError: hasError)
        tintYear(hasError: hasError)
    }

    func tintCvc(hasError: Bool) {
        cvcTextfield.layer.borderColor = hasError ?
            UIColor.red.cgColor :
            UIColor.black.cgColor
    }

    func tintZipcode(hasError: Bool) {
        zipcodeTextfield.layer.borderColor = hasError ?
            UIColor.red.cgColor :
            UIColor.black.cgColor
    }

    func tintTerms(hasError: Bool) {
        checkbox.layer.borderColor = hasError ?
            UIColor.red.cgColor :
            UIColor.black.cgColor
    }

    func showValidationError(errorString: String) {
        errorContainerView.isHidden = false
        errorTextLabel.text = errorString
    }

    func hideValidationError() {
        errorContainerView.isHidden = true
        errorTextLabel.text = ""
    }

    func populate(card: ScannedCardDetails) {
        cardNumberTextfield.text = card.number
        nameTextField.text = card.name
        expiryMonthTextField.text = card.expiryMonth
        expiryYearTextField.text = card.expiryYear
    }

    @objc
    func didChangeValueInForm() {
        presenter?.didChangeDataInForm()
    }

    func tintConfirmButton(highlight: Bool) {
        confirmButton.backgroundColor = highlight ? DevUtil.color(name: Colors.tmw_primary_button_background.rawValue) : .gray
    }

    func setup(card: Card?, isInitial: Bool) {
        addDefaultTitleViewLogo()

        confirmButton.setTitleColor(DevUtil.color(name: Colors.tmw_primary_button_text.rawValue), for: .normal)
        confirmButton.backgroundColor = DevUtil.color(name: Colors.tmw_primary_button_background.rawValue)
        confirmButton.layer.borderWidth = 1
        confirmButton.borderColor = DevUtil.color(name: Colors.tmw_primary_button_border.rawValue)
        confirmButton.titleLabel?.font = UIFont.with(size: .LargeMedium, weight: .primaryButton)

        if let card = card {
            confirmButton.setTitle("DELETE", for: .normal)
            confirmButton.removeTarget(self, action: #selector(didTapConfirmButton(_:)), for: .touchUpInside)
            confirmButton.addTarget(self, action: #selector(didTapDelete), for: .touchUpInside)

            cardNumberTextfield.text = card.cardNumber
            cardNumberTextfield.isUserInteractionEnabled = false
            nameTextField.isUserInteractionEnabled = false
            nameTextField.text = "XXXXX XXXXX"
            expiryMonthTextField.isUserInteractionEnabled = false
            expiryMonthTextField.text = "XX"
            expiryYearTextField.isUserInteractionEnabled = false
            expiryYearTextField.text = "XX"
            cvcTextfield.isUserInteractionEnabled = false
            cvcTextfield.text = "XXXX"
            zipcodeTextfield.isUserInteractionEnabled = false
            zipcodeTextfield.text = "XXXX"

            if !card.isDefault {
                makeDefaultButton.setTitle("MAKE DEFAULT", for: .normal)
                makeDefaultButton.setTitleColor(DevUtil.color(name: Colors.tmw_primary_button_text.rawValue), for: UIControl.State.normal)
                makeDefaultButton.layer.borderWidth = 1
                makeDefaultButton.borderColor = DevUtil.color(name: Colors.tmw_primary_button_border.rawValue)
                makeDefaultButton.titleLabel?.font = UIFont.with(size: .LargeMedium, weight: .primaryButton)
                makeDefaultButton.backgroundColor = DevUtil.color(name: Colors.tmw_primary_button_background.rawValue)
                makeDefaultButton.addDefaultRoundCorners()
            }
            else {
                hideMakeDefaultButton()
            }

            checkbox.isHidden = true
            termsAndConditionsLabel.isHidden = true
        }
        else {
            // I will do this later button
            let attributedTitle = NSAttributedString(string: "I'LL DO THIS LATER", attributes:
                [.underlineStyle: NSUnderlineStyle.single.rawValue])
            makeDefaultButton.setAttributedTitle(attributedTitle, for: .normal)
            makeDefaultButton.setTitleColor(.black, for: UIControl.State.normal)
            makeDefaultButton.titleLabel?.font = UIFont.with(size: .LargeMedium, weight: .primaryButton)
            makeDefaultButton.removeTarget(self, action: #selector(didTapMakeDefaultButton(_:)), for: .touchUpInside)
            makeDefaultButton.addTarget(self, action: #selector(didTapSkipAddCard), for: .touchUpInside)

            if !isInitial {
                makeDefaultButton.isHidden = true
            }

            confirmButton.setTitleColor(.white, for: .normal)
            confirmButton.backgroundColor = .gray
        }

        cardNumberTextfield.addTarget(self, action: #selector(didChangeValueInForm), for: .editingChanged)
        nameTextField.addTarget(self, action: #selector(didChangeValueInForm), for: .editingChanged)
        expiryMonthTextField.addTarget(self, action: #selector(didChangeValueInForm), for: .editingChanged)
        expiryYearTextField.addTarget(self, action: #selector(didChangeValueInForm), for: .editingChanged)
        cvcTextfield.addTarget(self, action: #selector(didChangeValueInForm), for: .editingChanged)
        zipcodeTextfield.addTarget(self, action: #selector(didChangeValueInForm), for: .editingChanged)
        checkbox.addTarget(self, action: #selector(didChangeValueInForm), for: .valueChanged)

        checkbox.checkmarkColor = DevUtil.color(name: Colors.tmw_primary_background.rawValue)
        checkbox.checkedBorderColor = .black
        checkbox.uncheckedBorderColor = .black
        checkbox.borderWidth = 1

        cardNumberTextfield.font = UIFont.with(size: .Medium, weight: .normal)
        nameTextField.font = cardNumberTextfield.font
        expiryYearTextField.font = cardNumberTextfield.font
        expiryMonthTextField.font = cardNumberTextfield.font
        cvcTextfield.font = cardNumberTextfield.font
        zipcodeTextfield.font = cardNumberTextfield.font

        visaImageView.image = DevUtil.imageSkin(name: "tmw_visa")
        mastercardImageView.image = DevUtil.imageSkin(name: "tmw_mastercard")
        americanExpressImageView.image = DevUtil.imageSkin(name: "tmw_american_express")
        paypalImageView.image = DevUtil.imageSkin(name: "tmw_discover")

        cvcTextfield.placeholder = "XXXX"

        confirmButton.addDefaultRoundCorners()

        cardNumberTextfield.addDefaultBorder(width: 1)
        cardNumberTextfield.addDoneCancelToolbar()
        nameTextField.addDefaultBorder(width: 1)
        expiryMonthTextField.addDefaultBorder(width: 1)
        expiryMonthTextField.addDoneCancelToolbar()
        expiryYearTextField.addDefaultBorder(width: 1)
        expiryYearTextField.addDoneCancelToolbar()
        cvcTextfield.addDefaultBorder(width: 1)
        cvcTextfield.addDoneCancelToolbar()
        zipcodeTextfield.addDefaultBorder(width: 1)
        zipcodeTextfield.addDoneCancelToolbar()

        paymentMethodsLabel.textColor = .black
        paymentMethodsLabel.font = UIFont.with(size: .LightNormal1, weight: .normal)
        cardNumberLabel.textColor = .black
        cardNumberLabel.font = paymentMethodsLabel.font
        nameLabel.textColor = .black
        nameLabel.font = paymentMethodsLabel.font
        expiryLabel.textColor = .black
        expiryLabel.font = paymentMethodsLabel.font
        cvcLabel.textColor = .black
        cvcLabel.font = paymentMethodsLabel.font
        zipcodeLabel.textColor = .black
        zipcodeLabel.font = paymentMethodsLabel.font

        termsAndConditionsLabel.font = UIFont.with(size: .Medium, weight: .normal)

        cardNumberTextfield.delegate = self
        cardNumberTextfield.returnKeyType = .next
        expiryMonthTextField.delegate = self
        expiryMonthTextField.returnKeyType = .next
        nameTextField.returnKeyType = .next
        expiryYearTextField.delegate = self
        expiryYearTextField.returnKeyType = .next
        cvcTextfield.delegate = self
        cvcTextfield.returnKeyType = .next
        nameTextField.delegate = self
        zipcodeTextfield.returnKeyType = .done

        expiryMonthTextField.addTarget(self, action: #selector(textFieldDidChangeValue), for: .editingChanged)
        expiryYearTextField.addTarget(self, action: #selector(textFieldDidChangeValue), for: .editingChanged)
        cvcTextfield.addTarget(self, action: #selector(textFieldDidChangeValue), for: .editingChanged)

        if #available(iOS 13, *), card == nil {
            cardNumberTextfield.rightViewMode = .always
            let button = UIButton()
            button.addTarget(self, action: #selector(didTapScanCard), for: .touchUpInside)
            var frame = button.frame
            frame.origin.x = CGFloat(-20)
            button.frame = frame
            button.setImage(UIImage(named: "camera-icon", in: Bundle(for: CardViewController.self), compatibleWith: nil), for: .normal)
            cardNumberTextfield.rightView = button
            cardNumberTextfield.rightView?.layer.sublayerTransform = CATransform3DMakeTranslation(-10.0, 0.0, 0.0)
        }

        let term = "Terms and Conditions"
        let termText = "By clicking ADD, you are agreeing to the "

        let attributedString = NSMutableAttributedString()
        attributedString.append(NSAttributedString(string: termText,
                                                   attributes: [.underlineStyle: 0]))
        attributedString.append(NSAttributedString(string: term,
                                                   attributes: [
                                                       .underlineStyle: NSUnderlineStyle.single.rawValue,
                                                       .foregroundColor: DevUtil.color(name: Colors.tmw_primary_background.rawValue),
                                                       .underlineColor: DevUtil.color(name: Colors.tmw_primary_background.rawValue)
                                                   ]))

        termsRange = (attributedString.string as NSString).range(of: term)

        termsAndConditionsLabel.attributedText = attributedString

        let gr = UITapGestureRecognizer(target: self, action: #selector(didTapTermsAndConditions))
        termsAndConditionsLabel.addGestureRecognizer(gr)
    }

    @objc
    func didTapSkipAddCard() {
        presenter!.didTapSkipAddCard()
    }

    @objc
    func didTapScanCard() {
        presenter?.didTapScanCardButton()
    }

    @objc
    func textFieldDidChangeValue() {
        if cvcTextfield.text?.count == .some(4) {
            zipcodeTextfield.becomeFirstResponder()
        }
        else if expiryYearTextField.text?.count == .some(2) {
            cvcTextfield.becomeFirstResponder()
        }
        else if expiryMonthTextField.text?.count == .some(2) {
            expiryYearTextField.becomeFirstResponder()
        }
    }
}

extension CardViewController: UITextFieldDelegate {
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if action == #selector(UIResponderStandardEditActions.copy(_:)) {
            return false
        }

        return super.canPerformAction(action, withSender: sender)
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == cardNumberTextfield {
            nameTextField.becomeFirstResponder()
        }
        else if textField == nameTextField {
            expiryMonthTextField.becomeFirstResponder()
        }
        else if textField == expiryMonthTextField {
            expiryYearTextField.becomeFirstResponder()
        }
        else if textField == expiryYearTextField {
            cvcTextfield.becomeFirstResponder()
        }
        else if textField == cvcTextfield {
            zipcodeTextfield.becomeFirstResponder()
        }
        else {
            textField.resignFirstResponder()
        }

        return true
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        scrollView.setContentOffset(CGPoint(x: 0, y: (textField.superview?.frame.origin.y)!), animated: true)
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard var presenter = presenter else {
            fatalError()
        }

        if textField == cvcTextfield {
            guard let text = textField.text, text.count <= 3 || string == "" else {
                return false
            }
        }
        else if textField == expiryMonthTextField {
            guard let currentText = textField.text as NSString? else {
                return false
            }
            let updatedText = currentText.replacingCharacters(in: range, with: string)

            if string == "" {
                return true
            }
            else if updatedText.count == 2 {
                if updatedText > "12" {
                    return false
                }
                return true
            }
            else if updatedText.count > 2 {
                return false
            }
            else if updatedText.count == 1 {
                if updatedText > "1" {
                    return false
                }
                return true
            }

            return false
        }
        else if textField == expiryYearTextField {
            guard let currentText = textField.text as NSString? else {
                return false
            }
            let updatedText = currentText.replacingCharacters(in: range, with: string)

            if string == "" {
                return true
            }
            else if updatedText.count == 2 {
                if let intValue = Int(updatedText), intValue <= 99, intValue >= 20 {
                    return true
                }
                return false
            }
            else if updatedText.count > 2 {
                return false
            }

            if updatedText.count < 2 {
                textField.text = updatedText
            }
            return false
        }
        else if textField == cardNumberTextfield {
            if string == "" {
                return true
            }

            // range.length will be greater than 0 if user is deleting text - allow it to replace
            if range.length > 0 {
                return true
            }

            // Don't allow empty strings
            if string == " " {
                return false
            }

            // Check for max length including the spacers we added
//            onlyDebugPrint(range.location)
            if range.location > 18 {
                return false
            }

            var originalText = textField.text
            let replacementText = string.replacingOccurrences(of: " ", with: "")

            // Verify entered text is a numeric value
            let digits = NSCharacterSet.decimalDigits
            for char in replacementText.unicodeScalars {
                if !(digits as NSCharacterSet).longCharacterIsMember(char.value) {
                    return false
                }
            }

            // Put an empty space after every 4 places
            if (originalText?.count)! > 0 {
                if (originalText?.count)! < 5, (originalText?.count)! % 4 == 0 {
                    originalText?.append(" ")
                }
                else if ((originalText?.count)! + 1) % 5 == 0 {
                    originalText?.append(" ")
                }
            }

            textField.text = originalText
        }
        else if textField == nameTextField {
            if let currentText = textField.text,
               currentText.count >= 254
            {
                return false
            }
        }
        else if textField == zipcodeTextfield {
            if let currentText = textField.text,
               currentText.count >= 50
            {
                return false
            }
        }

        return true
    }
}
