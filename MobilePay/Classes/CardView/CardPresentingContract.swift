//
//  CardPresentingContract.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 23/09/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation

protocol CardPresentingContract {
    var view: CardViewingContract? { get }

    func viewDidLoad()

    func willGoBack()

    func didTapTermsAndConditions()

    func didTapDelete()

    func didConfirmDelete()

    func viewDidAppear()

    func viewWillAppear()

    func didTapMakeDefault()

    func didConfirmMakeDefault()

    func didTapConfirm()

    func didTapScanCardButton()

    func didTapSkipAddCard()

    func didChangeDataInForm()
}
