//
//  MobilePayPresenter.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 04/06/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation
import PassKit
import UIKit

enum AuthenticationMethod: String {
    case biometric, pin
}

class MobilePayPresenter: MobilePayPresentingContract {
    weak var view: MobilePayViewContract?
    private var router: RouterContract

    init(view: MobilePayViewContract,
         navController: UINavigationController,
         ssoLogin: SSOLogin,
         credStore: CredentialsStoringContract,
         deepLink: DeepLink?)
    {
        self.view = view
        router = Router(navigationController: navController,
                        ssoLogin: ssoLogin,
                        credStore: credStore,
                        deepLink: deepLink)
        let pfm = PaymentFlowManager(currentState: .home,
                                     db: router.db,
                                     credStore: credStore,
                                     router: router)
        router.paymentFlowManager = pfm
        NotificationCenter.default.addObserver(self, selector: #selector(didEnterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
    }

    @objc
    func didEnterForeground() {
        router.goBack(to: .Authentication)
    }

    func viewDidLoad() {
        TMWCommonUtil.checkAndLoadFonts()

        showProperScreen()
    }

    func showProperScreen() {
        let internetAvailable = TMWCommonUtil.isInternetAvailable()
        let config = router.db.clientConfig()

        if internetAvailable || config == nil {
            LoginManager(db: router.db,
                         credStore: router.credStore)
                .softLogout()
            view?.setup(viewController: router.buildSSO())
        }
        else {
            let cards = router.db.allCreditCards()
            if cards.isEmpty, TMWCommonUtil.userDefaults.bool(forKey: Constants.kIsAlreadyLoginKey) {
                router.presentSSOInformation(animated: false)
            }
            else {
                isAuthendicated(router: router)
            }
        }
    }

    func willHandleUrl(url: URL) -> Bool {
        var termsUrl = DevUtil.termsUrl()
        if let remoteUrlString = router.db.clientConfig()?.configuration.settings.termsAndConditions,
           let remoteUrl = URL(string: remoteUrlString)
        {
            termsUrl = remoteUrl
        }

        let isTermsUrl = url.absoluteString.range(of: termsUrl.absoluteString) != nil

        var isPrivacyPolicyUrl = false
        if let privacyUrlString = router.db.clientConfig()?.configuration.settings.privacyPolicy,
           url.absoluteString.range(of: privacyUrlString) != nil
        {
            isPrivacyPolicyUrl = true
        }

        var helpUrl = DevUtil.helpUrl()
        if let remoteUrlString = router.db.clientConfig()?.configuration.settings.help,
           let remoteUrl = URL(string: remoteUrlString)
        {
            helpUrl = remoteUrl
        }

        let isHelpUrl = url.absoluteString.range(of: helpUrl.absoluteString) != nil

        return isHelpUrl || isTermsUrl || isPrivacyPolicyUrl
    }
}

extension String {
    private static let escapedChars = [
        (#"\0"#, "\0"),
        (#"\t"#, "\t"),
        (#"\n"#, "\n"),
        (#"\r"#, "\r"),
        (#"\""#, "\""),
        (#"\'"#, "\'"),
        (#"\\"#, "\\")
    ]
    var escaped: String {
        unicodeScalars.map { $0.escaped(asASCII: false) }.joined()
    }

    var asciiEscaped: String {
        unicodeScalars.map { $0.escaped(asASCII: true) }.joined()
    }

    var unescaped: String {
        var result: String = self
        String.escapedChars.forEach {
            result = result.replacingOccurrences(of: $0.0, with: $0.1)
        }
        return result
    }
}

extension Dictionary {
    /// Convert Dictionary to JSON string
    /// - Throws: exception if dictionary cannot be converted to JSON data or when data cannot be converted to UTF8 string
    /// - Returns: JSON string
    func toJson() throws -> String {
        let data = try JSONSerialization.data(withJSONObject: self)
        if let string = String(data: data, encoding: .utf8) {
            return string
        }
        throw NSError(domain: "Dictionary", code: 1, userInfo: ["message": "Data cannot be converted to .utf8 string"])
    }
}

internal func isAuthendicated(router: RouterContract, animated: Bool = false) {
    if TMWCommonUtil.userDefaults.bool(forKey: Constants.kIsAlreadyLoginKey) {
        if let authMethod = router.credStore.authenticationMethod {
            switch authMethod {
            case .biometric:
                router.presentEnterBiometric(animated: animated)
                return
            case .pin:
                router.presentEnterPin(animated: animated)
                return
            }
        }

        router.presentAuthentication(animated: animated)
    }
}
