//
//  CardInformation.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 25/02/2021.
//  Copyright © 2021 JadePayments. All rights reserved.
//

import Foundation

struct CardInformation: Encodable {
    let cardNumber: String
    let expirationMonth: String
    let expirationYear: String
    let nameOnCard: String
    let cvv: String
    var zipCode: String

    func encrypted() -> String? {
        let encoder = JSONEncoder()
        guard let encodedData = try? encoder.encode(self),
              let encodedString = String(data: encodedData, encoding: .utf8)
        else {
            assert(false)
            return nil
        }

        guard let encrypted = RSA.encrypt(string: encodedString, publicKey: Security.RSA_ENCRYPTION_KEY, keySize: Security.RSA_ENCRYPTION_KEY_SIZE) else {
            return nil
        }

        return encrypted
    }
}
