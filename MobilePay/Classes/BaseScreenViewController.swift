//
//  BaseScreenViewController.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 16/09/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation
import UIKit

class BaseScreenViewController: UIViewController {
    func addDefaultTitleViewLogo() {
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }

        guard let navigation = navigationController else {
            assert(false, "Navigation controller not found")
            return
        }

        let imageName = "tmw_navigation_logo"
        let navigationHeight = navigation.navigationBar.frame.size.height * 0.7
        guard let image = DevUtil.imageSkin(name: imageName) else {
            assert(false)
            return
        }
        let percent = navigationHeight / image.size.height
        let resized = image.resize(targetSize: CGSize(width: image.size.width * percent, height: navigationHeight))
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = resized

        navigationItem.titleView = imageView

        edgesForExtendedLayout = [.all]
        extendedLayoutIncludesOpaqueBars = true
    }

    func showBackButton() {
        let backImage = DevUtil.imageSkin(name: "tmw_navigation_back")?.withRenderingMode(.alwaysTemplate)
        let item = UIBarButtonItem(image: backImage, style: .plain, target: self, action: #selector(pop))
        item.tintColor = DevUtil.color(name: Colors.tmw_back_indicator.rawValue)
        navigationItem.leftBarButtonItems = [item]
    }

    @objc
    func pop() {
        navigationController?.popViewController(animated: true)
    }

    func hideBackButton() {
        navigationItem.leftBarButtonItems = [UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)]
    }
}
