//
//  PaymentsURLSessionWebService.swift
//  MobilePay-PAYG
//
//  Created by Marius Kurgonas on 02/06/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation

class PaymentsURLSessionWebService: NSURLSessionWebService, PaymentsWebServiceContract {
    var responseParser: PaymentsResponseParsingContract?

    func checkTransactionStatus(qr: String, completion: @escaping CheckTransactionStatusResponse) {
        guard let parser = responseParser else {
            assert(false)
            return
        }

        task = get(at: PaymentsApiRoute.checkTransactionStatus(qr: qr.toBase64()), completion: { data, error in
            DispatchQueue.main.async {
                if error == nil {
                    do {
                        let response = try parser.transactionStatusResponse(from: data)
                        DispatchQueue.main.async {
                            completion(response, nil)
                        }
                    }
                    catch {
                        DispatchQueue.main.async {
                            completion(nil, error)
                        }
                    }
                }
                else {
                    DispatchQueue.main.async {
                        completion(nil, error)
                    }
                }
            }
        })
    }

    func getUserStats(fanId: String, completion: @escaping UserStatsResult) {
        guard let parser = responseParser else {
            assert(false)
            return
        }

        task = get(at: PaymentsApiRoute.customerStats(fanId: fanId), completion: { data, error in
            DispatchQueue.main.async {
                if error == nil {
                    do {
                        let response = try parser.userStatsResponse(from: data)
                        DispatchQueue.main.async {
                            completion(response, nil)
                        }
                    }
                    catch {
                        DispatchQueue.main.async {
                            completion(nil, error)
                        }
                    }
                }
                else {
                    DispatchQueue.main.async {
                        completion(nil, error)
                    }
                }
            }
        })
    }

    func deletePaymentMethod(paymentProfileSettings: [PaymentProfileSettings], completion: @escaping GenericResponse) {
        guard let parser = responseParser else {
            assert(false)
            return
        }

        let parameters = paymentProfileSettings.map { $0.dictionary ?? [:] }

        task = delete(at: PaymentsApiRoute.deletePaymentMethod, params: parameters, completion: { data, error in
            DispatchQueue.main.async {
                if error == nil {
                    do {
                        try parser.deletePaymentMethodResponse(from: data)
                        DispatchQueue.main.async {
                            completion(nil)
                        }
                    }
                    catch {
                        DispatchQueue.main.async {
                            completion(error)
                        }
                    }
                }
                else {
                    DispatchQueue.main.async {
                        completion(error)
                    }
                }
            }
        })
    }

    func requestAllPaymentMethods(completion: @escaping PaymentMethodsResponse) {
        guard let parser = responseParser else {
            assert(false)
            return
        }

        task = get(at: PaymentsApiRoute.paymentMethods, completion: { data, error in
            DispatchQueue.main.async {
                if error == nil {
                    do {
                        let cards = try parser.paymentMethodsResponse(from: data)
                        DispatchQueue.main.async {
                            completion(cards, nil)
                        }
                    }
                    catch {
                        DispatchQueue.main.async {
                            completion(nil, error)
                        }
                    }
                }
                else {
                    DispatchQueue.main.async {
                        completion(nil, error)
                    }
                }
            }
        })
    }

    func makeDefaultPaymentMethod(settings: [PaymentProfileSettings], completion: @escaping DefaultPaymentMethodResponse) {
        guard let parser = responseParser else {
            assert(false)
            return
        }

        var params = [[String: Any]]()

        settings.forEach {
            if let item = $0.dictionary {
                params.append(item)
            }
            else {
                assert(false)
            }
        }

        task = put(at: PaymentsApiRoute.makePaymentMethodDefault, params: params, completion: { data, error in
            DispatchQueue.main.async {
                if error == nil {
                    do {
                        let response = try parser.makePaymentMethodDefaultResponse(from: data)
                        DispatchQueue.main.async {
                            completion(response, nil)
                        }
                    }
                    catch {
                        DispatchQueue.main.async {
                            completion(nil, error)
                        }
                    }
                }
                else {
                    DispatchQueue.main.async {
                        completion(nil, error)
                    }
                }
            }
        })
    }

    func addPaymentMethod(data: PaymentMethodRequest,
                          completion: @escaping AddPaymentMethodResponse)
    {
        guard let parser = responseParser,
              let params = data.dictionary
        else {
            assert(false)
            return
        }

        task = post(at: PaymentsApiRoute.addPaymentMethod, params: params, completion: { data, error in
            DispatchQueue.main.async {
                if error == nil {
                    do {
                        let card = try parser.addPaymentMethodResponse(from: data)
                        DispatchQueue.main.async {
                            completion(card, nil)
                        }
                    }
                    catch {
                        DispatchQueue.main.async {
                            completion(nil, error)
                        }
                    }
                }
                else {
                    DispatchQueue.main.async {
                        completion(nil, error)
                    }
                }
            }
        })
    }

    func checkIfAllPaymentProvidersWereConfigured(completion: @escaping PaymentProvidersConfigResponse) {
        guard let parser = responseParser else {
            assert(false)
            return
        }

        task = get(at: PaymentsApiRoute.paymentProvidersConfiguration, completion: { data, error in
            DispatchQueue.main.async {
                if error == nil {
                    do {
                        let isConfigured = try parser.paymentProvidersConfigResponse(from: data)
                        DispatchQueue.main.async {
                            completion(isConfigured, nil)
                        }
                    }
                    catch {
                        DispatchQueue.main.async {
                            completion(nil, error)
                        }
                    }
                }
                else {
                    DispatchQueue.main.async {
                        completion(nil, error)
                    }
                }
            }
        })
    }

    func requestClientConfig(integrationKey: String, completion: @escaping ClientConfigResult) {
        guard let parser = responseParser else {
            fatalError("Elements not set")
        }

        task = get(at: OnboardingApiRoute.config(integrationKey: integrationKey), completion: { data, error in
            DispatchQueue.main.async {
                if error == nil {
                    do {
                        let response = try parser.clientConfigResponse(from: data)
                        DispatchQueue.main.async {
                            completion(response, nil)
                        }
                    }
                    catch {
                        DispatchQueue.main.async {
                            completion(nil, error)
                        }
                    }
                }
                else {
                    DispatchQueue.main.async {
                        completion(nil, error)
                    }
                }
            }
        })
    }

    override func headers() -> [String: String] {
        var headers = super.headers()
        headers["x-api-key"] = credStore.apiKey ?? ""
        return headers
    }

    func cancel() {
        task?.cancel()
        task = nil
    }

    var task: URLSessionTask?
}

struct PaymentMethodRequest: Codable {
    let data: String
    let refId: String
}
