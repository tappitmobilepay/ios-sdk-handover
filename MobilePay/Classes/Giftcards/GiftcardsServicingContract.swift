//
//  GiftcardsServicingContract.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 2021-08-06.
//  Copyright © 2021 JadePayments. All rights reserved.
//

import Foundation

protocol GiftcardsServicingContract: CancelableService {
    func redeemGiftcard(request: RedeemGiftcardRequest,
                        completion: @escaping GenericResponse)
}
