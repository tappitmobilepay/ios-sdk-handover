//
//  RedeemGiftcardRequest.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 2021-08-06.
//  Copyright © 2021 JadePayments. All rights reserved.
//

import Foundation

enum GifcardsProviderType: Int, Codable { case
    mock = 0,
    givex = 1
}

struct RedeemGiftcardRequest: Codable {
    var provider: GifcardsProviderType = .givex
    let cardNumber: String
    let securityCode: String
}
