//
//  GiftcardTableViewCell.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 2021-08-02.
//  Copyright © 2021 JadePayments. All rights reserved.
//

import Foundation
import UIKit

class GiftcardTableViewCell: UITableViewCell {
    @IBOutlet var statusButton: UIButton!
    @IBOutlet var containerView: UIView!
    @IBOutlet var concessionaireLabel: UILabel!
    @IBOutlet var expirationLabel: UILabel!
    @IBOutlet var balanceLabel: UILabel!
    @IBOutlet var availableBalanceTitleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        containerView.addPrimaryColorBorder()
        concessionaireLabel.font = UIFont.with(size: .LargeMedium, weight: .bold)
        availableBalanceTitleLabel.font = UIFont.with(size: .LargeMedium, weight: .normal)
        expirationLabel.font = UIFont.with(size: .Medium, weight: .normal)
        statusButton.setTitle("ACTIVE", for: .normal)
        statusButton.setTitleColor(.white, for: .normal)
        statusButton.titleLabel?.font = UIFont.with(size: .Large, weight: .benefitStatusButton)
        balanceLabel.font = UIFont.with(size: .BigMedium, weight: .bold)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        statusButton.layer.borderWidth = 1

        if selected {
            statusButton.layer.borderColor = DevUtil.color(name: Colors.tmw_selected_benefit_button_border.rawValue).cgColor
            statusButton.backgroundColor = DevUtil.color(name: Colors.tmw_selected_benefit_button_background.rawValue)
            statusButton.setTitleColor(DevUtil.color(name: Colors.tmw_selected_benefit_button_text.rawValue), for: .normal)
        }
        else {
            statusButton.layer.borderColor = DevUtil.color(name: Colors.tmw_unselected_benefit_button_border.rawValue).cgColor
            statusButton.backgroundColor = DevUtil.color(name: Colors.tmw_unselected_benefit_button_background.rawValue)
            statusButton.setTitleColor(DevUtil.color(name: Colors.tmw_unselected_benefit_button_text.rawValue), for: .normal)
        }
    }

    func setup(giftcard: ComplimentaryBalance, mode: GiveawayTokensScreenMode) {
        if mode == .viewOnly {
            statusButton.removeFromSuperview()
        }

        let expiryDate = giftcard.expiryDate
        let date = expiryDate?.replacingOccurrences(of: "T", with: " ")
            .replacingOccurrences(of: "Z", with: "")
        let dateString = date?.toDate(dateFormat: "yyyy-MM-dd HH:mm:ss")?.toString() ?? date

        expirationLabel.text = "Expires " + (dateString ?? "-")

        concessionaireLabel.text = giftcard.description

        balanceLabel.text = giftcard.amount.format(f: ".2")
        concessionaireLabel.preferredMaxLayoutWidth = UIScreen.main.bounds.width * 0.55
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        statusButton.addRoundCorners(radius: Int(statusButton.bounds.size.height * 0.5))
    }
}
