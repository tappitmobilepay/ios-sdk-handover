//
//  GiftcardsURLSessionWebService.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 2021-08-06.
//  Copyright © 2021 JadePayments. All rights reserved.
//

import Foundation

class GiftcardsURLSessionWebService: NSURLSessionWebService, GiftcardsServicingContract {
    var responseParser: GiftcardsResponseParsingContract?

    func redeemGiftcard(request: RedeemGiftcardRequest,
                        completion: @escaping GenericResponse)
    {
        guard let responseParser = responseParser,
              let params = request.dictionary
        else {
            assert(false)
            return
        }

        task = post(at: GiftcardsApiRoute.redeem,
                    params: params,
                    completion: { data, error in
                        if error == nil {
                            do {
                                try responseParser.redeemResponse(from: data)
                                DispatchQueue.main.async {
                                    completion(nil)
                                }
                            }
                            catch {
                                DispatchQueue.main.async {
                                    completion(error)
                                }
                            }
                        }
                        else {
                            DispatchQueue.main.async {
                                completion(error)
                            }
                        }
                    })
    }

    func cancel() {
        task?.cancel()
        task = nil
    }

    var task: URLSessionDataTask?
}
