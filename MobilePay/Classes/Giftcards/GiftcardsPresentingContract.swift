//
//  GiftcardsPresentingContract.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 2021-08-02.
//  Copyright © 2021 JadePayments. All rights reserved.
//

import Foundation

protocol GiftcardsPresentingContract {
    func viewDidLoad()

    func viewWillAppear()

    func viewDidAppear()

    func refreshData()

    func didTapSkip()

    func didTapContinue()

    func didTapAddGiftcard()

    func didSelectGiftcardAt(position: Int)

    func didDeselectGiftcardAt(position: Int)
}
