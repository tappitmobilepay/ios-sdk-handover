//
//  GiftcardsResponseParsingContract.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 2021-08-06.
//  Copyright © 2021 JadePayments. All rights reserved.
//

import Foundation

protocol GiftcardsResponseParsingContract {
    func redeemResponse(from data: Data?) throws
}
