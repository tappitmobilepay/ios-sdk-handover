//
//  GiftcardsPresenter.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 2021-08-02.
//  Copyright © 2021 JadePayments. All rights reserved.
//

import Foundation

class GiftcardsPresenter: GiftcardsPresentingContract {
    weak var view: GiftcardsViewingContract?
    let router: RouterContract
    let giveawayTokensService: GiveawayTokensServiceContract
    let db: DatabaseManager
    let mode: GiveawayTokensScreenMode
    var paymentData: PaymentData
    let paymentFlowManager: PaymentFlowManagementContract

    var selectedGiftcards = NSMutableSet()

    init(mode: GiveawayTokensScreenMode,
         view: GiftcardsViewingContract,
         router: RouterContract,
         giveawayTokensService: GiveawayTokensServiceContract,
         db: DatabaseManager,
         paymentData: PaymentData,
         paymentFlowManager: PaymentFlowManagementContract)
    {
        self.view = view
        self.router = router
        self.db = db
        self.giveawayTokensService = giveawayTokensService
        self.mode = mode
        self.paymentData = paymentData
        self.paymentFlowManager = paymentFlowManager
    }

    func viewDidLoad() {
        view?.setup(mode: mode)
        refresh()
        refreshData()
    }

    func viewWillAppear() {
        refreshData()
    }

    func viewDidAppear() {
        if mode == .viewOnly {
            let giftcards = db.giftcards()
            if giftcards.isEmpty {
                Analytics.reportOpenGiftcardsEmptyEvent()
            }
            else {
                Analytics.reportOpenGiftcardsExistsEvent()
            }
        }
    }

    func refresh() {
        let giftcards = db.giftcards()
        view?.updateWith(giftcards: giftcards,
                         allowAddGiftcards: giftcards.count < 2)
    }

    func refreshData() {
        selectedGiftcards = NSMutableSet()
        getBenefits()
    }

    func getBenefits() {
        view?.showLoading()
        giveawayTokensService.cancel()
        giveawayTokensService.getBenefits(fanId: router.ssoLogin.customerId) { [weak self] benefits, _ in
            self?.view?.hideLoading()
            if let benefits = benefits {
                self?.db.save(giveawayTokens: benefits.tokens)
                self?.db.save(complimentaryBalances: benefits.complimentaryBalances)
                self?.refresh()
            }
        }
    }

    func didTapSkip() {
        moveNext()
    }

    func didTapContinue() {
        if let finalArray = selectedGiftcards.allObjects as? [Int] {
            paymentData.giftcards = finalArray
            moveNext()
        }
        else {
            showAlert(message: "Please select at least one giftcard before continuing.")
        }
    }

    func didTapAddGiftcard() {
        guard db.giftcards().count < 2 else {
            showAlert(message: "You can have a maximum of only 2 giftcards added at once.")
            refresh()
            return
        }
        router.presentRedeemGiftcard()
    }

    func didSelectGiftcardAt(position: Int) {
        let giftcards = db.giftcards()
        assert(giftcards.count > position)
        if mode == .select,
           giftcards.count > position
        {
            let giftcard = giftcards[position]
            guard let slot = giftcard.slot else {
                return
            }
            selectedGiftcards.add(slot)
        }
    }

    func didDeselectGiftcardAt(position: Int) {
        let giftcards = db.giftcards()
        assert(giftcards.count > position)
        if mode == .select,
           giftcards.count > position
        {
            let giftcard = giftcards[position]
            guard let slot = giftcard.slot else {
                return
            }
            selectedGiftcards.remove(slot)
        }
    }

    func moveNext() {
        do {
            try paymentFlowManager.moveToNextState(paymentData: paymentData)
        }
        catch {
            assert(false)
        }
    }
}
