//
//  GiftcardsViewController.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 2021-08-02.
//  Copyright © 2021 JadePayments. All rights reserved.
//

import Foundation
import UIKit

class GiftcardsViewController: BaseScreenViewController, GiftcardsViewingContract {
    var presenter: GiftcardsPresentingContract!
    @IBOutlet var skipButton: UIButton!
    @IBOutlet var continueButton: UIButton!
    @IBOutlet var addGiftcardButton: UIButton!
    @IBOutlet var topInfoLabel: UILabel!
    @IBOutlet var tableView: UITableView!
    var giftcards = [ComplimentaryBalance]()
    var mode: GiveawayTokensScreenMode = .viewOnly

    private let refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()

        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }

        presenter.viewDidLoad()
    }

    func setup(mode: GiveawayTokensScreenMode) {
        self.mode = mode
        addDefaultTitleViewLogo()
        tableView.delegate = self
        tableView.dataSource = self

        topInfoLabel.font = UIFont.with(size: .Large, weight: .normal)

        tableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refreshStarted(_:)), for: .valueChanged)

        if mode == .select {
            continueButton.setTitle("CONTINUE", for: .normal)
            skipButton.setTitle("NO THANKS", for: .normal)
            skipButton.layer.borderWidth = 1
            skipButton.layer.borderColor = DevUtil.color(name: Colors.tmw_secondary_button_border.rawValue).cgColor
            skipButton.setTitleColor(DevUtil.color(name: Colors.tmw_secondary_button_text.rawValue), for: .normal)
            skipButton.backgroundColor = DevUtil.color(name: Colors.tmw_secondary_button_background.rawValue)
            skipButton.titleLabel?.font = UIFont.with(size: .LargeMedium, weight: .secondaryButton)
            skipButton.addDefaultRoundCorners()

            continueButton.layer.borderWidth = 1
            continueButton.layer.borderColor = DevUtil.color(name: Colors.tmw_primary_button_border.rawValue).cgColor
            continueButton.setTitleColor(DevUtil.color(name: Colors.tmw_primary_button_text.rawValue), for: .normal)
            continueButton.backgroundColor = DevUtil.color(name: Colors.tmw_primary_button_background.rawValue)
            continueButton.addDefaultRoundCorners()
            continueButton.titleLabel?.font = UIFont.with(size: .LargeMedium, weight: .primaryButton)

            addGiftcardButton.removeFromSuperview()
        }
        else {
            skipButton.removeFromSuperview()
            continueButton.removeFromSuperview()
            addGiftcardButton.setTitle("ADD GIFT CARD", for: .normal)
            addGiftcardButton.layer.borderWidth = 1
            addGiftcardButton.layer.borderColor = DevUtil.color(name: Colors.tmw_primary_button_border.rawValue).cgColor
            addGiftcardButton.setTitleColor(DevUtil.color(name: Colors.tmw_primary_button_text.rawValue), for: .normal)
            addGiftcardButton.backgroundColor = DevUtil.color(name: Colors.tmw_primary_button_background.rawValue)
            addGiftcardButton.addDefaultRoundCorners()
            addGiftcardButton.titleLabel?.font = UIFont.with(size: .LargeMedium, weight: .primaryButton)
        }
    }

    func showLoading() {
        refreshControl.beginRefreshing()
    }

    func hideLoading() {
        refreshControl.endRefreshing()
    }

    func updateWith(giftcards: [ComplimentaryBalance], allowAddGiftcards: Bool) {
        addGiftcardButton.isEnabled = allowAddGiftcards
        self.giftcards = giftcards
        tableView.reloadData()
    }

    @objc
    func refreshStarted(_ sender: Any) {
        presenter.refreshData()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // Analytics.analyticsPaymentMethodsPageVisitEvent()
        presenter.viewDidAppear()
        showBackButton()
        navigationController?.setNavigationBarHidden(false, animated: true)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        showBackButton()
        navigationController?.setNavigationBarHidden(false, animated: true)

        presenter.viewWillAppear()
    }

    @IBAction func didTapSkipButton(_ sender: UIButton) {
        presenter.didTapSkip()
    }

    @IBAction func didTapAddGiftcard(_ sender: UIButton) {
        presenter.didTapAddGiftcard()
    }

    @IBAction func didTapContinueButton(_ sender: UIButton) {
        presenter.didTapContinue()
    }
}

extension GiftcardsViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return giftcards.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "\(GiftcardTableViewCell.self)") as? GiftcardTableViewCell else {
            return UITableViewCell()
        }

        cell.setup(giftcard: giftcards[indexPath.row], mode: mode)
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didSelectGiftcardAt(position: indexPath.row)
    }

    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        presenter.didDeselectGiftcardAt(position: indexPath.row)
    }
}
