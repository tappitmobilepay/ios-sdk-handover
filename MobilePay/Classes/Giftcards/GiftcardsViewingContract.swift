//
//  GiftcardsViewingContract.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 2021-08-02.
//  Copyright © 2021 JadePayments. All rights reserved.
//

import Foundation

protocol GiftcardsViewingContract: AnyObject {
    func setup(mode: GiveawayTokensScreenMode)

    func updateWith(giftcards: [ComplimentaryBalance], allowAddGiftcards: Bool)

    func showLoading()

    func hideLoading()
}
