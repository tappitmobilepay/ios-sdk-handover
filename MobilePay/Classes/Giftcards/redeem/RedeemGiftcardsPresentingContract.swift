//
//  RedeemGiftcardsPresentingContract.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 2021-08-05.
//  Copyright © 2021 JadePayments. All rights reserved.
//

import Foundation

protocol RedeemGiftcardsPresentingContract {
    func viewDidLoad()

    func viewDidAppear()

    func didTapRedeem()
}
