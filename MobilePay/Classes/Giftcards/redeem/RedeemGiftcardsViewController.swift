//
//  RedeemGiftcardsViewController.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 2021-08-05.
//  Copyright © 2021 JadePayments. All rights reserved.
//

import Foundation
import UIKit

class RedeemGiftcardsViewController: BaseScreenViewController, RedeemGiftcardsViewingContract {
    var giftcardNumber: String? {
        return giftcardNumberTextfield.text
    }

    var securityCode: String? {
        return securityCodeTextfield.text
    }

    @IBOutlet var giftcardNumberTextfield: UITextField!
    @IBOutlet var redeemButton: UIButton!
    @IBOutlet var securityCodeTextfield: UITextField!
    @IBOutlet var topInfoLabel: UILabel!
    var presenter: RedeemGiftcardsPresentingContract!

    func setup() {
        addDefaultTitleViewLogo()
        topInfoLabel.font = UIFont.with(size: .Large, weight: .normal)

        redeemButton.titleLabel?.font = UIFont.with(size: .LargeMedium, weight: .primaryButton)
        redeemButton.setTitle("SUBMIT", for: .normal)
        redeemButton.layer.borderWidth = 1
        redeemButton.layer.borderColor = DevUtil.color(name: Colors.tmw_primary_button_border.rawValue).cgColor
        redeemButton.setTitleColor(DevUtil.color(name: Colors.tmw_primary_button_text.rawValue), for: .normal)
        redeemButton.backgroundColor = DevUtil.color(name: Colors.tmw_primary_button_background.rawValue)
        redeemButton.addDefaultRoundCorners()

        giftcardNumberTextfield.addColorBorder(color: .black)
        giftcardNumberTextfield.font = UIFont.with(size: .LargeMedium, weight: .normal)
        giftcardNumberTextfield.addDoneCancelToolbar()
        giftcardNumberTextfield.delegate = self
        securityCodeTextfield.addColorBorder(color: .black)
        securityCodeTextfield.font = UIFont.with(size: .LargeMedium, weight: .normal)
        securityCodeTextfield.addDoneCancelToolbar()
        securityCodeTextfield.delegate = self
    }

    func showLoading() {
        TMWLoaderView.shareInstance().showLoader(navigationController!.view)
    }

    func hideLoading() {
        TMWLoaderView.shareInstance().hideLoader()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        presenter.viewDidLoad()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.viewDidAppear()
    }

    @IBAction func didTapRedeemButton(_ sender: Any) {
        presenter.didTapRedeem()
    }

    func showError(message: String, title: String?) {
        if let title = title {
            showAlert(message: message, title: title)
        }
        else {
            showAlert(message: message)
        }
    }
}

extension RedeemGiftcardsViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if giftcardNumberTextfield == textField {
            securityCodeTextfield.becomeFirstResponder()
        }
        else if securityCodeTextfield == textField {
            didTapRedeemButton(redeemButton)
        }

        return true
    }
}
