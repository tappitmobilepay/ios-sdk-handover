//
//  RedeemGiftcardsPresenter.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 2021-08-05.
//  Copyright © 2021 JadePayments. All rights reserved.
//

import Foundation

class RedeemGiftcardsPresenter: RedeemGiftcardsPresentingContract {
    func viewDidLoad() {
        view?.setup()
    }

    func viewDidAppear() {
        Analytics.reportOpenAddGiftcardsScreenEvent()
    }

    func didTapRedeem() {
        if let number = view?.giftcardNumber,
           !number.isEmpty,
           let code = view?.securityCode,
           !code.isEmpty
        {
            redeem(number: number, code: code)
        }
        else {
            showAlert(message: "Please fill in all the fields before proceeding")
        }
    }

    weak var view: RedeemGiftcardsViewingContract?
    var service: GiftcardsServicingContract
    let router: RouterContract

    init(view: RedeemGiftcardsViewingContract,
         service: GiftcardsServicingContract,
         router: RouterContract)
    {
        self.view = view
        self.service = service
        self.router = router
    }

    func redeem(number: String, code: String) {
        view?.showLoading()
        let request = RedeemGiftcardRequest(cardNumber: number,
                                            securityCode: code)
        service.cancel()
        service.redeemGiftcard(request: request) { [weak self] error in
            if error == nil {
                if self?.router.db.giftcards().isEmpty == .some(true) {
                    Analytics.reportAddedFirstGiftcardEvent()
                }
                else {
                    Analytics.reportAddedSubsequentGiftcardEvent()
                }
                self?.proceedNext()
            }
            else if let error = error {
                Analytics.reportAddingGiftcardFailed(failureMessage: error.localizedDescription)
                self?.view?.showError(message: "Please check the details and if the card has not been already redeemed.", title: "Could not the redeem card.")
            }
            self?.view?.hideLoading()
        }
    }

    func proceedNext() {
        router.presentSuccess(type: .GiftcardAdded, message: nil, completion: { [weak self] in
            self?.router.goBack(to: .Giftcards)
        })
    }
}
