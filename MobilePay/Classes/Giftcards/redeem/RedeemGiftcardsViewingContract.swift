//
//  RedeemGiftcardsViewingContract.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 2021-08-05.
//  Copyright © 2021 JadePayments. All rights reserved.
//

import Foundation

protocol RedeemGiftcardsViewingContract: AnyObject {
    var giftcardNumber: String? { get }

    var securityCode: String? { get }

    func setup()

    func showLoading()

    func hideLoading()

    func showError(message: String, title: String?)
}
