//
//  GiftcardsApiRoute.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 2021-08-06.
//  Copyright © 2021 JadePayments. All rights reserved.
//

import Foundation

enum GiftcardsApiRoute: ApiRoute { case

    redeem

    var path: String {
        switch self {
        case .redeem: return "redeem"
        }
    }

    func url(for environment: ApiEnvironment) -> String {
        return "\(environment.url)/\(path)"
    }
}
