//
//  MobilePayViewController.swift
//  Tappit
//
//  Created by Pandi on 20/09/17.
//  Copyright © 2017 JadePayments. All rights reserved.
//

import UIKit

/// Initial MobilePay SDK view controller that handles registration, login and payments for the SDK.
public class MobilePayViewController: UIViewController, MobilePayViewContract {
    var presenter: MobilePayPresentingContract?
    var ssoLogin: SSOLogin
    var deepLink: DeepLink?
    let credStore: CredentialsStoringContract

    /// Method to initialize the SDK
    /// - Parameter ssoLogin: structure to pass in to do a single sign on.
    /// - Parameter deepLink: optional structure to enable and modify the deep link button on the HOME screen
    public init(login: SSOLogin,
                deepLink: DeepLink? = nil)
    {
        self.deepLink = deepLink
        ssoLogin = login
        credStore = CredentialsStore(login: login)
        super.init(nibName: nil, bundle: nil)

        logoutIfNeeded()
        setupDevelopmentValues()
    }

    internal func logoutIfNeeded() {
        var needsLogOut = false
        if let loggedInEnvironment = credStore.loggedInWithEnvironment,
           loggedInEnvironment != ssoLogin.environment.rawValue
        {
            needsLogOut = true
        }
        else if let loggedInIntegrationKey = credStore.loggedInWithIntegrationKey, loggedInIntegrationKey != ssoLogin.integrationKey {
            needsLogOut = true
        }
        if needsLogOut {
            LoginManager(db: DatabaseManager(), credStore: credStore).logoutApp()
        }
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override public func viewDidAppear(_ animated: Bool) {
        navigationItem.leftBarButtonItems = [UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)]
    }

    override public func viewWillAppear(_ animated: Bool) {
        navigationItem.leftBarButtonItems = [UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)]
    }

    /// :nodoc:
    override public func viewDidLoad() {
        super.viewDidLoad()

        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }

        let tintColor = DevUtil.color(name: Colors.tmw_navigation_background.rawValue)

        if #available(iOS 15.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.backgroundColor = tintColor
            navigationController?.navigationBar.standardAppearance = appearance
            navigationController?.navigationBar.scrollEdgeAppearance = appearance
        }
        else {
            navigationController?.navigationBar.barTintColor = tintColor
            navigationController?.navigationBar.isTranslucent = false
        }

        assemble()

        guard let presenter = presenter else {
            assert(false)
            return
        }

        presenter.viewDidLoad()
    }

    /// Method to check if SDK will handle the request to open a URL
    /// - Parameter url: Url that was requested to be opened
    public func willHandle(url: URL) -> Bool {
        return presenter!.willHandleUrl(url: url)
    }

    internal func assemble() {
        guard let navController = navigationController else {
            fatalError("MobilePayViewController is expected to have a navigation controller as a parent.")
        }

        presenter = MobilePayPresenter(view: self,
                                       navController: navController,
                                       ssoLogin: ssoLogin,
                                       credStore: credStore,
                                       deepLink: deepLink)
    }

    internal func setup(viewController: UIViewController) {
        addChild(viewController)
        viewController.willMove(toParent: self)
        view.addSubview(viewController.view)

        viewController.view.translatesAutoresizingMaskIntoConstraints = false
        viewController.view.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        viewController.view.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        viewController.view.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        viewController.view.heightAnchor.constraint(equalTo: view.heightAnchor).isActive = true
    }

    // Relevant only for internal Tappit app integrations
    internal func setupDevelopmentValues() {
        // Tappit application is integrating this so will override some global SDK values
        if let skinName = ProcessInfo.processInfo.environment["tappit_dev_settings_skin_name"] {
            let globalConfig = MobilePayCore.shared
            globalConfig.devModeEnabled = true
            globalConfig.devModeSkinName = skinName

            let clearData = ProcessInfo.processInfo.environment["tappit_dev_settings_clear_data"]
            if clearData == .some("true") {
                LoginManager(db: DatabaseManager(), credStore: credStore).logoutApp()
            }
        }
    }
}
