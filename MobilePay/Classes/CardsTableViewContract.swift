//
//  CardsTableViewContract.swift
//  MobilePay-PAYG
//
//  Created by Marius Kurgonas on 10/06/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation

protocol CardsTableViewContract: AnyObject {
    var presenter: CardsTableViewPresentingContract! { get set }

    func showLoading()

    func hideLoading()

    func updateWith(cards: [Card])

    func setup()
}
