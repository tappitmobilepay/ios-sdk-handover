//
//  PaymentsResponseParser.swift
//  MobilePay-PAYG
//
//  Created by Marius Kurgonas on 05/06/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation

class PaymentsResponseParser: PaymentsResponseParsingContract {
    func addPaymentMethodResponse(from data: Data?) throws -> Card {
        let decoder = JSONDecoder()

        if let d = data, let response = try? decoder.decode(Card.self, from: d) {
            return response
        }

        else {
            throw invalidDataError
        }
    }

    func transactionStatusResponse(from data: Data?) throws -> TransactionStatusResponse {
        let decoder = JSONDecoder()

        if let d = data, let response = try? decoder.decode(TransactionStatusResponse.self, from: d) {
            return response
        }

        else {
            throw invalidDataError
        }
    }

    func userStatsResponse(from data: Data?) throws -> CustomerStats {
        let decoder = JSONDecoder()

        if let d = data, let response = try? decoder.decode(CustomerStats.self, from: d) {
            return response
        }

        else {
            throw invalidDataError
        }
    }

    func paymentMethodsResponse(from data: Data?) throws -> [Card] {
        let decoder = JSONDecoder()

        if let d = data, let response = try? decoder.decode([Card].self, from: d) {
            return response
        }

        else {
            throw invalidDataError
        }
    }

    func paymentProvidersConfigResponse(from data: Data?) throws -> Bool {
        if let d = data,
           let response = String(data: d, encoding: .utf8),
           let booleanValue = Bool(response)
        {
            return booleanValue
        }

        else {
            throw invalidDataError
        }
    }

    func deletePaymentMethodResponse(from data: Data?) throws {}

    func makePaymentMethodDefaultResponse(from data: Data?) throws -> MakePaymentMethodDefaultResponse {
        let decoder = JSONDecoder()

        if let d = data, d.isEmpty {
            return MakePaymentMethodDefaultResponse(missingConfiguration: false)
        }
        else if let d = data, let response = try? decoder.decode(MakePaymentMethodDefaultResponse.self, from: d) {
            return response
        }

        else {
            throw invalidDataError
        }
    }

    func clientConfigResponse(from data: Data?) throws -> ClientConfig {
        let decoder = JSONDecoder()

        if let d = data {
            do {
                let response = try decoder.decode(ClientConfig.self, from: d)
                return response
            }
            catch {
                throw error
            }
        }

        else {
            throw invalidDataError
        }
    }
}
