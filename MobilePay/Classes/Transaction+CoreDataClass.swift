//
//  Transaction+CoreDataClass.swift
//
//
//  Created by Marius Kurgonas on 17/07/2020.
//
//

import CoreData
import Foundation

@objc(Transaction)
public class Transaction: NSManagedObject {}
