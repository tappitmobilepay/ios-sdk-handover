//
//  CardsTableViewPresentingContract.swift
//  MobilePay-PAYG
//
//  Created by Marius Kurgonas on 10/06/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation

protocol CardsTableViewPresentingContract {
    func viewDidLoad()

    func viewWillAppear()

    func viewDidAppear()

    func refreshData()

    func didSelectCardAtIndex(index: Int)

    func didTapAddNewCard()
}
