//
//  Configuration.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 05/06/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation

enum Configuration {
    enum Error: Swift.Error {
        case missingKey, invalidValue
    }

    static func externalValue<T>(for key: String) throws -> T {
        return try value(for: key, in: .main)
    }

    static func internalValue<T>(for key: String) throws -> T {
        return try value(for: key, in: Bundle(for: MobilePayViewController.self))
    }

    static func value<T>(for key: String, in bundle: Bundle) throws -> T {
        guard let object = bundle.object(forInfoDictionaryKey: key) else {
            throw Error.missingKey
        }

        switch object {
        case let value as T:
            return value
        default:
            throw Error.invalidValue
        }
    }
}

enum Security {
    static var RSA_ENCRYPTION_KEY: String {
        return try! Configuration.internalValue(for: "RSA_ENCRYPTION_KEY")
    }

    static var RSA_ENCRYPTION_KEY_SIZE: Int {
        let value: String = try! Configuration.internalValue(for: "RSA_ENCRYPTION_KEY_SIZE")
        return Int(value)!
    }

    static var LOCAL_ENCRYPTION_KEY: String {
        return try! Configuration.internalValue(for: "LOCAL_ENCRYPTION_KEY")
    }

    static var LOCAL_IV_VECTOR: String {
        return try! Configuration.internalValue(for: "LOCAL_IV_VECTOR")
    }
}

enum ClientSpecificValue {
    static var fonts: ExternalFonts {
        var mainKey = "tmw_tappit_config"
        if MobilePayCore.shared.devModeEnabled {
            mainKey = MobilePayCore.shared.devModeSkinName + "_" + mainKey
        }

        guard let tappitConfig: NSDictionary = try? Configuration.externalValue(for: mainKey) else {
            print("MobilePay integration warning: value named 'tmw_tappit_config' is missing from application info.plist file. Please refer to the integration guide. Proceeding with the default value")
            return ExternalFonts()
        }

        guard let fontsDictionary = tappitConfig["tmw_fonts"] as? NSDictionary else {
            print("MobilePay integration warning: value named 'tmw_fonts' is missing from the application 'tmw_tappit_config' dictionary inside the info.plist file. Please refer to the integration guide. Proceeding with the default value")
            return ExternalFonts()
        }

        return ExternalFonts(normal: fontsDictionary["normal"] as? String,
                             bold: fontsDictionary["bold"] as? String,
                             payButton: fontsDictionary["pay_button"] as? String,
                             homeButton: fontsDictionary["home_button"] as? String,
                             primaryButton: fontsDictionary["primary_button"] as? String,
                             secondaryButton: fontsDictionary["secondary_button"] as? String,
                             customTipButton: fontsDictionary["custom_tip_button"] as? String,
                             benefitStatusButton: fontsDictionary["benefit_status_button"] as? String,
                             campaignStatusButton: fontsDictionary["campaign_status_button"] as? String)
    }
}

struct ExternalFonts {
    var normal: String?
    var bold: String?
    var payButton: String?
    var homeButton: String?
    var primaryButton: String?
    var secondaryButton: String?
    var customTipButton: String?
    var benefitStatusButton: String?
    var campaignStatusButton: String?

    func toDictionary() -> [TappitFont.Family: String] {
        var result = [TappitFont.Family: String]()
        result[.normal] = normal
        result[.bold] = bold
        result[.payButton] = payButton
        result[.homeButton] = homeButton
        result[.primaryButton] = primaryButton
        result[.secondaryButton] = secondaryButton
        result[.customTipButton] = customTipButton
        result[.benefitStatusButton] = benefitStatusButton
        result[.campaignStatusButton] = campaignStatusButton
        return result
    }
}
