//
//  MakePaymentMethodDefaultResponse.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 23/09/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation

struct MakePaymentMethodDefaultResponse: Codable {
    let missingConfiguration: Bool?
}
