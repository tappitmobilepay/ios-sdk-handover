//
//  PaymentsWebServiceContract.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 03/06/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation

typealias AddPaymentMethodResponse = (_ card: Card?, _ error: Error?) -> Void
typealias CheckTransactionStatusResponse = (_ response: TransactionStatusResponse?, _ error: Error?) -> Void
typealias PaymentMethodsResponse = (_ cards: [Card]?, _ error: Error?) -> Void
typealias GenericResponse = (_ error: Error?) -> Void
typealias DefaultPaymentMethodResponse = (_ response: MakePaymentMethodDefaultResponse?, _ error: Error?) -> Void
typealias PaymentProvidersConfigResponse = (_ areAllConfigured: Bool?, _ error: Error?) -> Void
typealias ClientConfigResult = (_ result: ClientConfig?, _ error: Error?) -> Void
typealias UserStatsResult = (_ response: CustomerStats?, _ error: Error?) -> Void

protocol PaymentsWebServiceContract: CancelableService {
    var responseParser: PaymentsResponseParsingContract? { get set }

    func addPaymentMethod(data: PaymentMethodRequest,
                          completion: @escaping AddPaymentMethodResponse)
    func requestAllPaymentMethods(completion: @escaping PaymentMethodsResponse)
    func deletePaymentMethod(paymentProfileSettings: [PaymentProfileSettings],
                             completion: @escaping GenericResponse)
    func checkTransactionStatus(qr: String,
                                completion: @escaping CheckTransactionStatusResponse)
    func makeDefaultPaymentMethod(settings: [PaymentProfileSettings],
                                  completion: @escaping DefaultPaymentMethodResponse)
    func checkIfAllPaymentProvidersWereConfigured(completion: @escaping PaymentProvidersConfigResponse)

    func requestClientConfig(integrationKey: String, completion: @escaping ClientConfigResult)

    func getUserStats(fanId: String, completion: @escaping UserStatsResult)
}
