//
//  AuthenticationViewContract.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 04/06/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation

protocol AuthenticationViewContract: AnyObject {
    var presenter: AuthenticationPresentingContact? { get set }

    func setup()
}
