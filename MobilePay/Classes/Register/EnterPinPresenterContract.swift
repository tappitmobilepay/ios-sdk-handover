//
//  EnterPinPresenterContract.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 02/01/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation

protocol EnterPinPresenterContract {
    func viewDidLoad()

    func viewWillAppear()

    func viewDidAppear()

    func didEnterPin()

    func didTapForgotPin()
}
