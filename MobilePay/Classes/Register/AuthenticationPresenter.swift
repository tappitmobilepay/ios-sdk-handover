//
//  AuthenticationPresenter.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 04/06/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation

class AuthenticationPresenter: AuthenticationPresentingContact {
    weak var view: AuthenticationViewContract?
    let router: RouterContract

    init(view: AuthenticationViewContract, router: RouterContract) {
        self.view = view
        self.router = router
    }

    func didTapSetupPin() {
        router.presentSetupPin(navigationMethod: .default)
    }

    func didTapSetupBiometry() {
        Analytics.reportSetupBiometricSucceededEvent()
        router.presentSetupBiometric(navigationMethod: .default)
    }

    func viewDidLoad() {
        view?.setup()
    }

    func authMethod() -> AuthenticationMethod? {
        return router.credStore.authenticationMethod
    }
}
