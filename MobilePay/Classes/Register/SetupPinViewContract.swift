//
//  SetupPinViewContract.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 04/06/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation

protocol SetupPinViewContract: AnyObject {
    var presenter: SetupPinPresentingContract? { get set }

    var pin1Text: String { get }
    var pin2Text: String { get }
    var pin3Text: String { get }
    var pin4Text: String { get }

    var confirmPin1Text: String { get }
    var confirmPin2Text: String { get }
    var confirmPin3Text: String { get }
    var confirmPin4Text: String { get }

    func setup()
}
