//
//  EnterBiometryViewController.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 16/12/2019.
//  Copyright © 2019 JadePayments. All rights reserved.
//

import LocalAuthentication
import UIKit

class EnterBiometryViewController: BaseScreenViewController {
    @IBOutlet var mainLogoImageView: UIImageView!
    @IBOutlet var mainTitleLabel: UILabel!
    @IBOutlet var biometricImageView: UIImageView!
    @IBOutlet var resetAuthenticationButton: UIButton!
    @IBOutlet var openAuthButton: UIButton!

    var router: RouterContract?

    var loginManager: LoginManagingContract?

    var webService: LoginWebServiceContract?

    var db: DatabaseManager?

    override func viewDidLoad() {
        super.viewDidLoad()

        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }

        NotificationCenter.default.addObserver(self, selector: #selector(didEnterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)

        view.backgroundColor = DevUtil.color(name: Colors.tmw_onboarding_background.rawValue)
        mainLogoImageView.image = DevUtil.imageSkin(name: "tmw_navigation_logo")
        mainTitleLabel.textColor = DevUtil.color(name: Colors.tmw_onboarding_background_text.rawValue)

        mainTitleLabel.font = UIFont.with(size: .LightNormal1, weight: .bold)

        mainTitleLabel.text = "Login with your Biometric ID"

        biometricImageView.image = DevUtil.imageSkin(name: "tmw_biometric")?.withRenderingMode(.alwaysTemplate)
        biometricImageView.tintColor = DevUtil.color(name: Colors.tmw_biometric_indicator.rawValue)

        resetAuthenticationButton.setTitleColor(DevUtil.color(name: Colors.tmw_onboarding_background_text.rawValue), for: .normal)
        resetAuthenticationButton.titleLabel?.font = UIFont.with(size: .Medium, weight: .normal)
        resetAuthenticationButton.underlineButton(text: "Reset biometrics")

        openAuthButton.setTitleColor(DevUtil.color(name: Colors.tmw_onboarding_background_text.rawValue), for: .normal)
        openAuthButton.titleLabel?.font = UIFont.with(size: .Medium, weight: .normal)
        openAuthButton.underlineButton(text: "Login with biometics")

        openBiometricID()
    }

    @objc
    func didEnterForeground() {
        openBiometricID()
    }

    override func viewDidAppear(_ animated: Bool) {
        hideBackButton()
        navigationController?.setNavigationBarHidden(true, animated: true)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        hideBackButton()
        navigationController?.setNavigationBarHidden(true, animated: true)
    }

    func openBiometricID() {
        showPasscodeAuthentication(message: "Enter passcode to authenticate")
    }

    // show passcode authentication
    func showPasscodeAuthentication(message: String) {
        BioMetricAuthenticator.authenticateWithPasscode(reason: message) { [weak self] result in
            switch result {
            case .success:
                self?.authSuccess() // passcode authentication success
            case .failure(let error):
                Analytics.reportFailedLoginEvent(failureMessage: error.message())
                switch error {
                // device does not support biometric (face id or touch id) authentication
                case .biometryNotAvailable:
                    self?.openBiometricID()

                // No biometry enrolled in this device, ask user to register fingerprint or face
                case .biometryNotEnrolled:
                    self?.openBiometricID()

                // show alternatives on fallback button clicked
                case .fallback:
                    break
                    // breakself?.openBiometricID()

                // Biometry is locked out now, because there were too many failed attempts.
                // Need to enter device passcode to unlock.
                case .biometryLockedout:
                    self?.openBiometricID()

                // do nothing on canceled by system or user
                case .canceledByUser:
                    self?.openBiometricID()

                case .canceledBySystem:
                    break

                // show error for any other reason
                default:
                    showAlert(message: error.message())
                }
            }
        }
    }

    func authSuccess() {
        if db?.hasCreditCards() == .some(false), router?.credStore.customerStats?.userSegmentation == .some(UsserSegmentation.registered.rawValue) {
            router?.presentAddInitialCard(animated: true)
        }
        else {
            Analytics.reportSuccessfulLoginEvent()
            router?.presentHome(animated: true)
        }
    }

    @IBAction func didPressResetAuthenticationButton(_ sender: Any) {
        Analytics.reportForgotPinPressedEvent()
        router?.presentLoader()

        guard let apiKey = router?.credStore.apiKey,
              let customerId = router?.ssoLogin.customerId,
              let email = router?.ssoLogin.email
        else {
            assert(false)
            return
        }

        let request = ResetPinRequest(clientCustomerId: customerId,
                                      clientToken: apiKey,
                                      email: email)

        webService?.cancel()
        webService?.forgotPin(resetPinRequest: request) { [weak self] error in
            if let error = error {
                Analytics.reportForgotPinEmailFailedEvent(failureMessage: error.localizedDescription)
                showAlert(message: "Could not reset authentication method at this time.", title: "Error")
            }
            else {
                Analytics.reportForgotPinEmailSentEvent()
                showAlert(message: "An email has been sent to your registered email address with the instructions to reset authentication.", title: "Success")
            }
            self?.router?.hideLoader()
        }
    }

    @IBAction
    func didTapOpenAuthButton(_ sender: Any) {
        openBiometricID()
    }
}
