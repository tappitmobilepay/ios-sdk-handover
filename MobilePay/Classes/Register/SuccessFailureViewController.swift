//
//  SuccessFailureViewController.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 06/05/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation
import UIKit

enum CompletionType {
    case TransactionSuccess, TransactionFailure, TransactionPending, TransactionUndetermined, ChangePassword, AddCard, DeleteCard, MakeCardDefault, GiftcardAdded
}

let autoDismissTime = 2

class SuccessFailureViewController: BaseScreenViewController {
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var mainTitleLabel: UILabel!
    @IBOutlet var messageLabel: UILabel!
    @IBOutlet var separatorView: UIView!

    var type: CompletionType?
    var router: Router?
    var completion: (() -> Void)?
    var message: String?
    var autoDismissSeconds: Int = autoDismissTime

    override func viewDidLoad() {
        super.viewDidLoad()

        guard let type = type else {
            fatalError("Success type not set")
        }

        let messageIsNil = message == nil
        autoDismissSeconds = messageIsNil ? autoDismissTime : 10
        messageLabel.isHidden = messageIsNil
        separatorView.isHidden = messageIsNil
        messageLabel.text = message
        mainTitleLabel.font = UIFont.with(size: .BigNormal, weight: .bold)
        messageLabel.font = UIFont.with(size: .LargeMedium, weight: .normal)

        switch type {
        case .TransactionUndetermined:
            imageView.image = DevUtil.imageSkin(name: "tmw_failure")
            mainTitleLabel.text = "Transaction status unknown"
        case .TransactionPending:
            imageView.image = DevUtil.imageSkin(name: "tmw_success")
            mainTitleLabel.text = "Transaction pending"
        case .TransactionSuccess:
            imageView.image = DevUtil.imageSkin(name: "tmw_success")
            mainTitleLabel.text = "Transaction successful"
        case .TransactionFailure:
            imageView.image = DevUtil.imageSkin(name: "tmw_failure")
            mainTitleLabel.text = "Transaction failed"
        case .ChangePassword:
            imageView.image = DevUtil.imageSkin(name: "tmw_success")
            mainTitleLabel.text = "Password changed"
        case .AddCard:
            imageView.image = DevUtil.imageSkin(name: "tmw_success")
            mainTitleLabel.text = "Card successfully added"
        case .DeleteCard:
            imageView.image = DevUtil.imageSkin(name: "tmw_success")
            mainTitleLabel.text = "Card successfully removed"
        case .MakeCardDefault:
            imageView.image = DevUtil.imageSkin(name: "tmw_success")
            mainTitleLabel.text = "Card successfully made default"
        case .GiftcardAdded:
            imageView.image = DevUtil.imageSkin(name: "tmw_success")
            mainTitleLabel.text = "Gift Card Successfully Added"
        }

        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        hideBackButton()
        navigationController?.setNavigationBarHidden(true, animated: true)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        guard let type = type else {
            fatalError("Success type not set")
        }

        hideBackButton()
        navigationController?.setNavigationBarHidden(true, animated: true)

        guard let _ = router else {
            fatalError("Item not set")
        }

        DispatchQueue.main.asyncAfter(deadline: .now() + Double(autoDismissSeconds)) { [weak self] in
            self?.completion?()
        }
    }
}
