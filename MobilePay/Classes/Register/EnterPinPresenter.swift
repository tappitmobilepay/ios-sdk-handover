//
//  EnterPinPresenter.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 02/01/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation

let kLockdownTime = 30.0

class EnterPinPresenter: EnterPinPresenterContract {
    weak var view: EnterPinViewContract?
    let router: RouterContract
    let db: DatabaseManager
    let webService: LoginWebServiceContract
    var credStore: CredentialsStoringContract
    var lockdownCount = kLockdownTime
    var lockdownTimer: Timer?

    init(view: EnterPinViewContract,
         router: RouterContract,
         db: DatabaseManager,
         credStore: CredentialsStoringContract,
         webService: LoginWebServiceContract)
    {
        self.view = view
        self.router = router
        self.db = db
        self.credStore = credStore
        self.webService = webService
    }

    func viewDidLoad() {
        view?.setup()
    }

    func viewDidAppear() {
        Analytics.reportShowEnterPinScreenEvent()
    }

    func viewWillAppear() {
        let now = Date()
        if let lockdownDate = credStore.pinLockdownDate {
            let difference = Double(Int(now.timeIntervalSince1970 - lockdownDate.timeIntervalSince1970))
            let timeLeft = kLockdownTime - difference
            onlyDebugPrint("TTT")
            onlyDebugPrint("\(lockdownDate)")
            onlyDebugPrint("\(now)")
            onlyDebugPrint("\(difference)")
            onlyDebugPrint("\(timeLeft)")
            if timeLeft > 0 {
                lockdown(time: timeLeft)
            }
        }
    }

    func didEnterPin() {
        guard let pin1 = view?.pin1,
              let pin2 = view?.pin2,
              let pin3 = view?.pin3,
              let pin4 = view?.pin4
        else {
            showWrongPinError()
            return
        }

        let enterPin = "\(pin1)\(pin2)\(pin3)\(pin4)"
        if let setupPin = credStore.pin, enterPin == setupPin {
            if !db.hasCreditCards(), credStore.customerStats?.userSegmentation == .some(UsserSegmentation.registered.rawValue) {
                router.presentAddInitialCard(animated: true)
            }
            else {
                Analytics.reportSuccessfulLoginEvent()
                router.presentHome(animated: true)
            }
        }
        else if credStore.pin != nil {
            var numberOfWrongPins = 0

            if let wrongPinsInARow = credStore.wrongPinsInARow {
                numberOfWrongPins = wrongPinsInARow
            }

            numberOfWrongPins += 1

            credStore.wrongPinsInARow = numberOfWrongPins

            if numberOfWrongPins < 3 {
                showWrongPinError()
            }
            else {
                credStore.pinLockdownDate = Date()
                lockdown(time: kLockdownTime)
            }
        }
        else {
            fatalError()
        }
    }

    func showWrongPinError() {
        let message = "enterpin_wrong".localized
        view?.showWrongPinEntered(message: message)
        Analytics.reportFailedLoginEvent(failureMessage: message)
    }

    func lockdown(time: Double) {
        lockdownCount = time
        credStore.wrongPinsInARow = 0
        view?.updateLockdownWith(count: time)
        view?.enableLockdown()
        lockdownTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
    }

    @objc func updateTime() {
        lockdownCount -= 1
        view?.updateLockdownWith(count: lockdownCount)
        if lockdownCount == 0 {
            lockdownTimer?.invalidate()
            lockdownTimer = nil
            lockdownCount = kLockdownTime
            view?.disableLockdown()
        }
    }

    func didTapForgotPin() {
        Analytics.reportForgotPinPressedEvent()

        router.presentLoader()

        guard let apiKey = credStore.apiKey else {
            assert(false)
            return
        }

        let request = ResetPinRequest(clientCustomerId: router.ssoLogin.customerId,
                                      clientToken: apiKey,
                                      email: router.ssoLogin.email)

        webService.cancel()
        webService.forgotPin(resetPinRequest: request) { [weak self] error in
            if let error = error {
                Analytics.reportForgotPinEmailFailedEvent(failureMessage: error.localizedDescription)
                showAlert(message: "Could not reset authentication method at this time.", title: "Error")
            }
            else {
                Analytics.reportForgotPinEmailSentEvent()
                showAlert(message: "An email has been sent to your registered email address with the instructions to reset authentication.", title: "Success")
            }
            self?.router.hideLoader()
        }
    }
}
