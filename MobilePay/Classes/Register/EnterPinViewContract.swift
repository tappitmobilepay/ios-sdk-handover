//
//  EnterPinViewContract.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 02/01/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation

protocol EnterPinViewContract: AnyObject {
    var presenter: EnterPinPresenterContract? { get set }

    func setup()

    func showWrongPinEntered(message: String)

    func enableLockdown()

    func updateLockdownWith(count: Double)

    func disableLockdown()

    var pin1: String? { get }
    var pin2: String? { get }
    var pin3: String? { get }
    var pin4: String? { get }
}
