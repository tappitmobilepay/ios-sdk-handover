//
//  SetupPinViewController.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 16/12/2019.
//  Copyright © 2019 JadePayments. All rights reserved.
//

import UIKit

enum NavigationMethod {
    case pop, `default`
}

class SetupPinViewController: BaseScreenViewController, SetupPinViewContract {
    var presenter: SetupPinPresentingContract?

    var pin1Text: String {
        return pin1TextField.text ?? ""
    }

    var pin2Text: String {
        return pin2TextField.text ?? ""
    }

    var pin3Text: String {
        return pin3TextField.text ?? ""
    }

    var pin4Text: String {
        return pin4TextField.text ?? ""
    }

    var confirmPin1Text: String {
        return confirmPin1TextField.text ?? ""
    }

    var confirmPin2Text: String {
        return confirmPin2TextField.text ?? ""
    }

    var confirmPin3Text: String {
        return confirmPin3TextField.text ?? ""
    }

    var confirmPin4Text: String {
        return confirmPin4TextField.text ?? ""
    }

    @IBOutlet var tandcLabel: UnderlinedLabel!
    @IBOutlet var nextButton: UIButton!
    @IBOutlet var mainLogoImageView: UIImageView!
    @IBOutlet var enterPasswordLabel: UILabel!
    @IBOutlet var confirmPasswordLabel: UILabel!
    @IBOutlet var pin1TextField: UITextField!
    @IBOutlet var pin2TextField: UITextField!
    @IBOutlet var pin3TextField: UITextField!
    @IBOutlet var pin4TextField: UITextField!
    @IBOutlet var confirmPin1TextField: UITextField!
    @IBOutlet var confirmPin2TextField: UITextField!
    @IBOutlet var confirmPin3TextField: UITextField!
    @IBOutlet var confirmPin4TextField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        guard let presenter = presenter else {
            assert(false)
            return
        }

        presenter.viewDidLoad()
    }

    override func viewDidAppear(_ animated: Bool) {
        showBackButton()
        navigationController?.setNavigationBarHidden(false, animated: true)

        presenter?.viewDidAppear()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        showBackButton()
        navigationController?.setNavigationBarHidden(false, animated: true)
    }

    func setup() {
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }

        addDefaultTitleViewLogo()

        nextButton.titleLabel?.font = UIFont.with(size: .LightNormal1, weight: .bold)
        nextButton.setTitleColor(DevUtil.color(name: Colors.tmw_onboarding_button_text.rawValue), for: .normal)
        nextButton.backgroundColor = DevUtil.color(name: Colors.tmw_onboarding_button_background.rawValue)
        nextButton.addDefaultRoundCorners()
        nextButton.setTitle("SAVE CHANGES", for: .normal)

        view.backgroundColor = DevUtil.color(name: Colors.tmw_onboarding_background.rawValue)
        mainLogoImageView.image = DevUtil.imageSkin(name: "tmw_navigation_logo")
        mainLogoImageView.isHidden = true

        enterPasswordLabel.text = "Create New Login PIN"
        confirmPasswordLabel.text = "Confirm New Login PIN"
        enterPasswordLabel.textColor = DevUtil.color(name: Colors.tmw_onboarding_background_text.rawValue)
        confirmPasswordLabel.textColor = enterPasswordLabel.textColor
        enterPasswordLabel.font = UIFont.with(size: .LightNormal1, weight: .bold)
        confirmPasswordLabel.font = enterPasswordLabel.font

        let gr = UITapGestureRecognizer(target: self, action: #selector(didTapTAndC))
        tandcLabel.textColor = DevUtil.color(name: Colors.tmw_onboarding_background_text.rawValue)
        tandcLabel.font = UIFont.with(size: .LightNormal1, weight: .bold)
        tandcLabel.addGestureRecognizer(gr)
        tandcLabel.text = "Terms and Conditions"

        if !isUITesting() {
            pin1TextField.becomeFirstResponder()
        }

        pin1TextField.addDefaultBorder(width: 1)
        pin3TextField.addDefaultBorder(width: 1)
        pin2TextField.addDefaultBorder(width: 1)
        pin4TextField.addDefaultBorder(width: 1)
        confirmPin1TextField.addDefaultBorder(width: 1)
        confirmPin2TextField.addDefaultBorder(width: 1)
        confirmPin3TextField.addDefaultBorder(width: 1)
        confirmPin4TextField.addDefaultBorder(width: 1)
    }

    @objc
    func didTapTAndC() {
        UIApplication.shared.open(presenter!.termsUrl(), options: [:], completionHandler: nil)
    }

    @IBAction func didTapNextButton(_ sender: Any) {
        guard let presenter = presenter else {
            assert(false)
            return
        }

        presenter.didTapConfirm()
    }
}

extension SetupPinViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        // On inputing value to textfield
        if let lastChar = string.last {
            if textField == pin1TextField {
                pin2TextField.becomeFirstResponder()
            }
            if textField == pin2TextField {
                pin3TextField.becomeFirstResponder()
            }
            if textField == pin3TextField {
                pin4TextField.becomeFirstResponder()
            }
            if textField == pin4TextField {
                confirmPin1TextField.becomeFirstResponder()
            }
            if textField == confirmPin1TextField {
                confirmPin2TextField.becomeFirstResponder()
            }
            if textField == confirmPin2TextField {
                confirmPin3TextField.becomeFirstResponder()
            }
            if textField == confirmPin3TextField {
                confirmPin4TextField.becomeFirstResponder()
            }
            if textField == confirmPin4TextField {
                textField.resignFirstResponder()
            }
            textField.text = "\(lastChar)"
            return false
        }
        else if (textField.text?.count)! >= 1, string.isEmpty {
            // on deleting value from Textfield
            if textField == pin4TextField {
                pin3TextField.becomeFirstResponder()
            }
            if textField == pin3TextField {
                pin2TextField.becomeFirstResponder()
            }
            if textField == pin2TextField {
                pin1TextField.becomeFirstResponder()
            }
            if textField == confirmPin4TextField {
                confirmPin3TextField.becomeFirstResponder()
            }
            if textField == confirmPin3TextField {
                confirmPin2TextField.becomeFirstResponder()
            }
            if textField == confirmPin2TextField {
                confirmPin1TextField.becomeFirstResponder()
            }
            if textField == confirmPin1TextField {
                pin4TextField.becomeFirstResponder()
            }
            textField.text = ""
            return false
        }
        else if (textField.text?.count)! >= 1 {
            textField.text = string
            return false
        }
        return true
    }
}
