//
//  AuthenticationViewController.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 16/12/2019.
//  Copyright © 2019 JadePayments. All rights reserved.
//

import UIKit

class AuthenticationViewController: BaseScreenViewController, AuthenticationViewContract {
    var presenter: AuthenticationPresentingContact?

    @IBOutlet var mainLogoImageView: UIImageView!
    @IBOutlet var mainTitleLabel: UILabel!
    @IBOutlet var detailLabel: UILabel!
    @IBOutlet var pinButton: UIButton!
    @IBOutlet var biometricButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }

        guard let presenter = presenter else {
            assert(false)
            return
        }

        presenter.viewDidLoad()
    }

    func setup() {
        view.backgroundColor = DevUtil.color(name: Colors.tmw_onboarding_background.rawValue)
        mainLogoImageView.image = DevUtil.imageSkin(name: "tmw_navigation_logo")

        pinButton.setTitle("SET UP PIN", for: .normal)
        pinButton.backgroundColor = DevUtil.color(name: Colors.tmw_onboarding_button_background.rawValue)
        pinButton.titleLabel?.font = UIFont.with(size: .LightNormal1, weight: .bold)
        pinButton.setTitleColor(DevUtil.color(name: Colors.tmw_onboarding_button_text.rawValue), for: UIControl.State.normal)
        pinButton.addDefaultRoundCorners()

        biometricButton.setTitle("SET UP BIOMETRIC ID", for: .normal)
        biometricButton.backgroundColor = DevUtil.color(name: Colors.tmw_onboarding_button_background.rawValue)
        biometricButton.titleLabel?.font = pinButton.titleLabel?.font
        biometricButton.setTitleColor(DevUtil.color(name: Colors.tmw_onboarding_button_text.rawValue), for: UIControl.State.normal)
        biometricButton.addDefaultRoundCorners()

        mainTitleLabel.textColor = DevUtil.color(name: Colors.tmw_onboarding_background_text.rawValue)
        detailLabel.textColor = mainTitleLabel.textColor

        mainTitleLabel.font = UIFont.with(size: .LightNormal1, weight: .bold)
        detailLabel.font = UIFont.with(size: .LightNormal1, weight: .normal)

        mainTitleLabel.text = "Set Up Authentication"
        detailLabel.text = """
        In order for you to have access to your account let’s set up your verification method.

        This can be by PIN or Biometric ID (Fingerprint or Facial Recognition).
        """

        if !BioMetricAuthenticator.shared.faceIDAvailable(), !BioMetricAuthenticator.shared.touchIDAvailable(), !isUITesting() {
            biometricButton.isHidden = true
        }

        if let authMethod = presenter?.authMethod(), authMethod == .biometric, !Util.Tappit.biometricIDAvailable() {
            showAlert(message: "finger_touch_notavail".localized)
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        Analytics.reportShowSetupAuthenticationScreenEvent()

        hideBackButton()
        navigationController?.setNavigationBarHidden(true, animated: true)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        hideBackButton()
        navigationController?.setNavigationBarHidden(true, animated: true)
    }

    @IBAction func didTapPinButton(_ sender: Any) {
        guard let presenter = presenter else {
            assert(false)
            return
        }

        presenter.didTapSetupPin()
    }

    @IBAction func didTapBiometricButton(_ sender: Any) {
        guard let presenter = presenter else {
            assert(false)
            return
        }

        presenter.didTapSetupBiometry()
    }
}
