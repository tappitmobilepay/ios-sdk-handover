//
//  EnterPinViewController.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 02/01/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import UIKit

class EnterPinViewController: BaseScreenViewController, EnterPinViewContract {
    @IBOutlet var errorLabel: UILabel!
    @IBOutlet var mainLogoImageView: UIImageView!
    @IBOutlet var enterPasswordLabel: UILabel!
    @IBOutlet var pin1TextField: UITextField!
    @IBOutlet var pin2TextField: UITextField!
    @IBOutlet var pin3TextField: UITextField!
    @IBOutlet var pin4TextField: UITextField!
    @IBOutlet var forgotPinButton: UIButton!

    var dismissAction: (() -> Void)?

    var loginManager: LoginManagingContract?

    var pin1: String? {
        return pin1TextField.text
    }

    var pin2: String? {
        return pin2TextField.text
    }

    var pin3: String? {
        return pin3TextField.text
    }

    var pin4: String? {
        return pin4TextField.text
    }

    var presenter: EnterPinPresenterContract?

    override func viewDidLoad() {
        super.viewDidLoad()

        guard let presenter = presenter else {
            assert(false)
            return
        }

        presenter.viewDidLoad()
    }

    override func viewDidAppear(_ animated: Bool) {
        presenter!.viewDidAppear()
        hideBackButton()
        navigationController?.setNavigationBarHidden(true, animated: true)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        hideBackButton()
        navigationController?.setNavigationBarHidden(true, animated: true)

        pin1TextField.becomeFirstResponder()
        pin1TextField.text = ""
        pin2TextField.text = ""
        pin3TextField.text = ""
        pin4TextField.text = ""

        guard let presenter = presenter else {
            fatalError()
        }

        presenter.viewWillAppear()
    }

    func setup() {
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }

        view.backgroundColor = DevUtil.color(name: Colors.tmw_onboarding_background.rawValue)
        mainLogoImageView.image = DevUtil.imageSkin(name: "tmw_navigation_logo")

        enterPasswordLabel.text = "enterpin_title".localized
        enterPasswordLabel.textColor = DevUtil.color(name: Colors.tmw_onboarding_background_text.rawValue)
        enterPasswordLabel.font = UIFont.with(size: .LightNormal1, weight: .bold)

        errorLabel.font = UIFont.with(size: .LightNormal1, weight: .bold)
        errorLabel.textColor = DevUtil.color(name: Colors.tmw_onboarding_background_text.rawValue)
        errorLabel.isHidden = true

        pin1TextField.becomeFirstResponder()

        pin1TextField.addDefaultBorder(width: 1)
        pin2TextField.addDefaultBorder(width: 1)
        pin3TextField.addDefaultBorder(width: 1)
        pin4TextField.addDefaultBorder(width: 1)

        forgotPinButton.setTitleColor(DevUtil.color(name: Colors.tmw_onboarding_background_text.rawValue), for: .normal)
        forgotPinButton.titleLabel?.font = UIFont.with(size: .Medium, weight: .normal)
        forgotPinButton.underlineButton(text: "Forgot pin?")
    }

    func showWrongPinEntered(message: String) {
        errorLabel.text = message
        errorLabel.isHidden = false
        resetFields()
    }

    func resetFields() {
        pin1TextField.text = ""
        pin2TextField.text = ""
        pin3TextField.text = ""
        pin4TextField.text = ""
        pin1TextField.becomeFirstResponder()
    }

    func enableLockdown() {
        setUI(enabled: false)
    }

    func disableLockdown() {
        setUI(enabled: true)
        resetFields()
    }

    func updateLockdownWith(count: Double) {
        errorLabel.text = String(format: "enterpin_tryagain".localized, Int(count))
    }

    func setUI(enabled: Bool) {
        errorLabel.isHidden = enabled
        view.isUserInteractionEnabled = enabled
        pin1TextField.isEnabled = enabled
        pin2TextField.isEnabled = enabled
        pin3TextField.isEnabled = enabled
        pin4TextField.isEnabled = enabled
    }

    @IBAction func didTapForgotPin(_ sender: Any) {
        presenter?.didTapForgotPin()
    }
}

extension EnterPinViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        // On inputing value to textfield
        if let lastChar = string.last {
            if textField == pin1TextField {
                pin2TextField.becomeFirstResponder()
            }
            if textField == pin2TextField {
                pin3TextField.becomeFirstResponder()
            }
            if textField == pin3TextField {
                pin4TextField.becomeFirstResponder()
            }

            textField.text = "\(lastChar)"
            if textField == pin4TextField {
                guard let presenter = presenter else {
                    assert(false)
                    return false
                }
                textField.resignFirstResponder()
                presenter.didEnterPin()
            }
            return false
        }
        else if (textField.text?.count)! >= 1, string.isEmpty {
            // on deleting value from Textfield
            if textField == pin4TextField {
                pin3TextField.becomeFirstResponder()
            }
            if textField == pin3TextField {
                pin2TextField.becomeFirstResponder()
            }
            if textField == pin2TextField {
                pin1TextField.becomeFirstResponder()
            }
            textField.text = ""
            return false
        }
        else if (textField.text?.count)! >= 1 {
            textField.text = string
            return false
        }
        return true
    }
}
