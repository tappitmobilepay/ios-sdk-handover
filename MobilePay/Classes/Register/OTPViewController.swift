////
////  OTPViewController.swift
////  MobilePay
////
////  Created by Marius Kurgonas on 09/12/2019.
////  Copyright © 2019 JadePayments. All rights reserved.
////
//
// import UIKit
//
// class OTPViewController: UIViewController {
//    var didTapCancelClosure: (() -> Void)?
//    var didTapSubmitClosure: ((_ otp: String?) -> Void)?
//
//    @IBOutlet var containerView: UIView!
//    @IBOutlet var cancelButton: UIButton!
//    @IBOutlet var submitButton: UIButton!
//    @IBOutlet var mainLabel: UILabel!
//    @IBOutlet var textField: UITextField!
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        containerView.layer.cornerRadius = 2
//        containerView.layer.masksToBounds = true
//        mainLabel.text = "An One Time Pin has been sent to your email, please enter the Pin here:"
//        mainLabel.font = UIFont.with(size: .LargeMedium, weight: .normal)
//        textField.font = mainLabel.font
//        cancelButton.titleLabel?.font = UIFont.with(size: .LargeMedium, weight: .normal)
//        submitButton.titleLabel?.font = cancelButton.titleLabel?.font
//        submitButton.backgroundColor = MobilePayCore.shared.config.secondaryColor
//        cancelButton.backgroundColor = MobilePayCore.shared.config.primaryColor
//
//        textField.becomeFirstResponder()
//    }
//
//    func dismiss() {
//        dismiss(animated: true, completion: nil)
//    }
//
//    @IBAction func didTapSubmitButton(_ sender: Any) {
//        didTapSubmitClosure?(textField.text)
//    }
//
//    @IBAction func didTapCancelButton(_ sender: Any) {
//        didTapCancelClosure?()
//        dismiss()
//    }
// }
