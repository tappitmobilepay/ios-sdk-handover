//
//  SetupPinPresenter.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 04/06/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation

class SetupPinPresenter: SetupPinPresentingContract {
    let navigationMethod: NavigationMethod
    weak var view: SetupPinViewContract?
    let router: RouterContract
    let db: DatabaseManager
    var credStore: CredentialsStoringContract

    init(view: SetupPinViewContract,
         router: RouterContract,
         navigationMethod: NavigationMethod,
         db: DatabaseManager,
         credStore: CredentialsStoringContract)
    {
        self.view = view
        self.router = router
        self.navigationMethod = navigationMethod
        self.db = db
        self.credStore = credStore
    }

    func viewDidLoad() {
        view?.setup()
    }

    func viewDidAppear() {
        Analytics.reportShowSetupPinScreenEvent()
    }

    func didTapConfirm() {
        guard let view = view else {
            // Do not need to assert or stuff like that as the view is optional
            return
        }

        if !isUITesting(),
           view.pin1Text.isEmpty ||
           view.pin2Text.isEmpty ||
           view.pin3Text.isEmpty ||
           view.pin4Text.isEmpty ||
           view.confirmPin1Text.isEmpty ||
           view.confirmPin2Text.isEmpty ||
           view.confirmPin3Text.isEmpty ||
           view.confirmPin4Text.isEmpty
        {
            let failureMessage = "pin_alert_value".localized
            showAlert(message: failureMessage)
            Analytics.reportSetupPinFailedEvent(failureMessage: failureMessage)
        }
        else if isUITesting() ||
            (view.pin1Text == view.confirmPin1Text &&
                view.pin2Text == view.confirmPin2Text &&
                view.pin3Text == view.confirmPin3Text &&
                view.pin4Text == view.pin4Text)
        {
            let pin = "\(view.pin1Text)\(view.pin2Text)\(view.pin3Text)\(view.pin4Text)"
            credStore.pin = pin
            if let _ = credStore.authenticationMethod {
                Analytics.reportSubsequestAuthenticationSetupSuccess()
            }
            credStore.authenticationMethod = .pin

            Analytics.reportPinSetupEvent()
            if credStore.shouldResetAuthentication == .some(true) {
                credStore.shouldResetAuthentication = false
                Analytics.reportPinResetSuccessEvent()
            }

            switch navigationMethod {
            case .default:

                if !db.hasCreditCards(), credStore.customerStats?.userSegmentation == .some(UsserSegmentation.registered.rawValue) {
                    router.presentAddInitialCard(animated: true)
                }
                else {
                    router.presentHome(animated: true)
                }
            default:
                router.goBack(pin: true)
            }
        }
        else {
            let failureMessage = "pin_alert_match".localized
            showAlert(message: failureMessage)
            Analytics.reportSetupPinFailedEvent(failureMessage: failureMessage)
        }
    }

    func termsUrl() -> URL {
        var devUrl = DevUtil.termsUrl()
        if let remoteUrlString = db.clientConfig()?.configuration.settings.termsAndConditions,
           let remoteUrl = URL(string: remoteUrlString)
        {
            devUrl = remoteUrl
        }

        return devUrl
    }
}
