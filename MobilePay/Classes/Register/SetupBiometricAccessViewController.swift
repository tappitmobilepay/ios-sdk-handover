//
//  SetupBiometricAccessViewController.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 16/12/2019.
//  Copyright © 2019 JadePayments. All rights reserved.
//

import LocalAuthentication
import UIKit

class SetupBiometricAccessViewController: BaseScreenViewController {
    @IBOutlet var mainLogoImageView: UIImageView!
    @IBOutlet var mainTitleLabel: UILabel!
    @IBOutlet var detailLabel: UILabel!
    @IBOutlet var biometricImageView: UIImageView!
    @IBOutlet var openAuthButton: UIButton!

    var termsRange: NSRange?

    var navigationMethod = NavigationMethod.default
    var router: RouterContract!
    var db: DatabaseManager!

    override func viewDidAppear(_ animated: Bool) {
        showBackButton()
        navigationController?.setNavigationBarHidden(false, animated: true)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        showBackButton()
        navigationController?.setNavigationBarHidden(false, animated: true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }

        addDefaultTitleViewLogo()

        view.backgroundColor = DevUtil.color(name: Colors.tmw_onboarding_background.rawValue)
        mainLogoImageView.image = DevUtil.imageSkin(name: "tmw_navigation_logo")
        mainLogoImageView.isHidden = true

        mainTitleLabel.textColor = DevUtil.color(name: Colors.tmw_onboarding_background_text.rawValue)
        detailLabel.textColor = mainTitleLabel.textColor

        mainTitleLabel.font = UIFont.with(size: .LightNormal1, weight: .bold)
        detailLabel.font = UIFont.with(size: .LightNormal1, weight: .normal)

        let brandName = db.clientConfig()?.configuration.appName ?? DevUtil.brandName()

        mainTitleLabel.text = "Set Up Your Biometric ID"

        let term = "Terms and Conditions."
        let termText = """
        All biometrics already registered on your mobile can be use to log on with \(brandName) Pay.

        By enabling biometrics you agree to the
        """

        let attributedString = NSMutableAttributedString()
        attributedString.append(NSAttributedString(string: termText + " ",
                                                   attributes: [.underlineStyle: 0]))
        attributedString.append(NSAttributedString(string: term,
                                                   attributes: [
                                                       .underlineStyle: NSUnderlineStyle.single.rawValue,
                                                       .font: UIFont.with(size: .LightNormal1, weight: .bold)
                                                   ]))

        termsRange = (attributedString.string as NSString).range(of: term)

        detailLabel.attributedText = attributedString

        let gr = UITapGestureRecognizer(target: self, action: #selector(didTapConditions(gesture:)))
        detailLabel.addGestureRecognizer(gr)

        biometricImageView.image = DevUtil.imageSkin(name: "tmw_biometric")?.withRenderingMode(.alwaysTemplate)
        biometricImageView.tintColor = DevUtil.color(name: Colors.tmw_biometric_indicator.rawValue)

        openAuthButton.setTitleColor(DevUtil.color(name: Colors.tmw_onboarding_background_text.rawValue), for: .normal)
        openAuthButton.titleLabel?.font = UIFont.with(size: .Medium, weight: .normal)
        openAuthButton.underlineButton(text: "Login with biometics")

        if !isUITesting() {
            openBiometricID()
        }
    }

    @objc
    func didTapConditions(gesture: UITapGestureRecognizer) {
        guard let range = termsRange else {
            return
        }

        if gesture.didTapAttributedTextInLabel(label: detailLabel, inRange: range) {
            var devUrl = DevUtil.termsUrl()
            if let remoteUrlString = db.clientConfig()?.configuration.settings.termsAndConditions,
               let remoteUrl = URL(string: remoteUrlString)
            {
                devUrl = remoteUrl
            }

            UIApplication.shared.open(devUrl, options: [:], completionHandler: nil)
        }
        else {}
    }

    func openBiometricID() {
        showPasscodeAuthentication(message: "Enter passcode to authenticate")
    }

    // show passcode authentication
    func showPasscodeAuthentication(message: String) {
        BioMetricAuthenticator.authenticateWithPasscode(reason: message) { [weak self] result in
            switch result {
            case .success:
                self?.authSuccess() // passcode authentication success
            case .failure(let error):
                switch error {
                // device does not support biometric (face id or touch id) authentication
                case .biometryNotAvailable:
                    showAlert(message: error.message())

                // No biometry enrolled in this device, ask user to register fingerprint or face
                case .biometryNotEnrolled:
                    showAlert(message: error.message())

                // show alternatives on fallback button clicked
                case .fallback:
                    break
                    // breakself?.openBiometricID()

                // Biometry is locked out now, because there were too many failed attempts.
                // Need to enter device passcode to unlock.
                case .biometryLockedout:
                    self?.openBiometricID()

                // do nothing on canceled by system or user
                case .canceledBySystem, .canceledByUser:
                    break

                // show error for any other reason
                default:
                    showAlert(message: error.message())
                }
            }
        }
    }

    func authSuccess() {
        var cs = router?.credStore

        if let _ = cs?.authenticationMethod {
            Analytics.reportSubsequestAuthenticationSetupSuccess()
        }

        cs?.authenticationMethod = AuthenticationMethod.biometric

        if cs?.shouldResetAuthentication == .some(true) {
            cs?.shouldResetAuthentication = false
            Analytics.reportBionResetSuccessEvent()
        }

        switch navigationMethod {
        case .default:

            guard let db = db else {
                assert(false)
                return
            }

            if !db.hasCreditCards(), router?.credStore.customerStats?.userSegmentation == .some(UsserSegmentation.registered.rawValue) {
                router?.presentAddInitialCard(animated: true)
            }
            else {
                router?.presentHome(animated: true)
            }

        default:
            router?.goBack(pin: false)
        }
    }

    @IBAction func didTapOpenAuthButton(_ sender: Any) {
        openBiometricID()
    }
}
