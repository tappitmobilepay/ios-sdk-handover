//
//  MobilePayPresentingContract.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 04/06/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation

protocol MobilePayPresentingContract {
    func viewDidLoad()

    func willHandleUrl(url: URL) -> Bool
}
