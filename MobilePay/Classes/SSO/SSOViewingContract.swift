//
//  SSOViewingContract.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 06/08/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation

protocol SSOViewingContract: AnyObject {
    var presenter: SSOPresentingContract? { set get }

    func hideNavBar()

    func setup()

    func show(message: String)
}
