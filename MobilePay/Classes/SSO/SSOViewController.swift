//
//  SSOViewController.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 06/08/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import UIKit

class SSOViewController: UIViewController, SSOViewingContract {
    var presenter: SSOPresentingContract?
    @IBOutlet var loader: CircularProgressBar!
    @IBOutlet var infoLabel: UILabel!
    @IBOutlet var logoImage: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()

        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }

        guard let presenter = presenter else {
            fatalError()
        }

        presenter.viewDidLoad()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        hideNavBar()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        guard let presenter = presenter else {
            fatalError()
        }

        presenter.viewWillAppear()
    }

    func show(message: String) {
        showAlert(message: message)
    }

    func hideNavBar() {
        navigationController?.setNavigationBarHidden(true, animated: true)
    }

    func setup() {
        infoLabel.font = UIFont.with(size: .LightNormal1, weight: .normal)
        infoLabel.textColor = DevUtil.color(name: Colors.tmw_onboarding_background_text.rawValue)
        view.backgroundColor = DevUtil.color(name: Colors.tmw_onboarding_background.rawValue)
        logoImage.image = DevUtil.imageSkin(name: "tmw_navigation_logo")
        loader.strokeColor = DevUtil.color(name: Colors.tmw_onboarding_button_background.rawValue)
        loader.bgStrokeColor = DevUtil.color(name: Colors.tmw_onboarding_background.rawValue)
        loader.lineWidth = 12

        load()
    }

    func load() {
        loader.setProgress(to: 1, withAnimation: true, begining: 0, duration: 5) { [weak self] in
            self?.load()
        }
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)

        loader.removeAnimations()
    }
}
