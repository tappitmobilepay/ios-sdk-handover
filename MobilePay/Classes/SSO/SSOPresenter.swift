//
//  SSOPresenter.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 06/08/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation

internal enum SDKErrors: Int { case
    apiKeyNotSet = 1
}

class SSOPresenter: SSOPresentingContract {
    weak var view: SSOViewingContract?
    var router: RouterContract
    let webService: LoginWebServiceContract
    let giveawayTokensService: GiveawayTokensServiceContract
    var credStore: CredentialsStoringContract
    var balancesWebServide: UserWebServiceContract
    let paymentsService: PaymentsWebServiceContract
    let campaignsService: CampaignsServicingContract
    let db: DatabaseManager

    init(view: SSOViewingContract,
         router: RouterContract,
         webService: LoginWebServiceContract,
         credStore: CredentialsStoringContract,
         giveawayTokensService: GiveawayTokensServiceContract,
         db: DatabaseManager,
         paymentsService: PaymentsWebServiceContract,
         balancesWebServide: UserWebServiceContract,
         campaignsService: CampaignsServicingContract)
    {
        self.view = view
        self.router = router
        self.campaignsService = campaignsService
        self.webService = webService
        self.credStore = credStore
        self.db = db
        self.giveawayTokensService = giveawayTokensService
        self.paymentsService = paymentsService
        self.balancesWebServide = balancesWebServide
    }

    func viewDidLoad() {
        view?.setup()
        getAppConfig()
    }

    func getAppConfig() {
        paymentsService.cancel()
        paymentsService.requestClientConfig(integrationKey: router.ssoLogin.integrationKey) { [weak self] config, error in
            if let error = error {
                self?.view?.show(message: error.localizedDescription)
            }
            else if let config = config {
                self?.db.saveClientConfig(clientConfig: config)
                self?.credStore.apiKey = config.clientToken
                self?.login(clientId: config.clientId)
            }
            else {
                assert(false)
            }
        }
    }

    func login(clientId: String) {
        webService.cancel()
        let loginRequest = LoginRequest(customerClientId: router.ssoLogin.customerId,
                                        clientId: clientId,
                                        integrationIds: router.ssoLogin.integrationData,
                                        IsUsernamePasswordAuthentication: false,
                                        userName: router.ssoLogin.email)
        webService.sso(request: loginRequest) { [weak self] response, error in
            if let e = error {
                self?.view?.show(message: e.localizedDescription)
            }
            else if let response = response {
                if self?.save(response: response) == .some(false) {
                    return
                }

                if response.forceResetAuth ||
                    self?.credStore.shouldResetAuthentication == .some(true)
                {
                    self?.credStore.shouldResetAuthentication = true
                    self?.resetAuth()
                }
                else {
                    self?.getPaymentMethods()
                }
            }
        }
    }

    func save(response: LoginResponse) -> Bool {
        guard let decodedToken = try? decode(jwt: response.token),
              let jwtBody = decodedToken.body["sub"] as? String,
              let jwtBodyData = jwtBody.data(using: .utf8),
              let jwtDecodedBody = try? JSONDecoder().decode(JWTBody.self, from: jwtBodyData)
        else {
            showAlert(message: "Could not parse the login data. Please contact the customer support.")
            return false
        }
        credStore.clientTimezone = jwtDecodedBody.TimeZone
        credStore.bearerToken = response.token
        credStore.userId = response.hashId
        credStore.loggedInWithEnvironment = router.ssoLogin.environment.rawValue
        credStore.loggedInWithIntegrationKey = router.ssoLogin.integrationKey

        return true
    }

    func resetAuth() {
        credStore.pin = nil
        credStore.pinLockdownDate = nil
        credStore.authenticationMethod = nil

        guard let apiKey = credStore.apiKey else {
            view?.show(message: "Internal SDK error code \(SDKErrors.apiKeyNotSet.rawValue)")
            return
        }
        
        let request = ResetPinRequest(clientCustomerId: router.ssoLogin.customerId,
                                      clientToken: apiKey,
                                      email: router.ssoLogin.email)

        webService.cancel()
        webService.informServerPinWasReset(resetPinRequest: request) { [weak self] error in
            if let e = error {
                self?.view?.show(message: e.localizedDescription)
            }
            else {
                self?.getPaymentMethods()
            }
        }
    }

    func getPaymentMethods() {
        paymentsService.cancel()
        paymentsService.requestAllPaymentMethods(completion: { [weak self] cards, error in
            if let e = error {
                self?.view?.show(message: e.localizedDescription)
            }
            else if let cards = cards {
                self?.db.saveCards(cards: cards)
                if self?.db.clientConfig()?.configuration.settings.showMyOffers == .some(true) {
                    self?.getGiveawayTokens()
                }
                else {
                    self?.db.save(giveawayTokens: [])
                    self?.db.save(complimentaryBalances: [])
                    self?.didLogin()
                }
            }
            else {
                assert(false)
            }
        })
    }

    func getGiveawayTokens() {
        giveawayTokensService.cancel()
        giveawayTokensService.getGiveawayTokens(completion: { [weak self] tokens, error in
            if let error = error {
                self?.view?.show(message: error.localizedDescription)
            }
            else if let tokens = tokens {
                self?.db.save(giveawayTokens: tokens)

                self?.getComplimentaryBalances()
            }
            else {
                assert(false)
            }
        })
    }

    func getComplimentaryBalances() {
        balancesWebServide.cancel()
        balancesWebServide.requestBalance(completion: { [weak self] balance, error in
            if let error = error {
                self?.view?.show(message: error.localizedDescription)
            }
            else if let balance = balance {
                self?.db.save(complimentaryBalances: balance.complimentaryBalances)

//                self?.getCampaigns()
                self?.getCampaigns()
            }
            else {
                assert(false)
            }
        })
    }

    func getCampaigns() {
        campaignsService.getCampaigns(completion: { [weak self] campaigns, error in
            if let error = error {
                self?.view?.show(message: error.localizedDescription)
            }
            else if let campaigns = campaigns {
                self?.db.save(campaigns: campaigns)

                self?.getCustomerStats()
            }
            else {
                assert(false)
            }
        })
    }

    func getCustomerStats() {
        paymentsService.getUserStats(fanId: router.ssoLogin.customerId) { [weak self] response, error in
            if let error = error {
                self?.view?.show(message: error.localizedDescription)
            }
            else if let stats = response {
                self?.credStore.customerStats = stats

                self?.didLogin()
            }
            else {
                assert(false)
            }
        }
    }

    func didLogin() {
        TMWCommonUtil.userDefaults.setValue(true, forKey: Constants.kIsAlreadyLoginKey)
        TMWCommonUtil.userDefaults.synchronize()
        let cards = db.allCreditCards()
        if cards.isEmpty {
            router.presentSSOInformation(animated: true)
        }
        else {
            isAuthendicated(router: router, animated: true)
        }
    }

    func viewWillAppear() {
        view?.hideNavBar()
    }
}
