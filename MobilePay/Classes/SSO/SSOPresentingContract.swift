//
//  SSOPresentingContract.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 06/08/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation

protocol SSOPresentingContract {
    func viewDidLoad()

    func viewWillAppear()
}
