//
//  PaymentFlowManagementContract.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 5/18/21.
//  Copyright © 2021 JadePayments. All rights reserved.
//

import Foundation

protocol PaymentFlowManagementContract {
    var currentState: PaymentState { get set }

    func moveToNextState(paymentData: PaymentData) throws
}
