//
//  CustomerStats.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 29/09/2021.
//  Copyright © 2021 JadePayments. All rights reserved.
//

import Foundation

struct CustomerStats: Codable {
    let userSegmentation: Int
}
