//
//  LoginManager.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 15/07/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation

protocol LoginManagingContract {
    func logoutApp()
}

class LoginManager: LoginManagingContract {
    let db: DatabaseManager
    var credStore: CredentialsStoringContract

    init(db: DatabaseManager,
         credStore: CredentialsStoringContract)
    {
        self.db = db
        self.credStore = credStore
    }

    func logoutApp() {
        TMWCommonUtil.userDefaults.set("", forKey: "cookies")
        TMWCommonUtil.userDefaults.set(false, forKey: Constants.kIsAlreadyLoginKey)
        credStore.wipe()
        db.deleteAllData()
    }

    func softLogout() {
        TMWCommonUtil.userDefaults.set("", forKey: "cookies")
        TMWCommonUtil.userDefaults.set(false, forKey: Constants.kIsAlreadyLoginKey)
        credStore.softWipe()
        db.deleteAllData()
    }
}
