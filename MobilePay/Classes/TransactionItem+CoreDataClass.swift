//
//  TransactionItem+CoreDataClass.swift
//
//
//  Created by Marius Kurgonas on 17/07/2020.
//
//

import CoreData
import Foundation

@objc(TransactionItem)
public class TransactionItem: NSManagedObject {}
