//
//  Constants.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 05/03/2021.
//  Copyright © 2021 JadePayments. All rights reserved.
//

import Foundation
import UIKit

struct Constants {
    static let kQRCodeTimeLeftUserDefaultsKey = "kQRCodeTimeLeftUserDefaultsKey"
    static let colorBorder = UIColor(hex: "#dddddd")
    static let mobilePayCustomerTokenKey = "mobilePayCustomerTokenKey"
    static let mobilePayCustomerTimeZoneKey = "mobilePayCustomerTimeZoneKey"
    static let mobilePayCustomerUserIdKey = "mobilePayCustomerUserIdKey"
    static let mobilePayCustomerLastLockdownDateKey = "mobilePayCustomerLastLockdownDateKey"
    static let mobilePayCustomerWrongPinsInRowKey = "mobilePayCustomerWrongPinsInRowKey"
    static let mobilePayCustomerPinValueKey = "mobilePayCustomerPinValueKey"
    static let mobilePayTipsPermissionsKey = "mobilePayTipsPermissionsKey"
    static let mobilePayLoggedInWithEnvironmentKey = "mobilePayLoggedInWithEnvironmentKey"
    static let mobilePayLoggedInWithIntegrationKey = "mobilePayLoggedInWithIntegrationKey"
    static let kAppAuthorizationMethodKey = "applicationAuthSuperKey"
    static let kClientApiKeyKey = "kClientApiKeyKey"
    static let kIsAlreadyLoginKey = "isAlreadyLoginSuperKey"
    static let kShowRedeemedCampaignsKey = "kShowRedeemedCampaignsKey"
    static let kShouldResetAuthenticationKey = "kShouldResetAuthenticationKey"
    static let kCustomerStatsKey = "kCustomerStatsKey"
}
