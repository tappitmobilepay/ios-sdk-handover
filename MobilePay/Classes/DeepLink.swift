//
//  DeepLink.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 09/08/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation

/// Class to enable and customize the deep link button on the HOME screen of the SDK
public struct DeepLink {
    internal let url: URL

    /// Method to initialize the deep link structure
    /// - Parameter url: url to open for the deep link
    public init(url: URL) {
        self.url = url
    }
}
