//
//  Card.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 05/05/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import CoreData
import Foundation

struct Card: Codable {
    let id: String
    let cardNumber: String
    let cardType: String
    let isDefault: Bool
    let paymentProfileSettings: [PaymentProfileSettings]?
}

struct PaymentProfileSettings: Codable {
    let paymentProfileId: String?
    let paymentProfileType: Int?

    static func from(managedPaymentProfileSettings: ManagedPaymentProfileSettings) -> PaymentProfileSettings {
        return PaymentProfileSettings(paymentProfileId: managedPaymentProfileSettings.paymentProfileId,
                                      paymentProfileType: managedPaymentProfileSettings.paymentProfileType?.intValue)
    }

    static func toManagedPaymentProfileSettings(paymentProfileSettings: PaymentProfileSettings, context: NSManagedObjectContext) -> ManagedPaymentProfileSettings {
        let managedPaymentProfileSettings = ManagedPaymentProfileSettings(context: context)
        if let ppi = paymentProfileSettings.paymentProfileId {
            managedPaymentProfileSettings.paymentProfileId = ppi
        }
        if let ppt = paymentProfileSettings.paymentProfileType {
            managedPaymentProfileSettings.paymentProfileType = NSDecimalNumber(value: ppt)
        }
        return managedPaymentProfileSettings
    }
}

extension Card {
    static func from(managedCreditCard: ManagedCreditCard) -> Card {
        return Card(id: managedCreditCard.id!,
                    cardNumber: managedCreditCard.cardNumber!,
                    cardType: managedCreditCard.cardType!,
                    isDefault: managedCreditCard.isDefault,
                    paymentProfileSettings: managedCreditCard.paymentProfileSettings?.map { PaymentProfileSettings.from(managedPaymentProfileSettings: $0 as! ManagedPaymentProfileSettings) })
    }

    static func toManagedCard(card: Card, context: NSManagedObjectContext) -> ManagedCreditCard {
        let managedCard = ManagedCreditCard(context: context)
        managedCard.id = card.id
        managedCard.cardNumber = card.cardNumber
        managedCard.cardType = card.cardType
        managedCard.isDefault = card.isDefault

        if let pps = card.paymentProfileSettings {
            managedCard.paymentProfileSettings = NSSet(array: pps.map { PaymentProfileSettings.toManagedPaymentProfileSettings(paymentProfileSettings: $0, context: context) })
        }
        return managedCard
    }
}
