////
////  TopUpViewController.swift
////  MobilePay
////
////  Created by Marius Kurgonas on 19/12/2019.
////  Copyright © 2019 JadePayments. All rights reserved.
////
//
// import UIKit
//
// class TopUpViewController: BaseScreenViewController {
//    var balanceController: BalanceViewController?
//
//    @IBOutlet var confirmButton: UIButton!
//    @IBOutlet var button6: UIButton!
//    @IBOutlet var button5: UIButton!
//    @IBOutlet var button4: UIButton!
//    @IBOutlet var button3: UIButton!
//    @IBOutlet var button2: UIButton!
//    @IBOutlet var button1: UIButton!
//    @IBOutlet var infoTitleLabel: UILabel!
//    @IBOutlet var currentBalanceContainerView: UIView!
//
//    private var selectedTopUpValue: Int?
//    private var buttons = [UIButton]()
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
////        currentBalanceContainerView.addDefaultColorBorder()
//
//        infoTitleLabel.font = UIFont.with(size: .LightNormal1, weight: .bold)
//        infoTitleLabel.textColor = MobilePayCore.shared.config.secondaryTextColor
//
//        addDefaultTitleViewLogo()
//
//        addChildViewController(child: balanceController!, toView: currentBalanceContainerView)
//
//        confirmButton.titleLabel?.font = UIFont.with(size: .Large, weight: .bold)
//        confirmButton.backgroundColor = MobilePayCore.shared.config.secondaryColor
//        confirmButton.setTitleColor(MobilePayCore.shared.config.primaryTextColor, for: UIControl.State.normal)
//        confirmButton.setTitle("Submit", for: .normal)
//        confirmButton.addDefaultRoundCorners()
//
//        buttons = [button1!, button2!, button3!, button4!, button5!, button6!]
//
//        for button in buttons {
//            button.addDefaultBorder()
//            button.layer.borderColor = MobilePayCore.shared.config.primaryColor.cgColor
//            button.setTitleColor(MobilePayCore.shared.config.primaryColor, for: .normal)
//        }
//    }
//
//    @IBAction func didTapNumberButton(_ sender: Any) {
//        guard let button = sender as? UIButton, let text = button.title(for: .normal) else {
//            return
//        }
//
//        for b in buttons {
//            if b != button {
//                b.backgroundColor = .clear
//                b.setTitleColor(MobilePayCore.shared.config.primaryColor, for: .normal)
//            }
//            else {
//                b.backgroundColor = MobilePayCore.shared.config.primaryColor
//                b.setTitleColor(MobilePayCore.shared.config.primaryTextColor, for: .normal)
//            }
//        }
//
//        let number = text.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
//
//        selectedTopUpValue = Int(number)
//    }
//
//    @IBAction func didTapConfirmButton(_ sender: Any) {
//        // showAlert(message: "No payment gateway found")
////        showDropIn(clientTokenOrTokenizationKey: "sandbox_gpvpjvy7_92cqm8mdsydfc4bw")
//    }
// }
