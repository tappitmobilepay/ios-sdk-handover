//
//  Decimal+Extensions.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 15/11/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation

extension Decimal {
    func double() -> Double {
        return (self as NSDecimalNumber).doubleValue
    }

    func int() -> Int {
        return (self as NSDecimalNumber).intValue
    }

    func format(f: String) -> String {
        return String(format: "%\(f)f", double())
    }
}
