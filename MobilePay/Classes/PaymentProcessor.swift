//
//  PaymentProcessor.swift
//  MobilePay-PAYG
//
//  Created by Marius Kurgonas on 02/06/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation

class PaymentProcessorr {
    var webService: PaymentsWebServiceContract
    var db: DatabaseManager

    init(webService: PaymentsWebServiceContract,
         db: DatabaseManager)
    {
        self.webService = webService
        self.db = db
    }

    func addCreditCard(cardInfo: CardInformation, completion: @escaping AddPaymentMethodResponse) {
        guard let encryptedCard = cardInfo.encrypted() else {
            completion(nil, NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Could not encrypt card data"]))
            return
        }

        let data = PaymentMethodRequest(data: encryptedCard,
                                        refId: NSUUID().uuidString)
        requestAddPaymentMethod(data: data, completion: completion)
    }
}

// MARK: Api calls

extension PaymentProcessorr {
    func requestAddPaymentMethod(data: PaymentMethodRequest, completion: @escaping AddPaymentMethodResponse) {
        webService.cancel()
        webService.addPaymentMethod(data: data, completion: completion)
    }

    func requestAllPaymentMethods(completion: @escaping PaymentMethodsResponse) {
        webService.cancel()
        webService.requestAllPaymentMethods { [weak self] cards, error in
            if let cardsData = cards {
                self?.db.saveCards(cards: cardsData)
            }
            completion(cards, error)
        }
    }

    func requestDeletePaymentMethod(id: String,
                                    paymentProfileSettings: [PaymentProfileSettings],
                                    completion: @escaping GenericResponse)
    {
        webService.cancel()
        webService.deletePaymentMethod(paymentProfileSettings: paymentProfileSettings, completion: { [weak self] error in
            if error == nil {
                self?.db.deleteCard(id: id)
            }
            completion(error)
        })
    }

    func requestMakePaymentMethodDefault(id: String, settings: [PaymentProfileSettings], completion: @escaping DefaultPaymentMethodResponse) {
        webService.cancel()
        webService.makeDefaultPaymentMethod(settings: settings) { [weak self] response, error in
            if error == nil {
                self?.db.makeCardDefault(id: id)
            }
            completion(response, error)
        }
    }

    func checkPaymentProviderConfigurations(completion: @escaping PaymentProvidersConfigResponse) {
        webService.cancel()
        webService.checkIfAllPaymentProvidersWereConfigured(completion: completion)
    }
}
