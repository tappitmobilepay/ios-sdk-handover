//
//  DevUtil.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 13/11/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation
import UIKit

class DevUtil {
    static func imageSkin(name: String) -> UIImage? {
        var adjustedName = name
        if MobilePayCore.shared.devModeEnabled {
            adjustedName = MobilePayCore.shared.devModeSkinName + "_" + adjustedName
        }

        if let _ = UIColor(named: adjustedName) {
            return nil
        }

        if let thirdPartyImage = UIImage(named: adjustedName) {
            return thirdPartyImage
        }

        return UIImage(named: name, in: Bundle(for: DevUtil.self), compatibleWith: nil)
    }

    static func color(name: String) -> UIColor {
        var adjustedName = name
        if MobilePayCore.shared.devModeEnabled {
            adjustedName = MobilePayCore.shared.devModeSkinName + "_" + adjustedName
        }

        if let color = UIColor(named: adjustedName) {
            return color
        }

        guard let defaultColor = UIColor(named: name, in: Bundle(for: DevUtil.self), compatibleWith: nil) else {
            fatalError()
        }

        return defaultColor
    }

    static func helpUrl() -> URL {
        return URL(string: "https://asgardvikings.zendesk.com/")!
    }

    static func termsUrl() -> URL {
        return URL(string: "https://asgardvikings.zendesk.com/hc/en-us/articles/360015453097-Asgard-Vikings-Terms-Conditions")!
    }

    static func brandName() -> String {
        return "Vikings"
    }
}
