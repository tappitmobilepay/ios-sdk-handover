//
//  PaymentNotification.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 2021-07-13.
//  Copyright © 2021 JadePayments. All rights reserved.
//

import Foundation

struct PaymentNotificationContent: Codable {
    let TransactionId: String
    let ClientId: String
    let CustomerId: String
    let FanId: String
    let Items: [Item]
    let SubTotal: Decimal
    let ChargedAmount: Decimal
    let TipAmount: Decimal
    let DonationAmount: Decimal
    let TaxAmount: Decimal
    let DiscountAmount: Decimal
    let ApplicationData: String
}

struct Item: Codable {
    let UnitPrice: Decimal
    let Sku: String
    let Name: String
    let Quantity: Int
    let Amount: Decimal
    let TaxAmount: Decimal
    let TaxPercentage: Decimal
    let TaxIncluded: Bool
    let DiscountAmount: Decimal
    let SubTotalAmount: Decimal
}

struct MerchantNotificationContent: Codable {
    let Success: Bool
    let Error: String
    let MerchantId: String
    let StoreName: String
    let CountryCode: String
    let CurrencyCode: String
    let Capabilities: [String]
    let SupportedNetworks: [String]
    let RequiredBillingContactFields: [String]
}

struct NotificationContent: Codable {
    let Merchant: MerchantNotificationContent
    let Payment: PaymentNotificationContent
}
