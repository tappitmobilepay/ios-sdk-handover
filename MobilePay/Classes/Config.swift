//
//  Config.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 21/01/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation

enum Colors: String, CaseIterable { case
    tmw_back_indicator,
    tmw_biometric_indicator,
    tmw_navigation_background,
    tmw_page_header_text,
    tmw_onboarding_background,
    tmw_onboarding_background_text,
    tmw_onboarding_button_background,
    tmw_onboarding_button_text,
    tmw_primary_background,
    tmw_primary_background_text,
    tmw_secondary_background,
    tmw_secondary_background_text,
    tmw_pay_button_background,
    tmw_pay_button_text,
    tmw_pay_button_border,
    tmw_home_button_background,
    tmw_home_button_text,
    tmw_home_button_border,
    tmw_primary_button_background,
    tmw_primary_button_text,
    tmw_primary_button_border,
    tmw_secondary_button_background,
    tmw_secondary_button_text,
    tmw_secondary_button_border,
    tmw_custom_unselected_tip_button_background,
    tmw_custom_unselected_tip_button_text,
    tmw_custom_unselected_tip_button_border,
    tmw_custom_selected_tip_button_background,
    tmw_custom_selected_tip_button_text,
    tmw_custom_selected_tip_button_border,
    tmw_unselected_benefit_button_background,
    tmw_unselected_benefit_button_text,
    tmw_unselected_benefit_button_border,
    tmw_selected_benefit_button_background,
    tmw_selected_benefit_button_text,
    tmw_selected_benefit_button_border,
    tmw_redeemable_campaign_card_text,
    tmw_redeemable_campaign_card_background,
    tmw_redeemable_campaign_card_border,
    tmw_redeemed_campaign_card_text,
    tmw_redeemed_campaign_card_background,
    tmw_redeemed_campaign_card_border,
    tmw_redeemable_campaign_button_background,
    tmw_redeemable_campaign_button_text,
    tmw_redeemable_campaign_button_border,
    tmw_redeemed_campaign_button_background,
    tmw_redeemed_campaign_button_text,
    tmw_redeemed_campaign_button_border,
    tmw_transaction_card_background,
    tmw_transaction_card_text,
    tmw_transaction_card_border,
    tmw_notification_badge_background,
    tmw_notification_badge_text
}
