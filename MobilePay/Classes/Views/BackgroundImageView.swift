//
//  BackgroundImageView.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 04/12/2019.
//  Copyright © 2019 JadePayments. All rights reserved.
//

import UIKit

class BackgroundImageView: UIImageView {
    init() {
        super.init(image: UIImage(named: "tmw_general_background"))
        contentMode = .scaleAspectFill
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        image = UIImage(named: "tmw_general_background")
        contentMode = .scaleAspectFill
    }
}
