//
//  CircularProgressBar.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 18/11/2019.
//  Copyright © 2019 JadePayments. All rights reserved.
//

import UIKit

class CircularProgressBar: UIView {
    // MARK: awakeFromNib

    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }

    // MARK: Public

    var animationCompletion: (() -> Void)?

    var lineWidth: CGFloat = 5 {
        didSet {
            foregroundLayer.lineWidth = lineWidth
            backgroundLayer.lineWidth = lineWidth - (0.20 * lineWidth)
        }
    }

    var strokeColor: UIColor = .lightGray {
        didSet {
            foregroundLayer.strokeColor = strokeColor.cgColor
        }
    }

    var bgStrokeColor: UIColor = .clear {
        didSet {
            backgroundLayer.strokeColor = bgStrokeColor.cgColor
        }
    }

    func setProgress(to progressConstant: Double,
                     withAnimation: Bool,
                     begining: Double = 0,
                     duration: Double = 2,
                     animationCompletion: (() -> Void)? = nil)
    {
        self.animationCompletion = animationCompletion

        var progress: Double {
            if progressConstant > 1 { return 1 }
            else if progressConstant < 0 { return 0 }
            else { return progressConstant }
        }

        foregroundLayer.strokeEnd = CGFloat(progress)

        if withAnimation {
            let animation = CABasicAnimation(keyPath: "strokeEnd")
            animation.delegate = self
            animation.fromValue = begining
            animation.toValue = progress
            animation.duration = duration
            foregroundLayer.add(animation, forKey: "foregroundAnimation")
        }
    }

    func removeAnimations() {
        animationCompletion = nil
        foregroundLayer.removeAllAnimations()
    }

    // MARK: Private

    private let foregroundLayer = CAShapeLayer()
    private let backgroundLayer = CAShapeLayer()
    private var radius: CGFloat {
        if frame.width < frame.height { return (frame.width - lineWidth) / 2 }
        else { return (frame.height - lineWidth) / 2 }
    }

    private var pathCenter: CGPoint { return convert(center, from: superview) }
    private func makeBar() {
        layer.sublayers = nil
        drawBackgroundLayer()
        drawForegroundLayer()
    }

    private func drawBackgroundLayer() {
        let path = UIBezierPath(arcCenter: pathCenter, radius: radius, startAngle: 0, endAngle: 2 * CGFloat.pi, clockwise: true)

        backgroundLayer.path = path.cgPath
        backgroundLayer.strokeColor = bgStrokeColor.cgColor
        backgroundLayer.lineWidth = lineWidth
        backgroundLayer.fillColor = UIColor.clear.cgColor
        layer.addSublayer(backgroundLayer)
    }

    private func drawForegroundLayer() {
        let startAngle = (-CGFloat.pi / 2)
        let endAngle = 2 * CGFloat.pi + startAngle

        let path = UIBezierPath(arcCenter: pathCenter, radius: radius, startAngle: startAngle, endAngle: endAngle, clockwise: true)

        foregroundLayer.path = path.cgPath
        foregroundLayer.lineCap = .round
        foregroundLayer.lineWidth = lineWidth
        foregroundLayer.fillColor = UIColor.clear.cgColor
        foregroundLayer.strokeColor = strokeColor.cgColor

        layer.addSublayer(foregroundLayer)
    }

    private func setupView() {
        makeBar()
    }

    // Layout Sublayers
    private var layoutDone = false
    override func layoutSublayers(of layer: CALayer) {
        if !layoutDone {
            setupView()
            layoutDone = true
        }
    }
}

extension CircularProgressBar: CAAnimationDelegate {
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        if flag == true {
            animationCompletion?()
        }
    }
}
