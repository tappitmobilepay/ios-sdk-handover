//
//  TitleLabel.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 28/11/2019.
//  Copyright © 2019 JadePayments. All rights reserved.
//

import UIKit

class TitleLabel: UILabel {
    @IBInspectable var topInset: CGFloat = 0.0
    @IBInspectable var bottomInset: CGFloat = -2.0
    @IBInspectable var leftInset: CGFloat = 16.0
    @IBInspectable var rightInset: CGFloat = 16.0

    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        super.drawText(in: rect.inset(by: insets))
    }

    override var intrinsicContentSize: CGSize {
        let size = super.intrinsicContentSize
        return CGSize(width: size.width + leftInset + rightInset,
                      height: size.height + topInset + bottomInset)
    }

    override func awakeFromNib() {
        super.awakeFromNib()

        font = UIFont.with(size: .LargeMedium, weight: .bold)
        textColor = DevUtil.color(name: Colors.tmw_page_header_text.rawValue)
        textAlignment = NSTextAlignment.left

        let lineView = UIView()
        lineView.backgroundColor = .black
        addSubview(lineView)

        lineView.translatesAutoresizingMaskIntoConstraints = false
        lineView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: bottomInset).isActive = true
        lineView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: leftInset).isActive = true
        lineView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -rightInset).isActive = true
        lineView.heightAnchor.constraint(equalToConstant: 1).isActive = true
    }
}
