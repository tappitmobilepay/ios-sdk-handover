//
//  MobilePayViewContract.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 04/06/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation
import UIKit

protocol MobilePayViewContract: AnyObject {
    func setup(viewController: UIViewController)
}
