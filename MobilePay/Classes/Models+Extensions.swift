//
//  Models+Extensions.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 08/01/2021.
//  Copyright © 2021 JadePayments. All rights reserved.
//

import CoreData
import Foundation

extension ClientConfig {
    func toManagedClientConfig(context: NSManagedObjectContext) -> ManagedClientConfig {
        let managedConfig = ManagedClientConfig(context: context)
        managedConfig.settings = configuration.settings.toManagedClientConfigSettings(context: context)
        managedConfig.currencyCode = clientCurrency.code
        managedConfig.currencySymbol = clientCurrency.symbol
        managedConfig.clientToken = clientToken
        managedConfig.clientId = clientId
        managedConfig.appName = configuration.appName
        return managedConfig
    }
}

extension TipsConfiguration {
    func toManagedTips(context: NSManagedObjectContext) -> ManagedTips {
        let managedTips = ManagedTips(context: context)
        managedTips.enabled = enabled
        managedTips.percentages = percentages as NSArray
        return managedTips
    }
}

extension ClientConfigSettings {
    func toManagedClientConfigSettings(context: NSManagedObjectContext) -> ManagedClientConfigSettings {
        let managedSettings = ManagedClientConfigSettings(context: context)
        managedSettings.loginEndpointOverride = loginEndpointOverride
        managedSettings.tokensEndpointOverride = tokensEndpointOverride
        managedSettings.balanceEndpointOverride = balanceEndpointOverride
        managedSettings.tips = tips.toManagedTips(context: context)
        managedSettings.showGiveawayTokens = showGiveawayTokens
        managedSettings.showGiftCard = showGiftCard
        managedSettings.showMyOffers = showMyOffers
        managedSettings.preloadTopup = preloadTopup
        managedSettings.showPrivacyPolicy = showPrivacyPolicy
        managedSettings.registrationType = Int32(registrationType)
        managedSettings.privacyPolicy = privacyPolicy
        managedSettings.termsAndConditions = termsAndConditions
        managedSettings.help = help
        return managedSettings
    }
}

extension ManagedClientConfig {
    func toClientConfig() -> ClientConfig {
        let configuration = ClientConfigConfiguration(settings: settings!.toClientConfigSettings(), appName: appName!)
        let clientCurrency = ClientCurrency(code: currencyCode!, symbol: currencySymbol!)
        return ClientConfig(configuration: configuration, clientToken: clientToken!, clientId: clientId!, clientCurrency: clientCurrency)
    }
}

extension ManagedTips {
    func toTipsConfiguration() -> TipsConfiguration {
        var p: [Int]?
        // Its array but for older users who might have had it as NSSet we check just in case for migration purposes
        if let percents = percentages as? NSSet {
            p = percents.allObjects as! [Int]
        }
        else {
            p = (percentages as! NSArray) as! [Int]
        }

        return TipsConfiguration(enabled: enabled, percentages: p!.sorted())
    }
}

extension ManagedClientConfigSettings {
    func toClientConfigSettings() -> ClientConfigSettings {
        return ClientConfigSettings(loginEndpointOverride: loginEndpointOverride,
                                    tokensEndpointOverride: tokensEndpointOverride,
                                    balanceEndpointOverride: balanceEndpointOverride,
                                    tips: tips!.toTipsConfiguration(),
                                    showGiveawayTokens: showGiveawayTokens,
                                    showGiftCard: showGiftCard,
                                    showMyOffers: showMyOffers,
                                    preloadTopup: preloadTopup,
                                    showPrivacyPolicy: showPrivacyPolicy,
                                    registrationType: Int(registrationType),
                                    privacyPolicy: privacyPolicy,
                                    termsAndConditions: termsAndConditions,
                                    help: help)
    }
}

extension ManagedBalance {
    static func from(displayBalance: Balance, context: NSManagedObjectContext) -> ManagedBalance {
        let managedBalance = ManagedBalance(entity: ManagedBalance.entity(), insertInto: context)
        managedBalance.preloadAmount = NSDecimalNumber(decimal: displayBalance.preloadAmount)
        managedBalance.currency = displayBalance.currency
        let items = displayBalance.complimentaryBalances.map { ManagedComplimentaryBalance.from(complimentaryBanalance: $0, context: context) }
        managedBalance.complimentaryBalances = NSSet(array: items)

        return managedBalance
    }
}

extension ManagedComplimentaryBalance {
    static func from(complimentaryBanalance: ComplimentaryBalance, context: NSManagedObjectContext) -> ManagedComplimentaryBalance {
        let managedComplimentaryBalance = ManagedComplimentaryBalance(entity: ManagedComplimentaryBalance.entity(), insertInto: context)
        managedComplimentaryBalance.expiryDate = complimentaryBanalance.expiryDate
        managedComplimentaryBalance.amount = NSDecimalNumber(decimal: complimentaryBanalance.amount)
        managedComplimentaryBalance.concessionaires = NSSet(array: complimentaryBanalance.concessionaires)
        managedComplimentaryBalance.id = complimentaryBanalance.id
        managedComplimentaryBalance.type = Int32(complimentaryBanalance.type)
        managedComplimentaryBalance.details = complimentaryBanalance.description
        if let slot = complimentaryBanalance.slot {
            managedComplimentaryBalance.slot = Int32(slot)
        }

        return managedComplimentaryBalance
    }
}
