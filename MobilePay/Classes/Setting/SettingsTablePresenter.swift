//
//  SettingsTablePresenter.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 01/12/2019.
//  Copyright © 2019 JadePayments. All rights reserved.
//

import Foundation

class SettingsTablePresenter: SettingsTablePresenterContract {
    func didSelectChangeAuthentication() {
        view?.showChangeAuthentication(biometricEnabled: router.credStore.authenticationMethod == .some(.biometric))
    }

    weak var view: SettingsTableViewContract?
    let router: RouterContract
    let webService: LoginWebServiceContract
    let loginManager: LoginManagingContract

    init(view: SettingsTableViewContract,
         router: RouterContract,
         loginManager: LoginManagingContract,
         webService: LoginWebServiceContract)
    {
        self.view = view
        self.router = router
        self.loginManager = loginManager
        self.webService = webService
    }

    func viewDidLoad() {
        view?.setup()
    }

    func viewWillAppear() {
        if let authenticationMethod = router.credStore.authenticationMethod {
            var value = ""
            switch authenticationMethod {
            case .pin:
                value = "PIN"
            case .biometric:
                value = "BIOMETRIC ID"
            }
            view?.updateAuthTitle(value: value)
        }
        else {
            fatalError()
        }
    }

    func didSelectChangePin() {
        Analytics.reportSettingsSetupPinSelectedEvent()
        router.presentSetupPin(navigationMethod: .pop)
    }

    func didSelectChangeBiometry() {
        Analytics.reportSettingsSetupBioSelectedEvent()
        Analytics.reportSetupBiometricSucceededEvent()
        router.presentSetupBiometric(navigationMethod: .pop)
    }
}
