//
//  SettingsTablePresenterContract.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 01/12/2019.
//  Copyright © 2019 JadePayments. All rights reserved.
//

import Foundation

protocol SettingsTablePresenterContract {
    func viewDidLoad()

    func viewWillAppear()

    func didSelectChangeAuthentication()

    func didSelectChangePin()

    func didSelectChangeBiometry()
}
