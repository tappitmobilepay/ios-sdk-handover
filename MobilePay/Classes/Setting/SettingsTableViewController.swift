//
//  SettingsTableViewController.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 01/12/2019.
//  Copyright © 2019 JadePayments. All rights reserved.
//

import UIKit

class SettingsTableViewController: UITableViewController, SettingsTableViewContract {
    @IBOutlet var authenticationLabel: UILabel!
    @IBOutlet var authenticationValueLabel: UILabel!

    var presenter: SettingsTablePresenterContract?

    override func viewDidLoad() {
        super.viewDidLoad()

        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }

        guard let presenter = presenter else {
            assert(false)
            return
        }

        presenter.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        guard let presenter = presenter else {
            fatalError()
        }

        presenter.viewWillAppear()
    }

    func updateAuthTitle(value: String) {
        authenticationValueLabel.text = value
    }

    func setup() {
        authenticationLabel.font = UIFont.with(size: .LargeMedium, weight: .bold)
        authenticationLabel.textColor = .black
        authenticationValueLabel.font = authenticationLabel.font
        authenticationValueLabel.textColor = DevUtil.color(name: Colors.tmw_primary_background.rawValue)
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        guard let presenter = presenter else {
            assert(false)
            return
        }

        switch indexPath.row {
        case 0:
            presenter.didSelectChangeAuthentication()
        case 1:
            return
        default:
            assert(false, "Unhandeled settings table cell case")
        }
    }

    func showChangeAuthentication(biometricEnabled: Bool) {
        var titles = ["PIN"]
        var actions: [() -> Void] = [
            { [weak self] in
                self?.presenter?.didSelectChangePin()
            }
        ]

        if BioMetricAuthenticator.shared.faceIDAvailable() ||
            BioMetricAuthenticator.shared.touchIDAvailable(),
            !biometricEnabled
        {
            titles.append("BIOMETRIC ID")
            actions.append { [weak self] in
                self?.presenter?.didSelectChangeBiometry()
            }
        }

        showActionSheet(titles: titles, actions: actions)
        Analytics.reportShowAuthenticationSettingsPopupEvent()
    }
}
