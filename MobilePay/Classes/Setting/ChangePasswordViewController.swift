////
////  ChangePasswordViewController.swift
////  MobilePay
////
////  Created by Marius Kurgonas on 14/01/2020.
////  Copyright © 2020 JadePayments. All rights reserved.
////
//
// import UIKit
//
// class ChangePasswordViewController: BaseScreenViewController {
//    @IBOutlet var mainTitleLabel: UILabel!
//    @IBOutlet var currentPasswordTextField: UITextField!
//    @IBOutlet var newPasswordTextField: UITextField!
//    @IBOutlet var confirmNewPasswordTextField: UITextField!
//    @IBOutlet var detailLabel: UILabel!
//    @IBOutlet var submitButton: UIButton!
//
//    var webService: RegisterWebServiceContract?
//    var router: RouterContract?
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        addDefaultTitleViewLogo()
//
//        submitButton.setTitle("SUBMIT", for: .normal)
//        submitButton.backgroundColor = MobilePayCore.shared.config.onboardingButtonsColor
//        submitButton.setTitleColor(MobilePayCore.shared.config.onboardingButtonsTextColor, for: UIControl.State.normal)
//        submitButton.addDefaultRoundCorners()
//
//        detailLabel.textColor = MobilePayCore.shared.config.primaryTextColor
//        detailLabel.font = UIFont.with(size: .LightNormal1, weight: .normal)
//        let attributedString = NSMutableAttributedString(string: "signup_pwd_msg".localized)
//        let nsstring = NSString(string: attributedString.string)
//        let range = nsstring.range(of: nsstring.substring(to: 7))
//        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: MobilePayCore.shared.config.onboardingButtonsColor, range: range)
//        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.with(size: .LightNormal1, weight: .bold), range: range)
//        detailLabel.attributedText = attributedString
//
//        mainTitleLabel.textColor = MobilePayCore.shared.config.primaryTextColor
//        mainTitleLabel.font = UIFont.with(size: .LargeMedium, weight: .bold)
//        mainTitleLabel.text = "Please fill the details below and press submit "
//
//        currentPasswordTextField.delegate = self
//        currentPasswordTextField.addDefaultRoundCorners()
//        newPasswordTextField.delegate = self
//        newPasswordTextField.addDefaultRoundCorners()
//        confirmNewPasswordTextField.delegate = self
//        confirmNewPasswordTextField.addDefaultRoundCorners()
//    }
//
//    @IBAction private func didTapSubmitButton(_ sender: Any) {
//        guard TMWCommonUtil.isInternetAvailable() else {
//            TMWCommonUtil.showInternetUnavailableAlert()
//            return
//        }
//
//        if currentPasswordTextField.text == "" || newPasswordTextField.text == "" || confirmNewPasswordTextField.text == "" {
//            let alert: UIAlertController = UIAlertController(title: "alert_title".localized, message: "pwd_alert_emptyfields".localized, preferredStyle: UIAlertController.Style.alert)
//            alert.addAction(UIAlertAction(title: "alert_accept".localized, style: UIAlertAction.Style.default, handler: nil))
//            present(alert, animated: true, completion: nil)
//        }
//        else if newPasswordTextField.text! != confirmNewPasswordTextField.text! {
//            let alert: UIAlertController = UIAlertController(title: "alert_title".localized, message: "pwd_alert_pwdnotmatch".localized, preferredStyle: UIAlertController.Style.alert)
//            alert.addAction(UIAlertAction(title: "alert_accept".localized, style: UIAlertAction.Style.default, handler: nil))
//            present(alert, animated: true, completion: nil)
//        }
//        else if !newPasswordTextField.text!.isValidPassword() || !confirmNewPasswordTextField.text!.isValidPassword() {
//            let alert: UIAlertController = UIAlertController(title: "alert_title".localized, message: "pwd_alert_format".localized, preferredStyle: UIAlertController.Style.alert)
//            alert.addAction(UIAlertAction(title: "alert_accept".localized, style: UIAlertAction.Style.default, handler: nil))
//            present(alert, animated: true, completion: nil)
//        }
//        else {
//            TMWLoaderView.shareInstance().showLoader(navigationController!.view)
//            request_changePassword()
//        }
//    }
//
//    func request_changePassword() {
//        guard let webService = webService,
//            let oldPassword = currentPasswordTextField.text,
//            let newPassword = newPasswordTextField.text,
//            let _ = router else {
//            fatalError("Item not set")
//        }
//
//        webService.cancel()
//        webService.requestChangePassword(oldPassword: oldPassword.toBase64(), newPassword: newPassword.toBase64()) { [weak self] error in
//            if let error = error {
//                showAlert(message: error.localizedDescription)
//            }
//            else {
//                self?.router?.presentSuccess(type: .ChangePassword, completion: { [weak self] in
//                    self?.router?.goBack(to: .Home)
//                })
//            }
//            TMWLoaderView.shareInstance().hideLoader()
//        }
//    }
// }
//
// extension ChangePasswordViewController: UITextFieldDelegate {
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        if textField == currentPasswordTextField {
//            newPasswordTextField.becomeFirstResponder()
//        }
//        else if textField == newPasswordTextField {
//            confirmNewPasswordTextField.becomeFirstResponder()
//        }
//        else {
//            confirmNewPasswordTextField.resignFirstResponder()
//            didTapSubmitButton(submitButton)
//        }
//
//        return true
//    }
// }
