//
//  SettingsViewController.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 28/11/2019.
//  Copyright © 2019 JadePayments. All rights reserved.
//

import UIKit

class SettingsViewController: BaseScreenViewController {
    @IBOutlet var containerView: UIView!
    @IBOutlet var sdkVersionLabel: UILabel!
    @IBOutlet var clientCustomerIdLabel: UILabel!
    var router: RouterContract!
    var settingsTable: SettingsTableViewController!

    override func viewDidLoad() {
        super.viewDidLoad()

        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }
        addDefaultTitleViewLogo()

        addChildViewController(child: settingsTable!, toView: containerView)

        sdkVersionLabel.font = UIFont.with(size: .LargeMedium, weight: .normal)
        clientCustomerIdLabel.font = sdkVersionLabel.font

        if let sdkVersion = Bundle(for: SettingsViewController.self).infoDictionary?["CFBundleShortVersionString"] as? String {
            sdkVersionLabel.text = "Version: \(sdkVersion)"
        }

        let brandName = router.db.clientConfig()?.configuration.appName ?? DevUtil.brandName()

        clientCustomerIdLabel.text = "\(brandName) Pay ID: \(router.ssoLogin.customerId)"
    }

    override func viewWillAppear(_ animated: Bool) {
        showBackButton()
        navigationController?.setNavigationBarHidden(false, animated: true)
    }

    override func viewDidAppear(_ animated: Bool) {
        Analytics.reportShowSettingsEvent()
        showBackButton()
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
}
