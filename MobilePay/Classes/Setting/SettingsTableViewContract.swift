//
//  SettingsTableViewContract.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 01/12/2019.
//  Copyright © 2019 JadePayments. All rights reserved.
//

import Foundation

protocol SettingsTableViewContract: AnyObject {
    func showChangeAuthentication(biometricEnabled: Bool)

    func setup()

    func updateAuthTitle(value: String)
}
