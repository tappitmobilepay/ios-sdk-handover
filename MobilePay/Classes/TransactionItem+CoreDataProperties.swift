//
//  TransactionItem+CoreDataProperties.swift
//
//
//  Created by Marius Kurgonas on 17/07/2020.
//
//

import CoreData
import Foundation

public extension TransactionItem {
    @nonobjc class func fetchRequest() -> NSFetchRequest<TransactionItem> {
        return NSFetchRequest<TransactionItem>(entityName: "TransactionItem")
    }

    @NSManaged var name: String?
    @NSManaged var unitPrice: Double
    @NSManaged var quantity: String?
    @NSManaged var sku: String?
    @NSManaged var type: String?
    @NSManaged var amount: Double
}
