//
//  CardsTableViewPresenter.swift
//  MobilePay-PAYG
//
//  Created by Marius Kurgonas on 10/06/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation
import UIKit

class CardsTableViewPresenter: CardsTableViewPresentingContract {
    weak var view: CardsTableViewContract?
    let router: RouterContract
    let paymentsService: PaymentsWebServiceContract
    let db: DatabaseManager
    var crediantialStore: CredentialsStoringContract

    init(view: CardsTableViewContract,
         router: RouterContract,
         paymentsService: PaymentsWebServiceContract,
         db: DatabaseManager,
         crediantialStore: CredentialsStoringContract)
    {
        self.view = view
        self.router = router
        self.paymentsService = paymentsService
        self.db = db
        self.crediantialStore = crediantialStore

        NotificationCenter.default.addObserver(self, selector: #selector(reloadData), name: .cardsUpdated, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadData), name: .cardDeteled, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadData), name: .cardMadeDefault, object: nil)
    }

    @objc
    func reloadData() {
        DispatchQueue.main.async { [weak self] in
            guard let allCards = self?.db.allCreditCards() else {
                return
            }
            self?.view?.updateWith(cards: allCards)
        }
    }

    func viewDidAppear() {
        Analytics.reportMyPaymentMethodsNumberOfCardsEvent()
    }

    func viewDidLoad() {
        reloadData()
        view?.showLoading()
        refreshData()

        view?.setup()
    }

    func viewWillAppear() {
        refreshData()
    }

    func didTapAddNewCard() {
        router.presentAddCard()
    }

    func refreshData() {
        guard TMWCommonUtil.isInternetAvailable() else {
            TMWCommonUtil.showInternetUnavailableAlert()
            view?.hideLoading()
            return
        }

        paymentsService.requestAllPaymentMethods(completion: { [weak self] cards, error in
            if let e = error {
                showAlert(message: e.localizedDescription)
            }
            else if let c = cards {
                self?.db.saveCards(cards: c)
                self?.view?.updateWith(cards: c)
            }
            else {
                assert(false)
            }

            self?.view?.hideLoading()
        })
    }

    func didSelectCardAtIndex(index: Int) {
        router.presentViewCardDetails(card: db.allCreditCards()[index])
    }
}

struct ApplePayButtonAppearance {
    var backgroundColor: UIColor = .black
    var foregroundColor: UIColor = .white
    var boarderColor: UIColor = .black
    var title: String = "Enable -"
}
