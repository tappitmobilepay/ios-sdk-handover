//
//  File.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 06/08/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation

public enum MobilePayEnvironment: String, CaseIterable { case
    Sandbox,
    Test,
    Demo,
    Staging,
    Production

    internal func baseEnvironment() -> ApiEnvironment {
        switch self {
        case .Sandbox:
            return .sandbox
        case .Test:
            return .test
        case .Demo:
            return .demo
        case .Staging:
            return .staging
        case .Production:
            return .production
        }
    }

    internal func basePaymentsEnvironment() -> ApiEnvironment {
        switch self {
        case .Sandbox:
            return .sandboxPayments
        case .Test:
            return .testPayments
        case .Demo:
            return .demoPayments
        case .Staging:
            return .stagingPayments
        case .Production:
            return .productionPayments
        }
    }

    internal func baseGiftcardsEnvironment() -> ApiEnvironment {
        switch self {
        case .Sandbox:
            return .sandboxGiftcards
        case .Test:
            return .testGiftcards
        case .Demo:
            return .demoGiftcards
        case .Staging:
            return .stagingGiftcards
        case .Production:
            return .productionGiftcards
        }
    }

    internal func baseCampaignsEnvironment() -> ApiEnvironment {
        switch self {
        case .Sandbox:
            return .sandboxCampaigns
        case .Test:
            return .testCampaigns
        case .Demo:
            return .demoCampaigns
        case .Staging:
            return .stagingCampaigns
        case .Production:
            return .productionCampaigns
        }
    }
}

extension IntegrationData: Codable {}

/// Wrapper for passing an integration id into the SDK like FanId
public struct IntegrationData {
    /// Type of the integration that this id is for e.g. values representing FanId, Ticketmaster, Tickets.com etc. Refer to the documentation for the full list of possible values.
    public let integrationType: Int

    /// Is value being passed is an actual id of the service or a token. Refer to the documentation for the full list of possible values.
    public let valueType: Int

    /// Actual value of the data being passed represented as a string
    public let value: String

    /// Indicator if the value of integration is unique globally or can change
    public let isGlobal: Bool

    /// Method to initialize login structure used for SSO
    /// - Parameter integrationType: Type of the integration that this id is for e.g. values representing FanId, Ticketmaster, Tickets.com etc. Refer to the documentation for the full list of possible values.
    /// - Parameter valueType: Is value being passed is an actual id of the service or a token. Refer to the documentation for the full list of possible values.
    /// - Parameter value: Actual value of the data being passed represented as a string
    /// - Parameter isGlobal: Indicator if the value of integration is unique globally or can change
    public init(integrationType: Int,
                valueType: Int,
                value: String,
                isGlobal: Bool)
    {
        self.integrationType = integrationType
        self.value = value
        self.valueType = valueType
        self.isGlobal = isGlobal
    }
}

/// Structure used to make single sign
public struct SSOLogin {
    /// Customer id withing the third party scope
    public let customerId: String

    /// List of different types of ids from the third party integrations such as Ticketmaster. Refer to the documentation for the full integration guide.
    public var integrationData: [IntegrationData]

    /// Client integration key
    internal var integrationKey: String = ""

    /// Customer email address if required
    public let email: String

    /// Customer email address if required
    public let environment: MobilePayEnvironment

    /// Method to initialize login structure used for SSO
    /// - Parameter customerId: customer id from the client side later used to make a connection between client and Tappit data
    /// - Parameter integrationData: list of third party data with the references to a various services that SDK needs to tie in with
    /// - Parameter integrationKey: Client integration key
    /// - Parameter email: Customer email
    /// - Parameter environment: environment to target. Default is production
    public init(customerId: String,
                integrationKey: String,
                email: String,
                environment: MobilePayEnvironment = .Production,
                integrationData: [IntegrationData]? = nil)
    {
        self.customerId = customerId
        self.email = email
        self.integrationData = integrationData ?? []
        self.environment = environment
        self.integrationKey = integrationKey
    }
}
