////
////  CashoutViewController.swift
////  MobilePay
////
////  Created by Marius Kurgonas on 19/12/2019.
////  Copyright © 2019 JadePayments. All rights reserved.
////
//
// import UIKit
//
// class CashOutViewController: BaseScreenViewController {
//    var balanceController: BalanceViewController?
//
//    @IBOutlet var confirmButton: UIButton!
//    @IBOutlet var infoTitleLabel: UILabel!
//    @IBOutlet var currentBalanceContainerView: UIView!
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
////        currentBalanceContainerView.addDefaultColorBorder()
//
//        infoTitleLabel.font = UIFont.with(size: .LargeMedium, weight: .bold)
//        infoTitleLabel.textColor = MobilePayCore.shared.config.secondaryTextColor
//
//        addDefaultTitleViewLogo()
//
//        addChildViewController(child: balanceController!, toView: currentBalanceContainerView)
//
//        confirmButton.titleLabel?.font = UIFont.with(size: .Large, weight: .bold)
//        confirmButton.backgroundColor = MobilePayCore.shared.config.secondaryColor
//        confirmButton.setTitleColor(MobilePayCore.shared.config.primaryTextColor, for: UIControl.State.normal)
//        confirmButton.setTitle("CONFIRM", for: .normal)
//        confirmButton.addDefaultRoundCorners()
//    }
//
//    @IBAction func didTapConfirmButton(_ sender: Any) {
//        showAlert(message: "No payment gateway found")
//    }
// }
