//
//  CreditCardViewController.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 06/05/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation
import UIKit

enum CreditCardTypes: String { case
    visa,
    mastercard,
    americanexpress,
    discover
}

class CreditCardViewController: UIViewController {
    @IBOutlet private var chipImageView: UIImageView!
    @IBOutlet private var bankImageView: UIImageView!
    @IBOutlet private var defaultLabel: UILabel!
    @IBOutlet private var nameLabel: UILabel!
    @IBOutlet private var cardLabel: UILabel!
    @IBOutlet private var expiryLabel: UILabel!
    @IBOutlet var cardTypeLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }
        chipImageView.image = DevUtil.imageSkin(name: "tmw_card_chip")
        cardTypeLabel.isHidden = true
        view.addPrimaryColorBorder()
    }

    func setup(card: Card, isDefault: Bool) {
        defaultLabel.isHidden = !isDefault
        defaultLabel.font = UIFont.with(size: .Large, weight: .bold)
        nameLabel.font = UIFont.with(size: .Large, weight: .normal)
        expiryLabel.font = UIFont.with(size: .Large, weight: .normal)
        cardLabel.font = UIFont.with(size: .BigRegular, weight: .normal)

        setupCardImage(type: card.cardType)

        setupCardNumber(card: card)
    }

    func setupCardImage(type: String) {
        switch type.lowercased() {
        case CreditCardTypes.visa.rawValue:
            bankImageView.image = DevUtil.imageSkin(name: "tmw_visa")
        case CreditCardTypes.mastercard.rawValue:
            bankImageView.image = DevUtil.imageSkin(name: "tmw_mastercard")
        case CreditCardTypes.americanexpress.rawValue:
            bankImageView.image = DevUtil.imageSkin(name: "tmw_american_express")
        default:
            bankImageView.image = DevUtil.imageSkin(name: "tmw_discover")
        }
    }

    func setupCardNumber(card: Card) {
        let index = card.cardNumber.index(card.cardNumber.endIndex, offsetBy: -4)
        let substring = card.cardNumber[index...]

        if CreditCardTypes.americanexpress.rawValue == card.cardType {
            cardLabel.text = "• • • •     • • • • • •     • " + substring
        }
        else {
            cardLabel.text = "• • • •     • • • •     • • • •     " + substring
        }
    }
}
