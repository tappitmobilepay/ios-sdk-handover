//
//  MobilePayCore.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 16/01/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation

/// Mobile Pay configuration point
class MobilePayCore {
    // When Tappit app integrates this SDK do something different overriding global values
    var devModeEnabled = false
    // Instead of default assets use this prefix for everything to use other skins, fonts etc
    var devModeSkinName = ""

    /// Shared global instance
    static let shared = MobilePayCore()
}
