//
//  TipsPresentingContract.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 02/11/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation

protocol TipsPresentingContract {
    func viewDidLoad()

    func viewWillAppear()

    func viewDidAppear()

    func didTapContinue()

    func didTapPercentageButton(index: Int)

    func didUpdateCustomTip(value: String?)

    func skipTips()
}
