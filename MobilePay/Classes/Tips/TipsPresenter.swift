//
//  TipsPresenter.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 02/11/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation

enum TipsType: Int { case
    NoTip = 0,
    Monetary = 1,
    Percentage = 2
}

class TipsPresenter: TipsPresentingContract {
    weak var view: TipsViewingContract?
    let router: RouterContract

    var tipsAmount: Decimal?
    var isPercentage = true
    var paymentData: PaymentData
    let paymentFlowManager: PaymentFlowManagementContract

    init(view: TipsViewingContract,
         router: RouterContract,
         paymentData: PaymentData,
         paymentFlowManager: PaymentFlowManagementContract)
    {
        self.view = view
        self.router = router
        self.paymentData = paymentData
        self.paymentFlowManager = paymentFlowManager
    }

    func viewDidLoad() {
        let percentages = router.db.clientConfig()?.configuration.settings.tips.percentages ?? []
        view?.setup(percentages: percentages)
    }

    func didTapPercentageButton(index: Int) {
        guard let percentages = router.db.clientConfig()?.configuration.settings.tips.percentages else {
            return
        }
        view?.clearCustomTip()
        view?.untintPercentageButtons()
        view?.tintPercentageButton(index: index)
        tipsAmount = Decimal(percentages[index])
        isPercentage = true
    }

    func didTapContinue() {
        Analytics.reportContinueOnTipsEvent()

        if let tipsAmount = tipsAmount {
            let adjustedTips = isPercentage ? tipsAmount : tipsAmount * 100
            let intTipsAmount = (adjustedTips as NSDecimalNumber).intValue
            let percentageIndicator = isPercentage ? TipsType.Percentage.rawValue : TipsType.Monetary.rawValue
            let finalTipsArray = [percentageIndicator, intTipsAmount]
            paymentData.tips = finalTipsArray
            moveNext()
        }
        else {
            showAlert(message: "Please select a valid tip amount before continuing.")
        }
    }

    func viewDidAppear() {
        Analytics.reportShowAddTipScreenEvent()
        view?.showNavBarWithBackButton()
    }

    func viewWillAppear() {
        view?.showNavBarWithBackButton()
    }

    func didUpdateCustomTip(value: String?) {
        var tip: Decimal?
        if let value = value, let _ = Double(value) {
            tip = Decimal(string: value)
        }

        view?.untintPercentageButtons()
        tipsAmount = tip
        isPercentage = false
    }

    func skipTips() {
        Analytics.reportNoThanksOnTipsEvent()
        moveNext()
    }

    func moveNext() {
        do {
            try paymentFlowManager.moveToNextState(paymentData: paymentData)
        }
        catch {
            assert(false)
        }
    }
}
