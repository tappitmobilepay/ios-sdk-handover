//
//  TipsViewController.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 02/11/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import UIKit

class TipsViewController: BaseScreenViewController, TipsViewingContract {
    var presenter: TipsPresentingContract?

    @IBOutlet var percentagesContainerView: UIStackView!
    @IBOutlet var continueButton: UIButton!
    @IBOutlet var tipsTextField: UITextField!
    @IBOutlet var skipButton: UIButton!
    @IBOutlet var topInfoLabel: UILabel!

    var percentButtons = [UIButton]()
    var percentagesForButtons = [Int]()

    override func viewDidLoad() {
        super.viewDidLoad()

        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }

        guard let presenter = presenter else {
            fatalError()
        }

        presenter.viewDidLoad()
    }

    func setup(percentages: [Int]) {
        addDefaultTitleViewLogo()

        percentButtons.removeAll()

        var tag = 0
        for percentage in percentages {
            let button = UIButton()
            button.tag = tag
            button.setTitle("\(percentage)%", for: .normal)
            button.layer.borderWidth = 1
            button.layer.borderColor = DevUtil.color(name: Colors.tmw_custom_unselected_tip_button_border.rawValue).cgColor
            button.setTitleColor(DevUtil.color(name: Colors.tmw_custom_unselected_tip_button_text.rawValue), for: .normal)
            button.backgroundColor = DevUtil.color(name: Colors.tmw_custom_unselected_tip_button_background.rawValue)
            button.titleLabel?.font = UIFont.with(size: .Large, weight: .customTipButton)
            button.addDefaultRoundCorners()
            percentButtons.append(button)
            percentagesContainerView.addArrangedSubview(button)
            button.addTarget(self, action: #selector(didTapPercentButton(_:)), for: .touchUpInside)
            tag += 1
        }

        continueButton.setTitle("CONTINUE", for: .normal)
        skipButton.setTitle("NO THANKS", for: .normal)

        skipButton.layer.borderWidth = 1
        skipButton.layer.borderColor = DevUtil.color(name: Colors.tmw_secondary_button_border.rawValue).cgColor
        skipButton.setTitleColor(DevUtil.color(name: Colors.tmw_secondary_button_text.rawValue), for: .normal)
        skipButton.backgroundColor = DevUtil.color(name: Colors.tmw_secondary_button_background.rawValue)
        skipButton.addDefaultRoundCorners()
        skipButton.titleLabel?.font = UIFont.with(size: .LargeMedium, weight: .secondaryButton)

        continueButton.layer.borderWidth = 1
        continueButton.layer.borderColor = DevUtil.color(name: Colors.tmw_primary_button_border.rawValue).cgColor
        continueButton.setTitleColor(DevUtil.color(name: Colors.tmw_primary_button_text.rawValue), for: .normal)
        continueButton.backgroundColor = DevUtil.color(name: Colors.tmw_primary_button_background.rawValue)
        continueButton.addDefaultRoundCorners()
        continueButton.titleLabel?.font = UIFont.with(size: .LargeMedium, weight: .primaryButton)

        tipsTextField.addDefaultBorder(width: 1)
        tipsTextField.addDoneCancelToolbar()

        topInfoLabel.font = UIFont.with(size: .Large, weight: .normal)

        tipsTextField.font = UIFont.with(size: .Large, weight: .normal)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        guard let presenter = presenter else {
            fatalError()
        }

        presenter.viewWillAppear()
    }

    func showNavBarWithBackButton() {
        showBackButton()
        navigationController?.setNavigationBarHidden(false, animated: true)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        guard let presenter = presenter else {
            fatalError()
        }

        presenter.viewDidAppear()
    }

    @IBAction func didTapPercentButton(_ sender: UIButton) {
        guard let presenter = presenter
        else {
            fatalError()
        }

        presenter.didTapPercentageButton(index: sender.tag)
    }

    @IBAction func didTapContinue(_ sender: UIButton) {
        guard let presenter = presenter else {
            fatalError()
        }

        presenter.didTapContinue()
    }

    func untintPercentageButtons() {
        percentButtons.forEach { button in
            button.layer.borderColor = DevUtil.color(name: Colors.tmw_custom_unselected_tip_button_border.rawValue).cgColor
            button.setTitleColor(DevUtil.color(name: Colors.tmw_custom_unselected_tip_button_text.rawValue), for: .normal)
            button.backgroundColor = DevUtil.color(name: Colors.tmw_custom_unselected_tip_button_background.rawValue)
        }
    }

    func tintPercentageButton(index: Int) {
        guard percentButtons.count > index else {
            return
        }
        let button = percentButtons[index]
        button.layer.borderColor = DevUtil.color(name: Colors.tmw_custom_selected_tip_button_border.rawValue).cgColor
        button.setTitleColor(DevUtil.color(name: Colors.tmw_custom_selected_tip_button_text.rawValue), for: .normal)
        button.backgroundColor = DevUtil.color(name: Colors.tmw_custom_selected_tip_button_background.rawValue)
    }

    @IBAction func customTipDidChange(_ sender: Any) {
        guard let presenter = presenter else {
            fatalError()
        }

        presenter.didUpdateCustomTip(value: tipsTextField.text)
    }

    @IBAction func didPressSkipButton(_ sender: UIButton) {
        guard let presenter = presenter else {
            fatalError()
        }

        presenter.skipTips()
    }

    func clearCustomTip() {
        tipsTextField.text = ""
        tipsTextField.resignFirstResponder()
    }
}
