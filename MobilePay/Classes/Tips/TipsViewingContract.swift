//
//  TipsViewingContract.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 02/11/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation

protocol TipsViewingContract: AnyObject {
    var presenter: TipsPresentingContract? { get set }

    func setup(percentages: [Int])

    func showNavBarWithBackButton()

    func untintPercentageButtons()

    func clearCustomTip()

    func tintPercentageButton(index: Int)
}
