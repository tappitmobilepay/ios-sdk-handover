//
//  CredentialsStore.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 07/07/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation

class CredentialsStore: CredentialsStoringContract {
    let login: SSOLogin

    func wipe() {
        let dictionary = TMWCommonUtil.userDefaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            TMWCommonUtil.userDefaults.removeObject(forKey: key)
        }
    }

    func softWipe() {
        let pld = pinLockdownDate
        let wpr = wrongPinsInARow
        let p = pin
        let am = authenticationMethod
        let src = showRedeemedCampaigns
        let ctz = clientTimezone
        let sra = shouldResetAuthentication
        let custStat = customerStats

        wipe()

        if let custStat = custStat {
            customerStats = custStat
        }

        if let sra = sra {
            shouldResetAuthentication = sra
        }

        if let showRC = src {
            showRedeemedCampaigns = showRC
        }

        if let clientTZ = ctz {
            clientTimezone = clientTZ
        }

        if let pinLock = pld {
            pinLockdownDate = pinLock
        }

        if let wrongAttempts = wpr {
            wrongPinsInARow = wrongAttempts
        }

        if let pinValue = p {
            pin = pinValue
        }

        if let authMethod = am {
            authenticationMethod = authMethod
        }
    }

    var wrongPinsInARow: Int? {
        set {
            guard let value = newValue, let data = "\(value)".data(using: .utf8),
                  let encrypted = try? cipher.encrypt(data)
            else {
                fatalError()
            }

            TMWCommonUtil.userDefaults.set(encrypted, forKey: Constants.mobilePayCustomerWrongPinsInRowKey)
            TMWCommonUtil.userDefaults.synchronize()
        }
        get {
            guard let encrypted = TMWCommonUtil.userDefaults.data(forKey: Constants.mobilePayCustomerWrongPinsInRowKey),
                  let decrypted = try? cipher.decrypt(encrypted),
                  let value = String(data: decrypted, encoding: .utf8),
                  let intValue = Int(value)
            else {
                return nil
            }

            return intValue
        }
    }

    var authenticationMethod: AuthenticationMethod? {
        set {
            if newValue == nil {
                TMWCommonUtil.userDefaults.removeObject(forKey: Constants.kAppAuthorizationMethodKey)
                TMWCommonUtil.userDefaults.synchronize()
                return
            }

            guard let data = newValue?.rawValue.data(using: .utf8),
                  let encrypted = try? cipher.encrypt(data)
            else {
                fatalError()
            }
            TMWCommonUtil.userDefaults.set(encrypted, forKey: Constants.kAppAuthorizationMethodKey)
            TMWCommonUtil.userDefaults.synchronize()
        }
        get {
            guard let encrypted = TMWCommonUtil.userDefaults.data(forKey: Constants.kAppAuthorizationMethodKey),
                  let decrypted = try? cipher.decrypt(encrypted),
                  let value = String(data: decrypted, encoding: .utf8),
                  let finalResult = AuthenticationMethod(rawValue: value)
            else {
                return nil
            }

            return finalResult
        }
    }

    var pinLockdownDate: Date? {
        set {
            if newValue == nil {
                TMWCommonUtil.userDefaults.removeObject(forKey: Constants.mobilePayCustomerLastLockdownDateKey)
                TMWCommonUtil.userDefaults.synchronize()
                return
            }

            guard let value = newValue, let data = "\(value.timeIntervalSince1970)".data(using: .utf8),
                  let encrypted = try? cipher.encrypt(data)
            else {
                fatalError()
            }
            TMWCommonUtil.userDefaults.set(encrypted, forKey: Constants.mobilePayCustomerLastLockdownDateKey)
            TMWCommonUtil.userDefaults.synchronize()
        }
        get {
            guard let encrypted = TMWCommonUtil.userDefaults.data(forKey: Constants.mobilePayCustomerLastLockdownDateKey),
                  let decrypted = try? cipher.decrypt(encrypted),
                  let value = String(data: decrypted, encoding: .utf8),
                  let timeInterval = TimeInterval(value)
            else {
                return nil
            }

            return Date(timeIntervalSince1970: timeInterval)
        }
    }

    var clientTimezone: String? {
        set {
            guard let data = newValue?.data(using: .utf8),
                  let encrypted = try? cipher.encrypt(data)
            else {
                fatalError()
            }
            TMWCommonUtil.userDefaults.set(encrypted, forKey: Constants.mobilePayCustomerTimeZoneKey)
            TMWCommonUtil.userDefaults.synchronize()
        }
        get {
            guard let encrypted = TMWCommonUtil.userDefaults.data(forKey: Constants.mobilePayCustomerTimeZoneKey),
                  let decrypted = try? cipher.decrypt(encrypted),
                  let value = String(data: decrypted, encoding: .utf8)
            else {
                return nil
            }

            return value
        }
    }

    var bearerToken: String? {
        set {
            guard let data = newValue?.data(using: .utf8),
                  let encrypted = try? cipher.encrypt(data)
            else {
                fatalError()
            }
            TMWCommonUtil.userDefaults.set(encrypted, forKey: Constants.mobilePayCustomerTokenKey)
            TMWCommonUtil.userDefaults.synchronize()
        }
        get {
            guard let encrypted = TMWCommonUtil.userDefaults.data(forKey: Constants.mobilePayCustomerTokenKey),
                  let decrypted = try? cipher.decrypt(encrypted),
                  let value = String(data: decrypted, encoding: .utf8)
            else {
                return nil
            }

            return value
        }
    }

    var apiKey: String? {
        set {
            guard let data = newValue?.data(using: .utf8),
                  let encrypted = try? cipher.encrypt(data)
            else {
                fatalError()
            }
            TMWCommonUtil.userDefaults.set(encrypted, forKey: Constants.kClientApiKeyKey)
            TMWCommonUtil.userDefaults.synchronize()
        }
        get {
            guard let encrypted = TMWCommonUtil.userDefaults.data(forKey: Constants.kClientApiKeyKey),
                  let decrypted = try? cipher.decrypt(encrypted),
                  let value = String(data: decrypted, encoding: .utf8)
            else {
                return nil
            }

            return value
        }
    }

    var loggedInWithEnvironment: String? {
        set {
            if newValue == nil {
                TMWCommonUtil.userDefaults.removeObject(forKey: Constants.mobilePayLoggedInWithEnvironmentKey)
                TMWCommonUtil.userDefaults.synchronize()
                return
            }

            guard let data = newValue?.data(using: .utf8),
                  let encrypted = try? cipher.encrypt(data)
            else {
                fatalError()
            }
            TMWCommonUtil.userDefaults.set(encrypted, forKey: Constants.mobilePayLoggedInWithEnvironmentKey)
            TMWCommonUtil.userDefaults.synchronize()
        }
        get {
            guard let encrypted = TMWCommonUtil.userDefaults.data(forKey: Constants.mobilePayLoggedInWithEnvironmentKey),
                  let decrypted = try? cipher.decrypt(encrypted),
                  let value = String(data: decrypted, encoding: .utf8)
            else {
                return nil
            }

            return value
        }
    }

    var loggedInWithIntegrationKey: String? {
        set {
            if newValue == nil {
                TMWCommonUtil.userDefaults.removeObject(forKey: Constants.mobilePayLoggedInWithIntegrationKey)
                TMWCommonUtil.userDefaults.synchronize()
                return
            }

            guard let data = newValue?.data(using: .utf8),
                  let encrypted = try? cipher.encrypt(data)
            else {
                fatalError()
            }
            TMWCommonUtil.userDefaults.set(encrypted, forKey: Constants.mobilePayLoggedInWithIntegrationKey)
            TMWCommonUtil.userDefaults.synchronize()
        }
        get {
            guard let encrypted = TMWCommonUtil.userDefaults.data(forKey: Constants.mobilePayLoggedInWithIntegrationKey),
                  let decrypted = try? cipher.decrypt(encrypted),
                  let value = String(data: decrypted, encoding: .utf8)
            else {
                return nil
            }

            return value
        }
    }

    var pin: String? {
        set {
            if newValue == nil {
                TMWCommonUtil.userDefaults.removeObject(forKey: Constants.mobilePayCustomerPinValueKey)
                TMWCommonUtil.userDefaults.synchronize()
                return
            }

            guard let data = newValue?.data(using: .utf8),
                  let encrypted = try? cipher.encrypt(data)
            else {
                fatalError()
            }
            TMWCommonUtil.userDefaults.set(encrypted, forKey: Constants.mobilePayCustomerPinValueKey)
            TMWCommonUtil.userDefaults.synchronize()
        }
        get {
            guard let encrypted = TMWCommonUtil.userDefaults.data(forKey: Constants.mobilePayCustomerPinValueKey),
                  let decrypted = try? cipher.decrypt(encrypted),
                  let value = String(data: decrypted, encoding: .utf8)
            else {
                return nil
            }

            return value
        }
    }

    var userId: Int? {
        set {
            guard let value = newValue, let data = "\(value)".data(using: .utf8),
                  let encrypted = try? cipher.encrypt(data)
            else {
                fatalError()
            }
            TMWCommonUtil.userDefaults.set(encrypted, forKey: Constants.mobilePayCustomerUserIdKey)
            TMWCommonUtil.userDefaults.synchronize()
        }
        get {
            guard let encrypted = TMWCommonUtil.userDefaults.data(forKey: Constants.mobilePayCustomerUserIdKey),
                  let decrypted = try? cipher.decrypt(encrypted),
                  let value = String(data: decrypted, encoding: .utf8),
                  let intValue = Int(value)
            else {
                return nil
            }

            return intValue
        }
    }

    var showRedeemedCampaigns: Bool? {
        set {
            guard let value = newValue, let data = try? JSONEncoder().encode(value),
                  let encrypted = try? cipher.encrypt(data)
            else {
                fatalError()
            }
            TMWCommonUtil.userDefaults.set(encrypted, forKey: Constants.kShowRedeemedCampaignsKey)
            TMWCommonUtil.userDefaults.synchronize()
        }
        get {
            guard let encrypted = TMWCommonUtil.userDefaults.data(forKey: Constants.kShowRedeemedCampaignsKey),
                  let decrypted = try? cipher.decrypt(encrypted),
                  let value = try? JSONDecoder().decode(Bool.self, from: decrypted)
            else {
                return nil
            }

            return value
        }
    }

    let cipher: AES256Crypter

    init(login: SSOLogin) {
        guard let key = Security.LOCAL_ENCRYPTION_KEY.data(using: .utf8),
              let iv = Security.LOCAL_IV_VECTOR.data(using: .utf8),
              let cipher = try? AES256Crypter(key: key, iv: iv)
        else {
            fatalError()
        }
        self.cipher = cipher
        self.login = login
    }

    var shouldResetAuthentication: Bool? {
        set {
            guard let value = newValue, let data = try? JSONEncoder().encode(value),
                  let encrypted = try? cipher.encrypt(data)
            else {
                fatalError()
            }
            TMWCommonUtil.userDefaults.set(encrypted, forKey: Constants.kShouldResetAuthenticationKey)
            TMWCommonUtil.userDefaults.synchronize()
        }
        get {
            guard let encrypted = TMWCommonUtil.userDefaults.data(forKey: Constants.kShouldResetAuthenticationKey),
                  let decrypted = try? cipher.decrypt(encrypted),
                  let value = try? JSONDecoder().decode(Bool.self, from: decrypted)
            else {
                return nil
            }

            return value
        }
    }

    var customerStats: CustomerStats? {
        set {
            guard
                let data = try? JSONEncoder().encode(newValue),
                let encrypted = try? cipher.encrypt(data)
            else {
                fatalError()
            }
            TMWCommonUtil.userDefaults.set(encrypted, forKey: login.customerId + Constants.kCustomerStatsKey)
            TMWCommonUtil.userDefaults.synchronize()
        }
        get {
            guard let encrypted = TMWCommonUtil.userDefaults.data(forKey: login.customerId + Constants.kCustomerStatsKey),
                  let decrypted = try? cipher.decrypt(encrypted),
                  let value = try? JSONDecoder().decode(CustomerStats.self, from: decrypted)
            else {
                return nil
            }

            return value
        }
    }
}
