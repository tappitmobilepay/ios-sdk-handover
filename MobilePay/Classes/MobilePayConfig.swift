//
//  MobilePayConfig.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 16/01/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation
import UIKit

// internal struct MobilePayConfig {
//    internal let backIndicatorColor: UIColor
//    internal let biometricColor: UIColor
//    internal let navigationBackgroundColor: UIColor
//    internal let pageHeaderTextColor: UIColor
//    internal let onboardingBackgroundColor: UIColor
//    internal let onboardingBackgroundTextColor: UIColor
//    internal let onboardingButtonBackgroundColor: UIColor
//    internal let onboardingButtonTextColor: UIColor
//    internal let payButtonBackgroundColor: UIColor
//    internal let payButtonTextColor: UIColor
//    internal let primaryBackgroundColor: UIColor
//    internal let primaryBackgroundTextColor: UIColor
//    internal let secondaryBackgroundColor: UIColor
//    internal let secondaryBackgroundTextColor: UIColor
// }

// extension MobilePayConfig {
//    init(config: Config) {
//        backIndicatorColor = UIColor(hex: config.theme.colors.tmw_back_indicator)
//        biometricColor = UIColor(hex: config.theme.colors.tmw_biometric)
//        navigationBackgroundColor = UIColor(hex: config.theme.colors.tmw_navigation_background)
//        pageHeaderTextColor = UIColor(hex: config.theme.colors.tmw_page_header_text)
//        onboardingBackgroundColor = UIColor(hex: config.theme.colors.tmw_onboarding_background)
//        onboardingBackgroundTextColor = UIColor(hex: config.theme.colors.tmw_onboarding_background_text)
//        onboardingButtonBackgroundColor = UIColor(hex: config.theme.colors.tmw_onboarding_button_background)
//        onboardingButtonTextColor = UIColor(hex: config.theme.colors.tmw_onboarding_button_text)
//        payButtonBackgroundColor = UIColor(hex: config.theme.colors.tmw_pay_button_background)
//        payButtonTextColor = UIColor(hex: config.theme.colors.tmw_pay_button_text)
//        primaryBackgroundColor = UIColor(hex: config.theme.colors.tmw_primary_background)
//        primaryBackgroundTextColor = UIColor(hex: config.theme.colors.tmw_primary_background_text)
//        secondaryBackgroundColor = UIColor(hex: config.theme.colors.tmw_secondary_background)
//        secondaryBackgroundTextColor = UIColor(hex: config.theme.colors.tmw_secondary_background_text)
//    }
// }
