//
//  ManagedCreditCard+Extensions.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 10/07/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import CoreData
import Foundation

extension ManagedCreditCard {
    static func from(card: Card, context: NSManagedObjectContext) -> ManagedCreditCard {
        let managedCard = ManagedCreditCard(context: context)
        managedCard.id = card.id
        managedCard.cardNumber = card.cardNumber
        managedCard.cardType = card.cardType
        managedCard.isDefault = card.isDefault
        return managedCard
    }
}
