//
//  HelpViewController.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 02/12/2019.
//  Copyright © 2019 JadePayments. All rights reserved.
//

import UIKit
import WebKit

class HelpViewController: BaseScreenViewController {
    @IBOutlet var webView: WKWebView!

    var db: DatabaseManager!

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: "HelpViewController", bundle: Bundle(for: HelpViewController.self))
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }

        var devUrl = DevUtil.helpUrl()
        let config = db.clientConfig()
        if let remoteHelpUrlString = config?.configuration.settings.help,
           let remoteHelpUrl = URL(string: remoteHelpUrlString)
        {
            devUrl = remoteHelpUrl
        }

        let request = URLRequest(url: devUrl)
        webView.load(request)

        addDefaultTitleViewLogo()

        if !TMWCommonUtil.isInternetAvailable() {
            TMWCommonUtil.showInternetUnavailableAlert()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        showBackButton()
        navigationController?.setNavigationBarHidden(false, animated: true)
    }

    override func viewDidAppear(_ animated: Bool) {
        Analytics.reportShowHelpScreenEvent()
        showBackButton()
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
}
