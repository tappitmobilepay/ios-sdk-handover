//
//  LoginPresenterContract.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 03/01/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation

protocol LoginPresenterContract {
    func viewDidLoad()

    func login()

    func register()

    func forgotPassword()

    func logout()
}
