////
////  LoginPresenter.swift
////  MobilePay
////
////  Created by Marius Kurgonas on 03/01/2020.
////  Copyright © 2020 JadePayments. All rights reserved.
////
//
// import Foundation
//
// class LoginPresenter: LoginPresenterContract {
//    weak var view: LoginViewContract?
//    let webService: LoginWebServiceContract
//    let router: RouterContract
//    let loginManager: LoginManagingContract
//    var credStore: CredentialsStoringContract
//    var paymentProcessor: PaymentProcessor?
//
//    init(view: LoginViewContract,
//         router: RouterContract,
//         webService: LoginWebServiceContract,
//         credStore: CredentialsStoringContract,
//         loginManager: LoginManagingContract) {
//        self.view = view
//        self.webService = webService
//        self.router = router
//        self.credStore = credStore
//        self.loginManager = loginManager
//    }
//
//    func viewDidLoad() {
//        TMWCommonUtil.startListeningForInternetChanges()
//        view?.setup()
//    }
//
//    func register() {
//        router.presentRegistration()
//    }
//
//    func forgotPassword() {
//        router.presentForgotPassword()
//    }
//
//    func login() {
//        guard TMWCommonUtil.isInternetAvailable() else {
//            view?.showInternetUnabailable()
//            return
//        }
//
//        guard let username = view?.username,
//            !username.isEmpty,
//            let password = view?.password,
//            !password.isEmpty else {
//            view?.showFillFieldsMessage()
//            return
//        }
//
//        view?.showLoading()
//        webService.cancel()
//        webService.login(username: username, password: password.toBase64()) { [weak self] response, error in
//            if let e = error {
//                self?.view?.show(message: e.localizedDescription)
//                self?.view?.hideLoading()
//            }
//            else if let response = response {
//                self?.credStore.bearerToken = response.token
//                self?.credStore.userId = response.hashId
//
//                self?.paymentProcessor?.requestAllPaymentMethods(completion: { cards, error in
//                    if let e = error {
//                        self?.view?.show(message: e.localizedDescription)
//                    }
//                    else if cards != nil {
//                        self?.didLogin()
//                    }
//                    else {
//                        assert(false)
//                    }
//                    self?.view?.hideLoading()
//                })
//            }
//        }
//    }
//
//    func didLogin() {
//        TMWCommonUtil.userDefaults.setValue(true, forKey: kIsAlreadyLoginKey)
//        TMWCommonUtil.userDefaults.synchronize()
//        router.presentAuthentication(animated: true)
//    }
//
//    func logout() {
//        loginManager.logoutApp()
//    }
// }
