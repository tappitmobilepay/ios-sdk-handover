////
////  LoginViewController.swift
////  MobilePay
////
////  Created by Marius Kurgonas on 03/01/2020.
////  Copyright © 2020 JadePayments. All rights reserved.
////
//
import UIKit
//
func isUITesting() -> Bool {
    return ProcessInfo.processInfo.arguments.contains("UI-TESTING")
}

//
// class LoginViewController: UIViewController, LoginViewContract {
//    @IBOutlet private var signUpButton: UIButton!
//    @IBOutlet private var backgroundImageView: UIImageView!
//    @IBOutlet private var mainLogoImageView: UIImageView!
//    @IBOutlet private var emailLabel: UILabel!
//    @IBOutlet private var passwordLabel: UILabel!
//    @IBOutlet private var emailTextfield: UITextField!
//    @IBOutlet private var passwordTextfield: UITextField!
//    @IBOutlet private var resetPasswordButton: UIButton!
//    @IBOutlet private var loginButton: UIButton!
//
//    var username: String? {
//        return emailTextfield.text
//    }
//
//    var password: String? {
//        return passwordTextfield.text
//    }
//
//    var presenter: LoginPresenterContract?
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        if #available(iOS 13.0, *) {
//            overrideUserInterfaceStyle = .light
//        }
//
//        guard let presenter = presenter else {
//            assert(false)
//            return
//        }
//
//        if isUITesting() {
//            presenter.logout()
//        }
//
//        presenter.viewDidLoad()
//    }
//
//    func showFillFieldsMessage() {
//        showAlert(message: "login_alert_message".localized)
//    }
//
//    func show(message: String) {
//        showAlert(message: message)
//    }
//
//    func showLoading() {
//        TMWLoaderView.shareInstance().showLoader(navigationController!.view)
//    }
//
//    func hideLoading() {
//        TMWLoaderView.shareInstance().hideLoader()
//    }
//
//    func setup() {
//        // TODO: LoginViewController - confirm when we need to ask for permission if we need it at all
//        TMWCommonUtil.registerForRichNotifications()
//
//        mainLogoImageViewDevUtil.imageSkin(name: "tmw_navigation_logo")
//
//        emailTextfield.textColor = colorTextNormal
//        emailTextfield.addDefaultRoundCorners()
//        emailTextfield.delegate = self
//        emailTextfield.font = UIFont.with(size: .Medium, weight: .normal)
//
//        emailLabel.text = "login_username".localized
//        emailLabel.font = UIFont.with(size: .LightNormal1, weight: .bold)
//        emailLabel.textColor = MobilePayCore.shared.config.onboardingPrimaryTextColor
//
//        passwordTextfield.font = UIFont.with(size: .Medium, weight: .normal)
//        passwordTextfield.delegate = self
//        passwordTextfield.textColor = colorTextNormal
//        passwordTextfield.addDefaultRoundCorners()
//
//        passwordLabel.text = "login_password".localized
//        passwordLabel.font = UIFont.with(size: .LightNormal1, weight: .bold)
//        passwordLabel.textColor = MobilePayCore.shared.config.onboardingPrimaryTextColor
//
//        loginButton.setTitle("login_btn_title".localized, for: UIControl.State.normal)
//        loginButton.setTitleColor(MobilePayCore.shared.config.onboardingButtonsTextColor, for: UIControl.State.normal)
//        loginButton.titleLabel?.font = UIFont.with(size: .Large, weight: .bold)
//        loginButton.backgroundColor = MobilePayCore.shared.config.onboardingButtonsColor
//        loginButton.addDefaultRoundCorners()
//
//        signUpButton.setTitleColor(MobilePayCore.shared.config.onboardingButtonsTextColor, for: UIControl.State.normal)
//        signUpButton.titleLabel?.font = UIFont.with(size: .Large, weight: .bold)
//        signUpButton.backgroundColor = MobilePayCore.shared.config.onboardingButtonsColor
//        signUpButton.addDefaultRoundCorners()
//
//        resetPasswordButton.setTitle("Forgot your Password?", for: .normal)
//        resetPasswordButton.setTitleColor(MobilePayCore.shared.config.onboardingPrimaryTextColor, for: UIControl.State.normal)
//        resetPasswordButton.titleLabel?.font = UIFont.with(size: .LightNormal1, weight: .bold)
//
//        backgroundImageView.image = UIImage(named: "tmw_general_background")
//    }
//
//    func showInternetUnabailable() {
//        TMWCommonUtil.showInternetUnavailableAlert()
//    }
//
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//
//        navigationController?.setNavigationBarHidden(true, animated: true)
//    }
//
//    @IBAction private func didTapResetPassword(_ sender: Any) {
//        guard let presenter = presenter else {
//            assert(false)
//            return
//        }
//
//        presenter.forgotPassword()
//    }
//
//    @IBAction private func didTapLoginButton(_ sender: Any) {
//        guard let presenter = presenter else {
//            assert(false)
//            return
//        }
//
//        presenter.login()
//    }
//
//    @IBAction private func didTapCreateAccountButton(_ sender: Any) {
//        guard let presenter = presenter else {
//            assert(false)
//            return
//        }
//
//        presenter.register()
//    }
// }
//
// extension LoginViewController: UITextFieldDelegate {
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        textField.resignFirstResponder()
//        if textField == emailTextfield {
//            passwordTextfield.becomeFirstResponder()
//        }
//        else if textField == passwordTextfield {
//            didTapLoginButton(loginButton)
//        }
//
//        return true
//    }
// }
