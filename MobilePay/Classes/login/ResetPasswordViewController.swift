////
////  ResetPasswordViewController.swift
////  MobilePay
////
////  Created by Marius Kurgonas on 15/01/2020.
////  Copyright © 2020 JadePayments. All rights reserved.
////
//
// import UIKit
//
// class ResetPasswordViewController: BaseScreenViewController {
//    var email: String?
//
//    var webService: LoginWebServiceContract?
//
//    @IBOutlet var infoLabel: UILabel!
//    @IBOutlet var otpTextField: UITextField!
//    @IBOutlet var newPasswordTextField: UITextField!
//    @IBOutlet var confirmPasswordTextField: UITextField!
//    @IBOutlet var submitButton: UIButton!
//    @IBOutlet var detailLabel: UILabel!
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        addDefaultTitleViewLogo()
//
//        otpTextField.delegate = self
//        otpTextField.addDefaultRoundCorners()
//        newPasswordTextField.delegate = otpTextField.delegate
//        newPasswordTextField.addDefaultRoundCorners()
//        confirmPasswordTextField.delegate = newPasswordTextField.delegate
//        confirmPasswordTextField.addDefaultRoundCorners()
//
//        infoLabel.textColor = MobilePayCore.shared.config.onboardingPrimaryTextColor
//        infoLabel.font = UIFont.with(size: .LargeMedium, weight: .bold)
//        infoLabel.text = "Please enter the one time password sent to your email, new password and press change password"
//
//        detailLabel.font = UIFont.with(size: .LightNormal1, weight: .normal)
//        detailLabel.textColor = MobilePayCore.shared.config.onboardingPrimaryTextColor
//        let attributedString = NSMutableAttributedString(string: "signup_pwd_msg".localized)
//        let nsstring = NSString(string: attributedString.string)
//        let range = nsstring.range(of: nsstring.substring(to: 7))
//        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: MobilePayCore.shared.config.onboardingSecondaryColor, range: range)
//        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.with(size: .LightNormal1, weight: .bold), range: range)
//        detailLabel.attributedText = attributedString
//
//        submitButton.setTitle("CHANGE PASSWORD", for: .normal)
//        submitButton.backgroundColor = MobilePayCore.shared.config.onboardingButtonsColor
//        submitButton.titleLabel?.font = UIFont.with(size: .LargeMedium, weight: .bold)
//        submitButton.setTitleColor(MobilePayCore.shared.config.onboardingButtonsTextColor, for: UIControl.State.normal)
//        submitButton.addDefaultRoundCorners()
//    }
//
//    @IBAction func didTapSubmitButton(_ sender: Any) {
//        guard TMWCommonUtil.isInternetAvailable() else {
//            TMWCommonUtil.showInternetUnavailableAlert()
//            return
//        }
//
//        if otpTextField.text == "" || newPasswordTextField.text == "" || confirmPasswordTextField.text == "" {
//            showAlert(message: "forgot_alert_otp".localized)
//        }
//        else if (newPasswordTextField.text!.elementsEqual(confirmPasswordTextField.text!)) != true {
//            showAlert(message: "forgot_alert_password_match".localized)
//        }
//        else if !newPasswordTextField.text!.isValidPassword() {
//            showAlert(message: "pwd_alert_format".localized)
//        }
//        else {
//            TMWLoaderView.shareInstance().showLoader(navigationController!.view)
//
//            guard let username = email,
//                let password = newPasswordTextField.text?.toBase64(),
//                let otp = otpTextField.text else {
//                assert(false)
//                return
//            }
//
//            webService?.cancel()
//            webService?.resetPassword(username: username, password: password, otp: otp, completion: { [weak self] error in
//                TMWLoaderView.shareInstance().hideLoader()
//                if let error = error {
//                    showAlert(message: error.localizedDescription)
//                }
//                else {
//                    showAlert(message: "forgot_alert_password_success".localized, title: "alert_title".localized, okTitle: "alert_accept".localized) {
//                        self?.navigationController?.popToRootViewController(animated: true)
//                    }
//                }
//            })
//        }
//    }
// }
//
// extension ResetPasswordViewController: UITextFieldDelegate {
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        textField.resignFirstResponder()
//        if textField == otpTextField {
//            newPasswordTextField.becomeFirstResponder()
//        }
//        else if textField == newPasswordTextField {
//            confirmPasswordTextField.becomeFirstResponder()
//        }
//        else if textField == confirmPasswordTextField {
//            didTapSubmitButton(submitButton)
//        }
//
//        return true
//    }
// }
