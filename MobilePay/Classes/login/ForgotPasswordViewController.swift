////
////  ForgotPasswordViewController.swift
////  MobilePay
////
////  Created by Marius Kurgonas on 15/01/2020.
////  Copyright © 2020 JadePayments. All rights reserved.
////
//
// import UIKit
//
// class ForgotPasswordViewController: BaseScreenViewController {
//    @IBOutlet var infoLabel: UILabel!
//    @IBOutlet var textField: UITextField!
//    @IBOutlet var submitButton: UIButton!
//
//    var webService: RegisterWebServiceContract?
//    var router: RouterContract?
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        addDefaultTitleViewLogo()
//
//        textField.delegate = self
//
//        infoLabel.textColor = MobilePayCore.shared.config.onboardingPrimaryTextColor
//        infoLabel.font = UIFont.with(size: .LargeMedium, weight: .bold)
//        infoLabel.text = "Please enter a valid email address below. We will send you a link to reset your password"
//
//        textField.addDefaultRoundCorners()
//
//        submitButton.setTitle("SUBMIT", for: .normal)
//        submitButton.backgroundColor = MobilePayCore.shared.config.onboardingButtonsColor
//        submitButton.titleLabel?.font = UIFont.with(size: .LargeMedium, weight: .bold)
//        submitButton.setTitleColor(MobilePayCore.shared.config.onboardingButtonsTextColor, for: UIControl.State.normal)
//        submitButton.addDefaultRoundCorners()
//    }
//
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//
//        navigationController?.setNavigationBarHidden(false, animated: true)
//    }
//
//    @IBAction
//    func didTapSubmitButton(_ sender: Any) {
//        guard TMWCommonUtil.isInternetAvailable() else {
//            TMWCommonUtil.showInternetUnavailableAlert()
//            return
//        }
//
//        textField.resignFirstResponder()
//
//        guard let emailAddress = textField.text, emailAddress.isValidEmailAddress() else {
//            showAlert(message: "forgot_alert_email".localized)
//            return
//        }
//
//        if isUITesting() {
//            goToResetPassword(email: emailAddress)
//            return
//        }
//
//        TMWLoaderView.shareInstance().showLoader(navigationController!.view)
//        webService?.cancel()
//        webService?.requestOTP(email: emailAddress, completion: { [weak self] error in
//            TMWLoaderView.shareInstance().hideLoader()
//
//            if let error = error {
//                showAlert(message: error.localizedDescription)
//            }
//            else {
//                self?.goToResetPassword(email: emailAddress)
//            }
//        })
//    }
//
//    func goToResetPassword(email: String) {
//        guard let router = router else {
//            assert(false)
//            return
//        }
//
//        router.presentResetPassword(email: email)
//    }
// }
//
// extension ForgotPasswordViewController: UITextFieldDelegate {
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        textField.resignFirstResponder()
//        if textField == self.textField {
//            didTapSubmitButton(submitButton)
//        }
//
//        return true
//    }
// }
