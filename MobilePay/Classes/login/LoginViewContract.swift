//
//  LoginViewContract.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 03/01/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation

protocol LoginViewContract: AnyObject {
    var presenter: LoginPresenterContract? { get set }

    var username: String? { get }

    var password: String? { get }

    func setup()

    func showFillFieldsMessage()

    func show(message: String)

    func showLoading()

    func hideLoading()

    func showInternetUnabailable()
}
