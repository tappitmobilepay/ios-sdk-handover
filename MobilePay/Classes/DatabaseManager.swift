//
//  DatabaseManager.swift
//  MobilePay
//
//  Created by Marius Kurgonas on 08/07/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import CoreData
import Foundation

extension Notification.Name {
    static let balanceUpdated = Notification.Name("balanceUpdated")
    static let cardsUpdated = Notification.Name("cardsUpdated")
    static let cardDeteled = Notification.Name("cardDeleted")
    static let cardMadeDefault = Notification.Name("cardMadeDefault")
}

enum CampaignsTypeFetch { case
    redeemed,
    notRedeemed,
    all
}

protocol DatabaseManagementContract: BalanceProvidingContract {
    func deleteAllData()

    func makeCardDefault(id: String)

    func deleteCard(id: String)

    func deleteAllCards()

    func save(balance: Balance)

    func save(giveawayTokens: [GiveawayToken])

    func save(campaigns: [Campaign])

    func save(complimentaryBalances: [ComplimentaryBalance])

    func saveCards(cards: [Card])

    func giveawayTokens() -> [GiveawayToken]

    func campaigns(type: CampaignsTypeFetch) -> [Campaign]

    func giftcards() -> [ComplimentaryBalance]

    func complimentaryBalances() -> [ComplimentaryBalance]

    func hasCreditCards() -> Bool

    func allCreditCards(onlyDefault: Bool) -> [Card]

    func add(card: Card)

    func allTransactions() -> [Transaction]

    func saveTransactions(transactions: [Transaction])

    func clientConfig() -> ClientConfig?

    func saveClientConfig(clientConfig: ClientConfig)
}

class DatabaseManager: DatabaseManagementContract {
    var dataStack: DATAStack

    init() {
        dataStack = DATAStack(modelName: "Tappit", bundle: Bundle(for: DatabaseManager.self), storeType: .sqLite)
    }

    private func deleteAllData(_ entity: String, context: NSManagedObjectContext) {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        fetchRequest.returnsObjectsAsFaults = false
        do {
            let results = try context.fetch(fetchRequest)
            for object in results {
                guard let objectData = object as? NSManagedObject else {
                    continue
                }
                context.delete(objectData)
            }
        }
        catch {
            fatalError("Could not delete objects")
        }
    }

    private func deleteCardById(id: String, context: NSManagedObjectContext) {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "\(ManagedCreditCard.self)")
        fetchRequest.predicate = NSPredicate(format: "id = %@", id)
        fetchRequest.returnsObjectsAsFaults = false
        do {
            let results = try context.fetch(fetchRequest)
            for object in results {
                guard let objectData = object as? NSManagedObject else {
                    continue
                }
                context.delete(objectData)
            }
        }
        catch {
            fatalError("Could not delete objects")
        }
    }

    func deleteAllData() {
        dataStack.drop()
        dataStack = DATAStack(modelName: "Tappit", bundle: Bundle(for: DatabaseManager.self), storeType: .sqLite)
    }

    func makeCardDefault(id: String) {
        dataStack.mainContext.performAndWait {
            let context = dataStack.mainContext
            context.performAndWait {
                let request: NSFetchRequest = ManagedCreditCard.fetchRequest()
                request.predicate = NSPredicate(format: "isDefault == true or id == %@", id)

                if let creditCards = try? context.fetch(request) {
                    creditCards.forEach { $0.isDefault = !$0.isDefault }
                    do {
                        try dataStack.mainContext.save()
                        NotificationCenter.default.post(name: .cardMadeDefault, object: nil)
                    }
                    catch {
                        assert(false)
                        showAlert(message: "Failed to make card default")
                    }
                }
            }
        }
    }

    func deleteCard(id: String) {
        dataStack.mainContext.performAndWait {
            deleteCardById(id: id, context: dataStack.mainContext)
            do {
                try dataStack.mainContext.save()
                NotificationCenter.default.post(name: .cardDeteled, object: nil)
            }
            catch {
                assert(false)
                showAlert(message: "Failed to cache cards")
            }
        }
    }

    func deleteAllCards() {
        deleteAllData("\(ManagedCreditCard.self)", context: dataStack.mainContext)
        do {
            try dataStack.mainContext.save()
        }
        catch {
            assert(false)
            showAlert(message: "Failed to purge cards")
        }
    }

    func save(giveawayTokens: [GiveawayToken]) {
        dataStack.mainContext.performAndWait {
            deleteAllData("\(ManagedGiveawayToken.self)", context: dataStack.mainContext)
            _ = giveawayTokens.map { GiveawayToken.toManagedGiveawayToken(token: $0, context: dataStack.mainContext) }
            do {
                try dataStack.mainContext.save()
            }
            catch {
                assert(false)
                showAlert(message: "Failed to cache giveaway tokens")
            }
        }
    }

    func save(campaigns: [Campaign]) {
        dataStack.mainContext.performAndWait {
            deleteAllData("\(ManagedCampaign.self)", context: dataStack.mainContext)
            _ = campaigns.map { Campaign.toManaged(campaign: $0, context: dataStack.mainContext) }
            do {
                try dataStack.mainContext.save()
            }
            catch {
                assert(false)
                showAlert(message: "Failed to cache campaigns")
            }
        }
    }

    func save(balance: Balance) {
        dataStack.mainContext.performAndWait {
            deleteAllData("\(ManagedBalance.self)", context: dataStack.mainContext)
            _ = ManagedBalance.from(displayBalance: balance, context: dataStack.mainContext)
            do {
                try dataStack.mainContext.save()
                NotificationCenter.default.post(name: .balanceUpdated, object: nil)
            }
            catch {
                assert(false)
                showAlert(message: "Failed to cache the balance")
            }
        }
    }

    func save(complimentaryBalances: [ComplimentaryBalance]) {
        dataStack.mainContext.performAndWait {
            deleteAllData("\(ManagedComplimentaryBalance.self)", context: dataStack.mainContext)
            _ = complimentaryBalances.map { ManagedComplimentaryBalance.from(complimentaryBanalance: $0, context: dataStack.mainContext) }
            do {
                try dataStack.mainContext.save()
            }
            catch {
                assert(false)
                showAlert(message: "Failed to cache complimentary balances")
            }
        }
    }

    func saveCards(cards: [Card]) {
        dataStack.mainContext.performAndWait {
            deleteAllData("\(ManagedCreditCard.self)", context: dataStack.mainContext)
            _ = cards.map { Card.toManagedCard(card: $0, context: dataStack.mainContext) }
            do {
                try dataStack.mainContext.save()
                NotificationCenter.default.post(name: .cardsUpdated, object: nil)
            }
            catch {
                assert(false)
                showAlert(message: "Failed to cache cards")
            }
        }
    }

    func giveawayTokens() -> [GiveawayToken] {
        var results: [GiveawayToken] = []
        let context = dataStack.newBackgroundContext()
        context.performAndWait {
            let request: NSFetchRequest = ManagedGiveawayToken.fetchRequest()
            request.sortDescriptors = [
                NSSortDescriptor(key: "expiryDate", ascending: true)
            ]
            if let objects = try? dataStack.mainContext.fetch(request) {
                let newArray = objects.map { GiveawayToken.from(managedGiveawayToken: $0) }
                results = newArray
            }
        }
        return results
    }

    func campaigns(type: CampaignsTypeFetch) -> [Campaign] {
        var results: [Campaign] = []
        let context = dataStack.newBackgroundContext()
        context.performAndWait {
            let request: NSFetchRequest = ManagedCampaign.fetchRequest()
            request.sortDescriptors = [
                NSSortDescriptor(key: "is_qualified", ascending: true),
                NSSortDescriptor(key: "campaign_name", ascending: true)
            ]
            switch type {
            case .all: break
            case .notRedeemed: request.predicate = NSPredicate(format: "is_qualified == false")
            case .redeemed: request.predicate = NSPredicate(format: "is_qualified == true")
            }
            if let objects = try? dataStack.mainContext.fetch(request) {
                let newArray = objects.map { Campaign.from(managedCampaign: $0) }
                results = newArray.sorted { !$0.is_qualified && $1.is_qualified }
            }
        }

        return results
    }

    func giftcards() -> [ComplimentaryBalance] {
        var results: [ComplimentaryBalance] = []
        let context = dataStack.newBackgroundContext()
        context.performAndWait {
            let request: NSFetchRequest = ManagedComplimentaryBalance.fetchRequest()
            request.sortDescriptors = [
                NSSortDescriptor(key: "expiryDate", ascending: true)
            ]
            request.predicate = NSPredicate(format: "type == \(ComplimentaryBalanceType.gift_card.rawValue)")
            if let objects = try? dataStack.mainContext.fetch(request) {
                let newArray = objects.map { ComplimentaryBalance.from(managedComplimentaryBalance: $0) }
                results = newArray
            }
        }
        return results
    }

    func complimentaryBalances() -> [ComplimentaryBalance] {
        var results: [ComplimentaryBalance] = []
        let context = dataStack.newBackgroundContext()
        context.performAndWait {
            let request: NSFetchRequest = ManagedComplimentaryBalance.fetchRequest()
            request.sortDescriptors = [
                NSSortDescriptor(key: "expiryDate", ascending: true)
            ]
            request.predicate = NSPredicate(format: "type == \(ComplimentaryBalanceType.complimentary_balance.rawValue)")
            if let objects = try? dataStack.mainContext.fetch(request) {
                let newArray = objects.map { ComplimentaryBalance.from(managedComplimentaryBalance: $0) }
                results = newArray
            }
        }
        return results
    }

    func hasCreditCards() -> Bool {
        return !allCreditCards().isEmpty
    }

    func allCreditCards(onlyDefault: Bool = false) -> [Card] {
        var cards: [Card] = []
        let context = dataStack.mainContext
        context.performAndWait {
            let request: NSFetchRequest = ManagedCreditCard.fetchRequest()
            if onlyDefault {
                request.predicate = NSPredicate(format: "isDefault = true")
            }

            request.sortDescriptors = [
                NSSortDescriptor(key: "isDefault", ascending: false),
                NSSortDescriptor(key: "id", ascending: false)
            ]

            if let creditCards = try? context.fetch(request) {
                let newArray = creditCards.map { Card.from(managedCreditCard: $0) }
                cards = newArray
            }
        }
        return cards
    }

    func add(card: Card) {
        dataStack.mainContext.performAndWait {
            if card.isDefault {
                if let creditCards = try? dataStack.mainContext.fetch(ManagedCreditCard.fetchRequest()) as? [ManagedCreditCard] {
                    creditCards.forEach { $0.isDefault = false }
                }
            }

            _ = Card.toManagedCard(card: card, context: dataStack.mainContext)
            do {
                try dataStack.mainContext.save()
                NotificationCenter.default.post(name: .cardsUpdated, object: nil)
            }
            catch {
                assert(false)
                showAlert(message: "Failed to cache the card")
            }
        }
    }

    func allTransactions() -> [Transaction] {
        var tns: [Transaction] = []
        let context = dataStack.newBackgroundContext()
        context.performAndWait {
            let request: NSFetchRequest = ManagedTransaction.fetchRequest()
            let sort = NSSortDescriptor(key: "date", ascending: false)
            request.sortDescriptors = [sort]
            if let transactions = try? dataStack.mainContext.fetch(request) {
                let newArray = transactions.map { managedTransactionToTransaction(managedTransaction: $0) }
                tns = newArray
            }
        }
        return tns
    }

    func saveTransactions(transactions: [Transaction]) {
        dataStack.mainContext.performAndWait {
            deleteAllData("\(ManagedTransaction.self)", context: dataStack.mainContext)
            deleteAllData("\(ManagedTransactionItem.self)", context: dataStack.mainContext)
            deleteAllData("\(ManagedComplimentaryBalanceUsage.self)", context: dataStack.mainContext)
            _ = transactions.map { self.transactionToManagedTransaction(transaction: $0, context: dataStack.mainContext) }
            do {
                try dataStack.mainContext.save()
            }
            catch {
                assert(false)
                showAlert(message: "Failed to cache transactions")
            }
        }
    }

    func clientConfig() -> ClientConfig? {
        var element: ClientConfig?
        dataStack.mainContext.performAndWait {
            if let items = try? dataStack.mainContext.fetch(ManagedClientConfig.fetchRequest()) as? [ManagedClientConfig],
               let firstItem = items.first
            {
                element = firstItem.toClientConfig()
            }
        }

        return element
    }

    func saveClientConfig(clientConfig: ClientConfig) {
        dataStack.mainContext.performAndWait {
            deleteAllData("\(ManagedClientConfig.self)", context: dataStack.mainContext)
            deleteAllData("\(ManagedClientConfigSettings.self)", context: dataStack.mainContext)
            _ = clientConfig.toManagedClientConfig(context: dataStack.mainContext)
            do {
                try dataStack.mainContext.save()
            }
            catch {
                assert(false)
                showAlert(message: "Failed to cache transactions")
            }
        }
    }

    func managedItemToItem(managedItem: ManagedTransactionItem) -> TransactionItem {
        var amount: Decimal?
        if let am = managedItem.amount, let decimalAmount = Decimal(string: am) {
            amount = decimalAmount
        }

        return TransactionItem(unitPrice: managedItem.unitPrice!.decimalValue,
                               sku: managedItem.sku!,
                               name: managedItem.name!,
                               quantity: managedItem.quantity!.decimalValue,
                               type: managedItem.type!,
                               amount: amount)
    }

    func itemToManagedItem(item: TransactionItem, context: NSManagedObjectContext) -> ManagedTransactionItem {
        let managedItem = ManagedTransactionItem(context: context)
        managedItem.unitPrice = NSDecimalNumber(decimal: item.unitPrice)
        managedItem.sku = item.sku
        managedItem.name = item.name
        managedItem.quantity = NSDecimalNumber(decimal: item.quantity)
        managedItem.type = item.type

        if let amount = item.amount {
            managedItem.amount = "\(amount)"
        }

        return managedItem
    }

    func managedTransactionToTransaction(managedTransaction: ManagedTransaction) -> Transaction {
        let items = (managedTransaction.items?.allObjects as? [ManagedTransactionItem])?.map { self.managedItemToItem(managedItem: $0) }
        let usages = managedTransaction.complimentaryBalanceUsage!.map { managedComplimentaryBalanceUsageToComplimentaryBalanceUsage(managedComplimentaryBalanceUsage: $0 as! ManagedComplimentaryBalanceUsage) }
        return Transaction(clientTransactionId: managedTransaction.clientTransactionId!,
                           type: managedTransaction.type!.decimalValue,
                           amount: managedTransaction.amount!.decimalValue,
                           date: managedTransaction.date!,
                           currency: managedTransaction.currency!,
                           event: managedTransaction.event!,
                           concessionaire: managedTransaction.concessionaire!,
                           items: items!,
                           tip: managedTransaction.tip!.decimalValue,
                           complimentaryBalanceUsage: usages,
                           chargedAmount: managedTransaction.chargedAmount!.decimalValue,
                           tokenAmount: managedTransaction.tokenAmount!.decimalValue,
                           donationAmount: managedTransaction.donationAmount!.decimalValue,
                           discountAmount: managedTransaction.discountAmount!.decimalValue,
                           taxAmount: managedTransaction.taxAmount!.decimalValue)
    }

    func managedComplimentaryBalanceUsageToComplimentaryBalanceUsage(managedComplimentaryBalanceUsage: ManagedComplimentaryBalanceUsage) -> ComplimentaryBalanceUsage {
        return ComplimentaryBalanceUsage(charged: managedComplimentaryBalanceUsage.charged!.decimalValue,
                                         concessionaires: (managedComplimentaryBalanceUsage.concessionaires as! NSSet).allObjects as! [String],
                                         amount: managedComplimentaryBalanceUsage.amount!.decimalValue,
                                         expiryDate: managedComplimentaryBalanceUsage.expiryDate)
    }

    func complimentaryBalanceUsageToManagedComplimentaryBalanceUsage(complimentaryBalanceUsage: ComplimentaryBalanceUsage, context: NSManagedObjectContext) -> ManagedComplimentaryBalanceUsage {
        let managedComplimentaryBalanceUsage = ManagedComplimentaryBalanceUsage(context: context)
        managedComplimentaryBalanceUsage.charged = NSDecimalNumber(decimal: complimentaryBalanceUsage.charged)
        managedComplimentaryBalanceUsage.amount = NSDecimalNumber(decimal: complimentaryBalanceUsage.amount)
        managedComplimentaryBalanceUsage.concessionaires = NSSet(array: complimentaryBalanceUsage.concessionaires)
        managedComplimentaryBalanceUsage.expiryDate = complimentaryBalanceUsage.expiryDate
        return managedComplimentaryBalanceUsage
    }

    func transactionToManagedTransaction(transaction: Transaction, context: NSManagedObjectContext) -> ManagedTransaction {
        let managedTransaction = ManagedTransaction(context: context)
        managedTransaction.clientTransactionId = transaction.clientTransactionId
        managedTransaction.type = NSDecimalNumber(decimal: transaction.type)
        managedTransaction.amount = NSDecimalNumber(decimal: transaction.amount)
        managedTransaction.date = transaction.date
        managedTransaction.currency = transaction.currency
        managedTransaction.event = transaction.event
        managedTransaction.donationAmount = NSDecimalNumber(decimal: transaction.donationAmount)
        managedTransaction.taxAmount = NSDecimalNumber(decimal: transaction.taxAmount)
        managedTransaction.discountAmount = NSDecimalNumber(decimal: transaction.discountAmount)
        managedTransaction.concessionaire = transaction.concessionaire
        managedTransaction.tip = NSDecimalNumber(decimal: transaction.tip)
        let usages = transaction.complimentaryBalanceUsage.map { self.complimentaryBalanceUsageToManagedComplimentaryBalanceUsage(complimentaryBalanceUsage: $0, context: context) }
        managedTransaction.complimentaryBalanceUsage = NSSet(array: usages)
        managedTransaction.chargedAmount = NSDecimalNumber(decimal: transaction.chargedAmount)
        managedTransaction.tokenAmount = NSDecimalNumber(decimal: transaction.tokenAmount)
        let items = transaction.items.map { itemToManagedItem(item: $0, context: context) }
        managedTransaction.items = NSSet(array: items)

        return managedTransaction
    }
}

extension DatabaseManager {
    var balance: Balance? {
        var element: Balance?
        dataStack.mainContext.performAndWait {
            if let balances = try? dataStack.mainContext.fetch(ManagedBalance.fetchRequest()) as? [ManagedBalance],
               let firstBalance = balances.first
            {
                element = Balance.from(managedBalance: firstBalance)
            }
        }

        return element
    }
}
