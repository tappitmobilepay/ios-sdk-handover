//
//  PaymentsResponseParsingContract.swift
//  MobilePay-PAYG
//
//  Created by Marius Kurgonas on 03/06/2020.
//  Copyright © 2020 JadePayments. All rights reserved.
//

import Foundation

protocol PaymentsResponseParsingContract {
    func addPaymentMethodResponse(from data: Data?) throws -> Card
    func transactionStatusResponse(from data: Data?) throws -> TransactionStatusResponse
    func paymentMethodsResponse(from data: Data?) throws -> [Card]
    func paymentProvidersConfigResponse(from data: Data?) throws -> Bool
    func deletePaymentMethodResponse(from data: Data?) throws
    func makePaymentMethodDefaultResponse(from data: Data?) throws -> MakePaymentMethodDefaultResponse
    func clientConfigResponse(from data: Data?) throws -> ClientConfig
    func userStatsResponse(from data: Data?) throws -> CustomerStats
}
