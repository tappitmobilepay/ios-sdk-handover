//
//  MobilePay.h
//  MobilePay
//
//  Created by Pandi on 08/07/19.
//  Copyright © 2019 JadePayments. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for MobilePay.
FOUNDATION_EXPORT double MobilePayVersionNumber;

//! Project version string for MobilePay.
FOUNDATION_EXPORT const unsigned char MobilePayVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <MobilePay/PublicHeader.h>
//#import <MobilePay/Util.h>

