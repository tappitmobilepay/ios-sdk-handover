
## MobilePay iOS SDK

SDK that provides easy prepaid payments solution

#### To create a folder with SDK, documentation and example project ready for deployment:
- Install fastlane tool by runing `brew install fastlane` in terminal
- Cd into the cloned project folder and run `fastlane` in terminal. Follow instructions on screen to choose the right script.
